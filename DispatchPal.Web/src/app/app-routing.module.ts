﻿import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const appRoutes: Routes = [
    {
        path: '',
        children: [
            {
                path: '',
                loadChildren: './modules/public/public.module#PublicModule'
            },
            {
                path: 'console',
                loadChildren: './modules/private/private.module#PrivateModule'
            }
        ]
    }/*,
    {
        path: '**',
        component: PageNotFoundComponent
    }*/

];

@NgModule({
    imports: [
        RouterModule.forRoot(appRoutes)
    ],
    exports: [
        RouterModule
    ]
})
export class AppRoutingModule { }