﻿import { FileModel } from './file';
export class Driver {
    Id: number;
    DriverID: string;
    FirstName: string;
    MiddleName: string;
    LastName: string;
    DisplayName: string;
    DOB: Date;
    SSN: string;
    FEIN: string;
    Address1: string;
    Address2: string;
    City: string;
    StateId: number;
    ZipCode: string;
    HomePhone: string;
    CellPhone: string;
    CellPhoneCarrier: string;
    Email: string;
    Fax: string;
    SendSMS: boolean;
    SendEMail: boolean;
    EmergencyContactName: string;
    EmergencyContactPhone: string;
    Relationship: string;
    MedicalExpirationDate: Date;
    PreEmploymentDrugTest: boolean;
    PreEmploymentDrugTestDate: Date;
    PreEmploymentDrugMVR: boolean;
    PreEmploymentDrugMVRDate: Date;
    RateType: number;
    RateAmountEmpty: number;
    RateAmountLoaded: number;
    RateAmountBobtail: number;
    TeamRateAmountEmpty: number;
    TeamRateAmountLoaded: number;
    TeamRateAmountBobtail: number;
    HireDate: Date;
    TerminationDate: Date;
    FuelCard: string;
    DispatchedBy: number;
    PayMathod: string;
    CDL: string;
    CDLStateId: number;
    CDLExpirationDate: Date;
    CDLClass: string;
    CDLEndorsement: string;
    YearsOfEprience: number;
    Note: string;
    TruckId: number;
    TrailerId: number;
    IsActive: boolean;
    ModifiedBy: number;
    ModifiedDate: Date;
    CompanyId: number;

    Truck: string;
    Trailer: string;

    Files: FileModel[];

    constructor() {
        this.Files = [];
    }

}