﻿export class InvoiceItem {
    Id: number;
    InvoiceId: number;
    ServiceId: number;
    ServiceName: string;
    Price: number;
    Quantity: number;
    Total: number;
    Tax: number;
    TaxAmmount: number;
    Discount: number;
    DiscountAmmount: number;
    Note: string;
}