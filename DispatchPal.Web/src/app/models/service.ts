﻿export class Service {
    Id: number;
    TruckId: number;
    Mileage: number;
    Date: Date;
    Location: string;
    Part: string;
    Job: string;
    Cost : number;
    WarrantyExpDate : Date;
    ScheduledServiceDate : Date;
    Description: string;
    CompanyId: number;
}