import {LoadPoint} from "./load_point";

export class Plan {
    DateValue: Date;
    LoadPoints: LoadPoint[];
}