﻿export class Broker {
    Id: number;
    PartnerId: number;
    DisplayName:string;
    FirstName:string;
    LastName:string;
    PhoneNumber:string;
    MobileNumber:string;
    Email:string;
    Fax:string;
    Fax1:string;
    Rate: number;
    Rating: number;
    IsActive: boolean;
    CompanyId: number;
}