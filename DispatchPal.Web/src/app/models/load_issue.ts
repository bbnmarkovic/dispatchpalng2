export class LoadIssue {
    Id: number;
    LoadId: number;
    Description: string;
    IsResolved: boolean;
}