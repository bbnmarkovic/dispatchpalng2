﻿export enum VehicleTypeEnum {
    Truck = 0,
    Trailer = 1
}

export enum ClientTypeEnum {
    Accountant = 1,
    Bank,
    Broker,
    Client,
    DMV,
    DrugTestLaboratory,
    Factoring,
    Faxing,
    FederalTax,
    Government,
    InsuranceAgency,
    Lawyer,
    Lessor,
    Mechanic,
    Miscellaneous,
    Owner,
    OwnerOperator,
    Parking,
    PartsSuppliesCompany,
    StateTax,
    TireCompany,
    TowingCompany,
    TruckWash
}

export enum UploadTarget {
    Load = 0,
    Driver
}

export enum LoadPointActionType {
    Loading = 0,
    Unloading
}