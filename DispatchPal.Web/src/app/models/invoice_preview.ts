/**
 * Created by Boban on 6/20/2017.
 */
export class InvoicePreview {
    Id: number;
    LoadNumber: string;
    InvoiceNumber: string;
    InvoiceDueDate: Date;
    InvoiceDate: Date;
    PaidDate: Date;
    Total: number;
    AmountPaid: number;
    BalanceDue: number;
    Brokerage: string;
}