/**
 * Created by Boban on 6/24/2017.
 */
export class CashFlowPreview {
    Id: number;
    Driver: string;
    Truck: string;
    Trailer: string;
    Date: Date;
    Type: string;
    Note: string;
    Amount: number;
    IsSettled: boolean;
    IsActive: boolean;
}