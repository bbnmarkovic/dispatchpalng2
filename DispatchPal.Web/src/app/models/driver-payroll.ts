export class DriverPayroll {
    LoadId: number;
    LoadNumber: string;
    DriverId: number;
    Driver: string;
    DateFrom: Date;
    DateTo: Date;
}