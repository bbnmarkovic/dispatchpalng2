﻿export class ResponseModel<T> {
    IsSuccess: boolean;
    TotalItems: number;
    Result: T;
    Message: string;
}