﻿export class DriverPreview {
    Id: number;
    DriverID: string;
    FirstName: string;
    MiddleName: string;
    LastName: string;
    DisplayName: string;
    DOB: Date;
    SSN: string;
    Address: string;
    CellPhone: string;
    Email: string;
    SendSMS: boolean;
    SendEMail: boolean;
    MedicalExpirationDate: Date;
    MedicalExpirationDays: number;
    FuelCard: string;
    DispatchedBy: string;
    RateType: number;
    PayMathod: string;
    CDL: string;
    CDLState: string;
    CDLExpirationDate: Date;
    CDLExpirationDays: number;
    Truck: string;
    Trailer: string;
}