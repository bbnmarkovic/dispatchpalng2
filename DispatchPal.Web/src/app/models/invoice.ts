﻿import { InvoiceItem } from './invoice_item';
import {InvoicePayment} from "./invoice_payment";

export class Invoice {
    LoadId: number;
    LoadNumber: string;
    TripId: number;
    PaidInFull: boolean;
    InvoiceNumber: string;
    InvoiceDate: Date;
    InvoiceDueDate: Date;
    BusinessTaxID: number;
    Total: number;
    SubTotal: number;
    AmountPaid: number;
    Discount: number;
    DiscountAmount: number;
    TaxInclusive: number;
    Tax: number;
    TaxAmmount: number;
    BalanceRemaining: number;
    PaymentDate: Date;
    SendReceipt: boolean;
    ExternalNote: string;
    IsActive: boolean;
    CompanyId: number;
    InvoiceItems: InvoiceItem[];
    InvoicePayments: InvoicePayment[];

    constructor() {
        this.InvoiceItems = [];
        this.InvoicePayments = [];
    }
}