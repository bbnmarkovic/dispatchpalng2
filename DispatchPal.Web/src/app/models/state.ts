﻿export class State {
    Id: number;
    ISO: string;
    Name: string;
    NickName: string;
}