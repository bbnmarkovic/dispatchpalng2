﻿export class User {
    Id: number;
    UserId: string;
    UserName: string;
    Password: string;
    Token: string;
    FirstName: string;
    LastName: string;
    FullName: string = this.FirstName + " " + this.LastName;
    Email: string;
    EmailConfirmed: boolean;
    PhoneNumber: string;
    MobileNumber: string;
    PhoneNumberConfirmed: boolean;
    Address: string;
    Description: string;
    AccessFailedCount: number;
    LockoutEnabled: boolean;
    LockoutEndDateUtc: Date;
    DateCreatedUtc: Date;
    DateActivatedUtc: Date;
    DateLastLoginUTC: string;
    TwoFactorEnabled: boolean;
    InviteCode: string;
    ResetPasswordKey: string;
    ResetPasswordExpiryUtc: Date;
    Status: number;
    IsActive: boolean;
    CompanyId: number;
    Companies: number[];

    constructor(){
        this.Companies = [];
    }
}