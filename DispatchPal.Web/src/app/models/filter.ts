﻿import { addDays } from 'date-fns';

export class Filter {
    Offset: number;
    PageSize: number;
    SearchParam: string;
    Aging: string;
    VehicleType: number;
    ClientType: number;
    ActiveStatus: string;
    StartDate: Date;

    constructor(vehicleType: number = 0, clientType: number = 0) {
        this.Offset = 0;
        this.PageSize = 15;
        this.SearchParam = "";
        this.Aging = "30";
        this.VehicleType = vehicleType;
        this.ClientType = clientType;
        this.ActiveStatus = "-1";
        this.StartDate = (addDays(new Date(), -30));
    }
}