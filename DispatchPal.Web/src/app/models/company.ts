﻿export class Company {
    Id: number;
    UserId: number;
    Name: string;
    Address: string;
    Address1: string;
    City: string;
    StateId: number;
    ZipCode: string;
    Country: string;
    PhoneNumber: string;
    AlternatePhoneNumber: string;
    Fax: string;
    Email: string;
    Contact: string;
    Notes: string;
    MotorCarrierNumber: string;
    NextInvoiceNumber: number;
    BusinessNumber: string;
    EIN: string;
    Domain: string;
    Logo: string;
    IsActive: boolean;
}