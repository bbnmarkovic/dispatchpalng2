﻿export class TripPreview {
    Id: number;
    TripNumber: string;
    DateFrom: Date;
    DateTo: Date;
    DriverA: string;
    DriverB: string;
    Truck: string;
    Trailer: string;
    PayTypeDriverA: number;
    PayRateFullDriverA: number;
    PayRateEmptyDriverA: number;
    PayRateBobtailDriverA: number;
    PayTypeDriverB: number;
    PayRateFullDriverB: number;
    PayRateEmptyDriverB: number;
    PayRateBobtailDriverB: number;
    IsActive: boolean;
    IsFinished: boolean;
    CompanyId: number;
}