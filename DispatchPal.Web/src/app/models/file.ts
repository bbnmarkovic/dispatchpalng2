﻿export class FileModel {
    Id: number;
    DocumentType: number;
    UniqueFileName: string;
    OriginalFileName: string;
    FilePath: string;
    FileSize: string;
    Extension: string;
    UploadDate: Date;
    ClientId: number;
}