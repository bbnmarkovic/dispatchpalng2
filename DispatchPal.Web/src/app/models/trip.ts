﻿export class Trip {
    Id: number;
    TripNumber: string;
    DateFrom: Date;
    DateTo: Date;
    DriverAId: number;
    DriverBId: number;
    TruckId: number;
    TrailerId: number;
    PayTypeDriverA: number;
    PayRateFullDriverA: number;
    PayRateEmptyDriverA: number;
    PayRateBobtailDriverA: number;
    PayTypeDriverB: number;
    PayRateFullDriverB: number;
    PayRateEmptyDriverB: number;
    PayRateBobtailDriverB: number;
    IsActive: boolean;
    IsFinished: boolean;
    CompanyId: number;
}