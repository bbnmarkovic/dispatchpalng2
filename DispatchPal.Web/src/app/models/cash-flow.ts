﻿export class CashFlow {
    Id: number;
    DriverId: number;
    TruckId: number;
    TrailerId: number;
    Date: Date;
    Amount: number;
    TypeId: number;
    Note: string;
    IsSettled: boolean;
    IsActive: boolean;
    CompanyId: number;
}