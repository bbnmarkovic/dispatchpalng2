﻿export class LoadPreview {
    Id: number;
    LoadNumber: string;
    TripId: number;
    TripNumber: string;
    LocationStart: string;
    LocationEnd: string;
    FromDate: Date;
    ToDate: Date;
    PO: string;
    Confirmation: string;
    BOL: string;
    DriverA: string;
    DriverB: string;
    Truck: string;
    Trailer: string;
    Brokerage: string;
    Price: number;
    Miles: number;
    PendingIssue: boolean;
    PaidInFull: boolean;
    DispatchPaid: boolean;
    IsFinished: boolean;
}