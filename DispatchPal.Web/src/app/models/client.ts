﻿export class Client {
    Id: number;
    ClientTypeId: number;
    AccountNumber: string;
    DisplayName: string;
    Name: string;
    Address: string;
    Address2: string;
    City: string;
    StateId: number;
    ZipCode: string;
    PhoneNumber: string;
    AlternatePhoneNumber: string;
    Fax: string;
    Email: string;
    Fee: string;
    MotorCarrierNumber: string;
    EIN: string;
    LastPaid: Date;
    ContactName: string;
    IsActive: boolean;
}