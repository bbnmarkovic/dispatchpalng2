/**
 * Created by Boban on 6/17/2017.
 */
export class InvoicePayment {
    Id: number;
    InvoiceId: number;
    Amount: number;
    PaymentDate: Date;
    PayedBy: string;
}