﻿import { Action } from '@ngrx/store';
import { User } from '../../models/user';
import {Company} from '../../models/company';

export class UserActions {
    static USER_LOGIN = 'USER_LOGIN';
    static USER_LOGOUT = 'USER_LOGOUT';
    static USER_UPDATE = 'USER_UPDATE';
}


export class userLogin implements Action {
    readonly type = UserActions.USER_LOGIN;

    constructor(public payload: User) {}
}

export class userLogout implements Action {
    readonly type = UserActions.USER_LOGOUT;

    constructor(public payload: User) {}
}

export class userUpdate implements Action {
    readonly type = UserActions.USER_UPDATE;

    constructor(public payload: User) {}
}

export type Actions = userLogin | userLogout | userUpdate;