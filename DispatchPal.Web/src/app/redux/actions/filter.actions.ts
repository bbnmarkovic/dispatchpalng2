﻿import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { Filter } from '../../models/filter';
import {Company} from '../../models/company';

export class FilterActions {
    static SAVE_FILTER_DRIVERS = 'SAVE_FILTER_DRIVERS';
    static SAVE_FILTER_TRUCKS = 'SAVE_FILTER_TRUCKS';
    static SAVE_FILTER_TRAILERS = 'SAVE_FILTER_TRAILERS';
    static SAVE_FILTER_COMPANIES = 'SAVE_FILTER_COMPANIES';
    static SAVE_FILTER_SERVICES = 'SAVE_FILTER_SERVICES';
    static SAVE_FILTER_CLIENTS = 'SAVE_FILTER_CLIENTS';
    static SAVE_FILTER_LOADS = 'SAVE_FILTER_LOADS';
    static SAVE_FILTER_TRIPS = 'SAVE_FILTER_TRIPS';
    static SAVE_FILTER_CASH_FLOW = 'SAVE_FILTER_CASH_FLOW';
    static SAVE_FILTER_PENDING_INVOICES = 'SAVE_FILTER_PENDING_INVOICES';
    static SAVE_FILTER_PAID_INVOICES = 'SAVE_FILTER_PAID_INVOICES';
}

export class saveFilterDrivers implements Action {
    readonly type = FilterActions.SAVE_FILTER_DRIVERS;

    constructor(public payload: Filter) {}
}

export class saveFilterTrucks implements Action {
    readonly type = FilterActions.SAVE_FILTER_TRUCKS;

    constructor(public payload: Filter) {}
}

export class saveFilterTrailers implements Action {
    readonly type = FilterActions.SAVE_FILTER_TRAILERS;

    constructor(public payload: Filter) {}
}

export class saveFilterCompanies implements Action {
    readonly type = FilterActions.SAVE_FILTER_COMPANIES;

    constructor(public payload: Filter) {}
}

export class saveFilterServices implements Action {
    readonly type = FilterActions.SAVE_FILTER_SERVICES;

    constructor(public payload: Filter) {}
}

export class saveFilterClients implements Action {
    readonly type = FilterActions.SAVE_FILTER_CLIENTS;

    constructor(public payload: Filter) {}
}

export class saveFilterLoads implements Action {
    readonly type = FilterActions.SAVE_FILTER_LOADS;

    constructor(public payload: Filter) {}
}

export class saveFilterTrips implements Action {
    readonly type = FilterActions.SAVE_FILTER_TRIPS;

    constructor(public payload: Filter) {}
}

export class saveFilterCashFlow implements Action {
    readonly type = FilterActions.SAVE_FILTER_CASH_FLOW;

    constructor(public payload: Filter) {}
}

export class saveFilterPendingInvoices implements Action {
    readonly type = FilterActions.SAVE_FILTER_PENDING_INVOICES;

    constructor(public payload: Filter) {}
}

export class saveFilterPaidInvoices implements Action {
    readonly type = FilterActions.SAVE_FILTER_PAID_INVOICES;

    constructor(public payload: Filter) {}
}

export type Actions = saveFilterDrivers | saveFilterTrucks | saveFilterTrailers | saveFilterCompanies | saveFilterServices
    | saveFilterClients | saveFilterLoads | saveFilterTrips | saveFilterCashFlow | saveFilterPendingInvoices | saveFilterPaidInvoices;