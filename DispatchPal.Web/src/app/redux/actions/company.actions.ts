﻿import { Action } from '@ngrx/store';
import { Company } from '../../models/company';

export class CompanyActions {
    static COMPANY_SELECTED = 'COMPANY_SELECTED';
}

export class companySelected implements Action {
    readonly type = CompanyActions.COMPANY_SELECTED;

    constructor(public payload: Company) {}
}

export type Actions = companySelected;