﻿import { Filter } from '../models/filter';
import { User } from '../models/user';
import { Company } from '../models/company';

export interface IAppState {
    filterDrivers?: Filter;
    filterTrucks?: Filter;
    filterTrailers?: Filter;
    filterCompanies?: Filter;
    filterServices?: Filter;
    filterClients?: Filter;
    filterLoads?: Filter;
    filterTrips?: Filter;
    filterCashFlow?: Filter;
    filterPendingInvoices?: Filter;
    filterPaidInvoices?: Filter;

    activeUser?: User;
    activeCompany?: Company;
}