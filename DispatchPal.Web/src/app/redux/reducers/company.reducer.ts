﻿import { Actions, CompanyActions } from '../actions/company.actions';
import { Company } from '../../models/company';

export function activeCompany(state: Company = new Company(), action: Actions): any {
    switch (action.type) {
        case CompanyActions.COMPANY_SELECTED:
            return Object.assign({}, state, action.payload);
        default:
            return state;
    }
}