﻿import { Action } from '@ngrx/store';
import {Actions, FilterActions} from '../actions/filter.actions';
import { Filter } from '../../models/filter';

export function filterDrivers(state: Filter = new Filter(), action: Actions): any {
    switch (action.type) {
        case FilterActions.SAVE_FILTER_DRIVERS:
            return Object.assign({}, state, action.payload);
        default:
            return state;
    }
}

export function filterTrucks(state: Filter = new Filter(), action: Actions): any {
    switch (action.type) {
        case FilterActions.SAVE_FILTER_TRUCKS:
            return Object.assign({}, state, action.payload);
        default:
            return state;
    }
}

export function filterTrailers(state: Filter = new Filter(), action: Actions): any {
    switch (action.type) {
        case FilterActions.SAVE_FILTER_TRAILERS:
            return Object.assign({}, state, action.payload);
        default:
            return state;
    }
}

export function filterCompanies(state: Filter = new Filter(), action: Actions): any {
    switch (action.type) {
        case FilterActions.SAVE_FILTER_COMPANIES:
            return Object.assign({}, state, action.payload);
        default:
            return state;
    }
}

export function filterServices(state: Filter = new Filter(), action: Actions): any {
    switch (action.type) {
        case FilterActions.SAVE_FILTER_SERVICES:
            return Object.assign({}, state, action.payload);
        default:
            return state;
    }
}

export function filterClients(state: Filter = new Filter(), action: Actions): any {
    switch (action.type) {
        case FilterActions.SAVE_FILTER_CLIENTS:
            return Object.assign({}, state, action.payload);
        default:
            return state;
    }
}
export function filterLoads(state: Filter = new Filter(), action: Actions): any {
    switch (action.type) {
        case FilterActions.SAVE_FILTER_LOADS:
            return Object.assign({}, state, action.payload);
        default:
            return state;
    }
}

export function filterTrips(state: Filter = new Filter(), action: Actions): any {
    switch (action.type) {
        case FilterActions.SAVE_FILTER_TRIPS:
            return Object.assign({}, state, action.payload);
        default:
            return state;
    }
}

export function filterCashFlow(state: Filter = new Filter(), action: Actions): any {
    switch (action.type) {
        case FilterActions.SAVE_FILTER_CASH_FLOW:
            return Object.assign({}, state, action.payload);
        default:
            return state;
    }
}

export function filterPendingInvoices(state: Filter = new Filter(), action: Actions): any {
    switch (action.type) {
        case FilterActions.SAVE_FILTER_PENDING_INVOICES:
            return Object.assign({}, state, action.payload);
        default:
            return state;
    }
}

export function filterPaidInvoices(state: Filter = new Filter(), action: Actions): any {
    switch (action.type) {
        case FilterActions.SAVE_FILTER_PAID_INVOICES:
            return Object.assign({}, state, action.payload);
        default:
            return state;
    }
}