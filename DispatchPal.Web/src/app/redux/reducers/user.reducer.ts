﻿import { Actions, UserActions } from '../actions/user.actions';
import { User } from '../../models/user';

export function activeUser(state: User = new User(), action: Actions): any {
    switch (action.type) {
        case UserActions.USER_LOGIN:
        case UserActions.USER_UPDATE:
            return Object.assign({}, state, action.payload);
        case UserActions.USER_LOGOUT:
            return new User();
        default:
            return state;
    }
}