﻿import { Injectable } from '@angular/core';
import {
    CanActivate, Router,
    ActivatedRouteSnapshot,
    RouterStateSnapshot
} from '@angular/router';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { IAppState } from '../../redux/app_state';
import { User } from '../../models/user';
import { userUpdate } from '../../redux/actions/user.actions';
import { SharedService } from '../../modules/shared/services/shared.service';

@Injectable()
export class UsersGuard implements CanActivate {
    private user: User = new User();

    constructor(
        private router: Router,
        private _store: Store<IAppState>,
        private _sharedService: SharedService
    ) {
        _store.select('activeUser').subscribe((state: User) => {
            this.user = state;
        });
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): any {
        if (this.user != null && this.user.Id > 0 && this.user.Token)
            return true;

        this._sharedService.returnUrl = state.url;
        //this._store.dispatch(userUpdate(this.user));
        this.router.navigate(['/login']);
        return false;
    }
}
