﻿import { NgModule, ModuleWithProviders } from '@angular/core';
import { ConfirmationService } from 'primeng/components/common/api';
import { UsersGuard } from './users/users_guard.service';
import {CanDeactivateGuard} from "./guards/can-deactivate-guard.service";

@NgModule({
    imports: [
    ],
    declarations: []
})
export class DispatchPalServicesModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: DispatchPalServicesModule,
            providers: [
                ConfirmationService,
                UsersGuard,
                CanDeactivateGuard
            ]
        };
    }

    static froChild(): ModuleWithProviders {
        return {
            ngModule: DispatchPalServicesModule
        };
    }
}
