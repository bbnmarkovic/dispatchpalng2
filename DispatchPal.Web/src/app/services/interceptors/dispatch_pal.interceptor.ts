import { Injectable } from '@angular/core';
import {
    HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpErrorResponse,
    HttpResponse, HttpHeaders
} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/do';
import {User} from '../../models/user';
import {Store} from '@ngrx/store';
import {IAppState} from '../../redux/app_state';
import {LoaderService} from '../../modules/shared/components/loader/loader.component';
import { Router } from '@angular/router';
import { userLogout } from '../../redux/actions/user.actions';

@Injectable()
export class DispatchPalInterceptor implements HttpInterceptor {
    protected user: User = new User();
    constructor(private loaderService: LoaderService,
                private router: Router,
                private _store: Store<IAppState>,) {
        this._store.select('activeUser').subscribe((v: User) => {
            this.user = v
        });
    }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        /*const loaderConfig: LoaderConfig = JSON.parse(req.params.get('loaderConfig'));
        let request = req,
            delayDuration = 0;

        if (loaderConfig) {
            delayDuration = loaderConfig.delay || 0;
            request = req.clone({
                params: req.params.delete('__showLoader')
            });
        }*/
        let headers = req.headers;

        if (!headers.has('Content-Type'))
            headers = headers.append('Content-Type', 'application/json; charset=utf-8');
        if (!req.headers.has('Accept'))
            headers = headers.append('Accept', 'application/json; charset=utf-8');

        if(!req.headers.has('Token') && this.user != null && this.user.Token) {
            headers = headers.append('Token', this.user.Token);
        }

        let request = req.clone({
            headers: headers
        });

        this.loaderService.show();

        return next.handle(request).delay(0).do((event: HttpEvent<any>) => {
            if (event instanceof HttpResponse) {
                this.loaderService.hide();
            }
        }, (err: any) => {
            if (err instanceof HttpErrorResponse) {
                /*if (err.status === 401) {
                  this.loaderService.hide();
                }*/
              if (err.status === 401) {
                this._store.dispatch(new userLogout(new User()));
                this.router.navigate(['/login']);
              }
                this.loaderService.hide();
            }
        });
    }
}
