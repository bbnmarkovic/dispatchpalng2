import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Company} from '../../../models/company';
import {Subscription} from 'rxjs/Subscription';
import {SelectItem} from 'primeng/primeng';
import {UploadTarget} from '../../../models/enums/dplEnums';
import {ResponseModel} from '../../../models/response';
import {Store} from '@ngrx/store';
import {Driver} from '../../../models/driver';
import {IAppState} from '../../../redux/app_state';
import {MessagesService} from '../../shared/components/messages/messages.component';
import {parse} from 'date-fns';
import {isNullOrUndefined} from 'util';
import { ResourcesService } from '../resources.service';

@Component({
  selector: 'app-driver-details',
  templateUrl: './driver-details.component.html',
  styleUrls: ['./driver-details.component.scss']
})
export class DriverDetailsComponent implements OnInit {
    rateTypes: Array<SelectItem> = [];
    payMethods: Array<SelectItem> = [];

    uploadTarget = UploadTarget;

    driverId: number;
    driverItem: Driver = new Driver();

    companyId: number;
    subscriptions: Subscription[];

    constructor(
        private _router: Router,
        private _store: Store<IAppState>,
        private _route: ActivatedRoute,
        private resourcesService: ResourcesService,
        private _messagesService: MessagesService)
    {
        this.subscriptions = [];

        this.rateTypes.push({ label: 'Per Mile', value: 0 });
        this.rateTypes.push({ label: 'Percentage', value: 1 });
        this.rateTypes.push({ label: 'Per Hour', value: 2 });

        this.payMethods.push({ label: 'W2', value: 'W2' });
        this.payMethods.push({ label: '1099', value: '1099' });
    }

    ngOnInit() {
        this._route.params.subscribe((v: { id: string }) => {
            let tempId = parseInt(v.id);
            this.driverId = isNaN(tempId) ? 0 : tempId;

            if (this.driverId > 0) {
                this.getDriver();
            } else {
                this.driverItem.Id = 0;
                this.driverItem.FirstName = "";
                this.driverItem.LastName = "";
                this.driverItem.IsActive = true;
                this.driverItem.PayMathod = 'W2';
                this.driverItem.HireDate = new Date();
                this.driverItem.CompanyId = this.companyId;
            }
        });

        this.subscriptions.push(this._store.select('activeCompany').subscribe((cmp: Company)=>{
            if(this.companyId > 0 && this.companyId != cmp.Id) {
                this._router.navigate(['/drivers']);
            } else {
                if (this.driverItem.CompanyId == null)
                    this.driverItem.CompanyId = cmp.Id;

                this.companyId = cmp.Id;
            }
        }));
    }

    ngOnDestroy(){
        this.subscriptions.forEach((item: Subscription)=>{
            item.unsubscribe();
        });
    }

    onFirstLastNameChanged(){
        if(this.driverItem.Id == 0){
            this.driverItem.DisplayName = this.driverItem.FirstName + " " + this.driverItem.LastName;
        }
    }

    getDriver() {
        if (this.driverId > 0) {
            this.resourcesService.getDriverById(this.driverId)
                .subscribe((data: ResponseModel<Driver>) => {
                    this.driverItem = data.Result;
                    if (!isNullOrUndefined(this.driverItem.DOB)) {
                        this.driverItem.DOB = parse(this.driverItem.DOB);
                    }
                    if (!isNullOrUndefined(this.driverItem.MedicalExpirationDate)) {
                        this.driverItem.MedicalExpirationDate = parse(this.driverItem.MedicalExpirationDate);
                    }
                    if (!isNullOrUndefined(this.driverItem.PreEmploymentDrugTestDate)) {
                        this.driverItem.PreEmploymentDrugTestDate = parse(this.driverItem.PreEmploymentDrugTestDate);
                    }
                    if (!isNullOrUndefined(this.driverItem.PreEmploymentDrugMVRDate)) {
                        this.driverItem.PreEmploymentDrugMVRDate = parse(this.driverItem.PreEmploymentDrugMVRDate);
                    }
                    if (!isNullOrUndefined(this.driverItem.HireDate)) {
                        this.driverItem.HireDate = parse(this.driverItem.HireDate);
                    }
                    if (!isNullOrUndefined(this.driverItem.TerminationDate)) {
                        this.driverItem.TerminationDate = parse(this.driverItem.TerminationDate);
                    }
                    if (!isNullOrUndefined(this.driverItem.CDLExpirationDate)) {
                        this.driverItem.CDLExpirationDate = parse(this.driverItem.CDLExpirationDate);
                    }
                    if (!isNullOrUndefined(this.driverItem.ModifiedDate)) {
                        this.driverItem.ModifiedDate = parse(this.driverItem.ModifiedDate);
                    }
                });
        }
    }

    onSubmitDriver() {
        this.resourcesService.saveDriver(this.driverItem)
            .subscribe((data: ResponseModel<Driver>) => {
                this.driverItem = data.Result;
                this._messagesService.success('', 'Driver details saved.');
            });
    }

}
