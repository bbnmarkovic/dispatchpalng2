import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Company} from '../../../models/company';
import {Subscription} from 'rxjs/Subscription';
import {ResponseModel} from '../../../models/response';
import {Store} from '@ngrx/store';
import {Vehicle} from '../../../models/vehicle';
import {IAppState} from '../../../redux/app_state';
import {MessagesService} from '../../shared/components/messages/messages.component';
import { ResourcesService } from '../resources.service';
import { parse } from 'date-fns';

@Component({
  selector: 'dp-vehicle-details',
  templateUrl: './vehicle-details.component.html',
  styleUrls: ['./vehicle-details.component.scss']
})
export class VehicleDetailsComponent implements OnInit, OnDestroy {
    pageTitle: string = "Truck details";
    VehicleType: number = 0;

    itemId: number;
    vehicleItem: Vehicle = new Vehicle();
    companyId: number;
    subscriptions: Subscription[];

    constructor(
        private router: Router,
        private _store: Store<IAppState>,
        private route: ActivatedRoute,
        private _resourcesService: ResourcesService,
        private _messagesService: MessagesService)
    {
        this.subscriptions = [];
    }

    ngOnInit() {
        if (this.route.data) {
            this.VehicleType = this.route.snapshot.data['vehicleType'];
        } else {
            this.VehicleType = 0;
        }

        if (this.VehicleType == 0) {
            this.pageTitle = "Truck details";
        } else {
            this.pageTitle = "Trailer details";
        }

        this.route.params.subscribe((v: { id: string }) => {
            let tempId = parseInt(v.id);
            this.itemId = isNaN(tempId) ? 0 : tempId;

            if (this.itemId > 0) {
                this.getItem();
            } else {
                this.vehicleItem.Id = 0;
                this.vehicleItem.VehicleType = this.VehicleType;
                this.vehicleItem.IsActive = true;
            }
        });

        this.subscriptions.push(this._store.select('activeCompany').subscribe((cmp: Company)=>{
            if(this.companyId > 0 && this.companyId != cmp.Id) {
                this.router.navigate(this.VehicleType == 0 ? ['/trucks'] : ['/trailers']);
            } else {
                if (this.vehicleItem.CompanyId == null)
                    this.vehicleItem.CompanyId = cmp.Id;
                this.companyId = cmp.Id;
            }
        }));
    }

    ngOnDestroy(){
        this.subscriptions.forEach((item: Subscription)=>{
            item.unsubscribe();
        });
    }

    getItem() {
        if (this.itemId > 0) {
          this._resourcesService.getVehicleById(this.itemId)
            .subscribe((data: ResponseModel<Vehicle>) => {
              this.vehicleItem = data.Result;
              if (this.vehicleItem.PlateExpiration != null) {
                this.vehicleItem.PlateExpiration = parse(this.vehicleItem.PlateExpiration);
              }
              if (this.vehicleItem.DOTExpiration != null) {
                this.vehicleItem.DOTExpiration = parse(this.vehicleItem.DOTExpiration);
              }
              if (this.vehicleItem.LeasedFrom != null) {
                this.vehicleItem.LeasedFrom = parse(this.vehicleItem.LeasedFrom);
              }
              if (this.vehicleItem.LeasedStart != null) {
                this.vehicleItem.LeasedStart = parse(this.vehicleItem.LeasedStart);
              }
              if (this.vehicleItem.LeasedEnd != null) {
                this.vehicleItem.LeasedEnd = parse(this.vehicleItem.LeasedEnd);
              }
              if (this.vehicleItem.PurchaseDate != null) {
                this.vehicleItem.PurchaseDate = parse(this.vehicleItem.PurchaseDate);
              }
              if (this.vehicleItem.SellDate != null) {
                this.vehicleItem.SellDate = parse(this.vehicleItem.SellDate);
              }
              if (this.vehicleItem.InsuranceDate != null) {
                this.vehicleItem.InsuranceDate = parse(this.vehicleItem.InsuranceDate);
              }
              if (this.vehicleItem.WarrantyDate != null) {
                this.vehicleItem.WarrantyDate = parse(this.vehicleItem.WarrantyDate);
              }
            });
        }
    }

    onSubmit() {
        this._resourcesService.saveVehicle(this.vehicleItem)
            .subscribe((data: ResponseModel<Vehicle>) => {
                this.vehicleItem = data.Result;
                this._messagesService.success('', this.VehicleType == 0 ? 'Truck details saved.' : 'Trailer details saved.');
            });
    }

}
