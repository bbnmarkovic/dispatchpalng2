import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs/Subscription';
import {IAppState} from '../../../redux/app_state';
import {ActivatedRoute, Router} from '@angular/router';
import {MessagesService} from '../../shared/components/messages/messages.component';
import {ResponseModel} from '../../../models/response';
import {Client} from '../../../models/client';
import {Company} from '../../../models/company';
import {Store} from '@ngrx/store';
import { ResourcesService } from '../resources.service';

@Component({
  selector: 'app-client-details',
  templateUrl: './client-details.component.html',
  styleUrls: ['./client-details.component.scss']
})
export class ClientDetailsComponent implements OnInit, OnDestroy {
  itemId: number;
  clientItem: Client = new Client();
  companyId: number;
  subscriptions: Subscription[];

  constructor(private router: Router,
              private _store: Store<IAppState>,
              private route: ActivatedRoute,
              private _resourcesService: ResourcesService,
              private _messagesService: MessagesService) {
    this.subscriptions = [];
  }

  ngOnInit() {
    this.route.params.subscribe((v: { id: string }) => {
      let tempId = parseInt(v.id);
      this.itemId = isNaN(tempId) ? 0 : tempId;

      if (this.itemId > 0) {
        this.getItem();
      } else {
        this.clientItem.Id = 0;
        this.clientItem.IsActive = true;
      }
    });

    this.subscriptions.push(this._store.select('activeCompany').subscribe((cmp: Company) => {
      if (this.companyId > 0 && this.companyId != cmp.Id) {
        this.router.navigate(['/clients']);
      }
      this.companyId = cmp.Id;
    }));
  }

  ngOnDestroy() {
    this.subscriptions.forEach((item: Subscription) => {
      item.unsubscribe();
    });
  }

  getItem() {
    if (this.itemId > 0) {
      this._resourcesService.getClientById(this.itemId)
        .subscribe((data: ResponseModel<Client>) => {
          this.clientItem = data.Result;
        });
    }
  }

  onSubmit() {
    this._resourcesService.saveClient(this.clientItem)
      .subscribe((data: ResponseModel<Client>) => {
        this.clientItem = data.Result;
        this._messagesService.success('', 'Client details saved.');
      });
  }

}
