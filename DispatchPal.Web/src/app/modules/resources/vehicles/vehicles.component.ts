import {Component, OnDestroy, OnInit} from '@angular/core';
import {LazyLoadEvent} from 'primeng/primeng';
import {ActivatedRoute} from '@angular/router';
import {Company} from '../../../models/company';
import {Store} from '@ngrx/store';
import {IAppState} from '../../../redux/app_state';
import {Filter} from '../../../models/filter';
import {Subscription} from 'rxjs/Subscription';
import {ResponseModel} from '../../../models/response';
import {VehiclePreview} from '../../../models/vehicle_preview';
import {saveFilterTrailers, saveFilterTrucks} from '../../../redux/actions/filter.actions';
import { ResourcesService } from '../resources.service';

@Component({
  selector: 'dp-vehicles',
  templateUrl: './vehicles.component.html',
  styleUrls: ['./vehicles.component.scss']
})
export class VehiclesComponent implements OnInit, OnDestroy {
  emptyMsg: string = "Loading...";
  filter: Filter = new Filter();
  subscriptions: Subscription[];
  vehicles: VehiclePreview[] = [];
  totalItems: number = 0;
  companyId: number;
  pageTitle: string = "Trucks";
  addNewTitle: string = "Add New Truck";

  constructor(private _store: Store<IAppState>,
              private route: ActivatedRoute,
              private _resourcesService: ResourcesService) {
    this.subscriptions = [];
    if (this.route.data) {
      this.filter.VehicleType = this.route.snapshot.data['vehicleType'];
    } else {
      this.filter.VehicleType = 0;
    }

    this.pageTitle = this.filter.VehicleType == 0 ? "Trucks" : "Trailers";
    this.addNewTitle = this.filter.VehicleType == 0 ? "Add New Truck" : "Add New Trailer";
  }

  ngOnInit() {
    if (this.filter.VehicleType == 0) {
      this.subscriptions.push(this._store.select('filterTrucks').subscribe((state: Filter) => {
        this.filter = state;
        this.filter.VehicleType = 0;
        this.getData();
      }));
    } else {
      this.subscriptions.push(this._store.select('filterTrailers').subscribe((state: Filter) => {
        this.filter = state;
        this.filter.VehicleType = 1;
        this.getData();
      }));
    }

    this.subscriptions.push(this._store.select('activeCompany').subscribe((cmp: Company) => {
      if (this.companyId > 0 && this.companyId != cmp.Id) {
        this.getData();
      }
      this.companyId = cmp.Id;
    }));
  }

  ngOnDestroy() {
    this.subscriptions.forEach((item: Subscription) => {
      item.unsubscribe();
    });
  }

  getData() {
    this._resourcesService.getVehiclesByFilter(this.filter)
      .subscribe((data: ResponseModel<VehiclePreview[]>) => {
        if (data != null) {
          this.totalItems = data.TotalItems;
          this.vehicles = data.Result;

          if (this.totalItems == 0)
            this.emptyMsg = "No records...";
        }
      });
  }

  onSubmitSearch() {
    if (this.filter.VehicleType == 0) {
      this._store.dispatch(new saveFilterTrucks(this.filter));
    } else {
      this._store.dispatch(new saveFilterTrailers(this.filter));
    }
  }

  loadData(event: LazyLoadEvent) {
    this.filter.PageSize = event.rows;
    this.filter.Offset = event.first;

    this.onSubmitSearch();
  }

  resetFilter() {
    this.filter.Offset = 0;
    this.filter.SearchParam = "";

    this.onSubmitSearch();
  }

}
