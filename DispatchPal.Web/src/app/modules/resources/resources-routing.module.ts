import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ResourcesComponent} from './resources.component';
import {DriversComponent} from './drivers/drivers.component';
import {DriverDetailsComponent} from './driver-details/driver-details.component';
import {VehiclesComponent} from './vehicles/vehicles.component';
import {VehicleDetailsComponent} from './vehicle-details/vehicle-details.component';
import {ClientsComponent} from './clients/clients.component';
import {ClientDetailsComponent} from './client-details/client-details.component';
import { UsersGuard } from '../../services/users/users_guard.service';

const routes: Routes = [
  {
    path: '',
    component: ResourcesComponent,
    children: [
      {
        path: 'drivers',
        component: DriversComponent,
        canActivate: [UsersGuard]
      },
      {
        path: 'drivers/:id',
        component: DriverDetailsComponent,
        canActivate: [UsersGuard]
      },
      {
        path: 'trucks',
        data: {
          vehicleType: 0
        },
        component: VehiclesComponent,
        canActivate: [UsersGuard]
      },
      {
        path: 'trucks/:id',
        data: {
          vehicleType: 0
        },
        component: VehicleDetailsComponent,
        canActivate: [UsersGuard]
      },
      {
        path: 'trailers',
        data: {
          vehicleType: 1
        },
        component: VehiclesComponent,
        canActivate: [UsersGuard]
      },
      {
        path: 'trailers/:id',
        data: {
          vehicleType: 1
        },
        component: VehicleDetailsComponent,
        canActivate: [UsersGuard]
      },
      {
        path: 'partners',
        component: ClientsComponent,
        canActivate: [UsersGuard]
      },
      {
        path: 'partners/:id',
        component: ClientDetailsComponent,
        canActivate: [UsersGuard]
      }
    ]
  }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ResourcesRoutingModule { }
