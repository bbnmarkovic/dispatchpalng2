import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ResourcesService } from './resources.service';
import { DriversComponent } from './drivers/drivers.component';
import { ResourcesComponent } from './resources.component';
import {FormsModule} from '@angular/forms';
import {CalendarModule, ConfirmDialogModule, DataTableModule, DropdownModule, MultiSelectModule} from 'primeng/primeng';
import {ResourcesRoutingModule} from './resources-routing.module';
import { DriverDetailsComponent } from './driver-details/driver-details.component';
import {DispatchPalSharedModule} from '../shared/shared.module';
import { VehiclesComponent } from './vehicles/vehicles.component';
import { VehicleDetailsComponent } from './vehicle-details/vehicle-details.component';
import { ClientsComponent } from './clients/clients.component';
import { ClientDetailsComponent } from './client-details/client-details.component';

@NgModule({
  imports: [
    FormsModule,
    CommonModule,
    DispatchPalSharedModule,
    ResourcesRoutingModule,
    DataTableModule,
    DropdownModule,
    CalendarModule,
    ConfirmDialogModule,
    MultiSelectModule
  ],
  declarations: [
    DriversComponent,
    ResourcesComponent,
    DriverDetailsComponent,
    VehiclesComponent,
    VehicleDetailsComponent,
    ClientsComponent,
    ClientDetailsComponent
  ],
  providers: [
    ResourcesService
  ]
})
export class ResourcesModule { }
