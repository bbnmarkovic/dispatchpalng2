import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';

import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import { Driver } from '../../models/driver'
import { DriverPreview } from '../../models/driver_preview'
import { Filter } from '../../models/filter'
import { ResponseModel } from '../../models/response'
import { Company } from '../../models/company';
import { IAppState } from '../../redux/app_state';
import {environment} from '../../../environments/environment';
import { VehiclePreview } from '../../models/vehicle_preview';
import { Vehicle } from '../../models/vehicle';
import { Client } from '../../models/client';
import { ClientType } from '../../models/client-type';

@Injectable()
export class ResourcesService {
  companyId: number;

  constructor(private http: HttpClient, private _store: Store<IAppState>) {
    _store.select('activeCompany').subscribe((state: Company) => {
      this.companyId = state.Id;
    });
  }

  getDriversByFilter(filter: Filter) {
    const url = `${environment.api}/drivers/preview`;

    let params: HttpParams = new HttpParams();
    params = params.append('offset', filter.Offset.toString());
    params = params.append('pageSize', filter.PageSize.toString());
    params = params.append('searchParam', filter.SearchParam);
    params = params.append('companyId', this.companyId.toString());

    return this.http.get<ResponseModel<DriverPreview[]>>(url, {
      params: params
    });
  }

  getDriverById(id: number) {
    const url = `${environment.api}/drivers/${id}`;

    return this.http.get<ResponseModel<Driver>>(url);
  }

  saveDriver(driver: Driver): Observable<ResponseModel<Driver>> {
    const url = `${environment.api}/drivers`;

    driver.CompanyId = this.companyId;
    return this.http.post<ResponseModel<Driver>>(url, driver);
  }

  downloadFile(id: number): Observable<any> {
    const url = `${environment.api}/drivers/files/${id}/download`;

    return this.http.get(url);
  }

  deleteFile(id: number): Observable<ResponseModel<boolean>> {
    const url = `${environment.api}/drivers/files/${id}/remove`;

    return this.http.get<ResponseModel<boolean>>(url);
  }

  getVehiclesByFilter(filter: Filter) {
    const url = `${environment.api}/vehicles/preview`;

    let params: HttpParams = new HttpParams();
    params = params.append('vehicleType', filter.VehicleType.toString());
    params = params.append('offset', filter.Offset.toString());
    params = params.append('pageSize', filter.PageSize.toString());
    params = params.append('searchParam', filter.SearchParam);
    params = params.append('companyId', this.companyId.toString());

    return this.http.get<ResponseModel<VehiclePreview[]>>(url, {
      params: params
    });
  }

  getVehicleById(id: number) {
    const url = `${environment.api}/vehicles/${id}`;

    return this.http.get<ResponseModel<Vehicle>>(url);
  }

  saveVehicle(vehicle: Vehicle) {
    const url = `${environment.api}/vehicles`;

    return this.http.post<ResponseModel<Vehicle>>(url, vehicle);
  }

  /* private extractSingle(res: Response) {
      let body = res.json();
      let temp: ResponseModel<Driver> = body || {};
      if (temp != null && temp.Result != null) {
          if (temp.Result.DOB != null)
              temp.Result.DOB = new Date(temp.Result.DOB);
          if (temp.Result.MedicalExpirationDate != null)
              temp.Result.MedicalExpirationDate = new Date(temp.Result.MedicalExpirationDate);
          if (temp.Result.PreEmploymentDrugTestDate != null)
              temp.Result.PreEmploymentDrugTestDate = new Date(temp.Result.PreEmploymentDrugTestDate);
          if (temp.Result.PreEmploymentDrugMVRDate != null)
              temp.Result.PreEmploymentDrugMVRDate = new Date(temp.Result.PreEmploymentDrugMVRDate);
          if (temp.Result.HireDate != null)
              temp.Result.HireDate = new Date(temp.Result.HireDate);
          if (temp.Result.TerminationDate != null)
              temp.Result.TerminationDate = new Date(temp.Result.TerminationDate);
          if (temp.Result.CDLExpirationDate != null)
              temp.Result.CDLExpirationDate = new Date(temp.Result.CDLExpirationDate);
      }
      return body || {};
  }

  private extractSingleBoolean(res: Response) {
      let body = res.json();
      return body || false;
  }*/

  getClientsByFilter(filter: Filter) {
    const url = `${environment.api}/clients`;

    let params: HttpParams = new HttpParams();
    params = params.append('clientType', filter.ClientType != null ? filter.ClientType.toString() : "0");
    params = params.append('offset', filter.Offset.toString());
    params = params.append('pageSize', filter.PageSize.toString());
    params = params.append('searchParam', filter.SearchParam);

    return this.http.get<ResponseModel<Client[]>>(url, {
      params: params
    });
  }

  getClientById(id: number): Observable<ResponseModel<Client>> {
    const url = `${environment.api}/clients/${id}`;

    return this.http.get<ResponseModel<Client>>(url);
  }

  saveClient(client: Client): Observable<ResponseModel<Client>> {
    const url = `${environment.api}/clients`;

    return this.http.post<ResponseModel<Client>>(url, client);
  }

}
