import {Component, OnDestroy, OnInit} from '@angular/core';
import {Filter} from '../../../models/filter';
import {Subscription} from 'rxjs/Rx';
import {IAppState} from '../../../redux/app_state';
import {ResponseModel} from '../../../models/response';
import {DomSanitizer} from '@angular/platform-browser';
import {Store} from '@ngrx/store';
import {LazyLoadEvent} from 'primeng/primeng';
import {DriverPreview} from '../../../models/driver_preview';
import {saveFilterDrivers} from '../../../redux/actions/filter.actions';
import { ResourcesService } from '../resources.service';
import { differenceInCalendarDays } from 'date-fns';

@Component({
  selector: 'app-drivers',
  templateUrl: './drivers.component.html',
  styleUrls: ['./drivers.component.scss']
})
export class DriversComponent implements OnInit, OnDestroy {
  emptyMsg: string = "Loading...";
  filter: Filter;

  drivers: DriverPreview[] = [];
  totalItems: number = 0;
  subscriptions: Subscription[];

  constructor(private _store: Store<IAppState>,
              private resourcesService: ResourcesService,
              private sanitizer: DomSanitizer) {
    this.subscriptions = [];
  }

  ngOnInit() {
    this.subscriptions.push(this._store.select('filterDrivers').subscribe((state: Filter) => {
      this.filter = state;
      this.getData();
    }));

    this.subscriptions.push(this._store.select('activeCompany').subscribe(() => {
      this.getData();
    }));
  }

  ngOnDestroy() {
    this.subscriptions.forEach((item: Subscription) => {
      item.unsubscribe();
    });
  }

  getData() {
    this.resourcesService.getDriversByFilter(this.filter)
      .subscribe((data: ResponseModel<DriverPreview[]>) => {
        if (data !== null) {
          this.totalItems = data.TotalItems;
          this.drivers = data.Result;
          this.drivers = this.drivers.map<DriverPreview>((val: DriverPreview) => {
            if (val.DOB != null)
              val.DOB = new Date(val.DOB);
            if (val.MedicalExpirationDate != null) {
              val.MedicalExpirationDate = new Date(val.MedicalExpirationDate);
              val.MedicalExpirationDays = differenceInCalendarDays(new Date(), new Date(val.MedicalExpirationDate));
            }
            if (val.CDLExpirationDate != null) {
              val.CDLExpirationDate = new Date(val.CDLExpirationDate);
              val.CDLExpirationDays = differenceInCalendarDays(new Date(), new Date(val.CDLExpirationDate));
            }
            return val;
          });

          if (this.totalItems == 0)
            this.emptyMsg = "No records...";
        }
      });
  }

  sanitize(url: string) {
    return this.sanitizer.bypassSecurityTrustUrl('callto:' + url);
  }

  onSubmitSearch() {
    this._store.dispatch(new saveFilterDrivers(this.filter));
  }

  loadData(event: LazyLoadEvent) {
    this.filter.PageSize = event.rows;
    this.filter.Offset = event.first;

    this.onSubmitSearch();
  }

  resetFilter() {
    this.filter.Offset = 0;
    this.filter.SearchParam = "";

    this.onSubmitSearch();
  }
}
