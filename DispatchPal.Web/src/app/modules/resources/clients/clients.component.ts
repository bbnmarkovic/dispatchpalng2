import {Component, OnDestroy, OnInit} from '@angular/core';
import {Filter} from '../../../models/filter';
import {Subscription} from 'rxjs/Subscription';
import {IAppState} from '../../../redux/app_state';
import {SelectItem} from 'primeng/components/common/api';
import {ResponseModel} from '../../../models/response';
import {Client} from '../../../models/client';
import {Store} from '@ngrx/store';
import {LazyLoadEvent} from 'primeng/primeng';
import {saveFilterClients} from '../../../redux/actions/filter.actions';
import { ResourcesService } from '../resources.service';

@Component({
  selector: 'app-clients',
  templateUrl: './clients.component.html',
  styleUrls: ['./clients.component.scss']
})
export class ClientsComponent implements OnInit, OnDestroy {
  filter: Filter = new Filter();
  emptyMsg: string = "Loading...";
  subscriptions: Subscription[];
  clients: Client[] = [];
  totalItems: number = 0;

  selectedColumns: any[];
  columnOptions: SelectItem[];

  constructor(private _store: Store<IAppState>,
              private _resourcesService: ResourcesService) {
    this.subscriptions = [];
  }

  ngOnInit() {
    this.subscriptions.push(this._store.select('filterClients').subscribe((state: Filter) => {
      this.filter = state;
      this.getData();
    }));

    this.subscriptions.push(this._store.select('activeCompany').subscribe(() => {
      this.getData();
    }));

    this.selectedColumns = [
      {field: 'DisplayName', header: 'Display Name'},
      {field: 'Name', header: 'Name'},
      {field: 'City', header: 'City'},
      {field: 'Email', header: 'Email'},
      {field: 'IsActive', header: 'Active'}
    ];

    this.columnOptions = [];
    for (let i = 0; i < this.selectedColumns.length; i++) {
      this.columnOptions.push({label: this.selectedColumns[i].header, value: this.selectedColumns[i]});
    }
  }

  ngOnDestroy() {
    this.subscriptions.forEach((item: Subscription) => {
      item.unsubscribe();
    });
  }

  getData() {
    this._resourcesService.getClientsByFilter(this.filter)
      .subscribe((data: ResponseModel<Client[]>) => {
        if (data !== null) {
          this.totalItems = data.TotalItems;
          this.clients = data.Result;

          if (this.totalItems == 0)
            this.emptyMsg = "No records...";
        }
      });
  }

  onSubmitSearch() {
    this._store.dispatch(new saveFilterClients(this.filter));
  }

  loadData(event: LazyLoadEvent) {
    this.filter.PageSize = event.rows;
    this.filter.Offset = event.first;

    this.onSubmitSearch();
  }

  resetFilter() {
    this.filter.Offset = 0;
    this.filter.SearchParam = "";

    this.onSubmitSearch();
  }
}
