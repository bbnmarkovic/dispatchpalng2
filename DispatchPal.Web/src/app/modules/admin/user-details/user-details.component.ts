import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {SelectItem} from 'primeng/primeng';
import {MessagesService} from '../../shared/components/messages/messages.component';
import {ResponseModel} from '../../../models/response';
import {Company} from '../../../models/company';
import {User} from '../../../models/user';
import { AdminService } from '../admin.service';

@Component({
  selector: 'dp-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.scss']
})
export class UserDetailsComponent implements OnInit {
  companies: SelectItem[] = [];
  selectedCompanies: string[] = [];

  itemId: number;
  modelItem: User = new User();

  constructor(
    private route: ActivatedRoute,
    private _adminService: AdminService,
    private _messagesService: MessagesService) { }

  ngOnInit() {
    this.route.params.subscribe((v: { id: string }) => {
      let tempId = parseInt(v.id);
      this.itemId = isNaN(tempId) ? 0 : tempId;

      if (this.itemId > 0) {
        this.getItem();
      } else {
        this.modelItem.Id = 0;
        this.modelItem.IsActive = true;
      }
    });

    this._adminService.getCompaniesForAutoComplete("").subscribe((data: ResponseModel<Company[]>)=>{
      for (let item of data.Result){
        this.companies.push({label:item.Name, value:item.Id.toString()});
      }
    });
  }

  getItem() {
    if (this.itemId > 0) {
      this._adminService.getUserById(this.itemId)
        .subscribe((data: ResponseModel<User>) => {
          this.modelItem = data.Result;

          for (let item of this.modelItem.Companies){
            this.selectedCompanies.push(item.toString());
          }
        });
    }
  }

  onSubmit() {
    this.modelItem.Companies.length = 0;
    for(let item of this.selectedCompanies){
      this.modelItem.Companies.push(parseInt(item));
    }
    this._adminService.saveUser(this.modelItem)
      .subscribe((data: ResponseModel<User>) => {
        this.modelItem = data.Result;
        this._messagesService.success('', 'User details saved.');
      });
  }

}
