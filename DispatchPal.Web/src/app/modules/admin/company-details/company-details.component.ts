import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MessagesService } from '../../shared/components/messages/messages.component';
import { ResponseModel } from '../../../models/response';
import { Company } from '../../../models/company';
import { AdminService } from '../admin.service';

@Component({
  selector: 'dp-company-details',
  templateUrl: './company-details.component.html',
  styleUrls: ['./company-details.component.scss']
})
export class CompanyDetailsComponent implements OnInit {

  constructor(
    private route: ActivatedRoute,
    private _adminService: AdminService,
    private _messagesService: MessagesService) { }

  itemId: number;
  companyItem: Company = new Company();

  ngOnInit() {
    this.route.params.subscribe((v: { id: string }) => {
      let tempId = parseInt(v.id);
      this.itemId = isNaN(tempId) ? 0 : tempId;

      if (this.itemId > 0) {
        this.getItem();
      } else {
        this.companyItem.Id = 0;
        this.companyItem.IsActive = true;
      }
    });
  }

  getItem() {
    if (this.itemId > 0) {
      this._adminService.getCompanyById(this.itemId)
        .subscribe((data: ResponseModel<Company>) => {
          this.companyItem = data.Result;
        });

    }
  }

  onSubmit() {
    this._adminService.saveCompany(this.companyItem)
      .subscribe((data: ResponseModel<Company>) => {
        this.companyItem = data.Result;
        this._messagesService.success('', 'Company details saved.');
      });
  }

}
