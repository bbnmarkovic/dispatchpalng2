import { Component, OnInit } from '@angular/core';
import {Filter} from '../../../models/filter';
import {ResponseModel} from '../../../models/response';
import {User} from '../../../models/user';
import {LazyLoadEvent} from 'primeng/primeng';
import { AdminService } from '../admin.service';

@Component({
  selector: 'dp-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {

  constructor(private _adminService: AdminService) { }

  emptyMsg: string = "Loading...";
  filter: Filter = new Filter();

  users: User[] = [];
  totalItems: number = 0;

  ngOnInit() {
  }

  onSubmitSearch() {
    this._adminService.getUsersByFilter(this.filter).subscribe((data: ResponseModel<User[]>) => {
      if (data !== null) {
        this.totalItems = data.TotalItems;
        this.users = data.Result;
      } else {
      }
    });
  }

  loadData(event: LazyLoadEvent) {
    this.filter.PageSize = event.rows;
    this.filter.Offset = event.first;

    this.onSubmitSearch();
  }

  resetFilter() {
    this.filter.Offset = 0;
    this.filter.SearchParam = "";

    this.onSubmitSearch();
  }

}
