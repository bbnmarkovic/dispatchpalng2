import { Injectable } from '@angular/core';
import { Company } from '../../models/company';
import { ResponseModel } from '../../models/response';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Filter } from '../../models/filter';
import { Store } from '@ngrx/store';
import { IAppState } from '../../redux/app_state';
import { User } from '../../models/user';

@Injectable()
export class AdminService {
  companyId: number;

  constructor(private http: HttpClient, private _store: Store<IAppState>) {
    _store.select('activeCompany').subscribe((state: Company) => {
      this.companyId = state.Id;
    });
  }

  getCompaniesByFilter(filter: Filter) {
    const url = `${environment.api}/companies`;

    let params: HttpParams = new HttpParams();
    params = params.append('offset', filter.Offset.toString());
    params = params.append('pageSize', filter.PageSize.toString());
    params = params.append('searchParam', filter.SearchParam);

    return this.http.get<ResponseModel<Company[]>>(url, {
      params: params
    });
  }

  getCompaniesForAutoComplete(searchParam: string) {
    const url = `${environment.api}/companies`;

    let params: HttpParams = new HttpParams();
    params = params.append('offset', "0");
    params = params.append('pageSize', "25");
    params = params.append('searchParam', searchParam);
    params = params.append('active', "true");
    /*params = params.append('__showLoader', "false");*/

    return this.http.get<ResponseModel<Company[]>>(url, {
      params: params
    });
  }

  getCompanyById(id: number) {
    const url = `${environment.api}/companies/${id}`;

    return this.http.get<ResponseModel<Company>>(url);
  }

  saveCompany(client: Company) {
    const url = `${environment.api}/companies`;

    return this.http.post<ResponseModel<Company>>(url, client);
  }

  getUsersByFilter(filter: Filter) {
    const url = `${environment.api}/users`;

    let params: HttpParams = new HttpParams();
    params = params.append('offset', filter.Offset.toString());
    params = params.append('pageSize', filter.PageSize.toString());
    params = params.append('searchParam', filter.SearchParam);

    return this.http.get<ResponseModel<User[]>>(url, {
      params: params
    });
  }

  getUserById(id: number) {
    const url = `${environment.api}/users/${id}`;

    return this.http.get<ResponseModel<User>>(url);
  }

  saveUser(usr: User) {
    const url = `${environment.api}/users}`;

    return this.http.post<ResponseModel<User>>(url, usr);
  }

  getToken(username: string, password: string) {
    const url = `${environment.api}/login`;

    let credentials = btoa(`${username}:${password}`);
    let headers = new HttpHeaders({
      'Authorization': `Basic ${credentials}`,
      'Content-Type': 'application/x-www-form-urlencoded'
    });

    return this.http.post<User>(url, null, { headers: headers });
  }
}
