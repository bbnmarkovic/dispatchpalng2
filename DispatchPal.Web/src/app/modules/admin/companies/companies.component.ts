import { Component, OnInit } from '@angular/core';
import { Filter } from '../../../models/filter';
import { IAppState } from '../../../redux/app_state';
import { ResponseModel } from '../../../models/response';
import { Company } from '../../../models/company';
import { Store } from '@ngrx/store';
import { LazyLoadEvent } from 'primeng/primeng';
import { saveFilterCompanies } from '../../../redux/actions/filter.actions';
import { AdminService } from '../admin.service';

@Component({
  selector: 'dp-companies',
  templateUrl: './companies.component.html',
  styleUrls: ['./companies.component.scss']
})
export class CompaniesComponent implements OnInit {

  constructor(
    private _store: Store<IAppState>,
    private _adminService: AdminService)
  {
    _store.select('filterCompanies').subscribe((state: Filter) => {
      this.filter = state;

      this._adminService.getCompaniesByFilter(this.filter)
        .subscribe((data: ResponseModel<Company[]>) => {
          if (data !== null) {
            this.totalItems = data.TotalItems;
            this.items = data.Result;
          }
        });
    });
  }

  emptyMsg: string = "Loading...";
  filter: Filter = new Filter();

  items: Company[] = [];
  totalItems: number = 0;

  ngOnInit() {
  }

  onSubmitSearch() {
    this._store.dispatch(new saveFilterCompanies(this.filter));
  }

  loadData(event: LazyLoadEvent) {
    this.filter.PageSize = event.rows;
    this.filter.Offset = event.first;

    this.onSubmitSearch();
  }

  resetFilter() {
    this.filter.Offset = 0;
    this.filter.SearchParam = "";

    this.onSubmitSearch();
  }

}
