import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminComponent } from './admin.component';
import {UsersComponent} from './users/users.component';
import { UserDetailsComponent } from './user-details/user-details.component';
import {AdminRoutingModule} from './admin-routing.module';
import {FormsModule} from '@angular/forms';
import {
  CalendarModule, ConfirmDialogModule, DataTableModule, DialogModule, DropdownModule, MultiSelectModule,
  SpinnerModule
} from 'primeng/primeng';
import { DispatchPalSharedModule } from '../shared/shared.module';
import { CompaniesComponent } from './companies/companies.component';
import { CompanyDetailsComponent } from './company-details/company-details.component';
import { AdminService } from './admin.service';

@NgModule({
  imports: [
    FormsModule,
    CommonModule,
    DispatchPalSharedModule,
    AdminRoutingModule,
    DataTableModule,
    DropdownModule,
    CalendarModule,
    ConfirmDialogModule,
    MultiSelectModule,
    DialogModule,
    SpinnerModule
  ],
  declarations: [
    AdminComponent,
    UsersComponent,
    UserDetailsComponent,
    CompaniesComponent,
    CompanyDetailsComponent
  ],
  providers: [
    AdminService
  ]
})
export class AdminModule { }
