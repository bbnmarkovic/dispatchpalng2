import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaintenanceComponent } from './maintenance.component';
import { ServicesComponent } from './services/services.component';
import { ServiceDetailsComponent } from './service-details/service-details.component';
import { MaintenanceRoutingModule } from './maintenance-routing.module';
import { FormsModule } from '@angular/forms';
import { DispatchPalSharedModule } from '../shared/shared.module';
import {
  CalendarModule, ConfirmDialogModule, DataTableModule, DialogModule, DropdownModule, MultiSelectModule,
  SpinnerModule
} from 'primeng/primeng';
import { MaintenanceService } from './maintenance.service';

@NgModule({
  imports: [
    FormsModule,
    CommonModule,
    DispatchPalSharedModule,
    MaintenanceRoutingModule,
    DataTableModule,
    DropdownModule,
    CalendarModule,
    ConfirmDialogModule,
    MultiSelectModule,
    DialogModule,
    SpinnerModule
  ],
  declarations: [
    MaintenanceComponent,
    ServicesComponent,
    ServiceDetailsComponent
  ],
  providers: [
    MaintenanceService
  ]
})
export class MaintenanceModule { }
