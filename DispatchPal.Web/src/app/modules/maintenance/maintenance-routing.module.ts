import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { ServicesComponent } from './services/services.component';
import { MaintenanceComponent } from './maintenance.component';
import { ServiceDetailsComponent } from './service-details/service-details.component';

const routes: Routes = [
  {
    path: '',
    component: MaintenanceComponent,
    children: [
      {
        path: 'services',
        component: ServicesComponent
      },
      {
        path: 'services/:id',
        component: ServiceDetailsComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MaintenanceRoutingModule { }
