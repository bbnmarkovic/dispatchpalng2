import { Component, OnDestroy, OnInit } from '@angular/core';
import { Filter } from '../../../models/filter';
import { Subscription } from 'rxjs/Subscription';
import { IAppState } from '../../../redux/app_state';
import { Service } from '../../../models/service';
import { ResponseModel } from '../../../models/response';
import { Company } from '../../../models/company';
import { Store } from '@ngrx/store';
import { LazyLoadEvent } from 'primeng/primeng';
import { saveFilterServices } from '../../../redux/actions/filter.actions';
import { MaintenanceService } from '../maintenance.service';

@Component({
  selector: 'dp-services',
  templateUrl: './services.component.html',
  styleUrls: ['./services.component.scss']
})
export class ServicesComponent implements OnInit, OnDestroy {
  emptyMsg: string = "Loading...";
  filter: Filter = new Filter();
  subscriptions: Subscription[];
  items: Service[] = [];
  totalItems: number = 0;
  companyId: number;

  constructor(
    private _store: Store<IAppState>,
    private _maintenanceService: MaintenanceService)
  {
    this.subscriptions = [];
  }

  ngOnInit() {
    this.subscriptions.push(this._store.select('filterServices').subscribe((state: Filter) => {
      this.filter = state;
      this.getData();
    }));
    this.subscriptions.push(this._store.select('activeCompany').subscribe((cmp: Company)=>{
      if(this.companyId > 0 && this.companyId != cmp.Id) {
        this.getData();
      }
      this.companyId = cmp.Id;
    }));
  }

  ngOnDestroy(){
    this.subscriptions.forEach((item: Subscription)=>{
      item.unsubscribe();
    });
  }

  getData(){
    this._maintenanceService.getServicesByFilter(this.filter)
      .subscribe((data: ResponseModel<Service[]>) => {
        if (data !== null) {
          this.totalItems = data.TotalItems;
          this.items = data.Result;

          if(this.totalItems == 0)
            this.emptyMsg = "No records...";
        }
      });
  }

  onSubmitSearch() {
    this._store.dispatch(new saveFilterServices(this.filter));
  }

  loadData(event: LazyLoadEvent) {
    this.filter.PageSize = event.rows;
    this.filter.Offset = event.first;

    this.onSubmitSearch();
  }

  resetFilter() {
    this.filter.Offset = 0;
    this.filter.SearchParam = "";

    this.onSubmitSearch();
  }

}
