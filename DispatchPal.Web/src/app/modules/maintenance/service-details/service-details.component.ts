import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { IAppState } from '../../../redux/app_state';
import { ActivatedRoute, Router } from '@angular/router';
import { MessagesService } from '../../shared/components/messages/messages.component';
import { Service } from '../../../models/service';
import { ResponseModel } from '../../../models/response';
import { Company } from '../../../models/company';
import { Store } from '@ngrx/store';
import { MaintenanceService } from '../maintenance.service';
import { parse } from 'date-fns';

@Component({
  selector: 'dp-service-details',
  templateUrl: './service-details.component.html',
  styleUrls: ['./service-details.component.scss']
})
export class ServiceDetailsComponent implements OnInit, OnDestroy {
  itemId: number;
  serviceItem: Service = new Service();

  companyId: number;
  subscriptions: Subscription[];

  constructor(
    private router: Router,
    private _store: Store<IAppState>,
    private route: ActivatedRoute,
    private _maintenanceService: MaintenanceService,
    private _messagesService: MessagesService)
  {
    this.subscriptions = [];
  }

  ngOnInit() {
    this.route.params.subscribe((v: { id: string }) => {
      let tempId = parseInt(v.id);
      this.itemId = isNaN(tempId) ? 0 : tempId;

      if (this.itemId > 0) {
        this.getItem();
      } else {
        this.serviceItem.Id = 0;
        this.serviceItem.Date = new Date();
      }
    });

    this.subscriptions.push(this._store.select('activeCompany').subscribe((cmp: Company)=>{
      if(this.companyId > 0 && this.companyId != cmp.Id) {
        this.router.navigate(['/services']);
      }
      this.companyId = cmp.Id;
    }));
  }

  ngOnDestroy(){
    this.subscriptions.forEach((item: Subscription)=>{
      item.unsubscribe();
    });
  }

  getItem() {
    if (this.itemId > 0) {
      this._maintenanceService.getServiceById(this.itemId)
        .subscribe((data: ResponseModel<Service>) => {
          this.serviceItem = data.Result;
          if (this.serviceItem.Date != null) {
            this.serviceItem.Date = parse(this.serviceItem.Date);
          }
          if (this.serviceItem.WarrantyExpDate != null) {
            this.serviceItem.WarrantyExpDate = parse(this.serviceItem.WarrantyExpDate);
          }
          if (this.serviceItem.ScheduledServiceDate != null) {
            this.serviceItem.ScheduledServiceDate = parse(this.serviceItem.ScheduledServiceDate);
          }
        });
    }
  }

  onSubmit() {
    this._maintenanceService.saveService(this.serviceItem)
      .subscribe((data: ResponseModel<Service>) => {
        this.serviceItem = data.Result;
        this._messagesService.success('', 'Service details saved.');
      });
  }

}
