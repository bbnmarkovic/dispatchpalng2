import { Injectable } from '@angular/core';
import { ResponseModel } from '../../models/response';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Filter } from '../../models/filter';
import { Service } from '../../models/service';
import { Store } from '@ngrx/store';
import { Company } from '../../models/company';
import { IAppState } from '../../redux/app_state';

@Injectable()
export class MaintenanceService {
  companyId: number;

  constructor(private http: HttpClient, private _store: Store<IAppState>) {
    _store.select('activeCompany').subscribe((state: Company) => {
      this.companyId = state.Id;
    });
  }

  getServicesByFilter(filter: Filter) {
    const url = `${environment.api}/services`;

    let params: HttpParams = new HttpParams();
    params = params.append('offset', filter.Offset.toString());
    params = params.append('pageSize', filter.PageSize.toString());
    params = params.append('searchParam', filter.SearchParam);
    params = params.append('companyId', this.companyId.toString());

    return this.http.get<ResponseModel<Service[]>>(url, {
      params: params
    });
  }

  getServiceById(id: number) {
    const url = `${environment.api}/services/${id}`;

    return this.http.get<ResponseModel<Service>>(url);
  }

  saveService(service: Service) {
    const url = `${environment.api}/services`;

    service.CompanyId = this.companyId;
    return this.http.post<ResponseModel<Service>>(url, service);
  }
}
