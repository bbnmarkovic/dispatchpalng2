import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';

import { Store } from '@ngrx/store';
import { Company } from '../../models/company';
import { IAppState } from '../../redux/app_state';
import {environment} from '../../../environments/environment';
import {User} from '../../models/user';
import {ResponseModel} from '../../models/response';

@Injectable()
export class PublicService {
    companyId: number;

    constructor(private http: HttpClient, private _store: Store<IAppState>) {
        _store.select('activeCompany').subscribe((state: Company) => {
            this.companyId = state.Id;
        });
    }

    getToken(username: string, password: string) {
        const url = `${environment.api}/login`;

        let credentials = btoa(`${username}:${password}`);
        let headers = new HttpHeaders({
            'Authorization': `Basic ${credentials}`,
            'Content-Type': 'application/x-www-form-urlencoded'
        });

        return this.http.post<User>(url, null, { headers: headers });
    }

    getCompaniesForAutoComplete(searchParam: string) {
        const url = `${environment.api}/companies`;

        let params: HttpParams = new HttpParams();
        params = params.append('offset', "0");
        params = params.append('pageSize', "25");
        params = params.append('searchParam', searchParam);
        params = params.append('active', "true");
        /*params = params.append('__showLoader', "false");*/

        return this.http.get<ResponseModel<Company[]>>(url, {
            params: params
        });
    }
}