import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home/home.component';
import { FeaturesComponent } from './features/features.component';
import { PricingComponent } from './pricing/pricing.component';
import { ContactComponent } from './contact/contact.component';
import {PublicRoutingModule} from './public-routing.module';
import { PublicComponent } from './public.component';
import { LoginComponent } from './login/login.component';
import {FormsModule} from '@angular/forms';
import {PublicService} from './public.service';

@NgModule({
    imports: [
        FormsModule,
        CommonModule,
        PublicRoutingModule
    ],
    declarations: [
        HomeComponent,
        FeaturesComponent,
        PricingComponent,
        ContactComponent,
        PublicComponent,
        LoginComponent
    ],
    providers: [
        PublicService
    ]
})
export class PublicModule { }
