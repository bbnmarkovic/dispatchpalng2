import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {IAppState} from '../../../redux/app_state';
import {ResponseModel} from '../../../models/response';
import {Company} from '../../../models/company';
import {User} from '../../../models/user';
import {Message} from 'primeng/components/common/api';
import {userLogin, userLogout} from '../../../redux/actions/user.actions';
import {NgForm} from '@angular/forms';
import {companySelected} from '../../../redux/actions/company.actions';
import {Store} from '@ngrx/store';
import {PublicService} from '../public.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
    user: User = new User();

    constructor(
        private _store: Store<IAppState>,
        private router: Router,
        private publicService: PublicService
    ) { }

    ngOnInit(): void {
        this._store.dispatch(new userLogout(new User()));
    }

    login(form: NgForm) {
        if (form.invalid)
            return;
        this.publicService.getToken(this.user.UserName, this.user.Password)
            .subscribe((v: User) => {
                    this._store.dispatch(new userLogin(v));
                    localStorage.setItem("token", v.Token);

                    this.publicService.getCompaniesForAutoComplete("").subscribe((data: ResponseModel<Company[]>) => {
                        if (data.Result.length > 0) {
                            this._store.dispatch(new companySelected(data.Result[0]));
                        }
                        this.router.navigate(['/console']);
                        /*if (this.config.returnUrl) {
                            let url = this.config.returnUrl;
                            this.config.returnUrl = null;
                            this.router.navigateByUrl(url, { replaceUrl: true });
                        } else
                            this.router.navigate(['/console/']);*/

                    });
                },
                (e: any) => {
                    console.error(e);

                });
    }

}
