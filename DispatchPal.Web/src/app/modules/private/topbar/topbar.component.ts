import {Component} from '@angular/core';
import {PrivateComponent} from '../private.component';
import { Company } from '../../../models/company';
import { Store } from '@ngrx/store';
import { IAppState } from '../../../redux/app_state';
import { companySelected } from '../../../redux/actions/company.actions';

@Component({
    selector: 'app-topbar',
    templateUrl: './topbar.component.html',
    styleUrls: ['./topbar.component.scss']
})
export class AppTopBarComponent {
  activeCompany: Company = new Company();

  constructor(public app: PrivateComponent,
              private _store: Store<IAppState>) {
    _store.select('activeCompany').subscribe((state: Company) => {
      this.activeCompany = state;
    });
  }

  onCompanySelected(item: Company) {
    this._store.dispatch(new companySelected(item));
  }
}
