import {animate, Component, OnInit, state, style, transition, trigger} from '@angular/core';
import { User } from '../../../models/user';
import { Company } from '../../../models/company';
import { Store } from '@ngrx/store';
import { IAppState } from '../../../redux/app_state';

@Component({
  selector: 'app-inline-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
    animations: [
        trigger('menu', [
            state('hidden', style({
                height: '0px'
            })),
            state('visible', style({
                height: '*'
            })),
            transition('visible => hidden', animate('400ms cubic-bezier(0.86, 0, 0.07, 1)')),
            transition('hidden => visible', animate('400ms cubic-bezier(0.86, 0, 0.07, 1)'))
        ])
    ]
})
export class ProfileComponent {

  active: boolean;
  activeUser: User = new User();

  constructor(private _store: Store<IAppState>) {
    _store.select('activeUser').subscribe((state: User) => {
      this.activeUser = state;
    });
  }

  onClick(event) {
    this.active = !this.active;
    event.preventDefault();
  }

}
