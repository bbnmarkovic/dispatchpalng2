import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {User} from '../../../models/user';
import {Company} from '../../../models/company';
import {companySelected} from '../../../redux/actions/company.actions';
import {Store} from '@ngrx/store';
import {IAppState} from '../../../redux/app_state';

@Component({
    selector: 'dp-active-user',
    templateUrl: './active-user.component.html',
    styleUrls: ['./active-user.component.scss']
})
export class ActiveUserComponent implements OnInit {
    @Output() onUserLogout = new EventEmitter<any>();

    activeCompany: Company = new Company();
    activeUser: User = new User();


    constructor(private _store: Store<IAppState>) {
        _store.select('activeUser').subscribe((state: User) => {
            this.activeUser = state;
        });

        _store.select('activeCompany').subscribe((state: Company) => {
            this.activeCompany = state;
        });
    }

    ngOnInit() {
    }

    onCompanySelected(item: Company) {
        this._store.dispatch(new companySelected(item));
    }
}
