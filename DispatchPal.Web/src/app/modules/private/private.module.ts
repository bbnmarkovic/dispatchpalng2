import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard/dashboard.component';
import { PrivateComponent } from './private.component';
import {PrivateRoutingModule} from './private-routing.module';
import { ProfileComponent } from './profile/profile.component';
import {AppMenuComponent, AppSubMenuComponent} from './menu/menu.component';
import {AppTopBarComponent} from './topbar/topbar.component';
import { FooterComponent } from './footer/footer.component';
import { ActiveUserComponent } from './active-user/active-user.component';
import {DispatchPalSharedModule} from '../shared/shared.module';
import {FormsModule} from '@angular/forms';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        PrivateRoutingModule,
        DispatchPalSharedModule
    ],
    declarations: [
        DashboardComponent,
        PrivateComponent,
        AppTopBarComponent,
        ProfileComponent,
        AppMenuComponent,
        AppSubMenuComponent,
        FooterComponent,
        ActiveUserComponent
    ]
})
export class PrivateModule { }
