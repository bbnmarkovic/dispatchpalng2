import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {PrivateComponent} from './private.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import { UsersGuard } from '../../services/users/users_guard.service';

const routes: Routes = [
  {
    path: '',
    component: PrivateComponent,
    canActivate: [UsersGuard],
    children: [
      {
        path: '',
        redirectTo: 'dashboard'

      },
      {
        path: 'dashboard',
        component: DashboardComponent
      },
      {
        path: 'resources',
        loadChildren: '../resources/resources.module#ResourcesModule'
      },
      {
        path: 'dispatch',
        loadChildren: '../dispatch/dispatch.module#DispatchModule'
      },
      {
        path: 'finance',
        loadChildren: '../finance/finance.module#FinanceModule'
      },
      {
        path: 'maintenance',
        loadChildren: '../maintenance/maintenance.module#MaintenanceModule'
      },
      {
        path: 'administration',
        loadChildren: '../admin/admin.module#AdminModule'
      }
    ]
  }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PrivateRoutingModule { }
