import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { LoadsComponent } from './loads/loads.component';
import { LoadDetailsComponent } from './load-details/load-details.component';
import { TripsComponent } from './trips/trips.component';
import { TripDetailsComponent } from './trip-details/trip-details.component';
import { DispatchComponent } from './dispatch.component';
import { PlannerComponent } from './planner/planner.component';

const routes: Routes = [
  {
    path: '',
    component: DispatchComponent,
    children: [
      {
        path: 'planner',
        component: PlannerComponent
      },
      {
        path: 'loads',
        component: LoadsComponent
      },
      {
        path: 'loads/:id',
        component: LoadDetailsComponent
      },
      {
        path: 'trips',
        component: TripsComponent
      },
      {
        path: 'trips/:id',
        component: TripDetailsComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DispatchRoutingModule { }
