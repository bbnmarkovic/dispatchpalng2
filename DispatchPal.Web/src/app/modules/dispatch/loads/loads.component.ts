import { AfterViewInit, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Filter } from '../../../models/filter';
import { Subscription } from 'rxjs/Subscription';
import { ResponseModel } from '../../../models/response';
import { DataTable, LazyLoadEvent } from 'primeng/primeng';
import { Company } from '../../../models/company';
import { LoadPreview } from '../../../models/load_preview';
import { IAppState } from '../../../redux/app_state';
import { Store } from '@ngrx/store';
import { saveFilterLoads } from '../../../redux/actions/filter.actions';
import { DispatchService } from '../dispatch.service';

@Component({
  selector: 'dp-loads',
  templateUrl: './loads.component.html',
  styleUrls: ['./loads.component.scss']
})
export class LoadsComponent implements OnInit, OnDestroy, AfterViewInit{
  @ViewChild('dataTable') public dataTable: DataTable;

  emptyMsg: string = "Loading...";
  filter: Filter = new Filter();
  subscriptions: Subscription[];
  items: LoadPreview[] = [];
  totalItems: number = 0;
  companyId: number;

  constructor(
    private _store: Store<IAppState>,
    private _dispatchService: DispatchService)
  {
    this.subscriptions = [];
  }

  ngOnInit() {
    this.subscriptions.push(this._store.select('filterLoads').subscribe((flt: Filter) => {
      this.filter = flt;
      this.getData();
    }));
    this.subscriptions.push(this._store.select('activeCompany').subscribe((cmp: Company)=>{
      if(this.companyId > 0 && this.companyId != cmp.Id) {
        this.getData();
      }
      this.companyId = cmp.Id;
    }));
  }

  ngOnDestroy(){
    this.subscriptions.forEach((item: Subscription)=>{
      item.unsubscribe();
    });
  }

  getData(){
    this._dispatchService.getLoadsByFilter(this.filter)
      .subscribe((data: ResponseModel<LoadPreview[]>) => {
        if (data !== null) {
          this.totalItems = data.TotalItems;
          this.items = data.Result;

          if(this.totalItems == 0)
            this.emptyMsg = "No records...";
        }
      });
  }

  ngAfterViewInit() {
    this.dataTable['first'] = this.filter.Offset;
  }

  onSubmitSearch() {
    this._store.dispatch(new saveFilterLoads(this.filter));
  }

  loadData(event: LazyLoadEvent) {
    this.filter.PageSize = event.rows;
    this.filter.Offset = event.first;

    this.onSubmitSearch();
  }

  resetFilter() {
    this.filter.Offset = 0;
    this.filter.SearchParam = "";

    this.onSubmitSearch();
  }

}
