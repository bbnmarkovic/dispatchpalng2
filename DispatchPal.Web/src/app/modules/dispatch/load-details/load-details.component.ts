import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { LoadPoint } from '../../../models/load_point';
import { Load } from '../../../models/load';
import { Trip } from '../../../models/trip';
import { ResponseModel } from '../../../models/response';
import { Company } from '../../../models/company';
import { User } from '../../../models/user';
import { IAppState } from '../../../redux/app_state';
import { Location } from '@angular/common';
import { MessagesService } from '../../shared/components/messages/messages.component';
import { Store } from '@ngrx/store';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { MapComponent } from '../components/map/map.component';
import { LoadCreateComponent } from '../components/load-create/load-create.component';
import { LoadPointsComponent } from '../components/load-points/load-points.component';
import { DispatchService } from '../dispatch.service';
import { parse } from 'date-fns';

@Component({
  selector: 'dp-load-details',
  templateUrl: './load-details.component.html',
  styleUrls: ['./load-details.component.scss']
})
export class LoadDetailsComponent implements OnInit, OnDestroy {
  @ViewChild("map") mapElement: MapComponent;
  @ViewChild("createLoad") createLoadComponent: LoadCreateComponent;
  @ViewChild("loadPointsComponent") loadPointsComponent: LoadPointsComponent;

  userId: number;
  tripToAssignId: number = 0;

  tripItem: Trip = new Trip();
  createNewTripVisible: boolean = false;

  loadId: number;
  loadItem: Load = new Load();

  // loadPoints: LoadPoint[] = [];

  assignLoadVisible: boolean = false;
  companyId: number;
  subscriptions: Subscription[];

  constructor(
    private router: Router,
    private location: Location,
    private route: ActivatedRoute,
    private _store: Store<IAppState>,
    private _dispatchService: DispatchService,
    private _messagesService: MessagesService,)
  {
    this.subscriptions = [];


  }

  ngOnInit() {
    this.loadItem.Id = 0;
    this._store.select('activeUser').subscribe((state: User) => {
      this.userId = state.Id;

      this.route.params.subscribe((v: { id: string }) => {
        let tempId = parseInt(v.id);
        this.loadId = isNaN(tempId) ? 0 : tempId;

        if (this.loadId > 0) {
          this.getLoad();
        } else {
          this.router.routeReuseStrategy.shouldReuseRoute = function(){
            return false;
          };

          this.router.events.subscribe((evt) => {
            if (evt instanceof NavigationEnd) {
              // trick the Router into believing it's last link wasn't previously loaded
              this.router.navigated = false;
              // if you need to scroll back to top, here is the right place
              window.scrollTo(0, 0);
            }
          });
          this._dispatchService.getNextLoadNumber()
            .subscribe((data: ResponseModel<string>) => {
              this.loadItem.Id = 0;
              this.loadItem.LoadNumber = data.Result;
              this.loadItem.IsActive = true;
              this.loadItem.AgreedPrice = 0;
              this.loadItem.BillableMiles = 0;
              this.loadItem.BobtailMiles = 0;
              this.loadItem.EmptyMiles = 0;
              this.loadItem.ExtraStops = 0;
              this.loadItem.FuelSurcharge = 1;
              this.loadItem.PaidInFull = false;
              this.loadItem.BookingDate = new Date();
              this.loadItem.BrokerRating = 5;
              this.loadItem.DispatcherId = this.userId;

              this.createLoadComponent.showDialog(this.loadItem);
            });
        }
      });
    });

    this.subscriptions.push(this._store.select('activeCompany').subscribe((cmp: Company)=>{
      if(this.companyId > 0 && this.companyId != cmp.Id) {
        this.router.navigate(['/dispatch']);
      }
      this.companyId = cmp.Id;
    }));
  }

  ngOnDestroy(){
    this.subscriptions.forEach((item: Subscription)=>{
      item.unsubscribe();
    });
  }

  getLoad() {
    if (this.loadId > 0) {
      this._dispatchService.getLoadById(this.loadId)
        .subscribe((data: ResponseModel<Load>) => {
          this.loadItem = data.Result;
          if (this.loadItem.DateFrom != null) {
            this.loadItem.DateFrom = parse(this.loadItem.DateFrom);
          }
          if (this.loadItem.DateTo != null) {
            this.loadItem.DateTo = parse(this.loadItem.DateTo);
          }
          if (this.loadItem.BookingDate != null) {
            this.loadItem.BookingDate = parse(this.loadItem.BookingDate);
          }
          this.loadPointsComponent.getLoadPoints(true, this.loadItem.Id);
        });
    } else {
      this.loadItem = new Load();
      this.loadItem.Id = 0;
      this.loadItem.IsActive = true;
    }
  }

  /*getLoadPoints() {
      this.loadPoints.length = 0;
      this._loadsService.getLoadPoints(this.loadId)
          .subscribe((data: ResponseModel<LoadPoint[]>) => {
              for (let dt of data.Result) {
                  this.loadPoints.push(dt);
              }
              this.mapElement.drawMarkers(this.loadPoints);
          });
  }*/

  onMapUpdate(items: LoadPoint[]) {
    this.mapElement.drawMarkers(items);
  }

  onLoadFromDialogCreated(load: Load){
    this.loadId = load.Id;
    this.getLoad();
    this.location.replaceState("/dispatch/" + load.Id);

  }

  onCancelCreateLoad() {
    this.location.back();
  }

  onLoadCreated(id: number) {
    this.location.replaceState("/dispatch/" + id);
  }

  onRouteDrawn(event: any) {
    this.loadItem.TotalDistanceMi = event;
  }

  assignToTrip() {
    this.tripToAssignId = 0;
    this.assignLoadVisible = true;
  }

  onTripSelected(event: any) {
    this.tripToAssignId = event;
  }

  assignLoadToTrip() {
    this._dispatchService.assignLoadToTrip(this.loadItem.Id, this.tripToAssignId)
      .subscribe((data: ResponseModel<boolean>) => {
        if (data.IsSuccess) {
          this.router.navigate(['/trips', this.tripToAssignId]);
        }
        else {
          this._messagesService.error('', data.Message);
        }
      });
  }

  createTrip(){
    this.createNewTripVisible = true;
    this._dispatchService.getNextTripNumber()
      .subscribe((data: ResponseModel<string>) => {
        if(data.IsSuccess) {
          this.tripItem = new Trip();
          this.tripItem.Id = 0;
          this.tripItem.TripNumber = data.Result;
          this.tripItem.IsActive = true;
        } else {
          this._messagesService.error('', data.Message);
        }
      });
  }

  onTripCancel(){
    this.createNewTripVisible = false;
  }

  onTripSaved(tripId: number){
    this._dispatchService.assignLoadToTrip(this.loadItem.Id, tripId)
      .subscribe((data: ResponseModel<boolean>) => {
        if (data.IsSuccess)
          this.router.navigate(['/trips', tripId]);
      });
  }
}
