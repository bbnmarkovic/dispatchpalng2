import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { LoadPoint } from '../../../models/load_point';
import { ConfirmationService } from 'primeng/components/common/api';
import { ResponseModel } from '../../../models/response';
import { Company } from '../../../models/company';
import { User } from '../../../models/user';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { Location } from '@angular/common';
import { MessagesService } from '../../shared/components/messages/messages.component';
import { Load } from '../../../models/load';
import { Trip } from '../../../models/trip';
import { IAppState } from '../../../redux/app_state';
import { Store } from '@ngrx/store';
import { TripItemComponent } from '../components/trip-item/trip-item.component';
import { LoadItemComponent } from '../components/load-item/load-item.component';
import { MapComponent } from '../components/map/map.component';
import { LoadPointsComponent } from '../components/load-points/load-points.component';
import { LoadCreateComponent } from '../components/load-create/load-create.component';
import { LoadAssignComponent } from '../components/load-assign/load-assign.component';
import { DispatchService } from '../dispatch.service';
import { parse } from 'date-fns';

@Component({
  selector: 'dp-trip-details',
  templateUrl: './trip-details.component.html',
  styleUrls: ['./trip-details.component.scss']
})
export class TripDetailsComponent implements OnInit, OnDestroy {
  @ViewChild("map") mapElement: MapComponent;
  @ViewChild("tripDetails") tripDetails: TripItemComponent;
  @ViewChild("loadDetails") loadDetails: LoadItemComponent;
  @ViewChild("loadPointsComponent") loadPointsComponent: LoadPointsComponent;
  @ViewChild("createLoad") createLoadComponent: LoadCreateComponent;
  @ViewChild("assignLoad") assignLoadComponent: LoadAssignComponent;

  startingPointDate: Date;
  companyId: number;
  userId: number;
  subscriptions: Subscription[];
  tripId: number;
  tripItem: Trip = new Trip();

  selectedLoadFromGridId: number;

  newLoadItem: Load = new Load();
  loads: Load[] = [];
  loadItem: Load = new Load();

  constructor(
    private router: Router,
    private location: Location,
    private route: ActivatedRoute,
    private _store: Store<IAppState>,
    private _dispatchService: DispatchService,
    private _messagesService: MessagesService,
    private _confirmationService: ConfirmationService)
  {
    this.subscriptions = [];

    this.selectedLoadFromGridId = 0;
  }

  ngOnInit() {
    this.loadItem.Id = 0;
    this.route.params.subscribe((v: { id: string, loadId: string }) => {
      let tempId = parseInt(v.id);
      this.selectedLoadFromGridId = parseInt(v.loadId);
      this.tripId = isNaN(tempId) ? 0 : tempId;

      if (this.tripId > 0) {
        this.getTrip();
        this.getStartingPointDate();
      } else {
        this.mapElement.drawMarkers([]);
        this.router.routeReuseStrategy.shouldReuseRoute = function(){
          return false;
        };

        this.router.events.subscribe((evt) => {
          if (evt instanceof NavigationEnd) {
            // trick the Router into believing it's last link wasn't previously loaded
            this.router.navigated = false;
            // if you need to scroll back to top, here is the right place
            window.scrollTo(0, 0);
          }
        });
        this._dispatchService.getNextTripNumber()
          .subscribe((data: ResponseModel<string>) => {
            this.tripItem = new Trip();
            this.tripItem.Id = 0;
            this.tripItem.TripNumber = data.Result;
            this.tripItem.IsActive = true;
          });

      }
    });

    this.subscriptions.push(this._store.select('activeUser').subscribe((state: User) => {
      this.userId = state.Id;
    }));

    this.subscriptions.push(this._store.select('activeCompany').subscribe((cmp: Company)=>{
      if(this.companyId > 0 && this.companyId != cmp.Id) {
        this.router.navigate(['/trips']);
      }
      this.companyId = cmp.Id;
    }));
  }

  ngOnDestroy(){
    this.subscriptions.forEach((item: Subscription)=>{
      item.unsubscribe();
    });
  }

  getTrip() {
    if (this.tripId > 0) {
      this._dispatchService.getTripById(this.tripId)
        .subscribe((data: ResponseModel<Trip>) => {
          this.tripItem = data.Result;
          if (this.tripItem.DateFrom != null) {
            this.tripItem.DateFrom = parse(this.tripItem.DateFrom);
          }
          if (this.tripItem.DateTo != null) {
            this.tripItem.DateTo = parse(this.tripItem.DateTo);
          }
        });

      this.getLoads();
    }
  }

  getStartingPointDate() {
    this._dispatchService.getStartingLoadPointDate(this.tripId).subscribe((res: ResponseModel<Date>) => {
      this.startingPointDate = res.Result;
      if (this.startingPointDate != null) {
        this.startingPointDate = parse(this.startingPointDate);
      }
    });
  }

  getLoads() {
    this._dispatchService.getLoadsByTripId(this.tripId)
      .subscribe((data: ResponseModel<Load[]>) => {
        this.loads = data.Result;
        this.loads.map((item) => {
          if (item.DateFrom != null) {
            item.DateFrom = parse(item.DateFrom);
          }
          if (item.DateTo != null) {
            item.DateTo = parse(item.DateTo);
          }
          if (item.BookingDate != null) {
            item.BookingDate = parse(item.BookingDate);
          }
          return item;
        });
        if(this.selectedLoadFromGridId > 0) {
          this.loadItem = this.loads.find((o) => {
            return o.Id == this.selectedLoadFromGridId;
          });
        }

        if(this.loadItem.Id == 0){
          if (this.loads.length > 0) {
            this.loadItem = this.loads[0];
          }
        }
        //this.getLoadPoints();
      });
  }

  addLoad() {
    this.newLoadItem = new Load();
    this.newLoadItem.Id = 0;
    this.newLoadItem.TourId = this.tripId;
    this.newLoadItem.BrokerRating = 1;
    this.newLoadItem.IsActive = true;
    this.newLoadItem.DispatcherId = this.userId;
    this.newLoadItem.BookingDate = new Date();

    this.createLoadComponent.showDialog(this.newLoadItem);
  }

  onLoadCreated(load: Load){
    this.loadItem = load;
    let temp = [...this.loads];
    temp.push(this.loadItem);
    this.loads = temp;
    this._messagesService.success('', 'Load created.');

    this.loadPointsComponent.getLoadPoints();
  }

  onTripFinalized(){
    this.tripItem.IsFinished = true;
  }

  onTripReopened(){
    this.tripItem.IsFinished = false;
  }

  onTripSaved(trId: number){
    this.location.replaceState("/trips/" + trId);
    this.tripId = trId;
    this.loadItem = new Load();
    this.loadItem.Id = 0;
    this.getTrip();
    this.loadPointsComponent.getLoadPoints(true);
  }

  onLoadPointSelected(loadId: number){
    for(let item of this.loads){
      if(item.Id == loadId){
        this.loadItem = item;
        break;
      }
    }
  }

  onMapUpdate(items: LoadPoint[]) {
    this.mapElement.drawMarkers(items);
  }

  modifyLoad(id: number) {
    this.router.navigate([`/console/finance/invoices/${id}`])
  }

  deleteLoad(item: Load) {
    this._confirmationService.confirm({
      message: 'Are you sure that you want to remove load #' + item.LoadNumber + ' from this trip?',
      accept: () => {
        this._dispatchService.deleteLoad(item.Id)
          .subscribe((data: ResponseModel<boolean>) => {
            if (data.IsSuccess) {
              this._messagesService.success('', 'Load removed from trip.');
              this.getLoads();
              this.loadPointsComponent.getLoadPoints();
            }
          });
      }
    });
  }

  assignLoadToTrip(){
    this.assignLoadComponent.show(this.startingPointDate);
  }

  onLoadsToAssignSelected(loadIds: number[]){
    this._dispatchService.importLoads(loadIds, this.tripId)
      .subscribe((data: ResponseModel<boolean>) => {
        if (data.IsSuccess) {
          this.assignLoadComponent.hide();
          this.getLoads();
          this.loadPointsComponent.getLoadPoints(true);
        } else {
          this._messagesService.error('', data.Message);
        }
      });
  }

  onRouteDrawn(event: any) {
    this.loadItem.TotalDistanceMi = event;
  }

  back() {
    this.router.navigate(['/trips']);
  }

  canDeactivate() {
    if(!this.tripDetails.tripForm.submitted && this.tripDetails.tripForm.dirty && this.tripDetails.tripForm.touched){
      return window.confirm('You have unsaved changes. Continue?');
    }

    if(!this.loadDetails.editLoadForm.submitted && this.loadDetails.editLoadForm.dirty && this.loadDetails.editLoadForm.touched){
      return window.confirm('You have unsaved changes. Continue?');
    }

    /*if(!this.loadPointsComponent.editLoadPointForm.submitted && this.loadPointsComponent.editLoadPointForm.dirty && this.loadPointsComponent.editLoadPointForm.touched){
        return window.confirm('You have unsaved changes. Continue?');
    }*/


    return true;
  }
}
