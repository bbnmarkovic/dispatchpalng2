import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { Company } from '../../models/company';
import { HttpClient, HttpParams } from '@angular/common/http';
import { IAppState } from '../../redux/app_state';
import { ResponseModel } from '../../models/response';
import { Observable } from 'rxjs/Observable';
import { LoadPreview } from '../../models/load_preview';
import { Load } from '../../models/load';
import { LoadPoint } from '../../models/load_point';
import { environment } from '../../../environments/environment';
import { Filter } from '../../models/filter';
import { Trip } from '../../models/trip';
import { TripPreview } from '../../models/trip_preview';
import { DriverPreview } from '../../models/driver_preview';
import { LoadIssue } from '../../models/load_issue';

@Injectable()
export class DispatchService {
  companyId: number;

  constructor(private http: HttpClient, private _store: Store<IAppState>) {
    _store.select('activeCompany').subscribe((state: Company) => {
      this.companyId = state.Id;
    });
  }

  getLoadsByFilter(filter: Filter) {
    const url = `${environment.api}/loads`;

    let params: HttpParams = new HttpParams();
    params = params.append('offset', filter.Offset.toString());
    params = params.append('pageSize', filter.PageSize.toString());
    params = params.append('searchParam', filter.SearchParam);
    params = params.append('companyId', this.companyId.toString());

    return this.http.get<ResponseModel<LoadPreview[]>>(url, {
      params: params
    });
  }

  getUnassignedLoads(filter: Filter) {
    const url = `${environment.api}/loads/unassigned`;

    let params: HttpParams = new HttpParams();
    params = params.append('startDate', filter.StartDate.toDateString());
    params = params.append('offset', filter.Offset.toString());
    params = params.append('pageSize', filter.PageSize.toString());
    params = params.append('searchParam', filter.SearchParam);
    params = params.append('companyId', this.companyId.toString());

    return this.http.get<ResponseModel<LoadPreview[]>>(url, {
      params: params
    });
  }

  assignLoadToTrip(loadId: number, tripId: number) {
    const url = `${environment.api}/loads/assign`;

    let params: HttpParams = new HttpParams();
    params = params.append('loadId', loadId.toString());
    params = params.append('tripId', tripId.toString());

    return this.http.get<ResponseModel<boolean>>(url, {
      params: params
    });
  }

  getLoadById(id: number) {
    const url = `${environment.api}/loads/${id}`;

    return this.http.get<ResponseModel<Load>>(url);
  }

  getNextLoadNumber() {
    const url = `${environment.api}/loads/next_load_number`;

    return this.http.get<ResponseModel<string>>(url);
  }

  getLoadPointsByLoadId(id: number) {
    const url = `${environment.api}/loads/${id}/load_points`;

    return this.http.get<ResponseModel<LoadPoint[]>>(url);
  }

  saveLoad(load: Load) {
    const url = `${environment.api}/loads`;

    load.CompanyId = this.companyId;
    return this.http.post<ResponseModel<Load>>(url, load);
  }

  deleteLoad(id: number): Observable<ResponseModel<boolean>> {
    const url = `${environment.api}/loads/${id}/remove`;

    return this.http.get<ResponseModel<boolean>>(url);
  }

  saveLoadIssue(loadIssue: LoadIssue) {
    const url = `${environment.api}/loads/issues`;
    return this.http.post<ResponseModel<number>>(url, loadIssue);
  }

  toggleLoadIssueResolved(itemId: number) {
    const url = `${environment.api}/loads/issues/${itemId}/toggle`;
    return this.http.get(url);
  }

  // previewFile(id: number) {
  // return this.http.get(ConfigService.getApiEndpoint() + 'dispatch/files/preview/' + id, { responseType: ResponseContentType.Blob }).map(LoadsService.mapBlobData);
  // }

  getTripsByFilter(filter: Filter) {
    const url = `${environment.api}/trips`;

    let params: HttpParams = new HttpParams();
    params = params.append('offset', filter.Offset.toString());
    params = params.append('pageSize', filter.PageSize.toString());
    params = params.append('searchParam', filter.SearchParam);
    params = params.append('active', filter.ActiveStatus != null ? filter.ActiveStatus.toString() : "-1");
    params = params.append('companyId', this.companyId.toString());

    return this.http.get<ResponseModel<TripPreview[]>>(url, {
      params: params
    });
  }

  getNotFinishedTrips(filter: Filter) {
    const url = `${environment.api}/trips`;

    let params: HttpParams = new HttpParams();
    params = params.append('offset', filter.Offset.toString());
    params = params.append('pageSize', filter.PageSize.toString());
    params = params.append('searchParam', filter.SearchParam);
    params = params.append('active', "1");
    params = params.append('companyId', this.companyId.toString());
    params = params.append('includeFinished', 'false');

    return this.http.get<ResponseModel<TripPreview[]>>(url, {
      params: params
    });
  }

  /*getForPlanner(driverId: number, date: Date) {
      const url = `${environment.api}/trips/planner`;

      let params: HttpParams = new HttpParams();
      params = params.append('driverId', driverId.toString());
      params = params.append('date', date.toISOString());

      return this.http.get<ResponseModel<LoadPoint[]>>(url, {
          params: params
      });
  }*/

  // Trips

  getTripById(id: number) {
    const url = `${environment.api}/trips/${id}`;

    return this.http.get<ResponseModel<Trip>>(url);
  }

  getNextTripNumber() {
    const url = `${environment.api}/trips/next_trip_number`;

    return this.http.get<ResponseModel<string>>(url);
  }

  getLoadsByTripId(id: number) {
    const url = `${environment.api}/trips/${id}/loads`;

    return this.http.get<ResponseModel<Load[]>>(url);
  }

  getLoadPointsByTripId(id: number) {
    const url = `${environment.api}/trips/${id}/load_points`;

    return this.http.get<ResponseModel<LoadPoint[]>>(url);
  }

  getStartingLoadPointDate(tripId: number) {
    const url = `${environment.api}/trips/${tripId}/starting-point`;

    return this.http.get<ResponseModel<Date>>(url);
  }

  saveTrip(trip: Trip) {
    const url = `${environment.api}/trips`;

    trip.CompanyId = this.companyId;
    return this.http.post<ResponseModel<Trip>>(url, trip);
  }

  importLoads(loadIds: number[], tripId: number) {
    const url = `${environment.api}/trips/import-loads`;

    return this.http.post<ResponseModel<boolean>>(url, {LoadIds: loadIds, TripId: tripId});
  }

  finalizeTrip(id: number) {
    const url = `${environment.api}/trips/${id}/finalize`;

    return this.http.get<ResponseModel<boolean>>(url);
  }

  reopenTrip(id: number) {
    const url = `${environment.api}/trips/${id}/reopen`;

    return this.http.get<ResponseModel<boolean>>(url);
  }

  // Load points

  reorderLoadPoints(loadPointId: number, order: number) {
    const url = `${environment.api}/load_points/reorder`;

    let params: HttpParams = new HttpParams();
    params = params.append('loadPointId', loadPointId.toString());
    params = params.append('order', order.toString());

    return this.http.get<ResponseModel<LoadPoint[]>>(url, {
      params: params
    });
  }

  getLoadPointsForPlanner(driverId: number, fromDate: Date, toDate: Date) {
    const url = `${environment.api}/load_points/planner`;

    let params: HttpParams = new HttpParams();
    params = params.append('driverId', driverId.toString());
    params = params.append('fromDate', fromDate.toDateString());
    params = params.append('toDate', toDate.toDateString());

    return this.http.get<ResponseModel<LoadPoint[]>>(url, {
      params: params
    });
  }

  saveLoadPoint(loadPoint: LoadPoint) {
    const url = `${environment.api}/load_points`;

    return this.http.post<ResponseModel<LoadPoint>>(url, loadPoint);
  }

  saveStartingLoadPoint(loadPoint: LoadPoint) {
    const url = `${environment.api}/load_points/starting`;

    return this.http.post<ResponseModel<LoadPoint>>(url, loadPoint);
  }

  updateDistances(loadPoints: LoadPoint[]) {
    const url = `${environment.api}/load_points/update_distances`;

    return this.http.post<ResponseModel<boolean>>(url, loadPoints);
  }

  deleteLoadPoint(id: number) {
    const url = `${environment.api}/load_points/${id}/remove`;

    return this.http.get<ResponseModel<boolean>>(url);
  }

  getDriversByFilter(filter: Filter) {
    const url = `${environment.api}/drivers/preview`;

    let params: HttpParams = new HttpParams();
    params = params.append('offset', filter.Offset.toString());
    params = params.append('pageSize', filter.PageSize.toString());
    params = params.append('searchParam', filter.SearchParam);
    params = params.append('companyId', this.companyId.toString());

    return this.http.get<ResponseModel<DriverPreview[]>>(url, {
      params: params
    });
  }

  getBrokerRating(id: number) {
    const url = `${environment.api}/brokers/${id}/rating`;

    return this.http.get<ResponseModel<number>>(url);
  }
}
