import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Filter } from '../../../models/filter';
import { Subscription } from 'rxjs/Subscription';
import { ResponseModel } from '../../../models/response';
import { DataTable, LazyLoadEvent, SelectItem } from 'primeng/primeng';
import { TripPreview } from '../../../models/trip_preview';
import { IAppState } from '../../../redux/app_state';
import { Store } from '@ngrx/store';
import { saveFilterTrips } from '../../../redux/actions/filter.actions';
import { DispatchService } from '../dispatch.service';

@Component({
  selector: 'dp-trips',
  templateUrl: './trips.component.html',
  styleUrls: ['./trips.component.scss']
})
export class TripsComponent implements OnInit, OnDestroy{
  @ViewChild('dataTable') public dataTable: DataTable;
  filterActive: string = "All";
  emptyMsg: string = "Loading...";
  filter: Filter = new Filter();
  subscriptions: Subscription[];
  items: TripPreview[] = [];
  totalItems: number = 0;
  statuses: SelectItem[];

  constructor(
    private _store: Store<IAppState>,
    private _dispatchService: DispatchService)
  {
    this.subscriptions = [];
    this.statuses = [];
    this.statuses.push({label:'All', value:'-1'});
    this.statuses.push({label:'Active only', value:'1'});
    this.statuses.push({label:'Inactive only', value:'0'});
  }

  ngOnInit() {
    this.subscriptions.push(this._store.select('filterTrips').subscribe((state: Filter) => {
      this.filter = state;
      this.getData();
    }));

    this.subscriptions.push(this._store.select('activeCompany').subscribe(res=>{
      this.getData();
    }));
  }

  ngOnDestroy(){
    this.subscriptions.forEach((item: Subscription)=>{
      item.unsubscribe();
    });
  }

  getData(){
    this._dispatchService.getTripsByFilter(this.filter)
      .subscribe((data: ResponseModel<TripPreview[]>) => {
        if (data !== null) {
          this.totalItems = data.TotalItems;
          this.items = data.Result;

          if(this.totalItems == 0)
            this.emptyMsg = "No records...";
        }
      });
  }

  onActiveFilerChange($event: any){
    if($event.value == null){
      this.filterActive = "Show All Trips"
    } else if($event.value == true){
      this.filterActive = "Show Active Trips"
    } else if($event.value == false){
      this.filterActive = "Show Inactive Trips"
    }
  }

  ngAfterViewInit() {
    this.dataTable['first'] = this.filter.Offset;
  }

  onSubmitSearch() {
    this._store.dispatch(new saveFilterTrips(this.filter));
  }

  loadData(event: LazyLoadEvent) {
    this.filter.PageSize = event.rows;
    this.filter.Offset = event.first;

    this.onSubmitSearch();
  }

  resetFilter() {
    this.filter.Offset = 0;
    this.filter.SearchParam = "";

    this.onSubmitSearch();
  }

}
