import { AfterViewInit, Component, EventEmitter, forwardRef, OnInit, Output, ViewChild } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { DirectionsMapDirective } from '../../../shared/directives/map-directions/directions.directive';
import { LoadPoint } from '../../../../models/load_point';
import { MapsAPILoader } from '@agm/core';
import { Marker } from '../../../../models/marker';

declare var google: any;

@Component({
  selector: 'dp-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => MapComponent),
      multi: true
    }
  ]
})
export class MapComponent implements OnInit, AfterViewInit {

  @ViewChild(DirectionsMapDirective) vc: DirectionsMapDirective;
  @Output() onRouteDrawn = new EventEmitter<any>();

  origin: any;
  destination: any;
  mapBounds: any;
  markers: Marker[] = [];

  constructor(
    private _mapsAPILoader: MapsAPILoader) { }

  ngOnInit() {

  }

  ngAfterViewInit() {
  }

  public drawMarkers(points: LoadPoint[]) {
    this._mapsAPILoader.load().then(() => {
        this.mapBounds = new google.maps.LatLngBounds();
        this.markers.length = 0;
        for (let loadPoint of points) {
          let mark: Marker = new Marker();
          mark.draggable = false;
          mark.label = loadPoint.Location;
          mark.lat = loadPoint.Lat;
          mark.lng = loadPoint.Lng;

          this.markers.push(mark);

          this.mapBounds.extend(new google.maps.LatLng({ lat: mark.lat, lng: mark.lng }));
        }
        this.drawPath();
      }
    );
  }

  public drawPath() {
    if (this.vc != null && this.markers.length > 1) {
      this.vc.origin = new google.maps.LatLng(this.markers[0].lat, this.markers[0].lng);
      this.vc.destination = new google.maps.LatLng(this.markers[this.markers.length - 1].lat, this.markers[this.markers.length - 1].lng);

      this.vc.waypoints = [];
      for (let i = 1; i < this.markers.length - 1; i++) {
        this.vc.waypoints.push({
          location: new google.maps.LatLng(this.markers[i].lat, this.markers[i].lng),
          stopover: true
        });
      }

      if (this.vc.directionsDisplay === undefined) {
        this._mapsAPILoader.load().then(() => {
          this.vc.directionsDisplay = new google.maps.DirectionsRenderer;
        });
      }

      this.vc.updateDirections();

    }
  }

  onDistanceCalculated(event: any) {
    this.onRouteDrawn.emit(event);
  }
}
