import { Component, ElementRef, EventEmitter, Input, NgZone, OnInit, Output, ViewChild } from '@angular/core';
import { LoadPoint } from '../../../../models/load_point';
import { MapsAPILoader } from '@agm/core';
import { ConfirmationService } from 'primeng/components/common/api';
import { ResponseModel } from '../../../../models/response';
import { SelectItem } from 'primeng/primeng';
import { MessagesService } from '../../../shared/components/messages/messages.component';
import { DispatchService } from '../../dispatch.service';

declare var google: any;

@Component({
  selector: 'dp-load-point-details',
  templateUrl: './load-point-details.component.html',
  styleUrls: ['./load-point-details.component.scss']
})
export class LoadPointDetailsComponent implements OnInit {
  @ViewChild("googleSearch") public googleSearch: ElementRef;
  @Input() loadPoint: LoadPoint = new LoadPoint();
  @Output() onLoadPointCreated = new EventEmitter<LoadPoint>();
  @Output() onLoadPointEdited = new EventEmitter<LoadPoint>();

  loadPointTime: Date;

  display: boolean;
  googleInitiated: boolean = false;

  loadActionTypes: Array<SelectItem> = [];

  constructor(
    private _dispatchService: DispatchService,
    private _messagesService: MessagesService,
    private _mapsAPILoader: MapsAPILoader,
    private _confirmationService: ConfirmationService,
    private _ngZone: NgZone) {

    this.loadActionTypes.push({ label: 'Loading', value: 0 });
    this.loadActionTypes.push({ label: 'Unloading', value: 1 });
  }

  ngOnInit() {
  }

  show(lp: LoadPoint){
    this.loadPoint = lp;
    this.loadPointTime = this.loadPoint.DateLoad;
    this.display = true;
  }

  hide(){
    this.display = false;
  }

  onDialogShown(){
    if(!this.googleInitiated) {
      /*google.maps.event.addDomListener(this.googleSearch.nativeElement, 'keydown', function(e) {
          if (e.keyCode == 13 && $('.pac-container:visible').length) {
              e.preventDefault();
          }
      });*/

      this._mapsAPILoader.load().then(() => {
        this.googleInitiated = true;
        // this.googleSearch.nativeElement.focus();
        let autoComplete = new google.maps.places.Autocomplete(this.googleSearch.nativeElement, {
          types: ["address"]
        });

        autoComplete.addListener("place_changed", () => {
          this._ngZone.run(() => {
            let place: any = autoComplete.getPlace();

            if (place.geometry === undefined || place.geometry === null) {
              return;
            }

            let city = "";
            let street = "";
            let zipCode = "";
            let state = "";
            let country = "";

            for (let i = 0; i < place.address_components.length; i++){
              if (place.address_components[i].types[0] == "route"){
                street = place.address_components[i].long_name;
              } else if (place.address_components[i].types[0] == "locality"){
                city = place.address_components[i].long_name;
              } else if (place.address_components[i].types[0] == "administrative_area_level_1"){
                state = place.address_components[i].short_name;
              } else if (place.address_components[i].types[0] == "country"){
                country = place.address_components[i].long_name;
              } else if (place.address_components[i].types[0] == "postal_code"){
                zipCode = place.address_components[i].long_name;
              }
            }

            this.loadPoint.Location = place.formatted_address;
            this.loadPoint.Country = country;
            this.loadPoint.State = state;
            this.loadPoint.City = city;
            this.loadPoint.Address = street;
            this.loadPoint.ZipCode = zipCode;
            this.loadPoint.Lat = place.geometry.location.lat();
            this.loadPoint.Lng = place.geometry.location.lng();
          });
        });
      });
    } else {
      // this.googleSearch.nativeElement.focus();
    }
  }

  onSubmitLoadPoint(){
    if(this.loadPointTime != null){
      this.loadPoint.DateLoad.setHours(this.loadPointTime.getHours(), this.loadPointTime.getMinutes(), this.loadPointTime.getSeconds());
    } else {
      this.loadPoint.DateLoad.setHours(0,0,0);
    }
    const isNew = this.loadPoint.Id == 0;
    if(this.loadPoint.OrderNumber == 0){
      this._dispatchService.saveStartingLoadPoint(this.loadPoint)
        .subscribe((data: ResponseModel<LoadPoint>) => {
          if(data.IsSuccess) {
            this.loadPoint = data.Result;
            this.hide();
            if (isNew) {
              this.onLoadPointCreated.emit(this.loadPoint);
            } else {
              this.onLoadPointEdited.emit(this.loadPoint);
            }
          } else {
            this._messagesService.error("", data.Message);
          }

        });
    } else {
      this._dispatchService.saveLoadPoint(this.loadPoint)
        .subscribe((data: ResponseModel<LoadPoint>) => {
          if(data.IsSuccess) {
            this.loadPoint = data.Result;
            this.hide();
            if(isNew) {
              this.onLoadPointCreated.emit(this.loadPoint);
            } else {
              this.onLoadPointEdited.emit(this.loadPoint);
            }
          } else {
            this._messagesService.error("", data.Message);
          }
        });
    }
  }
}
