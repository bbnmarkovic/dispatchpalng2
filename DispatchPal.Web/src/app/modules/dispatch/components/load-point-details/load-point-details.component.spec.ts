import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoadPointDetailsComponent } from './load-point-details.component';

describe('LoadPointDetailsComponent', () => {
  let component: LoadPointDetailsComponent;
  let fixture: ComponentFixture<LoadPointDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoadPointDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoadPointDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
