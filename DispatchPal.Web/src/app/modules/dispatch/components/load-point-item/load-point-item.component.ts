import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { LoadPoint } from '../../../../models/load_point';
import { ConfirmationService } from 'primeng/components/common/api';
import { ResponseModel } from '../../../../models/response';
import { DispatchService } from '../../dispatch.service';

@Component({
  selector: 'dp-load-point-item',
  templateUrl: './load-point-item.component.html',
  styleUrls: ['./load-point-item.component.scss']
})
export class LoadPointItemComponent implements OnInit {
  // @ViewChild("loadPointDetails") loadPointDetails: LoadPointDetailsComponent;
  @Output() onEditLoadPoint = new EventEmitter<LoadPoint>();
  @Output() onLoadPointDeleted = new EventEmitter();
  @Input() tripId: number;
  @Input() loadId: number;
  @Input() loadPoint: LoadPoint;
  @Input() IsFinished: boolean;
  @Input() letter: string;

  showDetails: boolean = false;

  constructor(
    private _dispatchService: DispatchService,
    private _confirmationService: ConfirmationService) {
  }

  ngOnInit() {
  }

  editLoadPoint(){
    this.onEditLoadPoint.emit(this.loadPoint);
  }

  deleteLoadPoint() {
    this._confirmationService.confirm({
      message: 'Are you sure that you want to delete load point (' + this.loadPoint.Location + ')?',
      accept: () => {
        this._dispatchService.deleteLoadPoint(this.loadPoint.Id)
          .subscribe((data: ResponseModel<boolean>) => {
            if (data.IsSuccess) {
              this.onLoadPointDeleted.emit();
            }
          });
      }
    });
  }

  toggleDetails(){
    this.showDetails = !this.showDetails;
  }

}
