import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoadPointItemComponent } from './load-point-item.component';

describe('LoadPointItemComponent', () => {
  let component: LoadPointItemComponent;
  let fixture: ComponentFixture<LoadPointItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoadPointItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoadPointItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
