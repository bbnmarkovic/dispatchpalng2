import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TripSelectComponent } from './trip-select.component';

describe('TripSelectComponent', () => {
  let component: TripSelectComponent;
  let fixture: ComponentFixture<TripSelectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TripSelectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TripSelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
