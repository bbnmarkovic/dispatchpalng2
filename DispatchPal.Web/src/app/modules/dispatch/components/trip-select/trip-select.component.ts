import { Component, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { Filter } from '../../../../models/filter';
import { ResponseModel } from '../../../../models/response';
import { DataTable, LazyLoadEvent } from 'primeng/primeng';
import { TripPreview } from '../../../../models/trip_preview';
import { DispatchService } from '../../dispatch.service';

@Component({
  selector: 'dp-trip-select',
  templateUrl: './trip-select.component.html',
  styleUrls: ['./trip-select.component.scss']
})
export class TripSelectComponent implements OnInit {
  @ViewChild('dataTable') public dataTable: DataTable;
  @Output() onTripSelected = new EventEmitter<number>();

  constructor(
    private _dispatchService: DispatchService)
  {
    this.onSubmitSearch();
  }

  emptyMsg: string = "Loading...";
  filter: Filter = new Filter();

  tripItem: TripPreview = new TripPreview();

  items: TripPreview[] = [];
  totalItems: number = 0;

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.dataTable['first'] = this.filter.Offset;
  }

  onSubmitSearch() {
    this._dispatchService.getNotFinishedTrips(this.filter)
      .subscribe((data: ResponseModel<TripPreview[]>) => {
        if (data !== null) {
          this.totalItems = data.TotalItems;
          this.items = data.Result;
        }
      });
  }

  loadData(event: LazyLoadEvent) {
    this.filter.PageSize = event.rows;
    this.filter.Offset = event.first;

    this.onSubmitSearch();
  }

  resetFilter() {
    this.filter.Offset = 0;
    this.filter.SearchParam = "";

    this.onSubmitSearch();
  }

  onSelectTrip(event: any) {
    this.onTripSelected.emit(event.data.Id);
  }

}
