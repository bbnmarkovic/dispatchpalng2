import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { LoadPoint } from '../../../../models/load_point';
import { Plan } from '../../../../models/plan';
import { isUndefined } from "util";
import { addDays, getDaysInMonth } from "date-fns";
import { ResponseModel } from '../../../../models/response';
import { DriverPreview } from '../../../../models/driver_preview';
import { DispatchService } from '../../dispatch.service';

@Component({
  selector: 'dp-planner-item',
  templateUrl: './planner-item.component.html',
  styleUrls: ['./planner-item.component.scss']
})
export class PlannerItemComponent implements OnInit, OnChanges {


  @Input() driver: DriverPreview;
  @Input() date: Date;

  points: Plan[] = [];
  startDate: Date;
  endDate: Date;

  constructor(private _dispatchService: DispatchService) {

  }

  ngOnInit() {
    this.loadData();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if(!isUndefined(changes["date"].previousValue)) {
      this.loadData();
    }
  }

  loadData() {
    let dt: Date = this.date;
    dt.setDate(1);
    this.points.length = 0;
    let lastDay: number = getDaysInMonth(dt);
    for (let i = 0; i < lastDay; i++) {
      let pl: Plan = new Plan();
      pl.LoadPoints = [];
      pl.DateValue  = addDays(dt, i);
      this.points.push(pl);
    }

    this.startDate = new Date(this.date);
    this.startDate.setDate(1);
    this.endDate = new Date(this.date);
    this.endDate.setDate(lastDay);

    this._dispatchService.getLoadPointsForPlanner(this.driver.Id, this.startDate, this.endDate).subscribe((res: ResponseModel<LoadPoint[]>)=>{
      for (let plan of this.points) {
        for (let lp of res.Result) {
          if(plan.DateValue.getDate() == lp.DateLoad.getDate() &&
            plan.DateValue.getMonth() == lp.DateLoad.getMonth() &&
            plan.DateValue.getFullYear() == lp.DateLoad.getFullYear()){
            plan.LoadPoints.push(lp);
          }
        }
      }
    });
  }
}
