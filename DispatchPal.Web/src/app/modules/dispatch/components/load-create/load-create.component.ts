import { Component, ElementRef, EventEmitter, NgZone, OnInit, Output, ViewChild } from '@angular/core';
import { LoadPoint } from '../../../../models/load_point';
import { Load } from '../../../../models/load';
import { MapsAPILoader } from '@agm/core';
import { ResponseModel } from '../../../../models/response';
import { MessagesService } from '../../../shared/components/messages/messages.component';
import { DispatchService } from '../../dispatch.service';

declare var google: any;

@Component({
  selector: 'dp-load-create',
  templateUrl: './load-create.component.html',
  styleUrls: ['./load-create.component.scss']
})
export class LoadCreateComponent implements OnInit {
  @ViewChild("startLoadPointSearch") public startLoadPointSearch: ElementRef;
  @ViewChild("endLoadPointSearch") public endLoadPointSearch: ElementRef;
  @Output() onLoadCreated = new EventEmitter<any>();
  @Output() onCancelCreateLoad = new EventEmitter();

  display: boolean;

  startLoadPoint: LoadPoint = new LoadPoint();
  endLoadPoint: LoadPoint = new LoadPoint();
  startLoadPointTime: Date;
  endLoadPointTime: Date;

  newLoad: Load = new Load();

  constructor(
    private _dispatchService: DispatchService,
    private _messagesService: MessagesService,
    private _mapsAPILoader: MapsAPILoader,
    private _ngZone: NgZone) {
  }

  ngOnInit() {

  }

  setupGoogleAutoComplete() {
    this._mapsAPILoader.load().then(() => {
      let autoCompleteStart = new google.maps.places.Autocomplete(this.startLoadPointSearch.nativeElement, {
        types: ["address"]
      });
      let autoCompleteEnd = new google.maps.places.Autocomplete(this.endLoadPointSearch.nativeElement, {
        types: ["address"]
      });

      autoCompleteStart.addListener("place_changed", () => {
        this._ngZone.run(() => {
          let place: any = autoCompleteStart.getPlace();

          if (place.geometry === undefined || place.geometry === null) {
            return;
          }

          let city = "";
          let street = "";
          let zipCode = "";
          let state = "";
          let country = "";

          for (let i = 0; i < place.address_components.length; i++){
            if(place.address_components[i].types[0] == "route"){
              street = place.address_components[i].long_name;
            } else if(place.address_components[i].types[0] == "locality"){
              city = place.address_components[i].long_name;
            } else if(place.address_components[i].types[0] == "country"){
              country = place.address_components[i].long_name;
            } else if (place.address_components[i].types[0] == "administrative_area_level_1"){
              state = place.address_components[i].short_name;
            } else if(place.address_components[i].types[0] == "postal_code"){
              zipCode = place.address_components[i].long_name;
            }
          }

          this.startLoadPoint.Location = place.formatted_address;
          this.startLoadPoint.Country = country;
          this.startLoadPoint.State = state;
          this.startLoadPoint.City = city;
          this.startLoadPoint.Address = street;
          this.startLoadPoint.ZipCode = zipCode;
          this.startLoadPoint.Lat = place.geometry.location.lat();
          this.startLoadPoint.Lng = place.geometry.location.lng();
        });
      });

      autoCompleteEnd.addListener("place_changed", () => {
        this._ngZone.run(() => {
          let place: any = autoCompleteEnd.getPlace();

          if (place.geometry === undefined || place.geometry === null) {
            return;
          }

          let city = "";
          let street = "";
          let zipCode = "";
          let state = "";
          let country = "";

          for (let i = 0; i < place.address_components.length; i++){
            if(place.address_components[i].types[0] == "route"){
              street = place.address_components[i].long_name;
            } else if(place.address_components[i].types[0] == "locality"){
              city = place.address_components[i].long_name;
            } else if(place.address_components[i].types[0] == "country"){
              country = place.address_components[i].long_name;
            } else if (place.address_components[i].types[0] == "administrative_area_level_1"){
              state = place.address_components[i].short_name;
            } else if(place.address_components[i].types[0] == "postal_code"){
              zipCode = place.address_components[i].long_name;
            }
          }

          this.endLoadPoint.Location = place.formatted_address;
          this.endLoadPoint.Country = country;
          this.endLoadPoint.State = state;
          this.endLoadPoint.City = city;
          this.endLoadPoint.Address = street;
          this.endLoadPoint.ZipCode = zipCode;
          this.endLoadPoint.Lat = place.geometry.location.lat();
          this.endLoadPoint.Lng = place.geometry.location.lng();
        });
      });
    });
  }

  onSubmitCreateLoad() {
    if (this.startLoadPoint.Lat != undefined && this.endLoadPoint.Lat != undefined) {
      if(this.startLoadPointTime != null){
        this.startLoadPoint.DateLoad.setHours(this.startLoadPointTime.getHours(), this.startLoadPointTime.getMinutes(), this.startLoadPointTime.getSeconds());
      } else {
        this.startLoadPoint.DateLoad.setHours(0,0,0);
      }

      if(this.endLoadPointTime != null){
        this.endLoadPoint.DateLoad.setHours(this.endLoadPointTime.getHours(), this.endLoadPointTime.getMinutes(), this.endLoadPointTime.getSeconds());
      } else {
        this.endLoadPoint.DateLoad.setHours(0,0,0);
      }

      this.newLoad.LoadPoints.push(this.startLoadPoint);
      this.newLoad.LoadPoints.push(this.endLoadPoint);
      this._dispatchService.saveLoad(this.newLoad)
        .subscribe((data: ResponseModel<Load>) => {
          if(data.IsSuccess) {
            this._messagesService.success('', 'Load created.');
            this.display = false;

            this.onLoadCreated.emit(data.Result);
          } else {
            this._messagesService.error("", data.Message);
          }
        });
    }
  }

  cancelCreateLoad() {
    this.display = false;
    this.onCancelCreateLoad.emit();
  }

  public showDialog(load: Load) {
    this.startLoadPoint.Location = "";
    this.startLoadPoint.DateLoad = new Date();
    this.startLoadPointTime = null;

    this.endLoadPoint.Location = "";
    this.endLoadPoint.DateLoad = new Date();
    this.endLoadPointTime = null;

    this.setupGoogleAutoComplete();
    this.display = true;
    this.newLoad = load;
  }

  onStartLoadDateChanged(){
    if(this.startLoadPoint.DateLoad > this.endLoadPoint.DateLoad){
      this.endLoadPoint.DateLoad = this.startLoadPoint.DateLoad;
      this.endLoadPointTime = this.startLoadPointTime;
    }
  }

  public hideDialog() {
    this.display = false;
  }
}
