import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { Load } from '../../../../models/load';
import { ResponseModel } from '../../../../models/response';
import { Client } from '../../../../models/client';
import { NgForm } from '@angular/forms';
import { User } from '../../../../models/user';
import { IAppState } from '../../../../redux/app_state';
import { MessagesService } from '../../../shared/components/messages/messages.component';
import { Store } from '@ngrx/store';
import { Broker } from '../../../../models/broker';
import { DispatchService } from '../../dispatch.service';
import { LoadIssueDetailsComponent } from '../load-issue-details/load-issue-details.component';
import { LoadIssue } from '../../../../models/load_issue';

@Component({
  selector: 'dp-load-item',
  templateUrl: './load-item.component.html',
  styleUrls: ['./load-item.component.scss']
})
export class LoadItemComponent implements OnInit {
  @ViewChild("loadIssueDetails") loadIssueDetails: LoadIssueDetailsComponent;
  @ViewChild("editLoadForm") editLoadForm: NgForm;
  @Input() loadItem: Load;
  @Input() IsFinished: boolean;
  @Output() onLoadCreated = new EventEmitter<number>();

  oldPartnerId: number;

  userId: number;
  shipper: Client;
  consignee: Client;
  broker: Broker;

  constructor(private _dispatchService: DispatchService,
              private _messagesService: MessagesService,
              private _store: Store<IAppState>) {

    this.shipper = new Client();
    this.consignee = new Client();
    this.broker = new Broker();
    this.userId = 0;
  }

  ngOnInit() {
    this._store.select('activeUser').subscribe((state: User) => {
      this.userId = state.Id;
    });
  }

  onShipperSelected(item: Client) {
    this.shipper = item;
  }

  onConsigneeSelected(item: Client) {
    this.consignee = item;
  }

  onBrokerageSelected(item: Client) {
    if (item == null || (this.oldPartnerId != null && item.Id != this.oldPartnerId)) {
      this.loadItem.BrokerId = null;
      this.broker = null;
    }
    else {
      this.oldPartnerId = item.Id;
    }
  }

  onBrokerSelected(item: Broker) {
    this.broker = item;
    if (item != null) {
      this.loadItem.BrokerageId = item.PartnerId;
    }
  }

  onBrokerCreated(item: Broker) {
    this.loadItem.BrokerId = item.Id;
    this.broker = item;
  }

  onSubmitEditLoad() {
    this.loadItem.PartnerConsigneeContact = this.consignee.ContactName;
    this.loadItem.PartnerConsigneePhone = this.consignee.PhoneNumber;
    this.loadItem.PartnerShipperContact = this.shipper.ContactName;
    this.loadItem.PartnerShipperPhone = this.shipper.PhoneNumber;

    if (this.broker != null) {
      this.loadItem.BrokerEmail = this.broker.Email;
      this.loadItem.BrokerFax = this.broker.Fax;
      this.loadItem.BrokerPhoneNumber = this.broker.PhoneNumber;
    }

    this._dispatchService.saveLoad(this.loadItem)
      .subscribe((data: ResponseModel<Load>) => {
        if (this.loadItem.Id == 0) {
          this.loadItem.Id = data.Result.Id;
          this.loadItem.LoadNumber = data.Result.LoadNumber;
          this.onLoadCreated.emit(data.Result.Id);
        } else {
          this.loadItem = data.Result;
          if (this.broker != null && this.broker.Id > 0) {
            this._dispatchService.getBrokerRating(this.broker.Id)
              .subscribe((data: ResponseModel<number>) => {
                this.broker.Rating = data.Result;
              });
          }
        }

        this._messagesService.success('', 'Load details saved.');
      });
  }

  createLoadIssue() {
    this.loadIssueDetails.showDialog(this.loadItem.Id);
  }

  OnLoadIssueSaved(loadIssue: LoadIssue) {
    this.loadItem.LoadIssues = [...this.loadItem.LoadIssues, loadIssue];
  }
}
