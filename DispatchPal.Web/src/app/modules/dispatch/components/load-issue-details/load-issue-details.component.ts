import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { LoadIssue } from '../../../../models/load_issue';
import { DispatchService } from '../../dispatch.service';
import { ResponseModel } from '../../../../models/response';

@Component({
  selector: 'dp-load-issue-details',
  templateUrl: './load-issue-details.component.html',
  styleUrls: ['./load-issue-details.component.scss']
})
export class LoadIssueDetailsComponent implements OnInit {
  @Output() OnLoadIssueSaved = new EventEmitter<LoadIssue>();
  display: boolean;
  loadIssue: LoadIssue;

  constructor(private dispatchService: DispatchService) { }

  ngOnInit() {
    this.loadIssue = new LoadIssue();
  }

  showDialog(loadId: number) {
    this.loadIssue = new LoadIssue();
    this.loadIssue.LoadId = loadId;
    this.display = true;
  }

  onSubmit() {
    this.dispatchService.saveLoadIssue(this.loadIssue).subscribe((res: ResponseModel<number>) => {
      if (res.IsSuccess) {
        this.display = false;
        this.loadIssue.Id = res.Result;
        this.OnLoadIssueSaved.emit(this.loadIssue);
      }
    });
  }

}
