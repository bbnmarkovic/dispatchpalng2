import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoadIssueDetailsComponent } from './load-issue-details.component';

describe('LoadIssueDetailsComponent', () => {
  let component: LoadIssueDetailsComponent;
  let fixture: ComponentFixture<LoadIssueDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoadIssueDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoadIssueDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
