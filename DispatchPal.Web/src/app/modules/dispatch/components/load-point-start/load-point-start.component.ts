import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { LoadPoint } from '../../../../models/load_point';
import { MessagesService } from '../../../shared/components/messages/messages.component';
import { ConfirmationService } from 'primeng/components/common/api';
import { ResponseModel } from '../../../../models/response';
import { DispatchService } from '../../dispatch.service';

@Component({
  selector: 'dp-load-point-start',
  templateUrl: './load-point-start.component.html',
  styleUrls: ['./load-point-start.component.scss']
})
export class LoadPointStartComponent implements OnInit {
  @Output() onStartingLoadPointSaved = new EventEmitter<LoadPoint>();
  loadPoint: LoadPoint;

  constructor(
    private _dispatchService: DispatchService,
    private _messagesService: MessagesService,
    private _confirmationService: ConfirmationService) {

  }

  ngOnInit() {

  }

  setLoadPoint(lp: LoadPoint){
    this.loadPoint = lp;
  }

  editLoadPoint(){

  }

  submitEditLoadPoint() {
    this._dispatchService.saveLoadPoint(this.loadPoint)
      .subscribe((data: ResponseModel<LoadPoint>) => {
        if (this.loadPoint.Id == 0) {
          this.loadPoint.Id = data.Result.Id;
        }

        this.onStartingLoadPointSaved.emit(this.loadPoint);
        this._messagesService.success('', 'Starting load point details saved.');
      });
  }

  deleteLoadPoint(item: LoadPoint) {
    this._confirmationService.confirm({
      message: 'Are you sure that you want to delete load point (' + item.Location + ')?',
      accept: () => {
        this._dispatchService.deleteLoadPoint(item.Id)
          .subscribe((data: ResponseModel<boolean>) => {
            if (data.IsSuccess) {
              this._messagesService.success('', 'Load point deleted.');
              this.loadPoint = null;
            }
          });
      }
    });
  }
}
