import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoadPointStartComponent } from './load-point-start.component';

describe('LoadPointStartComponent', () => {
  let component: LoadPointStartComponent;
  let fixture: ComponentFixture<LoadPointStartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoadPointStartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoadPointStartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
