import { Component, Input, OnInit } from '@angular/core';
import { LoadIssue } from '../../../../models/load_issue';
import { ResponseModel } from '../../../../models/response';
import { DispatchService } from '../../dispatch.service';

@Component({
  selector: 'dp-load-issue',
  templateUrl: './load-issue.component.html',
  styleUrls: ['./load-issue.component.scss']
})
export class LoadIssueComponent implements OnInit {
  @Input() loadIssue: LoadIssue;

  constructor(
    private _dispatchService: DispatchService) {
  }

  ngOnInit() {
  }

  toggleResolved(){
    this._dispatchService.toggleLoadIssueResolved(this.loadIssue.Id).subscribe((res: ResponseModel<boolean>) => {
      if(res.IsSuccess) {

      }
    });
  }

}
