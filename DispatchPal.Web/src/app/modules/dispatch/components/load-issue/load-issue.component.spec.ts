import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoadIssueComponent } from './load-issue.component';

describe('LoadIssueComponent', () => {
  let component: LoadIssueComponent;
  let fixture: ComponentFixture<LoadIssueComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoadIssueComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoadIssueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
