import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { Trip } from '../../../../models/trip';
import { ConfirmationService } from 'primeng/components/common/api';
import { ResponseModel } from '../../../../models/response';
import { Driver } from '../../../../models/driver';
import { NgForm } from '@angular/forms';
import { User } from '../../../../models/user';
import { IAppState } from '../../../../redux/app_state';
import { SelectItem } from 'primeng/primeng';
import { MessagesService } from '../../../shared/components/messages/messages.component';
import { Store } from '@ngrx/store';
import { Router } from '@angular/router';
import { DispatchService } from '../../dispatch.service';

@Component({
  selector: 'dp-trip-item',
  templateUrl: './trip-item.component.html',
  styleUrls: ['./trip-item.component.scss']
})
export class TripItemComponent implements OnInit {
  @ViewChild("tripForm") tripForm: NgForm;
  @Input() tripItem: Trip;
  @Input() loadId: number;
  @Input() showCancelButton: boolean;
  @Output() onTripCancel = new EventEmitter<number>();
  @Output() onTripSaved = new EventEmitter<number>();
  @Output() onTripFinalized = new EventEmitter<number>();
  @Output() onTripReopened = new EventEmitter<number>();

  payTypes: Array<SelectItem> = [];
  userId: number;

  isTeam: boolean;
  driverA: Driver;
  driverB: Driver;

  constructor(
    private _store: Store<IAppState>,
    private _dispatchService: DispatchService,
    private _messagesService: MessagesService,
    private router: Router,
    private _confirmationService: ConfirmationService) {

    this.userId = 0;
    this.payTypes.push({ label: 'Per Mile', value: 0 });
    this.payTypes.push({ label: 'Percentage', value: 1 });
    this.payTypes.push({ label: 'Per Hour', value: 2 });
  }

  ngOnInit() {
    this._store.select('activeUser').subscribe((state: User) => {
      this.userId = state.Id;
    });
  }

  finalizeTrip() {
    this._confirmationService.confirm({
      message: 'Are you sure you would like to finalize this trip? If you click YES, you will not be able to add or remove dispatch to and from this trip.',
      accept: () => {
        this._dispatchService.finalizeTrip(this.tripItem.Id)
          .subscribe((data: ResponseModel<boolean>) => {
            if (data.IsSuccess) {
              this._messagesService.success('', 'Trip finalized!');
              this.tripItem.IsFinished = true;

              this.onTripFinalized.emit();
            }
          });
      }
    });
  }

  reopenTrip() {
    this._confirmationService.confirm({
      message: 'Are you sure you would like to reopen this trip?',
      accept: () => {
        this._dispatchService.reopenTrip(this.tripItem.Id)
          .subscribe((data: ResponseModel<boolean>) => {
            if (data.IsSuccess) {
              this._messagesService.success('', 'Trip reopened!');
              this.tripItem.IsFinished = false;

              this.onTripReopened.emit();
            }
          });
      }
    });
  }

  onSubmitTrip() {
    if(!this.isTeam){
      this.tripItem.PayTypeDriverB = 0;
      this.tripItem.PayRateBobtailDriverB = 0;
      this.tripItem.PayRateEmptyDriverB = 0;
      this.tripItem.PayRateFullDriverB = 0;
    }
    this._dispatchService.saveTrip(this.tripItem)
      .subscribe((data: ResponseModel<Trip>) => {
        if (this.tripItem.Id == 0) {
          this.tripItem.Id = data.Result.Id;
          this.tripItem.TripNumber = data.Result.TripNumber;
          this.onTripSaved.emit(this.tripItem.Id);
          //this.location.replaceState("/trips/" + this.tripItem.Id);
        } else {
          this.tripItem = data.Result;
        }
        this._messagesService.success('', 'Trip details saved.');
      });
  }

  saveAndFinalizeTrip(){
    if(!this.isTeam){
      this.tripItem.PayTypeDriverB = 0;
      this.tripItem.PayRateBobtailDriverB = 0;
      this.tripItem.PayRateEmptyDriverB = 0;
      this.tripItem.PayRateFullDriverB = 0;
    }
    this._dispatchService.saveTrip(this.tripItem)
      .subscribe((data: ResponseModel<Trip>) => {
        this._dispatchService.assignLoadToTrip(this.loadId, data.Result.Id).subscribe((res) => {
          if(res.IsSuccess) {
            this._dispatchService.finalizeTrip(data.Result.Id)
              .subscribe((data: ResponseModel<boolean>) => {
                if (data.IsSuccess) {
                  this._messagesService.success('', 'Trip finalized!');
                  this.router.navigate(['/trips']);
                } else {
                  this._messagesService.error("", data.Message);
                }
              });
          }
        });
      });
  }

  onDriverASelected(dr: Driver){
    this.driverA = dr;
    this.tripItem.PayTypeDriverA = dr.RateType;
    this.tripItem.PayRateFullDriverA = this.isTeam ? dr.TeamRateAmountLoaded : dr.RateAmountLoaded;
    this.tripItem.PayRateEmptyDriverA = this.isTeam ? dr.TeamRateAmountEmpty : dr.RateAmountEmpty;
    this.tripItem.PayRateBobtailDriverA = this.isTeam ? dr.TeamRateAmountBobtail : dr.RateAmountBobtail;
  }

  onDriverBSelected(dr: Driver){
    this.driverB = dr;
    this.tripItem.PayTypeDriverB = dr.RateType;
    this.tripItem.PayRateFullDriverB = this.isTeam ? dr.TeamRateAmountLoaded : dr.RateAmountLoaded;
    this.tripItem.PayRateEmptyDriverB = this.isTeam ? dr.TeamRateAmountEmpty : dr.RateAmountEmpty;
    this.tripItem.PayRateBobtailDriverB = this.isTeam ? dr.TeamRateAmountBobtail : dr.RateAmountBobtail;
  }

  onIsTeamChanged(event: boolean){
    if (this.driverA != null) {
      this.tripItem.PayRateFullDriverA = this.isTeam ? this.driverA.TeamRateAmountLoaded : this.driverA.RateAmountLoaded;
      this.tripItem.PayRateEmptyDriverA = this.isTeam ? this.driverA.TeamRateAmountEmpty : this.driverA.RateAmountEmpty;
      this.tripItem.PayRateBobtailDriverA = this.isTeam ? this.driverA.TeamRateAmountBobtail : this.driverA.RateAmountBobtail;
    }

    if (this.driverB != null) {
      this.tripItem.PayRateFullDriverB = this.isTeam ? this.driverB.TeamRateAmountLoaded : this.driverB.RateAmountLoaded;
      this.tripItem.PayRateEmptyDriverB = this.isTeam ? this.driverB.TeamRateAmountEmpty : this.driverB.RateAmountEmpty;
      this.tripItem.PayRateBobtailDriverB = this.isTeam ? this.driverB.TeamRateAmountBobtail : this.driverB.RateAmountBobtail;
    }
  }

  cancel(){
    this.onTripCancel.emit();
  }
}
