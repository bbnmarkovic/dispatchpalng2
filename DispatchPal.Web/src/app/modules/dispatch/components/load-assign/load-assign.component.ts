import { Component, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { Filter } from '../../../../models/filter';
import { ResponseModel } from '../../../../models/response';
import { DataTable, LazyLoadEvent } from 'primeng/primeng';
import { LoadPreview } from '../../../../models/load_preview';
import { DispatchService } from '../../dispatch.service';
import { isNullOrUndefined } from 'util';

@Component({
  selector: 'dp-load-assign',
  templateUrl: './load-assign.component.html',
  styleUrls: ['./load-assign.component.scss']
})
export class LoadAssignComponent implements OnInit {
  @ViewChild('dataTable') public dataTable: DataTable;
  @Output() onLoadsToAssignSelected = new EventEmitter<number[]>();

  emptyMsg: string = "Loading...";
  filter: Filter = new Filter();

  loadItems: LoadPreview[] = [];

  items: LoadPreview[] = [];
  totalItems: number = 0;
  display: boolean = false;

  constructor(
    private _dispatchService: DispatchService)
  {

  }

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.dataTable['first'] = this.filter.Offset;
  }

  onSubmitSearch() {
    this._dispatchService.getUnassignedLoads(this.filter)
      .subscribe((data: ResponseModel<LoadPreview[]>) => {
        if (data !== null) {
          this.totalItems = data.TotalItems;
          this.items = data.Result;
        }
      });
  }

  loadData(event: LazyLoadEvent) {
    this.filter.PageSize = event.rows;
    this.filter.Offset = event.first;

    this.onSubmitSearch();
  }

  resetFilter() {
    this.filter.Offset = 0;
    this.filter.SearchParam = "";

    this.onSubmitSearch();
  }

  assign(){
    let selection: number[] = [];
    for (let item of this.loadItems)
      selection.push(item.Id);
    this.onLoadsToAssignSelected.emit(selection);
  }

  /*onSelectLoad(event: any) {
      this.onLoadToAssignSelected.emit(event.data.Id);
  }*/

  public show(startingDate: Date) {
    this.filter.StartDate = startingDate;
    this.display = true;

    this.onSubmitSearch();
  }

  public hide() {
    this.display = false;
  }

  isLoadSelected(id: number) {
    return !isNullOrUndefined(this.loadItems.find((item) => item.Id == id));
  }
}
