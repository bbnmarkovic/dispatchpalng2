import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoadAssignComponent } from './load-assign.component';

describe('LoadAssignComponent', () => {
  let component: LoadAssignComponent;
  let fixture: ComponentFixture<LoadAssignComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoadAssignComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoadAssignComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
