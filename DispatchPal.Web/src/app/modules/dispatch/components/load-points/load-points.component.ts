import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { LoadPoint } from '../../../../models/load_point';
import { MapsAPILoader } from '@agm/core';
import { LoadPointActionType } from '../../../../models/enums/dplEnums';
import { ResponseModel } from '../../../../models/response';
import { MessagesService } from '../../../shared/components/messages/messages.component';
import { LoadPointDetailsComponent } from '../load-point-details/load-point-details.component';
import { DispatchService } from '../../dispatch.service';
import { parse } from 'date-fns';

declare let google: any;

@Component({
  selector: 'dp-load-points',
  templateUrl: './load-points.component.html',
  styleUrls: ['./load-points.component.scss']
})
export class LoadPointsComponent implements OnInit {
  @ViewChild("loadPointDetails") loadPointDetails: LoadPointDetailsComponent;
  @Input() tripId: number;
  @Input() loadId: number;
  @Input() loadPoints: LoadPoint[] = [];
  @Input() IsFinished: boolean;
  @Output() onMapUpdate = new EventEmitter<LoadPoint[]>();
  @Output() onLoadPointSelected = new EventEmitter<number>();

  alphabets: string[] = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "#"];

  startingLoadPoint: LoadPoint;
  newLoadPointItem: LoadPoint = new LoadPoint();
  selectedLoadPoint: LoadPoint = new LoadPoint();

  constructor(
    private _dispatchService: DispatchService,
    private _messagesService: MessagesService,
    private _mapsAPILoader: MapsAPILoader) {
  }

  ngOnInit() {
    this.selectedLoadPoint.Id = 0;
    this.getLoadPoints();
    //this.setStartingLoadPoint();
  }

  getLoadPoints(shouldReloadLoadPoints: boolean = false, ldId: number = 0) {
    if (ldId > 0) {
      this.loadId = ldId;
    }
    this.loadPoints.length = 0;
    if(this.tripId > 0){
      this._dispatchService.getLoadPointsByTripId(this.tripId)
        .subscribe((data: ResponseModel<LoadPoint[]>) => {
          for (let dt of data.Result) {
            if (dt.DateLoad != null) {
              dt.DateLoad = parse(dt.DateLoad);
            }
            this.loadPoints.push(dt);
          }
          this.recalculateDistances(shouldReloadLoadPoints);
          this.setStartingLoadPoint();
          this.onMapUpdate.emit(this.loadPoints);
        });
    } else {
      this._dispatchService.getLoadPointsByLoadId(this.loadId)
        .subscribe((data: ResponseModel<LoadPoint[]>) => {
          for (let dt of data.Result) {
            if (dt.DateLoad != null) {
              dt.DateLoad = parse(dt.DateLoad);
            }
            this.loadPoints.push(dt);
          }
          this.recalculateDistances(shouldReloadLoadPoints);
          this.setStartingLoadPoint();
          this.onMapUpdate.emit(this.loadPoints);
        });
    }
  }

  setStartingLoadPoint(){
    this.startingLoadPoint = null;
    for (let item of this.loadPoints){
      if(item.OrderNumber == 0){
        this.startingLoadPoint = item;
        break;
      }
    }
  }

  addLoadPoint() {
    this.newLoadPointItem = new LoadPoint();
    this.newLoadPointItem.Id = 0;
    this.newLoadPointItem.ActionType = 0;
    this.newLoadPointItem.LoadId = this.loadId;
    this.newLoadPointItem.TourId = this.tripId;
    this.newLoadPointItem.DateLoad = new Date();

    this.loadPointDetails.show(this.newLoadPointItem);
  }

  addStartingLoadPoint(){
    this.newLoadPointItem = new LoadPoint();
    this.newLoadPointItem.Id = 0;
    this.newLoadPointItem.ActionType = 0;
    this.newLoadPointItem.LoadId = this.loadId;
    this.newLoadPointItem.TourId = this.tripId;
    this.newLoadPointItem.DateLoad = new Date();
    this.newLoadPointItem.OrderNumber = 0;

    this.loadPointDetails.show(this.newLoadPointItem);
  }

  onLoadPointCreated(lp: LoadPoint){
    this.selectedLoadPoint = lp;
    this.loadPoints.push(this.selectedLoadPoint);
    this._messagesService.success('', 'Load point details saved.');

    this.getLoadPoints(true);
  }

  onLoadPointEdited(lp: LoadPoint){
    this._messagesService.success('', 'Load point details saved.');
    this.getLoadPoints(true);
  }

  onStartingLoadPointSaved(lp: LoadPoint){
    this._messagesService.success('', 'Load point details saved.');
    this.getLoadPoints(true);
  }

  onSelectLoadPoint(loadPoint: any) {
    this.selectedLoadPoint = loadPoint;

    if(this.selectedLoadPoint.LoadId > 0){
      this.onLoadPointSelected.emit(this.selectedLoadPoint.LoadId);
    }

    this.loadPointDetails.show(this.selectedLoadPoint);
  }

  onLoadPointDeleted(){
    this._messagesService.success('', 'Load point deleted.');
    this.getLoadPoints(true);
    this.selectedLoadPoint = new LoadPoint();
    this.selectedLoadPoint.Id = 0;
  }

  onStartingLoadPointDeleted(){
    this._messagesService.success('', 'Load point deleted.');
    this.getLoadPoints(true);
  }

  closeLoadPointDetails() {
    this.selectedLoadPoint = new LoadPoint();
    this.selectedLoadPoint.Id = 0;
    this.getLoadPoints();
  }

  onDropLoadPoint(droppedPoint: LoadPoint, index: number) {
    let isSorted: boolean = true;
    for(let i = 0; i < this.loadPoints.length; i++){
      for(let j = i+1; j < this.loadPoints.length; j++){
        if(this.loadPoints[i].OrderNumber > 0 && this.loadPoints[j].OrderNumber > 0) {
          if (this.loadPoints[i].DateLoad > this.loadPoints[j].DateLoad) {
            isSorted = false;
            break;
          }
        }
      }
    }
    if(!isSorted){
      this.getLoadPoints();
      this._messagesService.error('', 'This load point can\'t be reordered. A chronological order must be applied!');
      return;
    }

    if(droppedPoint.ActionType == LoadPointActionType.Unloading && index == 1) {
      this.getLoadPoints();
      this._messagesService.error('', 'Unloading load point can\'t be first load point!');
      return;
    }

    let startPoint: LoadPoint = this.loadPoints.find((o)=>{
      return o.OrderNumber == 0;
    });
    let offset: number = startPoint == null ? 1 : 0;

    this._dispatchService.reorderLoadPoints(droppedPoint.Id, index + offset)
      .subscribe((data: ResponseModel<LoadPoint[]>) => {
        if (data.IsSuccess) {
          this.loadPoints.length = 0;
          for (let dt of data.Result) {
            this.loadPoints.push(dt);
          }
          this.onMapUpdate.emit(this.loadPoints);
          this.recalculateDistances(true);
        } else {
          this.getLoadPoints(true, this.loadId);
          this._messagesService.error('', data.Message);
        }
      });
  }

  recalculateDistances(shouldGetLoadPoints: boolean = false){
    if(this.loadPoints.length > 1) {
      this.loadPoints.map((item)=> {
        item.Distance = 0;
        //item.DistanceFromStart = 0;
      });
      this._mapsAPILoader.load().then(() => {
        let directionsService = new google.maps.DirectionsService;
        let length = this.loadPoints.length;
        let origin = new google.maps.LatLng(this.loadPoints[0].Lat, this.loadPoints[0].Lng);
        let destination = new google.maps.LatLng(this.loadPoints[length - 1].Lat, this.loadPoints[length - 1].Lng);
        let wayPoints = [];
        for (let i = 1; i < this.loadPoints.length - 1; i++) {
          wayPoints.push({
            location: new google.maps.LatLng(this.loadPoints[i].Lat, this.loadPoints[i].Lng),
            stopover: true
          });
        }
        directionsService.route({
          origin: origin,
          destination: destination,
          optimizeWaypoints: false,
          unitSystem: google.maps.UnitSystem.IMPERIAL,
          waypoints: wayPoints,
          avoidHighways: false,
          travelMode: google.maps.DirectionsTravelMode.DRIVING
        }, (response: any, status: any) => {
          if (status === 'OK') {
            for (let i = 0; i < response.routes[0].legs.length; i++) {
              let point = response.routes[0].legs[i];
              this.loadPoints[i + 1].EstimatedTime = point.duration.text;
              this.loadPoints[i + 1].Distance = point.distance.value / 1609.344;
            }
            this._dispatchService.updateDistances(this.loadPoints)
              .subscribe((data: ResponseModel<boolean>) => {
                if (shouldGetLoadPoints)
                  this.getLoadPoints();
              });
          } else {
            console.log('Directions request failed due to ' + status);
          }
        });
      });
    } else if(this.loadPoints.length == 1) {
      this.loadPoints[0].Distance = 0;
      this.loadPoints[0].EstimatedTime = "0";
      this._dispatchService.updateDistances(this.loadPoints)
        .subscribe((data: ResponseModel<boolean>) => {
          if (shouldGetLoadPoints)
            this.getLoadPoints();
        });
    }
  }

}
