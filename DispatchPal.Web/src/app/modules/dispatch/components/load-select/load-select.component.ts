import { Component, EventEmitter, forwardRef, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { LoadForAutocomplete } from '../../../../models/load-for-autocomplete';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { ResponseModel } from '../../../../models/response';
import { SharedService } from '../../../shared/services/shared.service';

@Component({
  selector: 'dp-load-select',
  templateUrl: './load-select.component.html',
  styleUrls: ['./load-select.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => LoadSelectComponent),
      multi: true
    }
  ]
})
export class LoadSelectComponent implements ControlValueAccessor, OnInit, OnChanges {
  @Output() onLoadSelected = new EventEmitter<any>();
  @Input() disabled: boolean = false;

  itemId: number = 0;
  selectedItem: LoadForAutocomplete = new LoadForAutocomplete();
  data: ResponseModel<LoadForAutocomplete[]> = new ResponseModel<LoadForAutocomplete[]>();

  isLoading: boolean = false;

  constructor(private _sharedService: SharedService) { }

  writeValue(value: any) {
    this.itemId = value;

    if (this.itemId > 0) {
      this.getItemById();
    } else {
      this.selectedItem = null;
      this.itemId = null;
      this.propagateChange(this.itemId);
      //this.onDriverSelected.emit(this.selectedItem);
    }
  }

  propagateChange = (_: any) => { };

  registerOnChange(fn: any) {
    this.propagateChange = fn;
  }

  registerOnTouched() { }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['itemId'] != null) {
      if (this.itemId > 0) {
        this.getItemById();
      } else {
        this.selectedItem = null;
      }
    }
  }

  search(event: any) {
    this.isLoading = true;
    this._sharedService.getLoadsForAutoComplete(event.query).subscribe((data: ResponseModel<LoadForAutocomplete[]>) => {
      this.isLoading = false;
      this.data = data;
    });
  }

  handleDropdown(event: any) {
    this.isLoading = true;
    this._sharedService.getLoadsForAutoComplete("").subscribe((data: ResponseModel<LoadForAutocomplete[]>) => {
      this.isLoading = false;
      this.data = data;
    });
  }

  selectItem(event: any) {
    this.selectedItem = event;
    this.itemId = this.selectedItem.Id;
    this.propagateChange(this.itemId);
    this.onLoadSelected.emit(this.selectedItem);
  }

  clearClick() {
    this.selectedItem = null;
    this.itemId = null;
    this.propagateChange(this.itemId);
    this.onLoadSelected.emit(this.selectedItem);
  }

  getItemById() {
    this._sharedService.getLoadForAutoCompleteById(this.itemId).subscribe(
      (data: ResponseModel<LoadForAutocomplete>) => {
        this.selectedItem = data.Result;
        this.propagateChange(this.itemId);
        this.onLoadSelected.emit(this.selectedItem);
      },
      (error: any) => console.log(error)
    );
  }
}
