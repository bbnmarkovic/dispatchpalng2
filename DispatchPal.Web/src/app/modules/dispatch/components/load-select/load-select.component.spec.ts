import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoadSelectComponent } from './load-select.component';

describe('LoadSelectComponent', () => {
  let component: LoadSelectComponent;
  let fixture: ComponentFixture<LoadSelectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoadSelectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoadSelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
