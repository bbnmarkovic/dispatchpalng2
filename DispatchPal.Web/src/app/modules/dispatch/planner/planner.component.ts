import { Component, OnInit } from '@angular/core';
import { Filter } from '../../../models/filter';
import { ResponseModel } from '../../../models/response';
import { addDays, addMonths, getDaysInMonth } from "date-fns";
import { DriverPreview } from '../../../models/driver_preview';
import { DispatchService } from '../dispatch.service';

@Component({
  selector: 'dp-planner',
  templateUrl: './planner.component.html',
  styleUrls: ['./planner.component.scss']
})
export class PlannerComponent implements OnInit {
  drivers: DriverPreview[] = [];
  selectedDate: Date;
  points: Date[] = [];

  constructor(
    private _dispatchService: DispatchService) {

    this.selectedDate = new Date();
  }

  ngOnInit() {
    this.setDates(this.selectedDate);
    let filter: Filter = new Filter();
    filter.Offset = 0;
    filter.PageSize = 100;
    this._dispatchService.getDriversByFilter(filter).subscribe((data: ResponseModel<DriverPreview[]>)=> {
      this.drivers = data.Result;
    });
  }

  setDates(dt: Date){
    this.points.length = 0;
    dt.setDate(1);
    let lastDay: number = getDaysInMonth(dt);
    for (let i = 0; i < lastDay; i++) {
      this.points.push(addDays(dt, i));
    }
  }

  previousMonth() {
    this.selectedDate = addMonths(this.selectedDate, -1);
    this.setDates(this.selectedDate);
  }

  nextMonth() {
    this.selectedDate = addMonths(this.selectedDate, 1);
    this.setDates(this.selectedDate);
  }
}
