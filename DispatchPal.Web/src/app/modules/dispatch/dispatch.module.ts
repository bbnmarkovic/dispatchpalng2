import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoadsComponent } from './loads/loads.component';
import { LoadDetailsComponent } from './load-details/load-details.component';
import { TripsComponent } from './trips/trips.component';
import { TripDetailsComponent } from './trip-details/trip-details.component';
import { TripItemComponent } from './components/trip-item/trip-item.component';
import { LoadItemComponent } from './components/load-item/load-item.component';
import { LoadAssignComponent } from './components/load-assign/load-assign.component';
import { LoadCreateComponent } from './components/load-create/load-create.component';
import { LoadPointDetailsComponent } from './components/load-point-details/load-point-details.component';
import { LoadPointItemComponent } from './components/load-point-item/load-point-item.component';
import { LoadPointStartComponent } from './components/load-point-start/load-point-start.component';
import { LoadPointsComponent } from './components/load-points/load-points.component';
import { LoadSelectComponent } from './components/load-select/load-select.component';
import { LoadIssueComponent } from './components/load-issue/load-issue.component';
import { MapComponent } from './components/map/map.component';
import { TripSelectComponent } from './components/trip-select/trip-select.component';
import { PlannerItemComponent } from './components/planner-item/planner-item.component';
import { PlannerComponent } from './planner/planner.component';
import { AgmCoreModule } from '@agm/core';
import { CurrencyMaskModule } from 'ng2-currency-mask';
import {
  AutoCompleteModule,
  CalendarModule, ConfirmDialogModule, DataTableModule, DialogModule, DropdownModule, MultiSelectModule, RatingModule,
  ScheduleModule,
  SelectButtonModule,
  SharedModule,
  SpinnerModule,
  StepsModule,
  TabViewModule, TooltipModule, TriStateCheckboxModule
} from 'primeng/primeng';
import { DndModule } from 'ng2-dnd';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DispatchPalSharedModule } from '../shared/shared.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DispatchComponent } from './dispatch.component';
import { DispatchRoutingModule } from './dispatch-routing.module';
import { DispatchService } from './dispatch.service';
import { LoadIssueDetailsComponent } from './components/load-issue-details/load-issue-details.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    DispatchRoutingModule,
    DataTableModule,
    SharedModule,
    CalendarModule,
    DialogModule,
    TooltipModule,
    DropdownModule,
    TabViewModule,
    ConfirmDialogModule,
    ScheduleModule,
    SpinnerModule,
    MultiSelectModule,
    SelectButtonModule,
    TriStateCheckboxModule,
    DispatchPalSharedModule,
    StepsModule,
    RatingModule,
    AutoCompleteModule,
    CurrencyMaskModule,
    DndModule.forRoot(),
    FlexLayoutModule,
    AgmCoreModule.forRoot({
      libraries: ['places'],
      apiKey: 'AIzaSyAm0VmjVpk4HBnk4AravBnURMO4kz4BhNg'
    })
  ],
  declarations: [
    LoadsComponent,
    LoadDetailsComponent,
    TripsComponent,
    TripDetailsComponent,
    TripItemComponent,
    LoadItemComponent,
    LoadAssignComponent,
    LoadCreateComponent,
    LoadPointDetailsComponent,
    LoadPointItemComponent,
    LoadPointStartComponent,
    LoadPointsComponent,
    LoadSelectComponent,
    LoadIssueComponent,
    MapComponent,
    TripSelectComponent,
    PlannerItemComponent,
    PlannerComponent,
    DispatchComponent,
    LoadIssueDetailsComponent
  ],
  providers: [
    DispatchService
  ]
})
export class DispatchModule { }
