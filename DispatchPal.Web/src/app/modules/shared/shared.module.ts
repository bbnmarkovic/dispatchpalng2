import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RateValueComponent } from './components/rate-value/rate-value.component';
import { BrokerComponent } from './components/broker/broker.component';
import { ClientComponent } from './components/client/client.component';
import { ClientTypeComponent } from './components/client-type/client-type.component';
import { CompanyComponent } from './components/company/company.component';
import { LoaderComponent, LoaderService } from './components/loader/loader.component';
import { MessagesComponent, MessagesService } from './components/messages/messages.component';
import { StateComponent } from './components/state/state.component';
import { TrailerComponent } from './components/trailer/trailer.component';
import { TruckComponent } from './components/truck/truck.component';
import { UploadComponent } from './components/upload/upload.component';
import { UserComponent } from './components/user/user.component';
import { DriverComponent } from './components/driver/driver.component';
import {
    AutoCompleteModule, CalendarModule, ConfirmDialogModule, DataTableModule, DialogModule, DropdownModule,
    FileUploadModule, GrowlModule,
    MultiSelectModule, OverlayPanelModule, RatingModule, ScheduleModule,
    SharedModule, SpinnerModule, TabViewModule, TooltipModule
} from 'primeng/primeng';
import {FormsModule} from '@angular/forms';
import {Ng2ImgToolsModule} from 'ng2-img-tools';
import {DndModule} from 'ng2-dnd';
import {FlexLayoutModule} from '@angular/flex-layout';
import {CurrencyMaskModule} from 'ng2-currency-mask';
import { TruncatePipe } from './pipes/truncate.pipe';
import { DirectionsMapDirective } from './directives/map-directions/directions.directive';
import { SharedService } from './services/shared.service';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    GrowlModule,
    DialogModule,
    DataTableModule,
    TooltipModule,
    FileUploadModule,
    DropdownModule,
    AutoCompleteModule,
    SpinnerModule,
    TabViewModule,
    CalendarModule,
    SharedModule,
    ConfirmDialogModule,
    ScheduleModule,
    MultiSelectModule,
    RatingModule,
    OverlayPanelModule,
    DndModule,
    CurrencyMaskModule,
    Ng2ImgToolsModule,
    FlexLayoutModule
  ],
  declarations: [
    RateValueComponent,
    BrokerComponent,
    ClientComponent,
    ClientTypeComponent,
    CompanyComponent,
    LoaderComponent,
    MessagesComponent,
    StateComponent,
    TrailerComponent,
    TruckComponent,
    UploadComponent,
    UserComponent,
    DriverComponent,
    TruncatePipe,
    DirectionsMapDirective
  ],
  exports: [
    RateValueComponent,
    BrokerComponent,
    ClientComponent,
    ClientTypeComponent,
    CompanyComponent,
    LoaderComponent,
    MessagesComponent,
    StateComponent,
    TrailerComponent,
    TruckComponent,
    UploadComponent,
    UserComponent,
    DriverComponent,
    TruncatePipe,
    DirectionsMapDirective
  ],
  providers: [
    MessagesService,
    LoaderService,
    SharedService
  ]
})
export class DispatchPalSharedModule { }
