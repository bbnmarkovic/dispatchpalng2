﻿import { Directive, Input, Output, EventEmitter } from '@angular/core';
import { GoogleMapsAPIWrapper } from '@agm/core';

declare let google: any;

@Directive({
    selector: 'sebm-google-map-directions'
})
export class DirectionsMapDirective {
    @Output() onDistanceCalculated = new EventEmitter<any>();

    @Input() origin: any;
    @Input() destination: any;
    @Input() waypoints: any;
    @Input() directionsDisplay: any;
    @Input() estimatedTime: any;
    @Input() estimatedDistance: any;

    constructor(private gMapsApi: GoogleMapsAPIWrapper) { }
    updateDirections() {
        this.gMapsApi.getNativeMap().then(map => {

            if (this.origin == undefined || this.destination == undefined)
                return;

            let directionsService = new google.maps.DirectionsService;
            let me = this;
            this.directionsDisplay.setMap(map);
            this.directionsDisplay.setOptions({
                polylineOptions: {
                    strokeWeight: 4,
                    strokeOpacity: 0.7,
                    strokeColor: '#00468c'
                }
            });
            
            this.directionsDisplay.setDirections({ routes: [] });
            directionsService.route({
                origin: this.origin,
                destination: this.destination,
                optimizeWaypoints: false,
                unitSystem: google.maps.UnitSystem.IMPERIAL,
                waypoints: this.waypoints,
                avoidHighways: false,
                travelMode: google.maps.DirectionsTravelMode.DRIVING
            }, function (response: any, status: any) {
                if (status === 'OK') {
                    me.directionsDisplay.setDirections(response);
                    map.setZoom(30);
                } else {
                    console.log('Directions request failed due to ' + status);
                }
            });
        });

    }

    /*private getComputedDistance(latLngA: any, latLngB: any) {
        return (google.maps.geometry.spherical.computeDistanceBetween(latLngA, latLngB) / 1000).toFixed(2);
    }*/
}