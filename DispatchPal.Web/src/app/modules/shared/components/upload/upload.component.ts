import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {ConfirmationService, SelectItem} from 'primeng/components/common/api';
import {FileModel} from '../../../../models/file';
import {UploadTarget} from '../../../../models/enums/dplEnums';
import {Load} from '../../../../models/load';
import {FileUpload} from 'primeng/primeng';
import {ResponseModel} from '../../../../models/response';
import {Driver} from '../../../../models/driver';
import {Ng2ImgToolsService} from 'ng2-img-tools';
import { SharedService } from '../../services/shared.service';
import { environment } from '../../../../../environments/environment';

@Component({
    selector: 'dp-upload',
    templateUrl: './upload.component.html',
    styleUrls: ['./upload.component.scss']
})
export class UploadComponent implements OnInit {
    @ViewChild("fileUploader") fileUploader: FileUpload;
    @Output() onTruckSelected = new EventEmitter<any>();
    @Input() disabled: boolean = false;
    @Input() itemId: number;
    @Input() uploadedFiles: FileModel[];
    @Input() uploadTarget: number;

    imgData: any;

    documentType: number = 0;
    baseUrl: string;
    fileId: number = 0;
    specificUrl: string;
    displayPreview: boolean;
    isUploadMode: boolean = false;

    documentTypes: Array<SelectItem> = [];

    constructor(private _sharedService: SharedService,
                private _confirmationService: ConfirmationService,
                private _ng2ImgToolsService: Ng2ImgToolsService)
    {
        this.baseUrl = environment.api;

        if (this.uploadedFiles == null)
            this.uploadedFiles = [];
    }

    ngOnInit() {
        if (this.uploadTarget == UploadTarget.Load) {
            this.documentTypes.push({ label: 'BOL', value: 0 });
            this.documentTypes.push({ label: 'Confirmation', value: 1 });
            this.documentTypes.push({ label: 'Receipts', value: 2 });
            this.documentTypes.push({ label: 'Other', value: 3 });
            this.specificUrl = "loads";
        } else if (this.uploadTarget == UploadTarget.Driver) {
            this.documentTypes.push({ label: 'CDL', value: 0 });
            this.documentTypes.push({ label: 'Medical', value: 1 });
            this.documentTypes.push({ label: 'MVR', value: 2 });
            this.documentTypes.push({ label: 'Application', value: 3 });
            this.documentTypes.push({ label: 'Contract', value: 4 });
            this.documentTypes.push({ label: 'Workers Compensation', value: 5 });
            this.documentTypes.push({ label: 'Previous employment 1', value: 6 });
            this.documentTypes.push({ label: 'Previous employment 2', value: 7 });
            this.documentTypes.push({ label: 'Previous employment 3', value: 8 });
            this.documentTypes.push({ label: 'Other', value: 9 });
            this.specificUrl = "drivers";
        }
    }

    getDocumentTypeName(typeId: number) {
        if (this.uploadTarget == UploadTarget.Load) {
            switch (typeId) {
                case 0: {
                    return 'BOL';
                }
                case 1: {
                    return 'Confirmation';
                }
                case 2: {
                    return 'Receipts';
                }
                case 3: {
                    return 'Other';
                }
                default:
                    return 'Other';
            }
        } else if (this.uploadTarget == UploadTarget.Driver) {
            switch (typeId) {
                case 0: {
                    return 'CDL';
                }
                case 1: {
                    return 'Medical';
                }
                case 2: {
                    return 'MVR';
                }
                case 3: {
                    return 'Application';
                }
                case 4: {
                    return 'Contract';
                }
                case 5: {
                    return 'Workers Compensation';
                }
                case 6: {
                    return 'Previous employment 1';
                }
                case 7: {
                    return 'Previous employment 2';
                }
                case 8: {
                    return 'Previous employment 3';
                }
                case 9: {
                    return 'Other';
                }
                default:
                    return 'Other';
            }
        }
    }

    onUpload(event: any) {
        if (this.uploadTarget == UploadTarget.Load) {
            this._sharedService.getLoadById(this.itemId).subscribe((res: ResponseModel<Load>) => {
                this.uploadedFiles = res.Result.Files;
            });
        } else if (this.uploadTarget == UploadTarget.Driver) {
            this._sharedService.getDriverById(this.itemId).subscribe((res: ResponseModel<Driver>) => {
                this.uploadedFiles = res.Result.Files;
            });
        }

    }

    onSelectFiles(event: any) {
        for (let fl of event.files) {
            if (fl.type == "image/jpeg" || fl.type == "image/jpg" || fl.type == "image/png") {
                this._ng2ImgToolsService.resize([fl], 1200, 1200).subscribe(result => {
                    result.objectUrl = window.URL.createObjectURL(result);
                    let index = this.fileUploader.files.indexOf(fl);
                    if (index > -1) {
                        this.fileUploader.files[index] = result;
                    }
                    //this.fileUploader.files.push(result);
                }, error => {
                    //something went wrong
                    //use result.compressedFile or handle specific error cases individually
                });
            }
        }
    }

    removeFile(item: File) {
        let index = this.fileUploader.files.indexOf(item);
        if (index > -1)
            this.fileUploader.files.splice(index, 1);
    }

    deleteFileFromServer(item: FileModel) {
        this._confirmationService.confirm({
            message: 'Are you sure that you want to delete selected file?',
            accept: () => {
                this._sharedService.deleteFileFromLoad(item.Id)
                    .subscribe((data: ResponseModel<boolean>) => {
                        if (data.IsSuccess) {
                            let index = this.uploadedFiles.indexOf(item);
                            if (index > -1)
                                this.uploadedFiles.splice(index, 1);
                        }
                    });
            }
        });
    }

    previewFile(item: FileModel) {
        this.displayPreview = true;
        this.fileId = item.Id;

        /*if(item.Extension === ".jpg"){
            this.displayPreview = true;
            this.fileId = item.Id;
        } else {
            this._loadsService.previewFile(item.Id)
                .subscribe((data: any) => {
                    let fileType = "application/pdf";
                    if (item.Extension === ".png") {
                        fileType = "image/png";
                    } else if (item.Extension === ".jpg") {
                        fileType = "image/jpg";
                    } else if (item.Extension === ".jpeg") {
                        fileType = "image/jpg";
                    }
                    let res = new Blob([data], {type: fileType});
                    let fileURL = window.URL.createObjectURL(res);
                    window.open(fileURL);
                });
        }*/
    }
}
