import {Component, EventEmitter, forwardRef, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';
import {ResponseModel} from '../../../../models/response';
import {Vehicle} from '../../../../models/vehicle';
import { SharedService } from '../../services/shared.service';

@Component({
    selector: 'dp-trailer',
    templateUrl: './trailer.component.html',
    styleUrls: ['./trailer.component.scss'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => TrailerComponent),
            multi: true
        }
    ]
})
export class TrailerComponent implements ControlValueAccessor, OnInit, OnChanges {
    @Output() onTrailerSelected = new EventEmitter<any>();
    @Input() disabled: boolean = false;

    itemId: number = 0;
    selectedItem: Vehicle = new Vehicle();
    data: ResponseModel<Vehicle[]> = new ResponseModel<Vehicle[]>();

    isLoading: boolean = false;

    constructor(private _sharedService: SharedService) { }

    writeValue(value: any) {
        this.itemId = value;

        if (this.itemId > 0) {
            this.getItemById();
        } else {
            this.selectedItem = null;
            this.itemId = null;
            this.propagateChange(this.itemId);
            this.onTrailerSelected.emit(this.selectedItem);
        }
    }

    propagateChange = (_: any) => { };

    registerOnChange(fn: any) {
        this.propagateChange = fn;
    }

    registerOnTouched() { }

    ngOnInit() {
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes['itemId'] != null) {
            if (this.itemId > 0) {
                this.getItemById();
            } else {
                this.selectedItem = null;
            }
        }
    }

    search(event: any) {
        this.isLoading = true;
        this._sharedService.getVehiclesForAutoComplete(1, event.query).subscribe((data: ResponseModel<Vehicle[]>) => {
            this.isLoading = false;
            this.data = data;
        });
    }

    handleDropdown(event: any) {
        this.isLoading = true;
        this._sharedService.getVehiclesForAutoComplete(1, "").subscribe((data: ResponseModel<Vehicle[]>) => {
            this.isLoading = false;
            this.data = data;
        });
    }

    selectItem(event: any) {
        this.selectedItem = event;
        this.itemId = this.selectedItem.Id;
        this.propagateChange(this.itemId);
        this.onTrailerSelected.emit(this.selectedItem);
    }

    clearClick() {
        this.selectedItem = null;
        this.itemId = null;
        this.propagateChange(this.itemId);
        this.onTrailerSelected.emit(this.selectedItem);
    }

    getItemById() {
        this._sharedService.getVehicleById(this.itemId).subscribe(
            (data: ResponseModel<Vehicle>) => {
                this.selectedItem = data.Result;
                this.propagateChange(this.itemId);
                this.onTrailerSelected.emit(this.selectedItem);
            },
            (error: any) => console.log(error)
        );
    }
}
