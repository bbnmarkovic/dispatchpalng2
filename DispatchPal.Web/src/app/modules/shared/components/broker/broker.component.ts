import {Component, EventEmitter, forwardRef, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';
import {Broker} from '../../../../models/broker';
import {ResponseModel} from '../../../../models/response';
import { SharedService } from '../../services/shared.service';

@Component({
    selector: 'dp-broker',
    templateUrl: './broker.component.html',
    styleUrls: ['./broker.component.scss'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => BrokerComponent),
            multi: true
        }
    ]
})
export class BrokerComponent implements ControlValueAccessor, OnInit, OnChanges {
    @Output() onBrokerSelected = new EventEmitter<any>();
    @Output() onBrokerCreated = new EventEmitter<any>();
    @Input() brokerageId: number = 0;
    @Input() disabled: boolean = false;

    itemId: number = 0;
    selectedItem: Broker = new Broker();
    data: ResponseModel<Broker[]> = new ResponseModel<Broker[]>();

    newBroker: Broker;
    showCreateBrokerDialog: boolean = false;

    isLoading: boolean = false;

    constructor(private _sharedService: SharedService) {
        this.newBroker = new Broker();
    }

    writeValue(value: any) {
        this.itemId = value;

        if (this.itemId > 0) {
            this.getItemById();
        } else {
            this.selectedItem = null;
            this.itemId = null;
            this.propagateChange(this.itemId);
            this.onBrokerSelected.emit(this.selectedItem);
        }
    }

    propagateChange = (_: any) => { };

    registerOnChange(fn: any) {
        this.propagateChange = fn;
    }

    registerOnTouched() { }

    ngOnInit() {
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes['itemId'] != null) {
            if (this.itemId > 0) {
                this.getItemById();
            } else {
                this.selectedItem = null;
            }
        }
    }

    search(event: any) {
        this.isLoading = true;
        if (this.brokerageId == null)
            this.brokerageId = 0;
        this._sharedService.getBrokersForAutoComplete(this.brokerageId, event.query).subscribe((data: ResponseModel<Broker[]>) => {
            this.isLoading = false;
            this.data = data;

            if (this.brokerageId > -1)
                this.addBlankItem();
        });
    }

    handleDropdown(event: any) {
        this.isLoading = true;
        if (this.brokerageId == null)
            this.brokerageId = 0;
        this._sharedService.getBrokersForAutoComplete(this.brokerageId, "").subscribe((data: ResponseModel<Broker[]>) => {
            this.isLoading = false;
            this.data = data;

            if (this.brokerageId > -1)
                this.addBlankItem();
        });
    }

    addBlankItem() {
        const br: Broker = new Broker();
        br.Id = -1;
        br.DisplayName = "+ New...";
        // this.data.Result.push(br);
        this.data.Result = [br, ...this.data.Result];
    }

    selectItem(event: any) {
        this.selectedItem = event;
        this.itemId = this.selectedItem.Id;
        if (this.itemId == -1) {
            this.showCreateBrokerDialog = true;
        } else {
            this.propagateChange(this.itemId);
            this.onBrokerSelected.emit(this.selectedItem);
        }
    }

    clearClick() {
        this.selectedItem = null;
        this.itemId = null;
        this.propagateChange(this.itemId);
        this.onBrokerSelected.emit(this.selectedItem);
    }

    getItemById() {
        this._sharedService.getBrokerById(this.itemId).subscribe(
            (data: ResponseModel<Broker>) => {
                this.selectedItem = data.Result;
                this.propagateChange(this.itemId);
                this.onBrokerSelected.emit(this.selectedItem);
            },
            (error: any) => console.log(error)
        );
    }

    createBroker() {
        this.newBroker.PartnerId = this.brokerageId;
        this.newBroker.IsActive = true;
        this._sharedService.saveBroker(this.newBroker).subscribe((data: ResponseModel<Broker>) => {
            this.data.Result.push(data.Result);
            this.onBrokerCreated.emit(data);
            this.showCreateBrokerDialog = false;
        });
    }

}
