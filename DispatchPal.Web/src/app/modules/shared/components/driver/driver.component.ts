import {Component, EventEmitter, forwardRef, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';
import {ResponseModel} from '../../../../models/response';
import {Driver} from '../../../../models/driver';
import { SharedService } from '../../services/shared.service';

@Component({
    selector: 'dp-driver',
    templateUrl: './driver.component.html',
    styleUrls: ['./driver.component.scss'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => DriverComponent),
            multi: true
        }
    ]
})

export class DriverComponent implements ControlValueAccessor, OnInit, OnChanges {
    @Output() onDriverSelected = new EventEmitter<any>();
    @Input() disabled: boolean = false;

    itemId: number = 0;
    selectedItem: Driver = new Driver();
    data: ResponseModel<Driver[]> = new ResponseModel<Driver[]>();

    isLoading: boolean = false;

    constructor(private _sharedService: SharedService) { }

    writeValue(value: any) {
        this.itemId = value;

        if (this.itemId > 0) {
            this.getItemById();
        } else {
            this.selectedItem = null;
            this.itemId = null;
            this.propagateChange(this.itemId);
            //this.onDriverSelected.emit(this.selectedItem);
        }
    }

    propagateChange = (_: any) => { };

    registerOnChange(fn: any) {
        this.propagateChange = fn;
    }

    registerOnTouched() { }

    ngOnInit() {
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes['itemId'] != null) {
            if (this.itemId > 0) {
                this.getItemById();
            } else {
                this.selectedItem = null;
            }
        }
    }

    search(event: any) {
        this.isLoading = true;
        this._sharedService.getDriversForAutoComplete(event.query).subscribe((data: ResponseModel<Driver[]>) => {
            this.isLoading = false;
            this.data = data;
        });
    }

    handleDropdown(event: any) {
        this.isLoading = true;
        this._sharedService.getDriversForAutoComplete("").subscribe((data: ResponseModel<Driver[]>) => {
            this.isLoading = false;
            this.data = data;
        });
    }

    selectItem(event: any) {
        this.selectedItem = event;
        this.itemId = this.selectedItem.Id;
        this.propagateChange(this.itemId);
        this.onDriverSelected.emit(this.selectedItem);
    }

    clearClick() {
        this.selectedItem = null;
        this.itemId = null;
        this.propagateChange(this.itemId);
        this.onDriverSelected.emit(this.selectedItem);
    }

    getItemById() {
        this._sharedService.getDriverById(this.itemId).subscribe(
            (data: ResponseModel<Driver>) => {
                this.selectedItem = data.Result;
                this.propagateChange(this.itemId);
                this.onDriverSelected.emit(this.selectedItem);
            },
            (error: any) => console.log(error)
        );
    }
}
