import {Component, EventEmitter, forwardRef, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';
import {Company} from '../../../../models/company';
import {ResponseModel} from '../../../../models/response';
import { SharedService } from '../../services/shared.service';


@Component({
    selector: 'dp-company',
    templateUrl: './company.component.html',
    styleUrls: ['./company.component.scss'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => CompanyComponent),
            multi: true
        }
    ]
})
export class CompanyComponent implements ControlValueAccessor, OnInit, OnChanges {
    @Output() onCompanySelected = new EventEmitter<any>();
    @Input() disabled: boolean = false;

    itemId: number = 0;
    selectedItem: Company = new Company();
    data: ResponseModel<Company[]> = new ResponseModel<Company[]>();

    isLoading: boolean = false;

    constructor(private _sharedService: SharedService) { }

    writeValue(value: any) {
        this.itemId = value;

        if (this.itemId > 0) {
            this.getItemById();
        } else {
            this.selectedItem = null;
            this.itemId = null;
            this.propagateChange(this.itemId);
            //this.onClientSelected.emit(this.selectedItem);
        }
    }

    propagateChange = (_: any) => { };

    registerOnChange(fn: any) {
        this.propagateChange = fn;
    }

    registerOnTouched() { }

    ngOnInit() {
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes['itemId'] != null) {
            if (this.itemId > 0) {
                this.getItemById();
            } else {
                this.selectedItem = null;
            }
        }
    }

    search(event: any) {
        this.isLoading = true;
        this._sharedService.getCompaniesForAutoComplete(event.query).subscribe((data: ResponseModel<Company[]>) => {
            this.isLoading = false;
            this.data = data;
        });
    }

    handleDropdown(event: any) {
        this.isLoading = true;
        this._sharedService.getCompaniesForAutoComplete("").subscribe((data: ResponseModel<Company[]>) => {
            this.isLoading = false;
            this.data = data;
        });
    }

    selectItem(event: any) {
        this.selectedItem = event;
        this.itemId = this.selectedItem.Id;
        this.propagateChange(this.itemId);
        this.onCompanySelected.emit(this.selectedItem);
    }

    clearClick() {
        this.selectedItem = null;
        this.itemId = null;
        this.propagateChange(this.itemId);
        this.onCompanySelected.emit(this.selectedItem);
    }

    getItemById() {
        this._sharedService.getCompanyById(this.itemId).subscribe(
            (data: ResponseModel<Company>) => {
                this.selectedItem = data.Result;
                this.propagateChange(this.itemId);
                this.onCompanySelected.emit(this.selectedItem);
            },
            (error: any) => console.log(error)
        );
    }
}
