import {Component, Injectable, OnInit} from '@angular/core';
import {Message} from 'primeng/components/common/api';
import {Subject} from 'rxjs/Subject';
import {Subscription} from 'rxjs/Subscription';

export enum MessageSeverity {
    SUCCESS,
    INFO,
    WARN,
    ERROR
}

export interface IMessageType {
    severity: MessageSeverity;
    summary: string;
    detail: string;
}

@Injectable()
export class MessagesService {
    private _subject: Subject<IMessageType> = new Subject<IMessageType>();

    public get subject(): Subject<IMessageType> {
        return this._subject;
    }

    public success(summary: string, detail: string) {
        this._subject.next({ severity: MessageSeverity.SUCCESS, summary: summary, detail: detail });
    }

    public info(summary: string, detail: string) {
        this._subject.next({ severity: MessageSeverity.INFO, summary: summary, detail: detail });
    }

    public warn(summary: string, detail: string) {
        this._subject.next({ severity: MessageSeverity.WARN, summary: summary, detail: detail });
    }

    public error(summary: string, detail: string) {
        this._subject.next({ severity: MessageSeverity.ERROR, summary: summary, detail: detail });
    }
}

@Component({
    selector: 'dp-messages',
    templateUrl: './messages.component.html',
    styleUrls: ['./messages.component.scss']
})
export class MessagesComponent implements OnInit {
    msgs: Message[] = [];

    private msgSubscription: Subscription;

    constructor(
        private messagesService: MessagesService
    ) { }

    ngOnInit() {
        this.msgSubscription = this.messagesService.subject.subscribe((v: IMessageType) => {
            switch (v.severity) {
                case MessageSeverity.SUCCESS:
                    this.success(v.summary, v.detail);
                    break;
                case MessageSeverity.INFO:
                    this.info(v.summary, v.detail);
                    break;
                case MessageSeverity.WARN:
                    this.warn(v.summary, v.detail);
                    break;
                case MessageSeverity.ERROR:
                    this.error(v.summary, v.detail);
                    break;
                default:
                    throw new Error('Invalid message severity.');
            }
        });
    }

    ngOnDestroy() {
        this.msgSubscription.unsubscribe();
    }

    private success(summary: string, detail: string) {
        this.msgs = [];
        this.msgs.push({ severity: 'success', summary: summary, detail: detail });
    }

    private info(summary: string, detail: string) {
        this.msgs = [];
        this.msgs.push({ severity: 'info', summary: summary, detail: detail });
    }

    private warn(summary: string, detail: string) {
        this.msgs = [];
        this.msgs.push({ severity: 'warn', summary: summary, detail: detail });
    }

    private error(summary: string, detail: string) {
        this.msgs = [];
        this.msgs.push({ severity: 'error', summary: summary, detail: detail });
    }
}