import {Component, EventEmitter, forwardRef, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';
import {ClientType} from '../../../../models/client-type';
import {ResponseModel} from '../../../../models/response';
import { SharedService } from '../../services/shared.service';

@Component({
    selector: 'dp-client-type',
    templateUrl: './client-type.component.html',
    styleUrls: ['./client-type.component.scss'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => ClientTypeComponent),
            multi: true
        }
    ]
})
export class ClientTypeComponent implements ControlValueAccessor, OnInit, OnChanges {
    @Output() onClientTypeSelected = new EventEmitter<any>();
    @Input() disabled: boolean = false;

    itemId: number = 0;
    selectedItem: ClientType = new ClientType();
    data: ClientType[] = [];

    isLoading: boolean = false;

    constructor(private _sharedService: SharedService) { }

    writeValue(value: any) {
        this.itemId = value;

        if (this.itemId > 0) {
            this.getItemById();
        } else {
            this.selectedItem = null;
            this.itemId = null;
            this.propagateChange(this.itemId);
            this.onClientTypeSelected.emit(this.selectedItem);
        }
    }

    propagateChange = (_: any) => { };

    registerOnChange(fn: any) {
        this.propagateChange = fn;
    }

    registerOnTouched() { }

    ngOnInit() {
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes['itemId'] != null) {
            if (this.itemId > 0) {
                this.getItemById();
            } else {
                this.selectedItem = null;
            }
        }
    }

    search(event: any) {
        this.isLoading = true;
        this._sharedService.getClientTypes(event.query).subscribe((data: ResponseModel<ClientType[]>) => {
            this.isLoading = false;
            this.data = data.Result;
        });
    }

    handleDropdown(event: any) {
        this.isLoading = true;
        this._sharedService.getClientTypes("").subscribe((data: ResponseModel<ClientType[]>) => {
            this.isLoading = false;
            this.data = data.Result;
        });
    }

    selectItem(event: any) {
        this.selectedItem = event;
        this.itemId = this.selectedItem.Id;
        this.propagateChange(this.itemId);
        this.onClientTypeSelected.emit(this.selectedItem);
    }

    clearClick() {
        this.selectedItem = null;
        this.itemId = null;
        this.propagateChange(this.itemId);
        this.onClientTypeSelected.emit(this.selectedItem);
    }

    getItemById() {
        this._sharedService.getClientTypeById(this.itemId).subscribe(
            (data: ResponseModel<ClientType>) => {
                this.selectedItem = data.Result;
                this.propagateChange(this.itemId);
                this.onClientTypeSelected.emit(this.selectedItem);
            },
            (error: any) => console.log(error)
        );
    }
}
