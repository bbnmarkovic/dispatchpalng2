import {Component, EventEmitter, forwardRef, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';
import {Client} from '../../../../models/client';
import {ResponseModel} from '../../../../models/response';
import { SharedService } from '../../services/shared.service';

@Component({
    selector: 'dp-client',
    templateUrl: './client.component.html',
    styleUrls: ['./client.component.scss'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => ClientComponent),
            multi: true
        }
    ]
})
export class ClientComponent implements ControlValueAccessor, OnInit, OnChanges {
  @Output() onClientSelected = new EventEmitter<any>();
  @Input() disabled: boolean = false;
  @Input() clientType: number = 0;

  itemId: number = 0;
  selectedItem: Client = new Client();
  data: ResponseModel<Client[]> = new ResponseModel<Client[]>();

  isLoading: boolean = false;

  constructor(private _sharedService: SharedService) {
  }

  writeValue(value: any) {
    this.itemId = value;

    if (this.itemId > 0) {
      this.getItemById();
    } else {
      this.selectedItem = null;
      this.itemId = null;
      this.propagateChange(this.itemId);
      //this.onClientSelected.emit(this.selectedItem);
    }
  }

  propagateChange = (_: any) => {
  };

  registerOnChange(fn: any) {
    this.propagateChange = fn;
  }

  registerOnTouched() {
  }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['itemId'] != null) {
      if (this.itemId > 0) {
        this.getItemById();
      } else {
        this.selectedItem = null;
      }
    }
  }

  search(event: any) {
    this.isLoading = true;
    this._sharedService.getClientsForAutoComplete(this.clientType, event.query).subscribe((data: ResponseModel<Client[]>) => {
      this.isLoading = false;
      this.data = data;
    });
  }

  handleDropdown(event: any) {
    this.isLoading = true;
    this._sharedService.getClientsForAutoComplete(this.clientType, "").subscribe((data: ResponseModel<Client[]>) => {
      this.isLoading = false;
      this.data = data;
    });
  }

  selectItem(event: any) {
    this.selectedItem = event;
    this.itemId = this.selectedItem.Id;
    this.propagateChange(this.itemId);
    this.onClientSelected.emit(this.selectedItem);
  }

  clearClick() {
    this.selectedItem = null;
    this.itemId = null;
    this.propagateChange(this.itemId);
    this.onClientSelected.emit(this.selectedItem);
  }

  getItemById() {
    this._sharedService.getClientById(this.itemId).subscribe(
      (data: ResponseModel<Client>) => {
        this.selectedItem = data.Result;
        this.propagateChange(this.itemId);
        this.onClientSelected.emit(this.selectedItem);
      },
      (error: any) => console.log(error)
    );
  }
}
