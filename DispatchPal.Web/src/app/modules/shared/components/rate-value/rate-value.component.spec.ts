import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RateValueComponent } from './rate-value.component';

describe('RateValueComponent', () => {
  let component: RateValueComponent;
  let fixture: ComponentFixture<RateValueComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RateValueComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RateValueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
