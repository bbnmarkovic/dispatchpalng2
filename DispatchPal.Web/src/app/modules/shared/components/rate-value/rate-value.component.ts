import {Component, forwardRef, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';

@Component({
    selector: 'dp-rate-value',
    templateUrl: './rate-value.component.html',
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => RateValueComponent),
            multi: true
        }
    ]
})
export class RateValueComponent implements ControlValueAccessor, OnInit, OnChanges {
    @Input() disabled: boolean = false;
    @Input() title: string;
    @Input() rateType: number = 0;
    @Input() size: number = 12;

    showPercentage: boolean = false;
    rateValue: number;

    constructor() {
    }

    writeValue(value: any) {
        this.rateValue = value;
        this.propagateChange(this.rateValue);
    }

    propagateChange = (_: any) => {
    };

    registerOnChange(fn: any) {
        this.propagateChange = fn;
    }

    registerOnTouched() {
    }

    ngOnInit() {
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes['rateType'] != null) {
            if (this.rateType == 0) {
                this.showPercentage = false;
            } else if (this.rateType == 1) {
                this.showPercentage = true;
            } else if (this.rateType == 2) {
                this.showPercentage = false;
            } else {
                this.showPercentage = false;
            }
        }
    }

    onChange() {
        this.propagateChange(this.rateValue);
    }
}
