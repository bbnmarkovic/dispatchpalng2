import {
    Component, EventEmitter, forwardRef, Input, OnChanges, OnInit, Output, SimpleChanges,
    ViewChild
} from '@angular/core';
import {AutoComplete} from 'primeng/primeng';
import {ResponseModel} from '../../../../models/response';
import {Vehicle} from '../../../../models/vehicle';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';
import { SharedService } from '../../services/shared.service';

@Component({
    selector: 'dp-truck',
    templateUrl: './truck.component.html',
    styleUrls: ['./truck.component.scss'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => TruckComponent),
            multi: true
        }
    ]
})
export class TruckComponent implements ControlValueAccessor, OnInit, OnChanges {
    @ViewChild('autoVehicleInput') autoVehicleInput : AutoComplete;
    @Output() onTruckSelected = new EventEmitter<any>();
    @Input() disabled: boolean = false;

    itemId: number = 0;
    selectedItem: Vehicle = new Vehicle();
    data: ResponseModel<Vehicle[]> = new ResponseModel<Vehicle[]>();

    isLoading: boolean = false;

    constructor(private _sharedService: SharedService) { }

    writeValue(value: any) {
        this.itemId = value;

        if (this.itemId > 0) {
            this.getItemById();
        } else {
            this.selectedItem = null;
            this.itemId = null;
            this.propagateChange(this.itemId);
            this.onTruckSelected.emit(this.selectedItem);
        }
    }

    propagateChange = (_: any) => { };

    registerOnChange(fn: any) {
        this.propagateChange = fn;
    }

    registerOnTouched() { }

    ngOnInit() {
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes['itemId'] != null) {
            if (this.itemId > 0) {
                this.getItemById();
            } else {
                this.selectedItem = null;
            }
        }
    }

    search(event: any) {
        this.isLoading = true;
        this._sharedService.getVehiclesForAutoComplete(0, event.query).subscribe((data: ResponseModel<Vehicle[]>) => {
            this.isLoading = false;
            this.data = data;
        });
    }

    handleDropdown(event: any) {
        this.isLoading = true;
        this._sharedService.getVehiclesForAutoComplete(0, "").subscribe((data: ResponseModel<Vehicle[]>) => {
            this.isLoading = false;
            this.data = data;
        });
    }

    selectItem(event: any) {
        this.selectedItem = event;
        this.itemId = this.selectedItem.Id;
        this.propagateChange(this.itemId);
        this.onTruckSelected.emit(this.selectedItem);
        this.autoVehicleInput.filled = true;
    }
    clearClick() {
        this.selectedItem = null;
        this.itemId = null;
        this.propagateChange(this.itemId);
        this.onTruckSelected.emit(this.selectedItem);
    }

    getItemById() {
        this._sharedService.getVehicleById(this.itemId).subscribe(
            (data: ResponseModel<Vehicle>) => {
                this.selectedItem = data.Result;
                this.propagateChange(this.itemId);
                this.onTruckSelected.emit(this.selectedItem);
            },
            (error: any) => console.log(error)
        );
    }
}
