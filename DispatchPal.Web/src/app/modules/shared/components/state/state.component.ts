import {Component, EventEmitter, forwardRef, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';
import {State} from '../../../../models/state';
import {ResponseModel} from '../../../../models/response';
import { SharedService } from '../../services/shared.service';

@Component({
    selector: 'dp-state',
    templateUrl: './state.component.html',
    styleUrls: ['./state.component.scss'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => StateComponent),
            multi: true
        }
    ]
})
export class StateComponent implements ControlValueAccessor, OnInit, OnChanges {
    @Output() onStateSelected = new EventEmitter<any>();
    @Input() disabled: boolean = false;

    itemId: number = 0;
    selectedItem: State = new State();
    itemsList: State[] = [];

    isLoading: boolean = false;

    constructor(private _sharedService: SharedService) { }

    writeValue(value: any) {
        this.itemId = value;

        if (this.itemId > 0) {
            this.getItemById();
        } else {
            this.selectedItem = null;
            this.itemId = null;
            this.propagateChange(this.itemId);
            this.onStateSelected.emit(this.selectedItem);
        }
    }

    propagateChange = (_: any) => { };

    registerOnChange(fn: any) {
        this.propagateChange = fn;
    }

    registerOnTouched() { }

    ngOnInit() {
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes['itemId'] != null) {
            if (this.itemId > 0) {
                this.getItemById();
            } else {
                this.selectedItem = null;
            }
        }
    }

    search(event: any) {
        this.isLoading = true;
        this._sharedService.getStatesByFilter(event.query).subscribe((data: ResponseModel<State[]>) => {
            this.isLoading = false;
            this.itemsList = data.Result;
        });
    }

    handleDropdown(event: any) {
        this.isLoading = true;
        this._sharedService.getStatesByFilter("").subscribe((data: ResponseModel<State[]>) => {
            this.isLoading = false;
            this.itemsList = data.Result;
        });
    }

    selectItem(event: any) {
        this.selectedItem = event;
        this.itemId = this.selectedItem.Id;
        this.propagateChange(this.itemId);
        this.onStateSelected.emit(this.selectedItem);
    }

    clearClick() {
        this.selectedItem = null;
        this.itemId = null;
        this.propagateChange(this.itemId);
        this.onStateSelected.emit(this.selectedItem);
    }

    getItemById() {
        this._sharedService.getStateById(this.itemId).subscribe(
            (data: ResponseModel<State>) => {
                this.selectedItem = data.Result;
                this.propagateChange(this.itemId);
                this.onStateSelected.emit(this.selectedItem);
            },
            (error: any) => console.log(error)
        );
    }
}
