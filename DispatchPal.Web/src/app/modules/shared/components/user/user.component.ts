import {Component, EventEmitter, forwardRef, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {User} from '../../../../models/user';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';
import {ResponseModel} from '../../../../models/response';
import { SharedService } from '../../services/shared.service';

@Component({
    selector: 'dp-user',
    templateUrl: './user.component.html',
    styleUrls: ['./user.component.scss'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => UserComponent),
            multi: true
        }
    ]
})

export class UserComponent implements ControlValueAccessor, OnInit, OnChanges {
  @Output() onUserSelected = new EventEmitter<any>();
  @Input() disabled: boolean = false;

  itemId: number = 0;
  selectedItem: User = new User();
  data: ResponseModel<User[]> = new ResponseModel<User[]>();

  isLoading: boolean = false;

  constructor(private _sharedService: SharedService) {
  }

  writeValue(value: any) {
    this.itemId = value;

    if (this.itemId > 0) {
      this.getItemById();
    } else {
      this.selectedItem = null;
      this.itemId = null;
      this.propagateChange(this.itemId);
      this.onUserSelected.emit(this.selectedItem);
    }
  }

  propagateChange = (_: any) => {
  };

  registerOnChange(fn: any) {
    this.propagateChange = fn;
  }

  registerOnTouched() {
  }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['itemId'] != null) {
      if (this.itemId > 0) {
        this.getItemById();
      } else {
        this.selectedItem = null;
      }
    }
  }

  search(event: any) {
    this.isLoading = true;
    this._sharedService.getUsersForAutoComplete(event.query).subscribe((data: ResponseModel<User[]>) => {
      this.isLoading = false;
      this.data = data;
      this.data.Result.map((item) => {
        item.FullName = `${item.FirstName} ${item.LastName}`;
        return item;
      });
    });
  }

  handleDropdown(event: any) {
    this.isLoading = true;
    this._sharedService.getUsersForAutoComplete("").subscribe((data: ResponseModel<User[]>) => {
      this.isLoading = false;
      this.data = data;
      this.data.Result.map((item) => {
        item.FullName = `${item.FirstName} ${item.LastName}`;
        return item;
      });
    });
  }

  selectItem(event: any) {
    this.selectedItem = event;
    this.itemId = this.selectedItem.Id;
    this.propagateChange(this.itemId);
    this.onUserSelected.emit(this.selectedItem);
  }

  clearClick() {
    this.selectedItem = null;
    this.itemId = null;
    this.propagateChange(this.itemId);
    this.onUserSelected.emit(this.selectedItem);
  }

  getItemById() {
    this._sharedService.getUserById(this.itemId).subscribe(
      (data: ResponseModel<User>) => {
        this.selectedItem = data.Result;
        this.propagateChange(this.itemId);
        this.onUserSelected.emit(this.selectedItem);
      },
      (error: any) => console.log(error)
    );
  }
}
