import {Component, Injectable, OnInit} from '@angular/core';
import {Subject} from 'rxjs/Subject';

export enum LoaderEventTypes {
    SHOW,
    HIDE
}

@Injectable()
export class LoaderService {
    private _subject: Subject<LoaderEventTypes> = new Subject<LoaderEventTypes>();

    public get subject(): Subject<LoaderEventTypes> {
        return this._subject;
    }

    public show() {
        this._subject.next(LoaderEventTypes.SHOW);
    }

    public hide() {
        this._subject.next(LoaderEventTypes.HIDE);
    }
}

@Component({
    selector: 'dp-loader',
    templateUrl: './loader.component.html',
    styleUrls: ['./loader.component.scss']
})
export class LoaderComponent implements OnInit {

    display: boolean = false;

    constructor(
        private loaderService: LoaderService
    ) { }

    ngOnInit() {
        this.loaderService.subject.subscribe((v: LoaderEventTypes) => {
            switch (v) {
                case LoaderEventTypes.SHOW:
                    this.show();
                    break;
                case LoaderEventTypes.HIDE:
                    this.hide();
                    break;
            }
        });
    }

    public show() {
        this.display = true;
    }

    public hide() {
        this.display = false;
    }
}
