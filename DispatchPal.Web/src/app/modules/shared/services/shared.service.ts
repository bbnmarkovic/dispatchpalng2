import { Injectable } from '@angular/core';
import { ResponseModel } from '../../../models/response';
import { HttpClient, HttpParams } from '@angular/common/http';
import { LoadForAutocomplete } from '../../../models/load-for-autocomplete';
import { environment } from '../../../../environments/environment';
import { Store } from '@ngrx/store';
import { Company } from '../../../models/company';
import { IAppState } from '../../../redux/app_state';
import { Observable } from 'rxjs/Observable';
import { Load } from '../../../models/load';
import { Vehicle } from '../../../models/vehicle';
import { Driver } from '../../../models/driver';
import { Client } from '../../../models/client';
import { ClientType } from '../../../models/client-type';
import { State } from '../../../models/state';
import { Broker } from '../../../models/broker';
import { User } from '../../../models/user';

@Injectable()
export class SharedService {
  private _returnUrl: string;
  companyId: number;

  constructor(private http: HttpClient, private _store: Store<IAppState>) {
    _store.select('activeCompany').subscribe((state: Company) => {
      this.companyId = state.Id;
    });
  }

  public get returnUrl(): string {
    return this._returnUrl;
  }

  public set returnUrl(value: string) {
    this._returnUrl = value;
  }

  getLoadsForAutoComplete(searchParam: string) {
    const url = `${environment.api}/loads/autocomplete`;

    let params: HttpParams = new HttpParams();
    params = params.append('offset', "0");
    params = params.append('pageSize', "25");
    params = params.append('searchParam', searchParam);
    params = params.append('companyId', this.companyId.toString());
    params.set('__showLoader', "false");

    return this.http.get<ResponseModel<LoadForAutocomplete[]>>(url, {
      params: params
    });
  }

  getLoadForAutoCompleteById(id: number): Observable<ResponseModel<LoadForAutocomplete>> {
    const url = `${environment.api}/loads/autocomplete/${id}`;

    return this.http.get<ResponseModel<LoadForAutocomplete>>(url);
  }


  getLoadById(id: number) {
    const url = `${environment.api}/loads/${id}`;

    return this.http.get<ResponseModel<Load>>(url);
  }


  deleteFileFromLoad(id: number) {
    const url = `${environment.api}/loads/files/${id}/remove`;

    return this.http.get<ResponseModel<boolean>>(url);
  }

  // previewFile(id: number) {
  // return this.http.get(ConfigService.getApiEndpoint() + 'dispatch/files/preview/' + id, { responseType: ResponseContentType.Blob }).map(LoadsService.mapBlobData);
  // }

  getDriversForAutoComplete(searchParam: string) {
    const url = `${environment.api}/drivers/auto-complete`;

    let params: HttpParams = new HttpParams();
    params = params.append('searchParam', searchParam);
    params = params.append('companyId', this.companyId.toString());
    params = params.append('pageSize', "false");

    return this.http.get<ResponseModel<Driver[]>>(url, {
      params: params
    });
  }

  getDriverById(id: number) {
    const url = `${environment.api}/drivers/${id}`;

    return this.http.get<ResponseModel<Driver>>(url);
  }

  getVehiclesForAutoComplete(vehicleType: number, searchParam: string) {
    const url = `${environment.api}/vehicles`;

    let params: HttpParams = new HttpParams();
    params = params.append('vehicleType', vehicleType.toString());
    params = params.append('offset', "0");
    params = params.append('pageSize', "25");
    params = params.append('searchParam', searchParam);
    params = params.append('active', "true");
    params = params.append('companyId', this.companyId.toString());
    params = params.append('__showLoader', "false");

    return this.http.get<ResponseModel<Vehicle[]>>(url, {
      params: params
    });
  }

  getVehicleById(id: number) {
    const url = `${environment.api}/vehicles/${id}`;

    return this.http.get<ResponseModel<Vehicle>>(url);
  }

  getClientsForAutoComplete(clientType: number, searchParam: string): Observable<ResponseModel<Client[]>> {
    const url = `${environment.api}/clients`;

    let params: HttpParams = new HttpParams();
    params = params.append('offset', "0");
    params = params.append('pageSize', "25");
    params = params.append('searchParam', searchParam);
    params = params.append('active', "true");
    params = params.append('clientType', clientType.toString());
    params = params.append('__showLoader', "false");

    return this.http.get<ResponseModel<Client[]>>(url, {
      params: params
    });
  }

  getClientById(id: number): Observable<ResponseModel<Client>> {
    const url = `${environment.api}/clients/${id}`;

    return this.http.get<ResponseModel<Client>>(url);
  }

  getClientTypes(searchParam: string): Observable<ResponseModel<ClientType[]>> {
    const url = `${environment.api}/clients/types`;

    let params: HttpParams = new HttpParams();
    params = params.append('searchParam', searchParam);

    return this.http.get<ResponseModel<ClientType[]>>(url, {
      params: params
    });
  }

  getClientTypeById(id: number): Observable<ResponseModel<ClientType>> {
    const url = `${environment.api}/clients/types/${id}`;

    return this.http.get<ResponseModel<ClientType>>(url);
  }

  getStateById(id: number) {
    const url = `${environment.api}/states/${id}`;

    return this.http.get<ResponseModel<State>>(url);
  }

  getStatesByFilter(searchParam: string) {
    const url = `${environment.api}/states`;

    let params: HttpParams = new HttpParams();
    params = params.append('searchParam', searchParam);
    params = params.append('__showLoader', "false");

    return this.http.get<ResponseModel<State[]>>(url, {
      params: params
    });
  }

  getBrokersForAutoComplete(brokerageId: number, searchParam: string) {
    const url = `${environment.api}/brokers`;

    let params: HttpParams = new HttpParams();
    params = params.append('offset', "0");
    params = params.append('pageSize', "25");
    params = params.append('searchParam', searchParam);
    params = params.append('active', "true");
    params = params.append('brokerageId', brokerageId.toString());
    params = params.append('__showLoader', "false");

    return this.http.get<ResponseModel<Broker[]>>(url, {
      params: params
    });
  }

  getBrokerById(id: number) {
    const url = `${environment.api}/brokers/${id}`;

    return this.http.get<ResponseModel<Broker>>(url);
  }

  saveBroker(client: Broker) {
    const url = `${environment.api}/brokers`;

    return this.http.post<ResponseModel<Broker>>(url, client);
  }

  getCompaniesForAutoComplete(searchParam: string) {
    const url = `${environment.api}/companies`;

    let params: HttpParams = new HttpParams();
    params = params.append('offset', "0");
    params = params.append('pageSize', "25");
    params = params.append('searchParam', searchParam);
    params = params.append('active', "true");
    /*params = params.append('__showLoader', "false");*/

    return this.http.get<ResponseModel<Company[]>>(url, {
      params: params
    });
  }

  getCompanyById(id: number) {
    const url = `${environment.api}/companies/${id}`;

    return this.http.get<ResponseModel<Company>>(url);
  }

  getUsersForAutoComplete(searchParam: string) {
    const url = `${environment.api}/users`;

    let params: HttpParams = new HttpParams();
    params = params.append('offset', "0");
    params = params.append('pageSize', "25");
    params = params.append('searchParam', searchParam);
    params = params.append('active', "true");
    params = params.append('__showLoader', "false");

    return this.http.get<ResponseModel<User[]>>(url, {
      params: params
    });
  }

  getUserById(id: number) {
    const url = `${environment.api}/users/${id}`;

    return this.http.get<ResponseModel<User>>(url);
  }
}
