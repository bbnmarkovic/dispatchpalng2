import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DispatchPayrollComponent } from './dispatch-payroll.component';

describe('DispatchPayrollComponent', () => {
  let component: DispatchPayrollComponent;
  let fixture: ComponentFixture<DispatchPayrollComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DispatchPayrollComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DispatchPayrollComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
