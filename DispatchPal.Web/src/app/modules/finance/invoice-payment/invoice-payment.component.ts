import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {InvoicePayment} from '../../../models/invoice_payment';
import { FinanceService } from '../finance.service';

@Component({
  selector: 'dp-invoice-payment',
  templateUrl: './invoice-payment.component.html',
  styleUrls: ['./invoice-payment.component.scss']
})
export class InvoicePaymentComponent implements OnInit {
    @Output() onInvoicePaymentSaved = new EventEmitter();

    display: boolean = false;
    invoicePayment: InvoicePayment = new InvoicePayment();

    constructor(private _financeService: FinanceService) { }

    ngOnInit() {
    }

    show(item: InvoicePayment){
        this.invoicePayment = item;
        this.display = true;
    }

    hide(){
        this.display = false;
    }

    onSubmit(){
        this._financeService.saveInvoicePayment(this.invoicePayment).subscribe((res)=>{
            this.display = false;
            this.onInvoicePaymentSaved.emit()
        });
    }
}

