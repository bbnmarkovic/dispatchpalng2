import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CashFlowDetailsComponent } from './cash-flow-details.component';

describe('CashFlowDetailsComponent', () => {
  let component: CashFlowDetailsComponent;
  let fixture: ComponentFixture<CashFlowDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CashFlowDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CashFlowDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
