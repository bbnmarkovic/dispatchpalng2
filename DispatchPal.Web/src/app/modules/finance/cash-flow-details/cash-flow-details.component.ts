import {AfterViewInit, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {CashFlowType} from '../../../models/cash-flow-type';
import {SelectItem} from 'primeng/components/common/api';
import {MessagesService} from '../../shared/components/messages/messages.component';
import {ResponseModel} from '../../../models/response';
import {CashFlow} from '../../../models/cash-flow';
import { FinanceService } from '../finance.service';
import { parse } from 'date-fns';

@Component({
  selector: 'dp-cash-flow-details',
  templateUrl: './cash-flow-details.component.html',
  styleUrls: ['./cash-flow-details.component.scss']
})
export class CashFlowDetailsComponent implements OnInit, AfterViewInit {
    @Input() cashFlowId: number;
    @Output() onCashFlowSaved = new EventEmitter<any>();
    cashFlowTypes: Array<SelectItem> = [];

    constructor(
        private _financeService: FinanceService,
        private _messagesService: MessagesService) { }

    item: CashFlow = new CashFlow();
    display: boolean = false;

    ngOnInit() {

    }

    ngAfterViewInit() {
        this.getTypes();
    }

    getItem() {
        if (this.cashFlowId > 0) {
            this._financeService.getCashFlowById(this.cashFlowId)
                .subscribe((data: ResponseModel<CashFlow>) => {
                    this.item = data.Result;
                    if (this.item.Date != null) {
                      this.item.Date = parse(this.item.Date);
                    }
                });
        } else {
            this.item = new CashFlow();
            this.item.Id = 0;
            this.item.TypeId = 1;
            this.item.Amount = 0;
            this.item.Date = new Date();
            this.item.IsActive = true;
            this.item.IsSettled = false;
        }
    }

    getTypes() {
        if (this.cashFlowTypes.length == 0) {
            this._financeService.getCashFlowTypes()
                .subscribe((data: ResponseModel<CashFlowType[]>) => {
                    for (let item of data.Result) {
                        this.cashFlowTypes.push({ label: item.Name, value: item.Id });
                    }
                });
        }
    }

    onSubmit() {
        this._financeService.saveCashFlow(this.item)
            .subscribe((data: ResponseModel<CashFlow>) => {
                this.item = data.Result;
                this.display = false;
                this.onCashFlowSaved.emit();
                this._messagesService.success('', 'Cash flow details saved.');
            });
    }

    public showDialog(id: number) {
        this.display = true;
        this.cashFlowId = id;
        this.getItem();
    }

    public hideDialog() {
        this.display = false;
    }
}
