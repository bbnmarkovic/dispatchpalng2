import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Filter} from '../../../models/filter';
import {Subscription} from 'rxjs/Subscription';
import {InvoicePreview} from '../../../models/invoice_preview';
import {ResponseModel} from '../../../models/response';
import {DataTable, LazyLoadEvent} from 'primeng/primeng';
import {IAppState} from '../../../redux/app_state';
import {Store} from '@ngrx/store';
import {saveFilterPaidInvoices} from '../../../redux/actions/filter.actions';
import { FinanceService } from '../finance.service';

@Component({
  selector: 'dp-paid-invoices',
  templateUrl: './paid-invoices.component.html',
  styleUrls: ['./paid-invoices.component.scss']
})
export class PaidInvoicesComponent implements OnInit, OnDestroy {
    @ViewChild('dataTable') public dataTable: DataTable;

    emptyMsg: string = "Loading...";
    filter: Filter = new Filter();

    items: InvoicePreview[] = [];
    totalItems: number = 0;

    subscriptions: Subscription[];

    constructor(
        private _store: Store<IAppState>,
        private _financeService: FinanceService)
    {
        this.subscriptions = [];
    }

    ngOnInit() {
        this.subscriptions.push(this._store.select('filterPaidInvoices').subscribe((state: Filter) => {
            this.filter = state;
            this.getData();
        }));

        this.subscriptions.push(this._store.select('activeCompany').subscribe(()=>{
            this.getData();
        }));
    }

    ngOnDestroy() {
        this.subscriptions.forEach((item: Subscription)=>{
            item.unsubscribe();
        });
    }

    getData(){
        this._financeService.getPaidInvoices(this.filter)
            .subscribe((data: ResponseModel<InvoicePreview[]>) => {
                if (data !== null) {
                    this.totalItems = data.TotalItems;
                    this.items = data.Result;

                    if(this.totalItems == 0)
                        this.emptyMsg = "No records...";
                }
            });
    }

    ngAfterViewInit() {
        this.dataTable['first'] = this.filter.Offset;
    }

    onSubmitSearch() {
        this._store.dispatch(new saveFilterPaidInvoices(this.filter));
    }

    loadData(event: LazyLoadEvent) {
        this.filter.PageSize = event.rows;
        this.filter.Offset = event.first;

        this.onSubmitSearch();
    }

    resetFilter() {
        this.filter.Offset = 0;
        this.filter.SearchParam = "";

        this.onSubmitSearch();
    }

}
