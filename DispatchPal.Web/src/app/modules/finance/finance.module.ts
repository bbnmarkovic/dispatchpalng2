import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FinanceComponent } from './finance.component';
import { FinanceRoutingModule } from './finance-routing.module';
import { PendingInvoicesComponent } from './pending-invoices/pending-invoices.component';
import {FormsModule} from '@angular/forms';
import {DispatchPalSharedModule} from '../shared/shared.module';
import {
    CalendarModule, ConfirmDialogModule, DataTableModule, DialogModule, DropdownModule,
    MultiSelectModule, SpinnerModule
} from 'primeng/primeng';
import { PaidInvoicesComponent } from './paid-invoices/paid-invoices.component';
import { CashFlowComponent } from './cash-flow/cash-flow.component';
import { CashFlowDetailsComponent } from './cash-flow-details/cash-flow-details.component';
import { InvoiceComponent } from './invoice/invoice.component';
import { InvoiceItemComponent } from './invoice-item/invoice-item.component';
import { InvoicePaymentComponent } from './invoice-payment/invoice-payment.component';
import { DriverPayrollComponent } from './driver-payroll/driver-payroll.component';
import { DispatchPayrollComponent } from './dispatch-payroll/dispatch-payroll.component';
import { FinanceService } from './finance.service';
import { FlexLayoutModule } from '@angular/flex-layout';

@NgModule({
  imports: [
    FormsModule,
    CommonModule,
    DispatchPalSharedModule,
    FlexLayoutModule,
    FinanceRoutingModule,
    DataTableModule,
    DropdownModule,
    CalendarModule,
    ConfirmDialogModule,
    MultiSelectModule,
    DialogModule,
    SpinnerModule
  ],
  declarations: [
    FinanceComponent,
    PendingInvoicesComponent,
    PaidInvoicesComponent,
    CashFlowComponent,
    CashFlowDetailsComponent,
    InvoiceComponent,
    InvoiceItemComponent,
    InvoicePaymentComponent,
    DriverPayrollComponent,
    DispatchPayrollComponent
  ],
  providers: [
    FinanceService
  ]
})
export class FinanceModule { }
