import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {ConfirmationService} from 'primeng/components/common/api';
import {InvoiceItem} from '../../../models/invoice_item';
import { FinanceService } from '../finance.service';

@Component({
  selector: 'dp-invoice-item',
  templateUrl: './invoice-item.component.html',
  styleUrls: ['./invoice-item.component.scss']
})
export class InvoiceItemComponent implements OnInit {
    @Output() onInvoiceItemSaved = new EventEmitter();

    display: boolean = false;
    invoiceItem: InvoiceItem = new InvoiceItem();

    constructor(private route: ActivatedRoute,
                private _financeService: FinanceService,
                private _confirmationService: ConfirmationService) { }

    ngOnInit() {
    }

    show(item: InvoiceItem){
        this.invoiceItem = item;
        this.display = true;
    }

    hide(){
        this.display = false;
    }

    onSubmit(){
        this._financeService.saveInvoiceItem(this.invoiceItem).subscribe((res)=>{
            this.display = false;
            this.onInvoiceItemSaved.emit()
        });
    }
}

