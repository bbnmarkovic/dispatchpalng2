import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Subscription} from 'rxjs/Subscription';
import {Filter} from '../../../models/filter';
import {ResponseModel} from '../../../models/response';
import {Company} from '../../../models/company';
import {LazyLoadEvent} from 'primeng/primeng';
import {IAppState} from '../../../redux/app_state';
import {SelectItem} from 'primeng/components/common/api';
import {CashFlow} from '../../../models/cash-flow';
import {Store} from '@ngrx/store';
import {saveFilterCashFlow} from '../../../redux/actions/filter.actions';
import {CashFlowDetailsComponent} from '../cash-flow-details/cash-flow-details.component';
import { FinanceService } from '../finance.service';

@Component({
  selector: 'dp-cash-flow',
  templateUrl: './cash-flow.component.html',
  styleUrls: ['./cash-flow.component.scss']
})
export class CashFlowComponent implements OnInit, OnDestroy {
    @ViewChild("cashFlowDialog") cashFlowDialog: CashFlowDetailsComponent;
    selectedColumns: any[];
    columnOptions: SelectItem[];
    subscriptions: Subscription[];
    companyId: number;

    constructor(
        private _store: Store<IAppState>,
        private _financeService: FinanceService) {
        this.subscriptions = [];
    }

    filter: Filter = new Filter();
    emptyMsg: string = "Loading...";

    items: CashFlow[] = [];
    totalItems: number = 0;

    ngOnInit() {
        this._store.select('filterCashFlow').subscribe((state: Filter) => {
            this.filter = state;
            this.getData();
        });

        this.subscriptions.push(this._store.select('activeCompany').subscribe((cmp: Company)=>{
            if(this.companyId > 0 && this.companyId != cmp.Id) {
                this.getData();
            }
            this.companyId = cmp.Id;
        }));
    }

    ngOnDestroy(){
        this.subscriptions.forEach((item: Subscription)=>{
            item.unsubscribe();
        });
    }

    getData(){
        this._financeService.getCashFlowByFilter(this.filter)
            .subscribe((data: ResponseModel<CashFlow[]>) => {
                if (data !== null) {
                    this.totalItems = data.TotalItems;
                    this.items = data.Result;
                }
            });
    }

    onSubmitSearch() {
        this._store.dispatch(new saveFilterCashFlow(this.filter));
    }

    loadData(event: LazyLoadEvent) {
        this.filter.PageSize = event.rows;
        this.filter.Offset = event.first;

        this.onSubmitSearch();
    }

    resetFilter() {
        this.filter.Offset = 0;
        this.filter.SearchParam = "";

        this.onSubmitSearch();
    }

    createCashFlow() {
        this.cashFlowDialog.showDialog(0);
    }

    editCashFlow(item: any) {
        this.cashFlowDialog.showDialog(item.Id);
    }

    onCashFlowSaved() {
        this._financeService.getCashFlowByFilter(this.filter)
            .subscribe((data: ResponseModel<CashFlow[]>) => {
                if (data !== null) {
                    this.totalItems = data.TotalItems;
                    this.items = data.Result;
                }
            });
    }
}
