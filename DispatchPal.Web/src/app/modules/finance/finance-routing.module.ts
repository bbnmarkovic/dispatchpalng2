import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {FinanceComponent} from './finance.component';
import {PendingInvoicesComponent} from './pending-invoices/pending-invoices.component';
import {PaidInvoicesComponent} from './paid-invoices/paid-invoices.component';
import {CashFlowComponent} from './cash-flow/cash-flow.component';
import {InvoiceComponent} from './invoice/invoice.component';
import {DriverPayrollComponent} from './driver-payroll/driver-payroll.component';
import {DispatchPayrollComponent} from './dispatch-payroll/dispatch-payroll.component';

const routes: Routes = [
  {
    path: '',
    component: FinanceComponent,
    children: [
      {
        path: 'pending-invoices',
        component: PendingInvoicesComponent
      },
      {
        path: 'paid-invoices',
        component: PaidInvoicesComponent
      },
      {
        path: 'cash-flow',
        component: CashFlowComponent
      },
      {
        path: 'invoices/:id',
        component: InvoiceComponent
      },
      {
        path: 'driver-payroll',
        component: DriverPayrollComponent
      },
      {
        path: 'dispatch-payroll',
        component: DispatchPayrollComponent
      }
    ]
  }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class FinanceRoutingModule { }
