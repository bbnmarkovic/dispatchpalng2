import {Component, OnInit, ViewChild} from '@angular/core';
import {Subscription} from 'rxjs/Subscription';
import {ConfirmationService} from 'primeng/components/common/api';
import {ResponseModel} from '../../../models/response';
import {InvoiceItem} from '../../../models/invoice_item';
import {Company} from '../../../models/company';
import {IAppState} from '../../../redux/app_state';
import {Location} from '@angular/common';
import {Invoice} from '../../../models/invoice';
import {InvoicePayment} from '../../../models/invoice_payment';
import {Store} from '@ngrx/store';
import {ActivatedRoute, Router} from '@angular/router';
import {InvoiceItemComponent} from '../invoice-item/invoice-item.component';
import {InvoicePaymentComponent} from '../invoice-payment/invoice-payment.component';
import { FinanceService } from '../finance.service';
import { parse } from 'date-fns';

@Component({
  selector: 'dp-invoice',
  templateUrl: './invoice.component.html',
  styleUrls: ['./invoice.component.scss']
})
export class InvoiceComponent implements OnInit {
  @ViewChild("invoiceDetails") invoiceDetails: InvoiceItemComponent;
  @ViewChild("invoicePaymentDetails") invoicePaymentDetails: InvoicePaymentComponent;
  companyId: number;
  subscriptions: Subscription[];
  loadId: number;
  invoice: Invoice = new Invoice();
  invoiceItems: InvoiceItem[] = [];
  invoicePayments: InvoicePayment[] = [];

  constructor(private route: ActivatedRoute,
              private router: Router,
              private _store: Store<IAppState>,
              private _financeService: FinanceService,
              private _location: Location,
              private _confirmationService: ConfirmationService) {
    this.subscriptions = [];
  }

  ngOnInit() {
    this.route.params.subscribe((v: { id: string }) => {
      this.loadId = parseInt(v.id);
      this.loadInvoice();
    });

    this.subscriptions.push(this._store.select('activeCompany').subscribe((cmp: Company) => {
      if (this.companyId > 0 && this.companyId != cmp.Id) {
        this.router.navigate(['/trips']);
      }
      this.companyId = cmp.Id;
    }));
  }

  loadInvoice() {
    this._financeService.getInvoiceById(this.loadId).subscribe((data: ResponseModel<Invoice>) => {
      if (data != null) {
        this.invoice = data.Result;
        if (this.invoice.InvoiceDate != null) {
          this.invoice.InvoiceDate = parse(this.invoice.InvoiceDate);
        }
        if (this.invoice.InvoiceDueDate != null) {
          this.invoice.InvoiceDueDate = parse(this.invoice.InvoiceDueDate);
        }
        if (this.invoice.PaymentDate != null) {
          this.invoice.PaymentDate = parse(this.invoice.PaymentDate);
        }
        this.invoiceItems = data.Result.InvoiceItems;
        this.invoicePayments = data.Result.InvoicePayments;
        this.invoicePayments.map((item) => {
          if (item.PaymentDate != null) {
            item.PaymentDate = parse(item.PaymentDate);
          }
          return item;
        })
      }
    });
  }

  onSubmitInvoiceDetails() {
    this._financeService.saveInvoice(this.invoice).subscribe((data: ResponseModel<Invoice>) => {
      if (data != null) {
        this.invoice = data.Result;
        this.invoiceItems = data.Result.InvoiceItems;
      }
    });
  }

  regenerateInvoiceItems() {
    this._confirmationService.confirm({
      message: 'Are you sure that you want regenerate invoice items? All current items will be removed and new items will be generated.',
      accept: () => {
        this._financeService.regenerateInvoiceItems(this.invoice.LoadId).subscribe((data: ResponseModel<Invoice>) => {
          if (data != null) {
            this.invoice = data.Result;
            this.invoiceItems = data.Result.InvoiceItems;
          }
        });
      }
    });

  }

  editInvoiceItem(item: InvoiceItem) {
    this.invoiceDetails.show(item);
  }

  createInvoiceItem() {
    let item: InvoiceItem = new InvoiceItem();
    item.InvoiceId = this.invoice.LoadId;
    item.Quantity = 1;
    item.Discount = 0;
    item.DiscountAmmount = 0;
    item.Price = 0;
    this.invoiceDetails.show(item);
  }

  onInvoiceItemSaved() {
    this.loadInvoice();
  }

  deleteInvoiceItem(item: InvoiceItem) {
    this._confirmationService.confirm({
      message: 'Are you sure that you want to remove selected invoice item?',
      accept: () => {
        this._financeService.deleteInvoiceItems(item.Id).subscribe((data: ResponseModel<boolean>) => {
          this.loadInvoice();
        });
      }
    });
  }

  createInvoicePayment() {
    let item: InvoicePayment = new InvoicePayment();
    item.InvoiceId = this.invoice.LoadId;
    item.Amount = 0;
    item.PaymentDate = new Date();
    item.PayedBy = "";
    this.invoicePaymentDetails.show(item);
  }

  editInvoicePayment(item: InvoicePayment) {
    this.invoicePaymentDetails.show(item);
  }

  deleteInvoicePayment(item: InvoicePayment) {
    this._confirmationService.confirm({
      message: 'Are you sure that you want to remove selected invoice payment?',
      accept: () => {
        this._financeService.deleteInvoicePayment(item.Id).subscribe((data: ResponseModel<boolean>) => {
          this.loadInvoice();
        });
      }
    });
  }

  onInvoicePaymentSaved() {
    this.loadInvoice();
  }

  createAndEmailPdf() {
    this._financeService.createAndEmailPdf(this.invoice).subscribe((data: any) => {
      /*let res = new Blob([data], {type: "application/pdf"});
      let fileURL = window.URL.createObjectURL(res);
      window.open(fileURL);*/
    }, error => {
      let temp = error;
    });
  }

  previewPdf() {
    /*this.invoiceService.previewPdf(this.invoice).subscribe((data: any) => {
        let res = new Blob([data], {type: "application/pdf"});
        let fileURL = window.URL.createObjectURL(res);
        window.open(fileURL);
    }, (error => {
        let temp = error;
    }));*/
  }

  back() {
    this._location.back();
  }
}
