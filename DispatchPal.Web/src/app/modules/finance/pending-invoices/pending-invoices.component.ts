import {AfterViewInit, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Filter} from '../../../models/filter';
import {Subscription} from 'rxjs/Subscription';
import {InvoicePreview} from '../../../models/invoice_preview';
import {ResponseModel} from '../../../models/response';
import {DataTable, LazyLoadEvent, SelectItem} from 'primeng/primeng';
import {IAppState} from '../../../redux/app_state';
import {Store} from '@ngrx/store';
import {saveFilterPendingInvoices} from '../../../redux/actions/filter.actions';
import { FinanceService } from '../finance.service';

@Component({
  selector: 'dp-pending-invoices',
  templateUrl: './pending-invoices.component.html',
  styleUrls: ['./pending-invoices.component.scss']
})
export class PendingInvoicesComponent implements OnInit, OnDestroy, AfterViewInit {
    @ViewChild('dataTable') public dataTable: DataTable;

    emptyMsg: string = "Loading...";
    filter: Filter = new Filter();
    invoiceAgings: Array<SelectItem> = [];

    items: InvoicePreview[] = [];
    totalItems: number = 0;

    subscriptions: Subscription[];

    constructor(
        private _store: Store<IAppState>,
        private _financeService: FinanceService)
    {
        this.subscriptions = [];
        this.invoiceAgings.push({ label: 'All', value: 0 });
        this.invoiceAgings.push({ label: '0-30', value: 30 });
        this.invoiceAgings.push({ label: '30-60', value: 60 });
        this.invoiceAgings.push({ label: '60-90', value: 90 });
        this.invoiceAgings.push({ label: '90+', value: 100000 });
    }


    ngOnInit() {
        this.subscriptions.push(this._store.select('filterPendingInvoices').subscribe((state: Filter) => {
            this.filter = state;
            this.getData();
        }));

        this.subscriptions.push(this._store.select('activeCompany').subscribe(()=>{
            this.getData();
        }));
    }

    ngOnDestroy() {
        this.subscriptions.forEach((item: Subscription)=>{
            item.unsubscribe();
        });
    }

    getData(){
        this._financeService.getPendingInvoices(this.filter)
            .subscribe((data: ResponseModel<InvoicePreview[]>) => {
                if (data !== null) {
                    this.totalItems = data.TotalItems;
                    this.items = data.Result;

                    if(this.totalItems == 0)
                        this.emptyMsg = "No records...";
                }
            });
    }

    ngAfterViewInit() {
        this.dataTable['first'] = this.filter.Offset;
    }

    onSubmitSearch() {
        this._store.dispatch(new saveFilterPendingInvoices(this.filter));
    }

    loadData(event: LazyLoadEvent) {
        this.filter.PageSize = event.rows;
        this.filter.Offset = event.first;

        this.onSubmitSearch();
    }

    resetFilter() {
        this.filter.Offset = 0;
        this.filter.SearchParam = "";

        this.onSubmitSearch();
    }

}
