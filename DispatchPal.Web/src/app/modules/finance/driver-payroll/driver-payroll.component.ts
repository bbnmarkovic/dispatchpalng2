import {Component, OnDestroy, OnInit} from '@angular/core';
import {Filter} from '../../../models/filter';
import {IAppState} from '../../../redux/app_state';
import {ResponseModel} from '../../../models/response';
import {Store} from '@ngrx/store';
import {LazyLoadEvent} from 'primeng/primeng';
import {Subscription} from 'rxjs/Rx';
import {DriverPayroll} from '../../../models/driver-payroll';
import { FinanceService } from '../finance.service';

@Component({
  selector: 'dp-driver-payroll',
  templateUrl: './driver-payroll.component.html',
  styleUrls: ['./driver-payroll.component.scss']
})
export class DriverPayrollComponent implements OnInit, OnDestroy {
    emptyMsg: string = "Loading...";
    filter: Filter = new Filter();
    driverId: number = 0;
    driverPayrolls: DriverPayroll[] = [];
    totalItems: number = 0;
    subscriptions: Subscription[];

    constructor(
        private _store: Store<IAppState>,
        private _financeService: FinanceService)
    {
        this.subscriptions = [];
    }

    ngOnInit() {
        this.getData();

        this.subscriptions.push(this._store.select('activeCompany').subscribe(()=>{
            this.getData();
        }));
    }

    ngOnDestroy() {
        this.subscriptions.forEach((item: Subscription)=>{
            item.unsubscribe();
        });
    }

    getData(){
        this._financeService.getDriverPayroll(this.filter, this.driverId)
            .subscribe((data: ResponseModel<DriverPayroll[]>) => {
                if (data !== null) {
                    this.totalItems = data.TotalItems;
                    this.driverPayrolls = data.Result;

                    if(this.totalItems == 0)
                        this.emptyMsg = "No records...";
                }
            });
    }

    loadData(event: LazyLoadEvent) {
        this.filter.PageSize = event.rows;
        this.filter.Offset = event.first;

        this.onSubmitSearch();
    }

    onSubmitSearch() {
        this.getData();
    }
}
