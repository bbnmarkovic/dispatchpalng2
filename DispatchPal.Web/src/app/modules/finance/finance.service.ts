import { Injectable } from '@angular/core';
import { ResponseModel } from '../../models/response';
import { HttpClient, HttpParams } from '@angular/common/http';
import { DriverPayroll } from '../../models/driver-payroll';
import { environment } from '../../../environments/environment';
import { Filter } from '../../models/filter';
import { Store } from '@ngrx/store';
import { Company } from '../../models/company';
import { IAppState } from '../../redux/app_state';
import { CashFlow } from '../../models/cash-flow';
import { CashFlowType } from '../../models/cash-flow-type';
import { InvoiceItem } from '../../models/invoice_item';
import { Invoice } from '../../models/invoice';
import { InvoicePreview } from '../../models/invoice_preview';
import { InvoicePayment } from '../../models/invoice_payment';

@Injectable()
export class FinanceService {
  companyId: number;

  constructor(private http: HttpClient, private _store: Store<IAppState>) {
    _store.select('activeCompany').subscribe((state: Company) => {
      this.companyId = state.Id;
    });
  }

  getDriverPayroll(filter: Filter, driverId: number) {
    const url = `${environment.api}/loads/driver-payroll/${driverId}`;

    let params: HttpParams = new HttpParams();
    params = params.append('offset', filter.Offset.toString());
    params = params.append('pageSize', filter.PageSize.toString());
    params = params.append('companyId', this.companyId.toString());

    return this.http.get<ResponseModel<DriverPayroll[]>>(url, {
      params: params
    });
  }

  getCashFlowByFilter(filter: Filter) {
    const url = `${environment.api}/cash-flow`;

    let params: HttpParams = new HttpParams();
    params = params.append('offset', filter.Offset.toString());
    params = params.append('pageSize', filter.PageSize.toString());
    params = params.append('searchParam', filter.SearchParam);
    params = params.append('companyId', this.companyId.toString());

    return this.http.get<ResponseModel<CashFlow[]>>(url, {
      params: params
    });
  }

  getCashFlowTypes() {
    const url = `${environment.api}/cash-flow/types`;

    return this.http.get<ResponseModel<CashFlowType[]>>(url);
  }

  getCashFlowById(id: number) {
    const url = `${environment.api}/cash-flow/${id}`;

    return this.http.get<ResponseModel<CashFlow>>(url);
  }

  saveCashFlow(item: CashFlow) {
    const url = `${environment.api}/cash-flow`;

    item.CompanyId = this.companyId;
    return this.http.post<ResponseModel<CashFlow>>(url, item);
  }

  getInvoiceById(id: number) {
    const url = `${environment.api}/invoices/${id}`;

    return this.http.get<ResponseModel<Invoice>>(url);
  }

  getPendingInvoices(filter: Filter) {
    const url = `${environment.api}/invoices/pending`;

    let params: HttpParams = new HttpParams();
    params = params.append('offset', filter.Offset.toString());
    params = params.append('pageSize', filter.PageSize.toString());
    params = params.append('searchParam', filter.SearchParam);
    params = params.append('aging', filter.Aging.toString());
    params = params.append('companyId', this.companyId.toString());

    return this.http.get<ResponseModel<InvoicePreview[]>>(url, {
      params: params
    });
  }

  getPaidInvoices(filter: Filter) {
    const url = `${environment.api}/invoices/paid`;

    let params: HttpParams = new HttpParams();
    params = params.append('offset', filter.Offset.toString());
    params = params.append('pageSize', filter.PageSize.toString());
    params = params.append('searchParam', filter.SearchParam);
    params = params.append('companyId', this.companyId.toString());

    return this.http.get<ResponseModel<InvoicePreview[]>>(url, {
      params: params
    });
  }

  saveInvoice(item: Invoice) {
    const url = `${environment.api}/invoices`;

    item.CompanyId = this.companyId;
    return this.http.post<ResponseModel<Invoice>>(url, item);
  }

  saveInvoiceItem(item: InvoiceItem) {
    const url = `${environment.api}/invoices/invoice_item`;

    return this.http.post<ResponseModel<Invoice>>(url, item);
  }

  deleteInvoiceItems(loadId: number) {
    const url = `${environment.api}/invoices/invoice_item/remove/${loadId}`;

    return this.http.get<ResponseModel<boolean>>(url);
  }

  regenerateInvoiceItems(loadId: number) {
    const url = `${environment.api}/invoices/${loadId}/regenerate`;

    return this.http.get<ResponseModel<Invoice>>(url);
  }

  saveInvoicePayment(item: InvoicePayment) {
    const url = `${environment.api}/invoices/invoice_payment`;

    return this.http.post<ResponseModel<Invoice>>(url, item);
  }

  deleteInvoicePayment(paymentId: number) {
    const url = `${environment.api}/invoices/invoice_payment/remove/${paymentId}`;

    return this.http.get<ResponseModel<boolean>>(url);
  }

  createAndEmailPdf(item: Invoice) {
    const url = `${environment.api}/invoices/pdf/email`;

    return this.http.post(url, item);
  }

}
