import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { DispatchPalServicesModule } from './services';
import { BrowserModule } from "@angular/platform-browser";
import { AppRoutingModule } from './app-routing.module';
import { ActionReducer, MetaReducer, StoreModule } from '@ngrx/store';
import { filterDrivers, filterTrucks, filterTrailers, filterCompanies, filterServices, filterClients, filterLoads, filterTrips, filterCashFlow, filterPendingInvoices, filterPaidInvoices } from './redux/reducers/filter.reducer';
import { activeUser } from './redux/reducers/user.reducer';
import { activeCompany } from './redux/reducers/company.reducer';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { DispatchPalInterceptor } from './services/interceptors/dispatch_pal.interceptor';
import { localStorageSync } from 'ngrx-store-localstorage';
import { DispatchPalSharedModule } from './modules/shared/shared.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

const reducers = {
    filterDrivers,
    filterTrucks,
    filterTrailers,
    filterCompanies,
    filterServices,
    filterClients,
    filterLoads,
    filterTrips,
    filterCashFlow,
    filterPendingInvoices,
    filterPaidInvoices,
    activeUser,
    activeCompany
};

export function localStorageSyncReducer(reducer: ActionReducer<any>): ActionReducer<any> {
    return localStorageSync({ keys: Object.keys(reducers), rehydrate: true })(reducer);
}
const metaReducers: Array<MetaReducer<any, any>> = [localStorageSyncReducer];

@NgModule({
  imports: [
    AppRoutingModule,
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    StoreModule.forRoot(
      reducers,
      {metaReducers}
    ),
    DispatchPalSharedModule,
    DispatchPalServicesModule.forRoot()
  ],
  declarations: [AppComponent],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: DispatchPalInterceptor,
      multi: true,
    }
  ],
  bootstrap: [AppComponent]

})
export class AppModule {
}
