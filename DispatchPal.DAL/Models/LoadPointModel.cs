﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DispatchPal.DAL.Models
{
    public class LoadPointModel
    {
        public int Id { get; set; }
        public int? TourId { get; set; }
        public int? LoadId { get; set; }
        public string LoadNumber { get; set; }
        public int OrderNumber { get; set; }
        public int ActionType { get; set; }
        public DateTime DateLoad { get; set; }
        public string Location { get; set; }
        public string Country { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string Address { get; set; }
        public string ZipCode { get; set; }
        public string Description { get; set; }
        public string Qty { get; set; }
        public decimal? Weight { get; set; }
        public string DeliveryNotes { get; set; }
        public string NumbersPO { get; set; }
        public decimal? Lat { get; set; }
        public decimal? Lng { get; set; }
        public decimal Distance { get; set; }
        public decimal DistanceFromStart { get; set; }
        public string EstimatedTime { get; set; }
        public bool EmptyPosition { get; set; }
        public string Contact { get; set; }
        public string Phone { get; set; }
        public int? DriverId { get; set; }
        public int? CompanyId { get; set; }
    }
}
