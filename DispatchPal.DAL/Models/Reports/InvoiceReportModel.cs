﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DispatchPal.DAL.Models.Reports
{
    public class InvoiceReportModel
    {
        public string CompanyName { get; set; }
        public string CompanyAddress { get; set; }
        public string CompanyCity { get; set; }
        public string CompanyState { get; set; }
        public string CompanyZipCode { get; set; }
        public string CompanyPhoneNumber { get; set; }
        public string CompanyFax { get; set; }
        public string CompanyEmail { get; set; }
        public string PartnerName { get; set; }
        public string PartnerAddress { get; set; }
        public string PartnerCity { get; set; }
        public string PartnerState { get; set; }
        public string PartnerZipCode { get; set; }
        public string LoadNumber { get; set; }
        public string BOL { get; set; }
        public string PO { get; set; }
        public DateTime InvoiceDate { get; set; }
        public DateTime DueDate { get; set; }
        public string BrokerTerms { get; set; }
        public decimal BillableMiles { get; set; }
        public decimal Rate { get; set; }
        public decimal Amount { get; set; }
        public DateTime LoadStartDate { get; set; }
        public string LocationStart { get; set; }
        public string LocationEnd { get; set; }
        public string DriverA { get; set; }
        public string DriverB { get; set; }
        public string Truck { get; set; }
        public string Trailer { get; set; }
        public List<InvoiceItemModel> InvoiceItems { get; set; }

        public InvoiceReportModel()
        {
            this.InvoiceItems = new List<InvoiceItemModel>();
        }
    }
}
