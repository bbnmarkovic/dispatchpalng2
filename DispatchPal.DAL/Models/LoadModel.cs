﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DispatchPal.DAL.Models
{
    public class LoadModel
    {
        public int Id { get; set; }
        public int TourId { get; set; }
        public int? DispatcherId { get; set; }
        public string LoadNumber { get; set; }
        public decimal AgreedPrice { get; set; }
        public decimal FuelSurcharge { get; set; }
        public decimal BillableMiles { get; set; }
        public decimal EmptyMiles { get; set; }
        public decimal BobtailMiles { get; set; }
        public int ExtraStops { get; set; }
        public string PO { get; set; }
        public string Confirmation { get; set; }
        public string BOL { get; set; }
        public string Commodity { get; set; }
        public decimal? Weight { get; set; }
        public decimal? Quantity { get; set; }
        public decimal? Size { get; set; }
        public decimal? Temperature { get; set; }
        public int? LoadType { get; set; }
        public int? PartnerShipperId { get; set; }
        public string PartnerShipperContact { get; set; }
        public string PartnerShipperPhone { get; set; }
        public string PartnerShipperShippingNumber { get; set; }
        public string PartnerShipperNote { get; set; }
        public int? PartnerConsigneeId { get; set; }
        public string PartnerConsigneeContact { get; set; }
        public string PartnerConsigneePhone { get; set; }
        public string PartnerConsigneeDeliveryNumber { get; set; }
        public string PartnerConsigneeNote { get; set; }
        public int? BrokerageId { get; set; }
        public int? BrokerId { get; set; }
        public int? BrokerRating { get; set; }
        public string BrokerPhoneNumber { get; set; }
        public string BrokerFax { get; set; }
        public string BrokerEmail { get; set; }
        public int? Broker1Id { get; set; }
        public decimal? FlatRate { get; set; }
        public decimal TotalDistanceMi { get; set; }
        public decimal TotalDistanceKm { get; set; }
        public string Chassis { get; set; }
        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }
        public DateTime? BookingDate { get; set; }
        public bool PaidInFull { get; set; }
        public bool PendingIssue { get; set; }
        public int Status { get; set; }
        public bool DispatchPaid { get; set; }
        public string DriverNotes { get; set; }
        public string InternalNotes { get; set; }
        public string ExternalNotes { get; set; }
        public string ShippingNumber { get; set; }
        public string ShippingNote { get; set; }
        public string DeliveryNumber { get; set; }
        public string DeliveryNote { get; set; }
        public bool IsActive { get; set; }
        public int CompanyId { get; set; }
        public List<FileModel> Files { get; set; }
        public List<LoadPointModel> LoadPoints { get; set; }
        public List<LoadIssueModel> LoadIssues { get; set; }
    }
}
