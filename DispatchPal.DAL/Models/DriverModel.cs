﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DispatchPal.DAL.Models
{
    public class DriverModel
    {
        public int Id { get; set; }
        public string DriverID { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string DisplayName { get; set; }
        public DateTime? DOB { get; set; }
        public string SSN { get; set; }
        public string FEIN { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public int? StateId { get; set; }
        public string ZipCode { get; set; }
        public string HomePhone { get; set; }
        public string CellPhone { get; set; }
        public string CellPhoneCarrier { get; set; }
        public string Email { get; set; }
        public string Fax { get; set; }
        public bool SendSMS { get; set; }
        public bool SendEMail { get; set; }
        public string EmergencyContactName { get; set; }
        public string EmergencyContactPhone { get; set; }
        public string Relationship { get; set; }
        public DateTime? MedicalExpirationDate { get; set; }
        public bool PreEmploymentDrugTest { get; set; }
        public DateTime? PreEmploymentDrugTestDate { get; set; }
        public bool PreEmploymentDrugMVR { get; set; }
        public DateTime? PreEmploymentDrugMVRDate { get; set; }
        public int RateType { get; set; }
        public decimal? RateAmountEmpty { get; set; }
        public decimal? RateAmountLoaded { get; set; }
        public decimal? RateAmountBobtail { get; set; }
        public decimal? TeamRateAmountLoaded { get; set; }
        public decimal? TeamRateAmountEmpty { get; set; }
        public decimal? TeamRateAmountBobtail { get; set; }
        public DateTime? HireDate { get; set; }
        public DateTime? TerminationDate { get; set; }
        public string FuelCard { get; set; }
        public int? DispatchedBy { get; set; }
        public string PayMathod { get; set; }
        public string CDL { get; set; }
        public int? CDLStateId { get; set; }
        public DateTime? CDLExpirationDate { get; set; }
        public string CDLClass { get; set; }
        public string CDLEndorsement { get; set; }
        public int? YearsOfEprience { get; set; }
        public string Note { get; set; }
        public bool IsActive { get; set; }
        public int ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        public int CompanyId { get; set; }

        public string Truck { get; set; }
        public string Trailer { get; set; }

        public List<FileModel> Files { get; set; }
    }
}
