﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DispatchPal.DAL.Models
{
    public class UserModel
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public bool EmailConfirmed { get; set; }
        public string PhoneNumber { get; set; }
        public string MobileNumber { get; set; }
        public bool PhoneNumberConfirmed { get; set; }
        public string Address { get; set; }
        public string Description { get; set; }
        public int? AccessFailedCount { get; set; }
        public bool LockoutEnabled { get; set; }
        public DateTime? LockoutEndDateUtc { get; set; }
        public DateTime DateCreatedUtc { get; set; }
        public DateTime? DateActivatedUtc { get; set; }
        public string DateLastLoginUTC { get; set; }
        public bool TwoFactorEnabled { get; set; }
        public string InviteCode { get; set; }
        public string ResetPasswordKey { get; set; }
        public DateTime? ResetPasswordExpiryUtc { get; set; }
        public int Status { get; set; }
        public bool IsActive { get; set; }
        public int? CompanyId { get; set; }
        public List<int> Companies { get; set; }
    }
}
