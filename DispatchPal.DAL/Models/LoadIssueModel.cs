﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DispatchPal.DAL.Models
{
    public class LoadIssueModel
    {
        public int Id { get; set; }
        public int LoadId { get; set; }
        public string Description { get; set; }
        public bool IsResolved { get; set; }

    }
}
