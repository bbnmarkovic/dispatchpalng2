﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DispatchPal.DAL.Models
{
    public class TripModel
    {
        public int Id { get; set; }
        public string TripNumber { get; set; }
        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }
        public int? DriverAId { get; set; }
        public int? DriverBId { get; set; }
        public int? TruckId { get; set; }
        public int? TrailerId { get; set; }
        public int PayTypeDriverA { get; set; }
        public decimal PayRateFullDriverA { get; set; }
        public decimal PayRateEmptyDriverA { get; set; }
        public decimal PayRateBobtailDriverA { get; set; }
        public int PayTypeDriverB { get; set; }
        public decimal PayRateFullDriverB { get; set; }
        public decimal PayRateEmptyDriverB { get; set; }
        public decimal PayRateBobtailDriverB { get; set; }
        public bool IsActive { get; set; }
        public bool IsFinished { get; set; }
        public int CompanyId { get; set; }
    }
}
