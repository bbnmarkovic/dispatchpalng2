﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DispatchPal.DAL.Models
{
    public class CompanyModel
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Address1 { get; set; }
        public string City { get; set; }
        public int? StateId { get; set; }
        public string ZipCode { get; set; }
        public string Country { get; set; }
        public string PhoneNumber { get; set; }
        public string AlternatePhoneNumber { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string Contact { get; set; }
        public string Notes { get; set; }
        public string MotorCarrierNumber { get; set; }
        public int NextInvoiceNumber { get; set; }
        public string BusinessNumber { get; set; }
        public string EIN { get; set; }
        public string Domain { get; set; }
        public string Logo { get; set; }
        public bool IsActive { get; set; }
    }
}
