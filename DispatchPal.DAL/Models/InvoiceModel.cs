﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DispatchPal.DAL.Models
{
    public class InvoiceModel
    {
        public int LoadId { get; set; }
        public string LoadNumber { get; set; }
        public int TripId { get; set; }
        public bool PaidInFull { get; set; }
        public string InvoiceNumber { get; set; }
        public DateTime InvoiceDate { get; set; }
        public DateTime InvoiceDueDate { get; set; }
        public int BusinessTaxID { get; set; }
        public decimal Total { get; set; }
        public decimal SubTotal { get; set; }
        public decimal AmountPaid { get; set; }
        public decimal Discount { get; set; }
        public decimal DiscountAmount { get; set; }
        public int TaxInclusive { get; set; }
        public decimal Tax { get; set; }
        public decimal TaxAmmount { get; set; }
        public decimal BalanceRemaining { get; set; }
        public DateTime PaymentDate { get; set; }
        public bool SendReceipt { get; set; }
        public string ExternalNote { get; set; }
        public bool IsActive { get; set; }
        public int? CompanyId { get; set; }
        public List<InvoiceItemModel> InvoiceItems { get; set; }
        public List<InvoicePaymentModel> InvoicePayments { get; set; }
    }
}
