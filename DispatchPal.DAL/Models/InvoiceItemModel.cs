﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DispatchPal.DAL.Models
{
    public class InvoiceItemModel
    {
        public int Id { get; set; }
        public int InvoiceId { get; set; }
        public int? ServiceId { get; set; }
        public string ServiceName { get; set; }
        public decimal? Price { get; set; }
        public decimal? Quantity { get; set; }
        public decimal? Total { get; set; }
        public decimal? Tax { get; set; }
        public decimal? TaxAmmount { get; set; }
        public decimal? Discount { get; set; }
        public decimal? DiscountAmmount { get; set; }
        public string Note { get; set; }
    }
}
