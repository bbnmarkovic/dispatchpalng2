﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DispatchPal.DAL.Models
{
    public class InvoicePaymentModel
    {
        public int Id { get; set; }
        public int? InvoiceId { get; set; }
        public decimal? Amount { get; set; }
        public DateTime? PaymentDate { get; set; }
        public string PayedBy { get; set; }
    }
}
