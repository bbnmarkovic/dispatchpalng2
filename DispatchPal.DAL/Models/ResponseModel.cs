﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DispatchPal.DAL.Models
{
    public class ResponseModel<T>
    {
        public bool IsSuccess { get; set; }
        public int TotalItems { get; set; }
        public T Result { get; set; }
        public string Message { get; set; }
    }
}
