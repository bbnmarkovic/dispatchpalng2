﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DispatchPal.DAL.Models
{
    public class PlannerModel
    {
        public int TripId { get; set; }
        public int TripNumber { get; set; }
        List<LoadPointModel> LoadPoints { get; set; }
    }
}
