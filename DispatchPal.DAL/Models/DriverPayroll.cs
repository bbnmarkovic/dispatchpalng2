﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DispatchPal.DAL.Models
{
    public class DriverPayroll
    {
        public int LoadId { get; set; }
        public string LoadNumber { get; set; }
        public int DriverId { get; set; }
        public string Driver { get; set; }
        public DateTime DateFrom { get; set; }
        public DateTime DateTo { get; set; }
    }
}
