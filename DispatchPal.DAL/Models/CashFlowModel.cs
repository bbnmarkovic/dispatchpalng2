﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DispatchPal.DAL.Models
{
    public class CashFlowModel
    {
        public int Id { get; set; }
        public int DriverId { get; set; }
        public int TruckId { get; set; }
        public int? TrailerId { get; set; }
        public DateTime Date { get; set; }
        public decimal Amount { get; set; }
        public int TypeId { get; set; }
        public string Note { get; set; }
        public bool IsSettled { get; set; }
        public bool IsActive { get; set; }
    }
}
