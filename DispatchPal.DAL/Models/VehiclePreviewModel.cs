﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DispatchPal.DAL.Models
{
    public class VehiclePreviewModel
    {
        public int Id { get; set; }
        public int VehicleType { get; set; }
        public string VehicleID { get; set; }
        public string DisplayName { get; set; }
        public string Number { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public int? Year { get; set; }
        public string Size { get; set; }
        public string Color { get; set; }
        public string InsuranceWith { get; set; }
        public string PlateNumber { get; set; }
        public string State { get; set; }
        public DateTime? PlateExpiration { get; set; }
        public string VIN { get; set; }
        public DateTime? DOTExpiration { get; set; }
        public string DispatchedBy { get; set; }
        public bool? Ownership { get; set; }
        public DateTime? LeasedFrom { get; set; }
        public DateTime? LeasedStart { get; set; }
        public DateTime? LeasedEnd { get; set; }
        public string TrailerNumber { get; set; }
        public DateTime? PurchaseDate { get; set; }
        public string PurchasedFrom { get; set; }
        public DateTime? SellDate { get; set; }
        public string SoldTo { get; set; }
        public bool? Insured { get; set; }
        public DateTime? InsuranceDate { get; set; }
        public decimal? InsurancePremium { get; set; }
        public string Note { get; set; }
        public DateTime? WarrantyDate { get; set; }
        public string Type { get; set; }
        public string Trailer { get; set; }
        public bool IsActive { get; set; }
        public string Company { get; set; }
        public string TireType { get; set; }
    }
}
