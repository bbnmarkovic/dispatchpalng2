﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DispatchPal.DAL.Models
{
    public class ServiceModel
    {
        public int Id { get; set; }
        public int? TruckId { get; set; }
        public decimal? Mileage { get; set; }
        public DateTime? Date { get; set; }
        public string Location { get; set; }
        public string Part { get; set; }
        public string Job { get; set; }
        public decimal? Cost { get; set; }
        public DateTime? WarrantyExpDate { get; set; }
        public DateTime? ScheduledServiceDate { get; set; }
        public string Description { get; set; }
        public int CompanyId { get; set; }
    }
}
