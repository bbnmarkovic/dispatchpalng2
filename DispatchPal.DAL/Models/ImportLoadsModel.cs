﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DispatchPal.DAL.Models
{
    public class ImportLoadsModel
    {
        public int TripId { get; set; }
        public List<int> LoadIds { get; set; }
        
    }
}
