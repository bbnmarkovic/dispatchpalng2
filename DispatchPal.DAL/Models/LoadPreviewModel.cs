﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DispatchPal.DAL.Models
{
    public class LoadPreviewModel
    {
        public int Id { get; set; }
        public string LoadNumber { get; set; }
        public int TripId { get; set; }
        public string TripNumber { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public string PO { get; set; }
        public string Confirmation { get; set; }
        public string BOL { get; set; }
        public string DriverA { get; set; }
        public string DriverB { get; set; }
        public string Truck { get; set; }
        public string Trailer { get; set; }
        public string Brokerage { get; set; }
        public decimal Price { get; set; }
        public decimal Miles { get; set; }
        public bool PendingIssue { get; set; }
        public bool DispatchPaid { get; set; }
        public bool PaidInFull { get; set; }
        public bool IsFinished { get; set; }
        public string LocationStart { get; set; }
        public string LocationEnd { get; set; }
        
    }
}
