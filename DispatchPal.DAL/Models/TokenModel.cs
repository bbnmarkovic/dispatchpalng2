﻿using System;

namespace DispatchPal.DAL.Models
{
    public class TokenModel
    {
        public string UserId { get; set; }
        public string AuthToken { get; set; }
        public DateTime IssuedOn { get; set; }
        public DateTime ExpiresOn { get; set; }
    }
}
