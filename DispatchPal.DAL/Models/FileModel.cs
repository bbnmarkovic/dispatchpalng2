﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DispatchPal.DAL.Models
{
    public class FileModel
    {
        public int Id { get; set; }
        public int DocumentType { get; set; }
        public string UniqueFileName { get; set; }
        public string OriginalFileName { get; set; }
        public string FilePath { get; set; }
        public string FileSize { get; set; }
        public string Extension { get; set; }
        public DateTime UploadDate { get; set; }
        public int ClientId { get; set; }
    }
}
