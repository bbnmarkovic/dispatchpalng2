﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DispatchPal.DAL.Models
{
    public class ClientModel
    {
        public int Id { get; set; }
        public int ClientTypeId { get; set; }
        public string AccountNumber { get; set; }
        public string DisplayName { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public int? StateId { get; set; }
        public string ZipCode { get; set; }
        public string PhoneNumber { get; set; }
        public string AlternatePhoneNumber { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string Fee { get; set; }
        public string MotorCarrierNumber { get; set; }
        public string EIN { get; set; }
        public DateTime LastPaid { get; set; }
        public string ContactName { get; set; }
        public bool IsActive { get; set; }
    }
}
