﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DispatchPal.DAL.Models
{
    public class BrokerModel
    {
        public int Id { get; set; }
        public int PartnerId { get; set; }
        public string DisplayName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public string MobileNumber { get; set; }
        public string Email { get; set; }
        public string Fax { get; set; }
        public string Fax1 { get; set; }
        public int Rate { get; set; }
        public decimal Rating { get; set; }
        public bool IsActive { get; set; }
        public int CompanyId { get; set; }
    }
}
