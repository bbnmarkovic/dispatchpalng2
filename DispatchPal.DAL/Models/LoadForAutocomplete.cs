﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DispatchPal.DAL.Models
{
    public class LoadForAutocomplete
    {
        public int Id { get; set; }
        public string DisplayName { get; set; }
    }
}
