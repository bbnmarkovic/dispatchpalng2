﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DispatchPal.DAL.Models
{
    public class DriverPreviewModel
    {
        public int Id { get; set; }
        public string DriverID { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string DisplayName { get; set; }
        public DateTime? DOB { get; set; }
        public string SSN { get; set; }
        public string Address { get; set; }
        public string CellPhone { get; set; }
        public string Email { get; set; }
        public bool SendSMS { get; set; }
        public bool SendEMail { get; set; }
        public DateTime? MedicalExpirationDate { get; set; }
        public string FuelCard { get; set; }
        public string DispatchedBy { get; set; }
        public int RateType { get; set; }
        public string PayMathod { get; set; }
        public string CDL { get; set; }
        public string CDLState { get; set; }
        public DateTime? CDLExpirationDate { get; set; }
        public string Truck { get; set; }
        public string Trailer { get; set; }
    }
}
