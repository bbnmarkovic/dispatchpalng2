﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DispatchPal.DAL.Models
{
    public class CashFlowPreviewModel
    {
        public int Id { get; set; }
        public string Driver { get; set; }
        public string Truck { get; set; }
        public string Trailer { get; set; }
        public DateTime Date { get; set; }
        public string Type { get; set; }
        public string Note { get; set; }
        public decimal Amount { get; set; }
        public bool IsSettled { get; set; }
        public bool IsActive { get; set; }
    }
}
