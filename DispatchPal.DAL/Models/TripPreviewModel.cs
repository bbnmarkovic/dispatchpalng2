﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DispatchPal.DAL.Models
{
    public class TripPreviewModel
    {
        public int Id { get; set; }
        public string TripNumber { get; set; }
        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }
        public string DriverA { get; set; }
        public string DriverB { get; set; }
        public string Truck { get; set; }
        public string Trailer { get; set; }
        public string PayTypeDriverA { get; set; }
        public decimal PayRateFullDriverA { get; set; }
        public decimal PayRateEmptyDriverA { get; set; }
        public decimal PayRateBobtailDriverA { get; set; }
        public string PayTypeDriverB { get; set; }
        public decimal PayRateFullDriverB { get; set; }
        public decimal PayRateEmptyDriverB { get; set; }
        public decimal PayRateBobtailDriverB { get; set; }
        public bool IsActive { get; set; }
        public bool IsFinished { get; set; }
        public int CompanyId { get; set; }
    }
}
