﻿using DispatchPal.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DispatchPal.DAL
{
    public partial class tour
    {
        public TripModel ToModel()
        {
            return new TripModel()
            {
                Id = Id,
                TripNumber = TripNumber,
                DateFrom = DateFrom,
                DateTo = DateTo,
                DriverAId = DriverAId,
                DriverBId = DriverBId,
                TruckId = TruckId,
                TrailerId = TrailerId,
                IsActive = IsActive,
                IsFinished = IsFinished,
                CompanyId = CompanyId,
                PayRateBobtailDriverA = PayRateBobtailDriverA,
                PayRateBobtailDriverB = PayRateBobtailDriverB,
                PayRateEmptyDriverA = PayRateEmptyDriverA,
                PayRateEmptyDriverB = PayRateEmptyDriverB,
                PayRateFullDriverA = PayRateFullDriverA,
                PayRateFullDriverB = PayRateFullDriverB,
                PayTypeDriverA = PayTypeDriverA,
                PayTypeDriverB = PayTypeDriverB
            };
        }
    }
}
