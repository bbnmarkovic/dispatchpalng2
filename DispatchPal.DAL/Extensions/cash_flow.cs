﻿using DispatchPal.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DispatchPal.DAL
{
    public partial class cash_flow
    {
        public CashFlowModel ToModel()
        {
            return new CashFlowModel()
            {
                Id = Id,
                DriverId = DriverId,
                TruckId = TruckId,
                TrailerId = TrailerId,
                Date = Date,
                Amount = Amount,
                TypeId = TypeId,
                Note = Note,
                IsSettled = IsSettled,
                IsActive = IsActive
            };
        }
    }
}
