﻿using DispatchPal.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DispatchPal.DAL
{
    public partial class company
    {
        public CompanyModel ToModel()
        {
            return new CompanyModel()
            {
                Id = Id,
                UserId = UserId,
                Name = Name,
                Address = Address,
                Address1 = Address1,
                City = City,
                StateId = StateId,
                ZipCode = ZipCode,
                Country = Country,
                PhoneNumber = PhoneNumber,
                AlternatePhoneNumber = AlternatePhoneNumber,
                Fax = Fax,
                Email = Email,
                Contact = Contact,
                Notes = Notes,
                MotorCarrierNumber = MotorCarrierNumber,
                NextInvoiceNumber = NextInvoiceNumber,
                BusinessNumber = BusinessNumber,
                EIN = EIN,
                Domain = Domain,
                Logo = Logo,
                IsActive = IsActive
            };
        }
    }
}
