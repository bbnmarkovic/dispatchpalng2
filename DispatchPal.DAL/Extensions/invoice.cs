﻿using DispatchPal.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DispatchPal.DAL
{
    public partial class invoice
    {
        public InvoiceModel ToModel()
        {
            return new InvoiceModel()
            {
                LoadId = LoadId,
                LoadNumber = load != null ? load.LoadNumber : "",
                PaidInFull = load != null ? load.PaidInFull : false,
                TripId = load != null ? (int)load.TourId : 0,
                InvoiceNumber = InvoiceNumber,
                InvoiceDate = InvoiceDate,
                InvoiceDueDate = InvoiceDueDate,
                BusinessTaxID = BusinessTaxID,
                Total = Total,
                SubTotal = SubTotal,
                AmountPaid = AmountPaid,
                Discount = Discount,
                DiscountAmount = DiscountAmount,
                TaxInclusive = TaxInclusive,
                Tax = Tax,
                TaxAmmount = TaxAmmount,
                BalanceRemaining = BalanceRemaining,
                PaymentDate = PaymentDate,
                SendReceipt = SendReceipt,
                ExternalNote = ExternalNote,
                IsActive = IsActive,
                CompanyId = CompanyId,
                InvoiceItems = invoice_item.Select(item=>item.ToModel()).ToList(),
                InvoicePayments = invoice_payment.Select(item=>item.ToModel()).ToList()
            };
        }
    }
}
