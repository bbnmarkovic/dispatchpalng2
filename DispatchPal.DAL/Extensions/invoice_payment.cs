﻿using DispatchPal.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DispatchPal.DAL
{
    public partial class invoice_payment
    {
        public InvoicePaymentModel ToModel()
        {
            return new InvoicePaymentModel()
            {
                Id = Id,
                InvoiceId = InvoiceId,
                Amount = Amount,
                PaymentDate = PaymentDate,
                PayedBy = PayedBy
            };
        }
    }
}
