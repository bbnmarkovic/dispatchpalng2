﻿using DispatchPal.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DispatchPal.DAL
{
    partial class user
    {
        public UserModel ToModel()
        {
            return new UserModel()
            {
                Id = Id,
                UserId = UserId,
                UserName = UserName,
                FirstName = FirstName,
                LastName = LastName,
                Email = Email,
                EmailConfirmed = EmailConfirmed,
                PhoneNumber = PhoneNumber,
                MobileNumber = MobileNumber,
                PhoneNumberConfirmed = PhoneNumberConfirmed,
                Address = Address,
                Description = Description,
                AccessFailedCount = AccessFailedCount,
                LockoutEnabled = LockoutEnabled,
                LockoutEndDateUtc = LockoutEndDateUtc,
                DateCreatedUtc = DateCreatedUtc,
                DateActivatedUtc = DateActivatedUtc,
                DateLastLoginUTC = DateLastLoginUTC,
                TwoFactorEnabled = TwoFactorEnabled,
                InviteCode = InviteCode,
                ResetPasswordKey = ResetPasswordKey,
                ResetPasswordExpiryUtc = ResetPasswordExpiryUtc,
                Status = Status,
                IsActive = IsActive,
                CompanyId = CompanyId,
                Companies = companies.Select(item=>item.Id).ToList()
            };
        }
    }
}
