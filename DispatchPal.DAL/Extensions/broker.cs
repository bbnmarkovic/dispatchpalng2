﻿using DispatchPal.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DispatchPal.DAL
{
    public partial class broker
    {
        public BrokerModel ToModel()
        {
            return new BrokerModel()
            {
                Id = Id,
                CompanyId = CompanyId,
                Fax = Fax,
                Fax1 = Fax1,
                DisplayName = DisplayName,
                FirstName = FirstName,
                LastName = LastName,
                MobileNumber = MobileNumber,
                PartnerId = PartnerId,
                PhoneNumber = PhoneNumber,
                Rate = Rate,
                Rating = Rating,
                Email = Email,
                IsActive = IsActive
            };
        }
    }
}
