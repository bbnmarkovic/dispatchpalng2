﻿using DispatchPal.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DispatchPal.DAL
{
    public partial class load
    {
        public LoadModel ToModel()
        {
            return new LoadModel()
            {
                Id = Id,
                TourId = TourId != null ? (int)TourId : 0,
                DispatcherId = DispatcherId,
                LoadNumber = LoadNumber,
                AgreedPrice = AgreedPrice,
                FuelSurcharge = FuelSurcharge,
                BillableMiles = BillableMiles,
                EmptyMiles = EmptyMiles,
                BobtailMiles = BobtailMiles,
                ExtraStops = ExtraStops,
                PO = PO,
                Confirmation = Confirmation,
                BOL = BOL,
                Commodity = Commodity,
                Weight = Weight,
                Quantity = Quantity,
                Size = Size,
                Temperature = Temperature,
                LoadType = LoadType,
                PartnerShipperId = PartnerShipperId,
                PartnerConsigneeId = PartnerConsigneeId,
                BrokerageId = BrokerageId,
                BrokerId = BrokerId,
                BrokerRating = BrokerRating,
                Broker1Id = Broker1Id,
                FlatRate = FlatRate,
                TotalDistanceMi = TotalDistanceMi,
                TotalDistanceKm = TotalDistanceKm,
                Chassis = Chassis,
                DateFrom = DateFrom,
                DateTo = DateTo,
                BookingDate = BookingDate,
                Status = Status,
                PaidInFull = PaidInFull,
                PendingIssue = PendingIssue,
                DispatchPaid = DispatchPaid,
                DriverNotes = DriverNotes,
                InternalNotes = InternalNotes,
                ExternalNotes = ExternalNotes,
                ShippingNumber = ShippingNumber,
                ShippingNote = ShippingNote,
                DeliveryNumber = DeliveryNumber,
                DeliveryNote = DeliveryNote,
                IsActive = IsActive,
                CompanyId = CompanyId,
                Files = uploaded_file.OrderByDescending(item => item.UploadDate).Select(item => item.ToModel()).ToList(),
                LoadIssues = load_issue.OrderBy(item => item.Id).Select(item => item.ToModel()).ToList()
            };
        }
    }
}
