﻿using DispatchPal.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DispatchPal.DAL
{
    public partial class w_state
    {
        public StateModel ToModel()
        {
            return new StateModel()
            {
                Id = Id,
                ISO = ISO,
                Name = Name,
                NickName = NickName
            };
        }
    }
}
