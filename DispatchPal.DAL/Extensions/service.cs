﻿using DispatchPal.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DispatchPal.DAL
{
    public partial class service
    {
        public ServiceModel ToModel()
        {
            return new ServiceModel()
            {
                Id = Id,
                TruckId = TruckId,
                Mileage = Mileage,
                Date = Date,
                Location = Location,
                Part = Part,
                Job = Job,
                Cost = Cost,
                WarrantyExpDate = WarrantyExpDate,
                ScheduledServiceDate = ScheduledServiceDate,
                Description = Description,
                CompanyId = CompanyId
            };
        }
    }
}

