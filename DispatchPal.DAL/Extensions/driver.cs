﻿using DispatchPal.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DispatchPal.DAL
{
    public partial class driver
    {
        public DriverModel ToModel()
        {
            return new DriverModel()
            {
                Id = Id,
                DriverID = DriverID,
                FirstName = FirstName,
                MiddleName = MiddleName,
                LastName = LastName,
                DisplayName = DisplayName,
                DOB = DOB,
                SSN = SSN,
                FEIN = FEIN,
                Address1 = Address1,
                Address2 = Address2,
                City = City,
                StateId = StateId,
                ZipCode = ZipCode,
                HomePhone = HomePhone,
                CellPhone = CellPhone,
                CellPhoneCarrier = CellPhoneCarrier,
                Email = Email,
                Fax = Fax,
                SendSMS = SendSMS,
                SendEMail = SendEMail,
                EmergencyContactName = EmergencyContactName,
                EmergencyContactPhone = EmergencyContactPhone,
                Relationship = Relationship,
                MedicalExpirationDate = MedicalExpirationDate,
                PreEmploymentDrugTest = PreEmploymentDrugTest,
                PreEmploymentDrugTestDate = PreEmploymentDrugTestDate,
                PreEmploymentDrugMVR = PreEmploymentDrugMVR,
                PreEmploymentDrugMVRDate = PreEmploymentDrugMVRDate,
                RateType = RateType,
                RateAmountEmpty = RateAmountEmpty,
                RateAmountLoaded = RateAmountLoaded,
                RateAmountBobtail = RateAmountBobtail,
                TeamRateAmountEmpty = TeamRateAmountEmpty,
                TeamRateAmountLoaded = TeamRateAmountLoaded,
                TeamRateAmountBobtail = TeamRateAmountBobtail,
                HireDate = HireDate,
                TerminationDate = TerminationDate,
                FuelCard = FuelCard,
                DispatchedBy = DispatchedBy,
                PayMathod = PayMathod,
                CDL = CDL,
                CDLStateId = CDLStateId,
                CDLExpirationDate = CDLExpirationDate,
                CDLClass = CDLClass,
                CDLEndorsement = CDLEndorsement,
                YearsOfEprience = YearsOfEprience,
                Note = Note,
                IsActive = IsActive,
                CompanyId = CompanyId,
                Files = uploaded_file.OrderByDescending(item=>item.UploadDate).Select(item => item.ToModel()).ToList()
            };
        }
    }
}
