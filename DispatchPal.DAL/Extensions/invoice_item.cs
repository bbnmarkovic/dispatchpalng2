﻿using DispatchPal.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DispatchPal.DAL
{
    public partial class invoice_item
    {
        public InvoiceItemModel ToModel()
        {
            return new InvoiceItemModel()
            {
                Id = Id,
                InvoiceId = InvoiceId,
                ServiceId = ServiceId,
                ServiceName = ServiceName,
                Price = Price,
                Quantity = Quantity,
                Total = Total,
                Tax = Tax,
                TaxAmmount = TaxAmmount,
                Discount = Discount,
                DiscountAmmount = DiscountAmmount,
                Note = Note
            };
        }
    }
}
