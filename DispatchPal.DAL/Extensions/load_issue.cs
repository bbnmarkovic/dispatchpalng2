﻿using DispatchPal.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DispatchPal.DAL
{
    public partial class load_issue
    {
        public LoadIssueModel ToModel()
        {
            return new LoadIssueModel() {
                Id = Id,
                LoadId = LoadId,
                Description = Description,
                IsResolved = IsResolved
            };

        }
    }
}
