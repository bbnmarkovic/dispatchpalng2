﻿using DispatchPal.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DispatchPal.DAL
{
    public partial class vehicle
    {
        public VehicleModel ToModel()
        {
            return new VehicleModel()
            {
                Id = Id,
                VehicleType = VehicleType,
                VehicleID = VehicleID,
                //DisplayName = DisplayName,
                Number = Number,
                Make = Make,
                Model = Model,
                Year = Year,
                Size = Size,
                Color = Color,
                InsuranceWith = InsuranceWith,
                PlateNumber = PlateNumber,
                StateId = StateId,
                PlateExpiration = PlateExpiration,
                VIN = VIN,
                DOTExpiration = DOTExpiration,
                DispatchedBy = DispatchedBy,
                Ownership = Ownership,
                LeasedFrom = LeasedFrom,
                LeasedStart = LeasedStart,
                LeasedEnd = LeasedEnd,
                TrailerNumber = TrailerNumber,
                PurchaseDate = PurchaseDate,
                PurchasedFrom = PurchasedFrom,
                SellDate = SellDate,
                SoldTo = SoldTo,
                Insured = Insured,
                InsuranceDate = InsuranceDate,
                InsurancePremium = InsurancePremium,
                Note = Note,
                WarrantyDate = WarrantyDate,
                Type = Type,
                DriverAId = DriverAId,
                DriverBId = DriverBId,
                TrailerId = TrailerId,
                IsActive = IsActive,
                CompanyId = CompanyId,
                TireType = TireType
            };
        }
    }
}
