﻿using DispatchPal.DAL.Models;

namespace DispatchPal.DAL
{
    public partial class load_point
    {
        public LoadPointModel ToModel()
        {
            return new LoadPointModel()
            {
                Id = Id,
                TourId = TourId,
                LoadId = LoadId,
                LoadNumber = load != null ? load.LoadNumber : "",
                OrderNumber = OrderNumber,
                ActionType = ActionType,
                DateLoad = DateLoad,
                Location = Location,
                Country = Country,
                State = State,
                City = City,
                Address = Address,
                ZipCode = ZipCode,
                Description = Description,
                Qty = Qty,
                Weight = Weight,
                DeliveryNotes = DeliveryNotes,
                NumbersPO = NumbersPO,
                Lat = Lat,
                Lng = Lng,
                Distance = Distance,
                DistanceFromStart = DistanceFromStart,
                EstimatedTime = EstimatedTime,
                EmptyPosition = EmptyPosition,
                Contact = Contact,
                Phone = Phone,
                DriverId = DriverId,
                CompanyId = CompanyId
            };
        }
    }
}
