﻿using DispatchPal.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DispatchPal.DAL
{
    public partial class client
    {
        public ClientModel ToModel()
        {
            return new ClientModel()
            {
                Id = Id,
                ClientTypeId = ClientTypeId,
                AccountNumber = AccountNumber,
                DisplayName = DisplayName,
                Name = Name,
                Address = Address,
                Address2 = Address2,
                City = City,
                StateId = StateId,
                ZipCode = ZipCode,
                PhoneNumber = PhoneNumber,
                AlternatePhoneNumber = AlternatePhoneNumber,
                Fax = Fax,
                Email = Email,
                Fee = Fee,
                MotorCarrierNumber = MotorCarrierNumber,
                EIN = EIN,
                LastPaid = LastPaid,
                ContactName = ContactName,
                IsActive = IsActive
            };
        }
    }
}
