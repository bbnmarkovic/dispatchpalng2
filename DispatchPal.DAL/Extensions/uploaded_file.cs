﻿using DispatchPal.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DispatchPal.DAL
{
    public partial class uploaded_file
    {
        public FileModel ToModel()
        {
            return new FileModel()
            {
                Id = Id,
                DocumentType = DocumentType,
                UniqueFileName = UniqueFileName,
                OriginalFileName = OriginalFileName,
                FilePath = FilePath,
                FileSize = FileSize,
                Extension = Extension,
                UploadDate = UploadDate,
                ClientId = ClientId
            };
        }
    }
}
