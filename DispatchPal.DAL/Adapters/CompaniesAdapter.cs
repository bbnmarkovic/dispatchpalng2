﻿using DispatchPal.DAL.Adapters.Interface;
using DispatchPal.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DispatchPal.DAL.Adapters
{
    public class CompaniesAdapter : BaseAdapter, ICompaniesAdapter
    {
        public CompaniesAdapter(dbEntities1 context) : base(context)
        {
        }

        public ResponseModel<List<CompanyModel>> Get(int offset, int pageSize, string searchParam, bool active)
        {
            if (searchParam == null)
                searchParam = "";

            IQueryable<company> items = context.companies.AsNoTracking().Where(item =>
                (!active || item.IsActive == true) &&
                (item.Name.Contains(searchParam) ||
                item.Email.Contains(searchParam)));

            return new ResponseModel<List<CompanyModel>>()
            {
                IsSuccess = true,
                TotalItems = items.Count(),
                Result = items.OrderBy(item => item.Id).Skip(offset).Take(pageSize).AsEnumerable().Select(item => item.ToModel()).ToList()
            };
        }

        public ResponseModel<CompanyModel> Get(int id)
        {
            company dr = context.companies.Where(item => item.Id == id).FirstOrDefault();
            if (dr != null)
            {
                return new ResponseModel<CompanyModel>()
                {
                    IsSuccess = true,
                    Result = dr.ToModel()
                };
            }
            else
            {
                return new ResponseModel<CompanyModel>()
                {
                    IsSuccess = false,
                    Message = "Company not found"
                };
            }
        }

        public ResponseModel<CompanyModel> Save(CompanyModel model)
        {
            try
            {
                company dr = context.companies.Where(item => item.Id == model.Id).FirstOrDefault();
                if (dr == null)
                {
                    dr = new company();
                    context.companies.Add(dr);
                }
                dr.Address = model.Address;
                dr.Address1 = model.Address1;
                dr.AlternatePhoneNumber = model.AlternatePhoneNumber;
                dr.BusinessNumber = model.BusinessNumber;
                dr.City = model.City;
                dr.Contact = model.Contact;
                dr.Country = model.Country;
                dr.Domain = model.Domain;
                dr.EIN = model.EIN;
                dr.Email = model.Email;
                dr.Fax = model.Fax;
                dr.IsActive = model.IsActive;
                dr.Logo = model.Logo;
                dr.ModifiedBy = 1;
                dr.ModifiedDate = DateTime.UtcNow;
                dr.MotorCarrierNumber = model.MotorCarrierNumber;
                dr.Name = model.Name;
                dr.NextInvoiceNumber = model.NextInvoiceNumber;
                dr.Notes = model.Notes;
                dr.PhoneNumber = model.PhoneNumber;
                dr.StateId = model.StateId;
                dr.UserId = model.UserId;
                dr.ZipCode = model.ZipCode;
                
                context.SaveChanges();

                return new ResponseModel<CompanyModel>()
                {
                    IsSuccess = true,
                    Result = dr.ToModel()
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ResponseModel<bool> Delete(int id)
        {
            company dr = context.companies.Where(item => item.Id == id).FirstOrDefault();
            if (dr != null)
            {
                context.companies.Remove(dr);
                context.SaveChanges();

                return new ResponseModel<bool>()
                {
                    IsSuccess = true
                };
            }
            else
            {
                return new ResponseModel<bool>()
                {
                    IsSuccess = false,
                    Message = "Company not found!"
                };
            }
        }

    }
}
