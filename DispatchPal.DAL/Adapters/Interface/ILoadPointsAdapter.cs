﻿using DispatchPal.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DispatchPal.DAL.Adapters.Interface
{
    public interface ILoadPointsAdapter
    {
        ResponseModel<List<LoadPointModel>> GetLoadPointsByLoadId(int loadId);
        ResponseModel<List<LoadPointModel>> GetLoadPointsByTripId(int tripId);
        ResponseModel<LoadPointModel> Get(int id);
        ResponseModel<DateTime> GetStartingLoadPointDate(int tripId);
        ResponseModel<LoadPointModel> Save(LoadPointModel model);
        ResponseModel<LoadPointModel> SaveStartingLoadPoint(LoadPointModel model);
        ResponseModel<bool> UpdateDistances(List<LoadPointModel> loadPoints);
        ResponseModel<bool> Delete(int id);
        ResponseModel<List<LoadPointModel>> GetLoadPointsForPlanner(int driverId, DateTime fromDate, DateTime toDate);
        ResponseModel<List<LoadPointModel>> SetOrderNumber(int loadPointId, int order);
    }
}
