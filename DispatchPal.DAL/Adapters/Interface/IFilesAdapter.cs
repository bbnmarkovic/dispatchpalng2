﻿using DispatchPal.DAL.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DispatchPal.DAL.Adapters.Interface
{
    public interface IFilesAdapter
    {
        FileModel Get(int id);
        bool SaveFilesForLoad(int loadId, int fileType, List<FileInfo> files);
        bool SaveFilesForDriver(int driverId, int fileType, List<FileInfo> files);
        bool Delete(int id);
    }
}
