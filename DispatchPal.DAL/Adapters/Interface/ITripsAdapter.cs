﻿using DispatchPal.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DispatchPal.DAL.Adapters.Interface
{
    public interface ITripsAdapter
    {
        ResponseModel<List<TripPreviewModel>> Get(int offset, int pageSize, string searchParam, int active, bool includeFinished, int companyId);
        ResponseModel<List<LoadPointModel>> GetForPlanner(int driverId, DateTime date);
        ResponseModel<TripModel> Get(int id);
        ResponseModel<string> GetNextTripNumber();
        ResponseModel<TripModel> Save(TripModel model);
        ResponseModel<bool> ImportLoads(List<int> loadIds, int tripId);
        ResponseModel<bool> Delete(int id);
        ResponseModel<bool> FinalizeTrip(int tripId);
        ResponseModel<bool> ReopenTrip(int tripId);
    }
}
