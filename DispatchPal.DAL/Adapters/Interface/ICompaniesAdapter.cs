﻿using DispatchPal.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DispatchPal.DAL.Adapters.Interface
{
    public interface ICompaniesAdapter
    {
        ResponseModel<List<CompanyModel>> Get(int offset, int pageSize, string searchParam, bool active);
        ResponseModel<CompanyModel> Get(int id);
        ResponseModel<CompanyModel> Save(CompanyModel model);
        ResponseModel<bool> Delete(int id);
    }
}
