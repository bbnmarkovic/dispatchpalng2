﻿using DispatchPal.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DispatchPal.DAL.Adapters.Interface
{
    public interface IClientsAdapter
    {
        ResponseModel<List<ClientModel>> Get(int clientType, int offset, int pageSize, string searchParam, bool active);
        ResponseModel<ClientModel> Get(int id);
        ResponseModel<ClientModel> Save(ClientModel model);
        ResponseModel<bool> Delete(int id);
        ResponseModel<List<ClientTypeModel>> GetClientTypes(string searchParam);
        ResponseModel<ClientTypeModel> GetClientTypeById(int id);
    }
}
