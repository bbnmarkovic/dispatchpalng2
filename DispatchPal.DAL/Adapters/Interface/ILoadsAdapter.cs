﻿using DispatchPal.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DispatchPal.DAL.Adapters.Interface
{
    public interface ILoadsAdapter
    {
        ResponseModel<List<LoadPreviewModel>> GetPreview(int offset, int pageSize, string searchParam, bool active, int companyId);
        ResponseModel<List<LoadForAutocomplete>> GetForAutocomplete(int offset, int pageSize, string searchParam);
        ResponseModel<List<LoadModel>> Get(int offset, int pageSize, string searchParam, bool active);
        ResponseModel<List<LoadModel>> GetLoadsByTripId(int tripId);
        ResponseModel<List<LoadPreviewModel>> GetUnassignedLoads(DateTime? startDate, int offset, int pageSize, string searchParam, int companyId);
        ResponseModel<List<DriverPayroll>> GetDriverPayroll(int offset, int pageSize, int companyId, int driverId);
        ResponseModel<LoadModel> Get(int id);
        ResponseModel<string> GetNextLoadNumber();
        ResponseModel<LoadForAutocomplete> GetForAutocomplete(int id);
        ResponseModel<LoadModel> Save(LoadModel model);
        ResponseModel<bool> Delete(int id);
        ResponseModel<bool> AssignLoadToTrip(int loadId, int tripId);
        ResponseModel<bool> ToggleLoadIssueResolved(int loadIssueId);
        ResponseModel<int> SaveLoadIssue(LoadIssueModel model);
    }
}
