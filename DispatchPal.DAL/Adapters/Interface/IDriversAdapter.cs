﻿using DispatchPal.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DispatchPal.DAL.Adapters.Interface
{
    public interface IDriversAdapter
    {
        ResponseModel<List<DriverPreviewModel>> GetPreview(int offset, int pageSize, string searchParam, bool active, int companyId);
        ResponseModel<List<DriverModel>> Get(int offset, int pageSize, string searchParam, bool active, int companyId);
        ResponseModel<List<DriverModel>> GetDriversForAutoComplete(string searchParam, int companyId);
        ResponseModel<DriverModel> Get(int id);
        ResponseModel<DriverModel> Save(DriverModel model);
        ResponseModel<bool> Delete(int id); 
    }
}
