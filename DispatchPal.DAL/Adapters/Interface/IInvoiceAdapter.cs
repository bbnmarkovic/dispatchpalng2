﻿using DispatchPal.DAL.Models;
using DispatchPal.DAL.Models.Reports;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DispatchPal.DAL.Adapters.Interface
{
    public interface IInvoiceAdapter
    {
        ResponseModel<InvoiceModel> Get(int loadId);
        ResponseModel<List<InvoiceModel>> GetList(int offset, int pageSize, string searchParam, bool active, int companyId);
        ResponseModel<List<InvoicePreviewModel>> GetPendingInvoices(int offset, int pageSize, string searchParam, int aging, int companyId);
        ResponseModel<List<InvoicePreviewModel>> GetPaidInvoices(int offset, int pageSize, string searchParam, int companyId);
        ResponseModel<InvoiceModel> Save(InvoiceModel invModel);
        ResponseModel<InvoiceModel> SaveInvoiceItem(InvoiceItemModel model);
        ResponseModel<InvoiceModel> SaveInvoicePayment(InvoicePaymentModel model);
        ResponseModel<bool> DeleteInvoiceItem(int id);
        ResponseModel<bool> DeleteInvoicePayment(int id);
        ResponseModel<InvoiceModel> RegenerateInvoiceItems(int loadId);
        ResponseModel<InvoiceReportModel> GenerateReportModel(int loadId);
    }
}
