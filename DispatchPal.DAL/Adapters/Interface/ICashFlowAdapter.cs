﻿using DispatchPal.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DispatchPal.DAL.Adapters.Interface
{
    public interface ICashFlowAdapter
    {
        ResponseModel<List<CashFlowPreviewModel>> GetPreview(int offset, int pageSize, string searchParam, bool active);
        ResponseModel<CashFlowModel> Get(int id);
        ResponseModel<List<CashFlowTypeModel>> GetTypes();
        ResponseModel<CashFlowModel> Save(CashFlowModel model);
        ResponseModel<bool> Delete(int id);
    }
}
