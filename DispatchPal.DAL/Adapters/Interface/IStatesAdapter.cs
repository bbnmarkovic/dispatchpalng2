﻿using DispatchPal.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DispatchPal.DAL.Adapters.Interface
{
    public interface IStatesAdapter
    {
        ResponseModel<List<StateModel>> Get(string searchParam);
        ResponseModel<StateModel> Get(int id);
    }
}
