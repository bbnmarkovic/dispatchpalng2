﻿using DispatchPal.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DispatchPal.DAL.Adapters.Interface
{
    public interface IUsersAdapter
    {
        ResponseModel<List<UserModel>> Get(int offset, int pageSize, string searchParam, bool active);
        ResponseModel<UserModel> Get(int id);
        ResponseModel<UserModel> Save(UserModel model);
        ResponseModel<bool> Delete(int id);
        ResponseModel<UserModel> GetUserByUsernameAndPassword(string userName, string password);
    }
}
