﻿using DispatchPal.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DispatchPal.DAL.Adapters.Interface
{
    public interface IVehiclesAdapter
    {
        ResponseModel<List<VehiclePreviewModel>> GetPreview(int vehicleType, int offset, int pageSize, string searchParam, bool active, int companyId);
        ResponseModel<List<VehicleModel>> Get(int vehicleType, int offset, int pageSize, string searchParam, bool active, int companyId);
        ResponseModel<VehicleModel> Get(int id);
        ResponseModel<VehicleModel> Save(VehicleModel model);
        ResponseModel<bool> Delete(int id);
    }
}
