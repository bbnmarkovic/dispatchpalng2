﻿using DispatchPal.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DispatchPal.DAL.Adapters.Interface
{
    public interface ILoginTokenAdapter
    {
        void RemoveOldTokens();
        login_token Get(string loginToken);
        login_token Get(Guid loginToken);
        void Delete(Guid loginToken);
        LoginTokenCreationResult CreateNew(string userName, string password);
        void Update(login_token loginToken);
        void SaveChanges();
    }

    public class LoginTokenCreationResult
    {
        public login_token token { get; set; }
        public UserModel _user { get; set; }
    }
}
