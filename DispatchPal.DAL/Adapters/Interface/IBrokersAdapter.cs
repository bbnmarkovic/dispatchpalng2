﻿using DispatchPal.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DispatchPal.DAL.Adapters.Interface
{
    public interface IBrokersAdapter
    {
        ResponseModel<List<BrokerModel>> Get(int offset, int pageSize, string searchParam, bool active, int brokerageId);
        ResponseModel<BrokerModel> Get(int id);
        ResponseModel<decimal> GetBrokerRating(int id);
        ResponseModel<BrokerModel> Save(BrokerModel model);
        ResponseModel<bool> Delete(int id);
    }
}
