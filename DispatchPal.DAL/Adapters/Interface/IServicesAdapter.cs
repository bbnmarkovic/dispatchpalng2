﻿using DispatchPal.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DispatchPal.DAL.Adapters.Interface
{
    public interface IServicesAdapter
    {
        ResponseModel<List<ServiceModel>> Get(int offset, int pageSize, string searchParam, int companyId);
        ResponseModel<ServiceModel> Get(int id);
        ResponseModel<ServiceModel> Save(ServiceModel model);
        ResponseModel<bool> Delete(int id);
    }
}
