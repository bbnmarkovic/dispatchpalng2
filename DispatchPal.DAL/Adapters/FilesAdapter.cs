﻿using DispatchPal.DAL.Adapters.Interface;
using DispatchPal.DAL.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DispatchPal.DAL.Adapters
{
    public class FilesAdapter : BaseAdapter, IFilesAdapter
    {
        public FilesAdapter(dbEntities1 context) : base(context)
        {
        }

        public FileModel Get(int id)
        {
            uploaded_file dr = context.uploaded_file.Where(item => item.Id == id).FirstOrDefault();
            if (dr != null)
            {
                return dr.ToModel();
            }

            return null;
        }

        public bool SaveFilesForLoad(int loadId, int fileType, List<FileInfo> files)
        {
            using (var cc = new dbEntities1())
            {
                load ld = cc.loads.Where(item => item.Id == loadId).FirstOrDefault();
                if (ld != null)
                {
                    foreach (FileInfo info in files)
                    {
                        uploaded_file file = new uploaded_file();
                        file.DocumentType = fileType;
                        file.Extension = info.Extension;
                        file.FilePath = info.FullName;
                        file.FileSize = info.Length.ToString();
                        int origIndex = info.Name.IndexOf("_dpl_");
                        string orig = info.Name.Substring(origIndex + 5, info.Name.Length - origIndex - 5);
                        file.OriginalFileName = orig;
                        file.UniqueFileName = info.Name;
                        file.UploadDate = DateTime.UtcNow;

                        ld.uploaded_file.Add(file);
                    }
                    cc.SaveChanges();

                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public bool SaveFilesForDriver(int driverId, int fileType, List<FileInfo> files)
        {
            using (var cc = new dbEntities1())
            {
                driver dr = cc.drivers.Where(item => item.Id == driverId).FirstOrDefault();
                if (dr != null)
                {
                    foreach (FileInfo info in files)
                    {
                        uploaded_file file = new uploaded_file();
                        file.DocumentType = fileType;
                        file.Extension = info.Extension;
                        file.FilePath = info.FullName;
                        file.FileSize = info.Length.ToString();
                        int origIndex = info.Name.IndexOf("_dpl_");
                        string orig = info.Name.Substring(origIndex + 5, info.Name.Length - origIndex - 5);
                        file.OriginalFileName = orig;
                        file.UniqueFileName = info.Name;
                        file.UploadDate = DateTime.Now;

                        dr.uploaded_file.Add(file);
                    }
                    cc.SaveChanges();

                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public bool Delete(int id)
        {
            uploaded_file dr = context.uploaded_file.Where(item => item.Id == id).FirstOrDefault();
            if (dr != null)
            {
                //foreach (load ld in dr.loads) { }
                dr.loads.Clear();
                dr.drivers.Clear();
                context.uploaded_file.Remove(dr);
                context.SaveChanges();

                return true;
            }

            return false;
        }
    }
}
