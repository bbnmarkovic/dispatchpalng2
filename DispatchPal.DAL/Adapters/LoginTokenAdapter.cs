﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Omu.ValueInjecter;
using DispatchPal.DAL;
using DispatchPal.DAL.Adapters.Interface;
using DispatchPal.DAL.Utils;

namespace DispatchPal.DAL.Adapters
{
    public class LoginTokenAdapter : BaseAdapter, ILoginTokenAdapter
    {
        #region Private member variable declarations
        
        private UsersAdapter usersAdapter;

        #endregion

        #region Constructor method definitions

        public LoginTokenAdapter(dbEntities1 context, IUsersAdapter _usersAdapter) : base(context)
        {
            this.usersAdapter = (UsersAdapter)_usersAdapter;
        }

        #endregion

        public void RemoveOldTokens()
        {
            var dtLimit = DateTime.Now - TimeSpan.FromMinutes(LocalSettings.TokenDurationIntervalInMinutes);
            context.login_token.RemoveRange(context.login_token.Where(x => x.created < dtLimit));
            context.SaveChanges();
        }

        public login_token Get(string loginToken)
        {
            try
            {
                return Get(new Guid(loginToken));
            }
            catch
            {
                return null;
            }
        }

        public login_token Get(Guid loginToken)
        {
            var res = context.login_token.FirstOrDefault(x => x.token.Equals(loginToken));
            if (res != null)
            {
                res.accessed = DateTime.Now;
                context.SaveChangesAsync();
            }
            return res;
        }

        public LoginTokenCreationResult CreateNew(string userName, string password)
        {
            LoginTokenCreationResult res = null;

            var usr = usersAdapter.GetUserByUsernameAndPassword(userName, password);
            if (usr != null)
            {
                if (usr.Result.IsActive)
                {
                    var newItem = new login_token
                    {
                        token = Guid.NewGuid(),
                        created = DateTime.Now,
                        accessed = DateTime.Now,
                        user_id = usr.Result.Id
                    };
                    context.login_token.Add(newItem);
                    context.SaveChanges();
                    res = new LoginTokenCreationResult
                    {
                        token = newItem,
                        _user = usr.Result
                    };
                }
            }
            return res;
        }

        public void Update(login_token loginToken)
        {
            var item = Get(loginToken.token);
            item.InjectFrom(loginToken);
            context.SaveChanges();
        }

        public void SaveChanges()
        {
            context.SaveChanges();
        }

        public void Delete(Guid loginToken)
        {
            var item = context.login_token.FirstOrDefault(x => x.token.Equals(loginToken));
            if (item != null)
            {
                context.login_token.Remove(item);
                context.SaveChanges();
            }
        }
    }
}
