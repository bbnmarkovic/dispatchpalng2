﻿using DispatchPal.DAL.Adapters.Interface;
using DispatchPal.DAL.Models;
using DispatchPal.DAL.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static DispatchPal.DAL.Models.Enums.DplEnums;

namespace DispatchPal.DAL.Adapters
{
    public class LoadsAdapter : BaseAdapter, ILoadsAdapter
    {
        public LoadsAdapter(dbEntities1 context) : base(context)
        {
        }
        public ResponseModel<List<LoadPreviewModel>> GetPreview(int offset, int pageSize, string searchParam, bool active, int companyId)
        {
            if (searchParam == null)
                searchParam = "";

            IQueryable<LoadPreviewModel> items = context.loads.AsNoTracking().Where(item =>
                (!active || item.IsActive == true) &&
                (companyId ==0 || item.CompanyId == companyId) &&
                (item.LoadNumber.Contains(searchParam) ||
                item.Confirmation.Contains(searchParam) ||
                item.BOL.Contains(searchParam) ||
                item.PO.Contains(searchParam) ||
                (item.tour != null && item.tour.driver != null && item.tour.driver.DisplayName.Contains(searchParam)) ||
                (item.tour != null && item.tour.driver1 != null && item.tour.driver1.DisplayName.Contains(searchParam)) ||
                (item.tour != null && item.tour.vehicle != null && item.tour.vehicle.VehicleID.Contains(searchParam)) ||
                (item.tour != null && item.tour.vehicle1 != null && item.tour.vehicle1.VehicleID.Contains(searchParam)))).Select(item => new LoadPreviewModel()
                {
                    BOL = item.BOL,
                    Brokerage = item.client != null ? item.client.DisplayName : "",
                    Confirmation = item.Confirmation,
                    DriverA = item.tour != null ? (item.tour.driver != null ? item.tour.driver.DisplayName : "") : "",
                    DriverB = item.tour != null ? (item.tour.driver1 != null ? item.tour.driver1.DisplayName : "") : "",
                    FromDate = item.DateFrom,
                    Id = item.Id,
                    IsFinished = item.tour != null ? item.tour.IsFinished : false,
                    LoadNumber = item.LoadNumber,
                    Miles = item.TotalDistanceMi,
                    PaidInFull = item.PaidInFull,
                    PendingIssue = item.PendingIssue,
                    DispatchPaid = item.DispatchPaid,
                    PO = item.PO,
                    Price = item.AgreedPrice,
                    ToDate = item.DateTo,
                    Trailer = item.tour != null ? (item.tour.vehicle != null ? item.tour.vehicle.VehicleID : "") : "",
                    Truck = item.tour != null ? (item.tour.vehicle1 != null ? item.tour.vehicle1.VehicleID : "") : "",
                    TripId = item.TourId != null ? (int)item.TourId : 0,
                    TripNumber = item.tour != null ? item.tour.TripNumber : ""
                });

            List<LoadPreviewModel> list = items.OrderBy(item => item.Id).Skip(offset).Take(pageSize).ToList();
            foreach (LoadPreviewModel preview in list)
            {
                load_point startPoint = context.load_point.AsNoTracking().Where(pp => pp.LoadId == preview.Id && pp.ActionType == (int)LoadPointType.Loading && pp.OrderNumber > 0).OrderBy(pp => pp.DateLoad).FirstOrDefault();
                preview.LocationStart = startPoint != null ? startPoint.Location : "";
                preview.FromDate = startPoint != null ? startPoint.DateLoad : default(DateTime?);

                load_point endPoint = context.load_point.AsNoTracking().Where(pp => pp.LoadId == preview.Id && pp.ActionType == (int)LoadPointType.Unloading && pp.OrderNumber > 0).OrderByDescending(pp => pp.DateLoad).FirstOrDefault();
                preview.LocationEnd = endPoint != null ? endPoint.Location : "";
                preview.ToDate = endPoint != null ? endPoint.DateLoad : default(DateTime?);
            }

            return new ResponseModel<List<LoadPreviewModel>>()
            {
                IsSuccess = true,
                TotalItems = items.Count(),
                Result = list
            };
        }
        public ResponseModel<List<LoadForAutocomplete>> GetForAutocomplete(int offset, int pageSize, string searchParam)
        {
            if (searchParam == null)
                searchParam = "";

            IQueryable<LoadForAutocomplete> items = context.loads.AsNoTracking().Where(item =>
                item.IsActive == true &&
                (item.LoadNumber.Contains(searchParam) ||
                item.BOL.Contains(searchParam) ||
                item.PO.Contains(searchParam))).Select(item => new LoadForAutocomplete()
                {
                    Id = item.Id,
                    DisplayName = "Load #" + item.LoadNumber/* + " (PO: " + item.PO + ", BOL: "+ item.BOL + ")"*/
                });

            return new ResponseModel<List<LoadForAutocomplete>>()
            {
                IsSuccess = true,
                TotalItems = items.Count(),
                Result = items.OrderBy(item => item.Id).Skip(offset).Take(pageSize).ToList()
            };
        }
        public ResponseModel<LoadForAutocomplete> GetForAutocomplete(int id)
        {
            load dr = context.loads.Where(item => item.Id == id).FirstOrDefault();
            if (dr != null)
            {
                LoadForAutocomplete result = new LoadForAutocomplete();
                result.Id = dr.Id;
                result.DisplayName = "Load #" + dr.LoadNumber;/* + " (PO: " + dr.PO + ", BOL: " + dr.BOL + ")";*/
                return new ResponseModel<LoadForAutocomplete>()
                {
                    IsSuccess = true,
                    Result = result
                };
            } else
            {
                return new ResponseModel<LoadForAutocomplete>()
                {
                    IsSuccess = false,
                    Message = "Load not found!"
                };
            }
        }
        public ResponseModel<List<LoadModel>> Get(int offset, int pageSize, string searchParam, bool active)
        {
            if (searchParam == null)
                searchParam = "";

            IQueryable<load> items = context.loads.AsNoTracking().Where(item =>
                !active || item.IsActive == true);

            return new ResponseModel<List<LoadModel>>()
            {
                IsSuccess = true,
                TotalItems = items.Count(),
                Result = items.OrderBy(item => item.Id).Skip(offset).Take(pageSize).AsEnumerable().Select(item => item.ToModel()).ToList()
            };
        }
        public ResponseModel<List<LoadModel>> GetLoadsByTripId(int tripId)
        {
            IQueryable<load> result = context.loads.AsNoTracking().Where(item => item.TourId == tripId);//.AsEnumerable().Select(item => item.ToModel()).ToList();
            return new ResponseModel<List<LoadModel>>()
            {
                IsSuccess = true,
                TotalItems = result.Count(),
                Result = result.AsEnumerable().Select(item => item.ToModel()).ToList()
            };
        }
        public ResponseModel<List<LoadPreviewModel>> GetUnassignedLoads(DateTime? startDate, int offset, int pageSize, string searchParam, int companyId)
        {
            if (searchParam == null)
                searchParam = "";

            IQueryable<LoadPreviewModel> items = context.loads.AsNoTracking().Where(item =>
                (startDate == null || item.DateFrom >= startDate) &&
                item.IsActive == true &&
                item.CompanyId == companyId &&
                item.TourId == null && 
                (item.LoadNumber.Contains(searchParam) ||
                item.BOL.Contains(searchParam) ||
                item.PO.Contains(searchParam))).Select(item => new LoadPreviewModel()
                {
                    BOL = item.BOL,
                    Brokerage = item.client != null ? item.client.DisplayName : "",
                    Confirmation = item.Confirmation,
                    Id = item.Id,
                    LoadNumber = item.LoadNumber,
                    Miles = item.TotalDistanceMi,
                    PaidInFull = item.PaidInFull,
                    PendingIssue = item.PendingIssue,
                    DispatchPaid = item.DispatchPaid,
                    PO = item.PO,
                    Price = item.AgreedPrice
                });

            List<LoadPreviewModel> list = items.OrderBy(item => item.Id).Skip(offset).Take(pageSize).ToList();
            foreach (LoadPreviewModel preview in list)
            {
                load_point startPoint = context.load_point.Where(pp => pp.LoadId == preview.Id).OrderBy(pp => pp.OrderNumber).FirstOrDefault();
                preview.FromDate = startPoint != null ? startPoint.DateLoad : default(DateTime?);
                preview.LocationStart = startPoint != null ? startPoint.Location : "";

                load_point endPoint = context.load_point.Where(pp => pp.LoadId == preview.Id).OrderByDescending(pp => pp.OrderNumber).FirstOrDefault();
                preview.ToDate = endPoint != null ? endPoint.DateLoad : default(DateTime?);
                preview.LocationEnd = endPoint != null ? endPoint.Location : "";
            }

            return new ResponseModel<List<LoadPreviewModel>>()
            {
                IsSuccess = true,
                TotalItems = items.Count(),
                Result = list
            };
        }
        public ResponseModel<List<DriverPayroll>> GetDriverPayroll(int offset, int pageSize, int companyId, int driverId)
        {
            IQueryable<DriverPayroll> result = context.loads.Where(item => item.PaidInFull == false &&
                                                    item.TourId > 0 &&
                                                    (item.tour.DriverAId == driverId || item.tour.DriverBId == driverId)).Select(item=> new DriverPayroll()
                                                    {
                                                        LoadId = item.Id,
                                                        LoadNumber = item.LoadNumber,
                                                        DateFrom = item.DateFrom.Value,
                                                        DateTo = item.DateTo.Value,
                                                        DriverId = driverId,
                                                        Driver = item.tour.DriverAId == driverId ? item.tour.driver.FirstName : item.tour.driver1.FirstName
                                                    });

            return new ResponseModel<List<DriverPayroll>>()
            {
                IsSuccess = true,
                TotalItems = result.Count(),
                Result = result.OrderBy(item => item.DateFrom).Skip(offset).Take(pageSize).ToList()
            };
        }
        public ResponseModel<LoadModel> Get(int id)
        {
            load dr = context.loads.Where(item => item.Id == id).FirstOrDefault();
            if (dr != null)
            {
                return new ResponseModel<LoadModel>()
                {
                    IsSuccess = true,
                    Result = dr.ToModel()
                };
            }
            else
            {
                return new ResponseModel<LoadModel>()
                {
                    IsSuccess = false,
                    Message = "Load not found!"
                };
            }
        }
        public ResponseModel<string> GetNextLoadNumber()
        {
            load lastLoad = context.loads.OrderByDescending(item => item.Id).FirstOrDefault();
            string loadNo = "DPAL_1";
            if (lastLoad != null)
            {
                string temp = lastLoad.LoadNumber.Substring(5, lastLoad.LoadNumber.Length - 5);
                int tempInt = Convert.ToInt32(temp) + 1;
                loadNo = "DPAL_" + tempInt;
            }

            return new ResponseModel<string>()
            {
                IsSuccess = true,
                Result = loadNo
            };
        }
        public ResponseModel<LoadModel> Save(LoadModel model)
        {
            try
            {
                load dr = context.loads.Where(item => item.Id == model.Id).FirstOrDefault();
                if (dr == null)
                {
                    
                    load lastLoad = context.loads.OrderByDescending(item=>item.Id).FirstOrDefault();
                    string loadNo = "DPAL_1";
                    if(lastLoad != null)
                    {
                        string[] parts = lastLoad.LoadNumber.Split('_');
                        if (parts.Length > 1)
                        {
                            int tempInt = Convert.ToInt32(parts[1]) + 1;
                            loadNo = "DPAL_" + tempInt;
                        }
                    }
                    dr = new load();
                    dr.LoadNumber = loadNo;

                    int numberOfLoadPoints = context.load_point.AsNoTracking().Where(item => item.TourId == model.TourId).Count() + 2;
                    dr.ExtraStops = numberOfLoadPoints - 2 > 0 ? numberOfLoadPoints - 2 : 0;

                    if (model.LoadPoints != null)
                    {
                        if (model.LoadPoints.Count > 0)
                        {
                            if (model.TourId > 0)
                            {
                                model.LoadPoints[0].TourId = model.TourId;
                                ResponseModel<bool> validationResult = LoadPointsManager.ValidateLoadPointDate(model.LoadPoints[0]);
                                if (!validationResult.IsSuccess)
                                {
                                    return new ResponseModel<LoadModel>()
                                    {
                                        IsSuccess = false,
                                        Message = validationResult.Message
                                    };
                                }
                            }

                            load_point stLP = new load_point();
                            stLP.TourId = model.TourId;
                            stLP.Lat = model.LoadPoints[0].Lat;
                            stLP.Lng = model.LoadPoints[0].Lng;
                            stLP.Location = model.LoadPoints[0].Location;
                            stLP.Country = model.LoadPoints[0].Country;
                            stLP.State = model.LoadPoints[0].State;
                            stLP.City = model.LoadPoints[0].City;
                            stLP.Address = model.LoadPoints[0].Address;
                            stLP.ZipCode = model.LoadPoints[0].ZipCode;
                            stLP.DateLoad = model.LoadPoints[0].DateLoad.ToUniversalTime();
                            stLP.OrderNumber = 1;
                            stLP.ActionType = (int)LoadPointType.Loading;
                            stLP.Weight = 0;
                            dr.load_point.Add(stLP);
                        }

                        if (model.LoadPoints.Count > 1)
                        {
                            if (model.TourId > 0)
                            {
                                model.LoadPoints[1].TourId = model.TourId;
                                ResponseModel<bool> validationResult = LoadPointsManager.ValidateLoadPointDate(model.LoadPoints[1]);
                                if (!validationResult.IsSuccess)
                                {
                                    return new ResponseModel<LoadModel>()
                                    {
                                        IsSuccess = false,
                                        Message = validationResult.Message
                                    };
                                }
                            }

                            load_point stLP = new load_point();
                            stLP.TourId = model.TourId;
                            stLP.Lat = model.LoadPoints[1].Lat;
                            stLP.Lng = model.LoadPoints[1].Lng;
                            stLP.Location = model.LoadPoints[1].Location;
                            stLP.Country = model.LoadPoints[1].Country;
                            stLP.State = model.LoadPoints[1].State;
                            stLP.City = model.LoadPoints[1].City;
                            stLP.Address = model.LoadPoints[1].Address;
                            stLP.ZipCode = model.LoadPoints[1].ZipCode;
                            stLP.DateLoad = model.LoadPoints[1].DateLoad.ToUniversalTime();
                            stLP.OrderNumber = 2;
                            stLP.ActionType = (int)LoadPointType.Unloading;
                            stLP.Weight = 0;
                            dr.load_point.Add(stLP);
                        }
                    }

                    context.loads.Add(dr);
                }

                client shipper = context.clients.Where(item => item.Id == model.PartnerShipperId).FirstOrDefault();
                if(shipper != null)
                {
                    shipper.PhoneNumber = model.PartnerShipperPhone;
                    shipper.ContactName = model.PartnerShipperContact;
                }

                client consignee = context.clients.Where(item => item.Id == model.PartnerConsigneeId).FirstOrDefault();
                if (consignee != null)
                {
                    consignee.PhoneNumber = model.PartnerConsigneePhone;
                    consignee.ContactName = model.PartnerConsigneeContact;
                }

                dr.AgreedPrice = model.AgreedPrice;
                dr.FuelSurcharge = model.FuelSurcharge;
                dr.BOL = model.BOL;
                dr.BookingDate = model.BookingDate != null ? model.BookingDate.Value.ToUniversalTime() : default(DateTime?);
                dr.Broker1Id = model.Broker1Id;
                dr.BrokerId = model.BrokerId;
                dr.BrokerRating = model.BrokerRating;
                dr.Chassis = model.Chassis;
                dr.Commodity = model.Commodity;
                dr.CompanyId = model.CompanyId;
                dr.Confirmation = model.Confirmation;
                dr.DateFrom = model.DateFrom.HasValue ? model.DateFrom.Value.ToUniversalTime() : default(DateTime?);
                dr.DateTo = model.DateTo.HasValue ? model.DateTo.Value.ToUniversalTime() : default(DateTime?);
                dr.DispatcherId = model.DispatcherId;
                dr.FlatRate = model.FlatRate;
                dr.IsActive = model.IsActive;
                dr.LoadType = model.LoadType;
                dr.ModifiedBy = 1;
                dr.BrokerageId = model.BrokerageId;
                dr.PartnerConsigneeId = model.PartnerConsigneeId;
                dr.PartnerShipperId = model.PartnerShipperId;
                dr.PO = model.PO;
                dr.Quantity = model.Quantity;
                dr.Size = model.Size;
                dr.Status = model.Status;
                dr.PaidInFull = model.PaidInFull;
                dr.PendingIssue = model.PendingIssue;
                dr.Temperature = model.Temperature;
                dr.TotalDistanceKm = model.TotalDistanceKm;
                dr.TotalDistanceMi = model.TotalDistanceMi;
                dr.TourId = model.TourId > 0 ? model.TourId : default(int?);
                dr.Weight = model.Weight;
                dr.DriverNotes = model.DriverNotes;
                dr.InternalNotes = model.InternalNotes;
                dr.ExternalNotes = model.ExternalNotes;
                dr.ShippingNumber = model.ShippingNumber;
                dr.ShippingNote = model.ShippingNote;
                dr.DeliveryNumber = model.DeliveryNumber;
                dr.DeliveryNote = model.DeliveryNote;
                dr.BillableMiles = model.BillableMiles;
                dr.BobtailMiles = model.BobtailMiles;
                dr.EmptyMiles = model.EmptyMiles;

                if(model.Id != 0)
                    dr.ExtraStops = model.ExtraStops;

                context.SaveChanges();

                broker brok = context.brokers.Where(item => item.Id == model.BrokerId).FirstOrDefault();
                if (brok != null)
                {
                    brok.PhoneNumber = model.BrokerPhoneNumber;
                    brok.Fax = model.BrokerFax;
                    brok.Email = model.BrokerEmail;
                    brok.Rating = CalculateBrokerRating(brok.Id);

                    context.SaveChanges();
                }

                if(model.LoadPoints != null && model.LoadPoints.Count > 0)
                {
                    LoadPointsManager.ReorderAllLoadPoints(model.TourId, dr.Id);
                }

                LoadPointsManager.SetStartAndEndDates(dr.TourId != null ? (int)dr.TourId : 0, dr.Id);

                return new ResponseModel<LoadModel>()
                {
                    IsSuccess = true,
                    Result = dr.ToModel()
                }; 
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public ResponseModel<bool> Delete(int id)
        {
            load dr = context.loads.Where(item => item.Id == id).FirstOrDefault();
            if (dr != null)
            {
                dr.TourId = null;
                foreach (load_point pt in dr.load_point)
                    pt.TourId = null;
                context.SaveChanges();

                LoadPointsManager.SetStartAndEndDates(dr.TourId != null ? (int)dr.TourId : 0, dr.Id);

                return new ResponseModel<bool>()
                {
                    IsSuccess = true
                };
            }
            else
            {
                return new ResponseModel<bool>()
                {
                    IsSuccess = false,
                    Message = "Load not found!"
                };
            }
        }
        public ResponseModel<bool> AssignLoadToTrip(int loadId, int tripId)
        {
            load ld = context.loads.Where(item => item.Id == loadId).FirstOrDefault();
            if (ld != null)
            {
                ld.TourId = tripId;
                foreach (load_point pt in ld.load_point)
                {
                    pt.TourId = tripId;
                }

                foreach (load_point pt in ld.load_point)
                {
                    ResponseModel<bool> validationResult = LoadPointsManager.ValidateLoadPointDate(pt.ToModel());
                    if (!validationResult.IsSuccess)
                    {
                        return new ResponseModel<bool>()
                        {
                            IsSuccess = false,
                            Message = validationResult.Message
                        };
                    }
                }

                context.SaveChanges();

                if (LoadPointsManager.ReorderAllLoadPoints(tripId, 0))
                {
                    LoadPointsManager.SetStartAndEndDates(ld.TourId != null ? (int)ld.TourId : 0, ld.Id);
                    return new ResponseModel<bool>()
                    {
                        IsSuccess = true
                    };
                } else
                {
                    return new ResponseModel<bool>()
                    {
                        IsSuccess = false,
                        Message = "There was an error when trying to reorder load points! Please try again."
                    };
                }
            }
            else
            {
                return new ResponseModel<bool>()
                {
                    IsSuccess = false,
                    Message = "Load not found!"
                };
            }
        }
        private decimal CalculateBrokerRating(int brokerId)
        {
            IQueryable<load> loads = context.loads.Where(item => item.IsActive == true && item.BrokerId == brokerId);

            int count = loads.Count();
            int? sumRating = loads.Sum(item => item.BrokerRating);

            decimal result = (decimal)sumRating / count;

            return result < 1 ? 1 : result;
        }
        public ResponseModel<bool> ToggleLoadIssueResolved(int loadIssueId)
        {
            load_issue issue = context.load_issue.Where(item => item.Id == loadIssueId).FirstOrDefault();
            if (issue != null)
            {
                issue.IsResolved = !issue.IsResolved;
                context.SaveChanges();

                return new ResponseModel<bool>()
                {
                    IsSuccess = true
                };
            }
            else
            {
                return new ResponseModel<bool>()
                {
                    IsSuccess = false,
                    Message = "Issue not found!"
                };
            }
        }

        public ResponseModel<int> SaveLoadIssue(LoadIssueModel model)
        {
            try
            {
                load_issue loadIssue = context.load_issue.Where(item => item.Id == model.Id).FirstOrDefault();
                if (loadIssue == null)
                {
                    loadIssue = new load_issue();
                    loadIssue.LoadId = model.LoadId;
                    context.load_issue.Add(loadIssue);
                }

                loadIssue.Description = model.Description;
                loadIssue.IsResolved = false;

                context.SaveChanges();
                return new ResponseModel<int>()
                {
                    IsSuccess = true,
                    Result = loadIssue.Id
                };

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
