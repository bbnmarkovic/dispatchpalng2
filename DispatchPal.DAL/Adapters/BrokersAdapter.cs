﻿using DispatchPal.DAL.Adapters.Interface;
using DispatchPal.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DispatchPal.DAL.Adapters
{
    public class BrokersAdapter : BaseAdapter, IBrokersAdapter
    {
        public BrokersAdapter(dbEntities1 context) : base(context)
        {
        }

        public ResponseModel<List<BrokerModel>> Get(int offset, int pageSize, string searchParam, bool active, int brokerageId)
        {
            if (searchParam == null)
                searchParam = "";

            IQueryable<broker> items = context.brokers.AsNoTracking().Where(item =>
                (brokerageId == 0 || item.PartnerId == brokerageId) &&
                (!active || item.IsActive == true) &&
                (item.DisplayName.Contains(searchParam) ||
                item.FirstName.Contains(searchParam) ||
                item.LastName.Contains(searchParam)));

            return new ResponseModel<List<BrokerModel>>()
            {
                IsSuccess = true,
                TotalItems = items.Count(),
                Result = items.OrderBy(item => item.Id).Skip(offset).Take(pageSize).AsEnumerable().Select(item => item.ToModel()).ToList()
            };
        }
        public ResponseModel<BrokerModel> Get(int id)
        {
            broker dr = context.brokers.Where(item => item.Id == id).FirstOrDefault();
            if (dr != null)
            {
                return new ResponseModel<BrokerModel>()
                {
                    IsSuccess = true,
                    Result = dr.ToModel()
                };
            }
            else
            {
                return new ResponseModel<BrokerModel>()
                {
                    IsSuccess = false,
                    Message = "Broker not found"
                };
            }
        }
        public ResponseModel<decimal> GetBrokerRating(int id)
        {
            broker br = context.brokers.AsNoTracking().Where(item => item.Id == id).FirstOrDefault();
            return new ResponseModel<decimal>()
            {
                IsSuccess = true,
                Result = br != null ? br.Rating : 1
            };
        }
        public ResponseModel<BrokerModel> Save(BrokerModel model)
        {
            try
            {
                broker dr = context.brokers.Where(item => item.Id == model.Id).FirstOrDefault();
                if (dr == null)
                {
                    dr = new broker();
                    context.brokers.Add(dr);
                }
                dr.CompanyId = model.CompanyId;
                dr.DisplayName = model.DisplayName;
                dr.Email = model.Email;
                dr.Fax = model.Fax;
                dr.Fax1 = model.Fax1;
                dr.FirstName = model.FirstName;
                dr.IsActive = model.IsActive;
                dr.LastName = model.LastName;
                dr.MobileNumber = model.MobileNumber;
                dr.PartnerId = model.PartnerId;
                dr.PhoneNumber = model.PhoneNumber;
                dr.Rate = model.Rate;

                context.SaveChanges();

                return new ResponseModel<BrokerModel>()
                {
                    IsSuccess = true,
                    Result = dr.ToModel()
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ResponseModel<bool> Delete(int id)
        {
            broker dr = context.brokers.Where(item => item.Id == id).FirstOrDefault();
            if (dr != null)
            {
                context.brokers.Remove(dr);
                context.SaveChanges();

                return new ResponseModel<bool>()
                {
                    IsSuccess = true
                };
            }
            else
            {
                return new ResponseModel<bool>()
                {
                    IsSuccess = false,
                    Message = "Broker not found!"
                };
            }
        }
    }
}
