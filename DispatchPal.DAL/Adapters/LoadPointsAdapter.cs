﻿using DispatchPal.DAL.Adapters.Interface;
using DispatchPal.DAL.Models;
using DispatchPal.DAL.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using static DispatchPal.DAL.Models.Enums.DplEnums;

namespace DispatchPal.DAL.Adapters
{
    public class LoadPointsAdapter : BaseAdapter, ILoadPointsAdapter
    {
        public LoadPointsAdapter(dbEntities1 context) : base(context)
        {
        }
        public ResponseModel<List<LoadPointModel>> GetLoadPointsByLoadId(int loadId)
        {
            IQueryable<load_point> result = context.load_point.AsNoTracking().Where(item => item.LoadId == loadId).OrderBy(item => item.OrderNumber);

            return new ResponseModel<List<LoadPointModel>>()
            {
                IsSuccess = true,
                TotalItems = result.Count(),
                Result = result.AsEnumerable().Select(item => item.ToModel()).ToList()
            };
        }
        public ResponseModel<List<LoadPointModel>> GetLoadPointsByTripId(int tripId)
        {
            IQueryable<load_point> result = context.load_point.AsNoTracking().Where(item => item.TourId == tripId).OrderBy(item => item.OrderNumber);

            return new ResponseModel<List<LoadPointModel>>()
            {
                IsSuccess = true,
                TotalItems = result.Count(),
                Result = result.AsEnumerable().Select(item => item.ToModel()).ToList()
            };
        }
        public ResponseModel<LoadPointModel> Get(int id)
        {
            load_point dr = context.load_point.Where(item => item.Id == id).FirstOrDefault();
            if (dr != null)
            {
                return new ResponseModel<LoadPointModel>()
                {
                    IsSuccess = true,
                    Result = dr.ToModel()
                };
            }
            else
            {
                return new ResponseModel<LoadPointModel>()
                {
                    IsSuccess = false,
                    Message = "Load point not found!"
                };
            }
        }
        public ResponseModel<DateTime> GetStartingLoadPointDate(int tripId)
        {
            load_point dr = context.load_point.Where(item => item.Id == tripId && item.OrderNumber == 0).FirstOrDefault();
            if (dr != null)
            {
                return new ResponseModel<DateTime>()
                {
                    IsSuccess = true,
                    Result = dr.DateLoad
                };
            }
            else
            {
                return new ResponseModel<DateTime>()
                {
                    IsSuccess = false,
                    Message = "Load point not found!"
                };
            }
        }
        public ResponseModel<LoadPointModel> Save(LoadPointModel model)
        {
            try
            {
                load ld = context.loads.Where(item => item.Id == model.LoadId).FirstOrDefault();

                ResponseModel<bool> validationResult = LoadPointsManager.ValidateLoadPointDate(model);
                if (!validationResult.IsSuccess)
                {
                    return new ResponseModel<LoadPointModel>()
                    {
                        IsSuccess = false,
                        Message = validationResult.Message
                    };
                }

                load_point dr = context.load_point.Where(item => item.Id == model.Id).FirstOrDefault();
                if (dr == null)
                {
                    int orderNumber = 1;
                    IQueryable<load_point> query = context.load_point.Where(item => (model.TourId > 0 && item.TourId == model.TourId) || item.LoadId == model.LoadId);
                    if (query.Count() > 0)
                    {
                        orderNumber = query.Max(item => item.OrderNumber) + 1;
                    }

                    dr = new load_point();
                    dr.LoadId = model.LoadId;
                    dr.TourId = model.TourId;
                    dr.OrderNumber = orderNumber;
                    context.load_point.Add(dr);
                }
                dr.ActionType = model.ActionType;
                dr.CompanyId = model.CompanyId;
                dr.DateLoad = model.DateLoad.ToUniversalTime();
                dr.DeliveryNotes = model.DeliveryNotes;
                dr.Description = model.Description;
                dr.DriverId = model.DriverId;
                dr.EmptyPosition = model.EmptyPosition;
                dr.Lat = model.Lat;
                dr.Lng = model.Lng;
                dr.Location = model.Location;
                dr.Country = model.Country;
                dr.State = model.State;
                dr.City = model.City;
                dr.Address = model.Address;
                dr.ZipCode = model.ZipCode;
                dr.NumbersPO = model.NumbersPO;
                dr.Qty = model.Qty;
                dr.Weight = model.Weight;
                dr.Contact = model.Contact;
                dr.Phone = model.Phone;

                context.SaveChanges();

                
                if(ld != null)
                {
                    LoadPointsManager.ReorderAllLoadPoints(model.TourId != null ? (int)model.TourId : 0, (int)model.LoadId);
                }

                LoadPointsManager.SetStartAndEndDates(model.TourId != null ? (int)model.TourId : 0, model.LoadId != null ? (int)model.LoadId : 0);

                load_point pt = context.load_point.Where(item => item.Id == dr.Id).FirstOrDefault();
                if(pt != null)
                {
                    return new ResponseModel<LoadPointModel>()
                    {
                        IsSuccess = true,
                        Result = pt.ToModel()
                    };
                }
                else
                {
                    return new ResponseModel<LoadPointModel>()
                    {
                        IsSuccess = false,
                        Message = "Load point not found!"
                    };
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public ResponseModel<LoadPointModel> SaveStartingLoadPoint(LoadPointModel model)
        {
            try
            {
                load_point dr = context.load_point.Where(item => item.Id == model.Id).FirstOrDefault();
                if (dr == null)
                {
                    dr = new load_point();
                    dr.ActionType = 0;
                    dr.LoadId = model.LoadId;
                    dr.TourId = model.TourId;
                    dr.OrderNumber = 0;
                    context.load_point.Add(dr);
                }
                
                dr.CompanyId = model.CompanyId;
                dr.DateLoad = model.DateLoad.ToUniversalTime();
                dr.DeliveryNotes = model.DeliveryNotes;
                dr.Description = model.Description;
                dr.DriverId = model.DriverId;
                dr.EmptyPosition = model.EmptyPosition;
                dr.Lat = model.Lat;
                dr.Lng = model.Lng;
                dr.Location = model.Location;
                dr.Country = model.Country;
                dr.State = model.State;
                dr.City = model.City;
                dr.Address = model.Address;
                dr.ZipCode = model.ZipCode;
                dr.NumbersPO = model.NumbersPO;
                dr.Qty = model.Qty;
                dr.Weight = model.Weight;
                dr.Contact = model.Contact;
                dr.Phone = model.Phone;

                context.SaveChanges();

                load_point pt = context.load_point.Where(item => item.Id == dr.Id).FirstOrDefault();
                if (pt != null)
                {
                    return new ResponseModel<LoadPointModel>()
                    {
                        IsSuccess = true,
                        Result = pt.ToModel()
                    };
                }
                else
                {
                    return new ResponseModel<LoadPointModel>()
                    {
                        IsSuccess = false,
                        Message = "Load point not found!"
                    };
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public ResponseModel<bool> UpdateDistances(List<LoadPointModel> loadPoints)
        {
            foreach(LoadPointModel item in loadPoints)
            {
                load_point lp = context.load_point.Where(pp => pp.Id == item.Id).FirstOrDefault();
                if(lp != null)
                {
                    lp.Distance = item.Distance;
                    lp.DistanceFromStart = loadPoints.Where(pp => pp.OrderNumber <= lp.OrderNumber).Count() > 0 ? loadPoints.Where(pp => pp.OrderNumber <= lp.OrderNumber).Sum(pp => pp.Distance) : 0;
                    lp.EstimatedTime = item.EstimatedTime;
                }
            }
            context.SaveChanges();
            return new ResponseModel<bool>()
            {
                IsSuccess = true
            };
        }
        public ResponseModel<bool> Delete(int id)
        {
            load_point lp = context.load_point.Where(item => item.Id == id).FirstOrDefault();
            if (lp != null)
            {
                int loadId = lp.LoadId != null ? (int)lp.LoadId : 0;
                int orderNumber = lp.OrderNumber;
                context.load_point.Remove(lp);
                context.SaveChanges();

                load ld = context.loads.Where(item => item.Id == loadId).FirstOrDefault();
                if (ld != null)
                {
                    LoadPointsManager.ReorderAllLoadPoints(lp.TourId != null ? (int)lp.TourId : 0, lp.LoadId != null ? (int)lp.LoadId : 0);
                }

                LoadPointsManager.SetStartAndEndDates(lp.TourId != null ? (int)lp.TourId : 0, lp.LoadId != null ? (int)lp.LoadId : 0);

                return new ResponseModel<bool>()
                {
                    IsSuccess = true
                };
            }
            else
            {
                return new ResponseModel<bool>()
                {
                    IsSuccess = false,
                    Message = "Load point not found!"
                };
            }
        }
        public ResponseModel<List<LoadPointModel>> GetLoadPointsForPlanner(int driverId, DateTime fromDate, DateTime toDate)
        {
            DateTime endDate = toDate.AddDays(1).Date;
            IQueryable<load_point> result = context.load_point.AsNoTracking().Where(item => item.load != null &&
                                                            item.load.tour != null &&
                                                            (item.load.tour.DriverAId == driverId || item.load.tour.DriverBId == driverId) &&
                                                            item.DateLoad > fromDate && 
                                                            item.DateLoad < endDate).OrderBy(item => item.OrderNumber);

            return new ResponseModel<List<LoadPointModel>>()
            {
                IsSuccess = true,
                TotalItems = result.Count(),
                Result = result.AsEnumerable().Select(item => item.ToModel()).ToList()
            };
        }
        public ResponseModel<List<LoadPointModel>> SetOrderNumber(int loadPointId, int newOrderNumber)
        {
            load_point pointToReorder = context.load_point.Where(item => item.Id == loadPointId).FirstOrDefault();
            if(pointToReorder != null)
            {
                if(newOrderNumber == 1 && pointToReorder.ActionType == (int)LoadPointType.Unloading)
                {
                    return new ResponseModel<List<LoadPointModel>>
                    {
                        IsSuccess = false,
                        Result = new List<LoadPointModel>(),
                        Message = "Unloading load point can't be first load point!"
                    };
                }
                List<load_point> pointsList = context.load_point.Where(item => (pointToReorder.TourId > 0 && item.TourId == pointToReorder.TourId) || item.LoadId == pointToReorder.LoadId).ToList();
                foreach(load_point pt in pointsList)
                {
                    if (pt.Id != pointToReorder.Id)
                    {
                        if (newOrderNumber <= pt.OrderNumber && pt.OrderNumber < pointToReorder.OrderNumber)
                        {
                            pt.OrderNumber++;
                        }
                        else if (newOrderNumber >= pt.OrderNumber && pointToReorder.OrderNumber < pt.OrderNumber)
                        {
                            pt.OrderNumber--;
                        }
                    }
                }
                pointToReorder.OrderNumber = newOrderNumber;

                context.SaveChanges();

                return pointToReorder.TourId > 0 ? GetLoadPointsByTripId((int)pointToReorder.TourId) : GetLoadPointsByLoadId((int)pointToReorder.LoadId);
            }

            return null;
        }
    }
}
