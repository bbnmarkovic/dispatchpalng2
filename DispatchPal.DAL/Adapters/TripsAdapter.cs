﻿using DispatchPal.DAL.Adapters.Interface;
using DispatchPal.DAL.Models;
using DispatchPal.DAL.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static DispatchPal.DAL.Models.Enums.DplEnums;

namespace DispatchPal.DAL.Adapters
{
    public class TripsAdapter : BaseAdapter, ITripsAdapter
    {
        public TripsAdapter(dbEntities1 context) : base(context)
        {
        }

        public ResponseModel<List<TripPreviewModel>> Get(int offset, int pageSize, string searchParam, int active, bool includeFinished, int companyId)
        {
            if (searchParam == null)
                searchParam = "";

            IQueryable<TripPreviewModel> items = context.tours.AsNoTracking().Where(item =>
                (active == -1 || (active == 0 && item.IsActive == false) || (active == 1 && item.IsActive == true)) &&
                (companyId == 0 || item.CompanyId == companyId) &&
                (includeFinished || item.IsFinished == false) &&
                item.TripNumber.Contains(searchParam)).Select(item=>new TripPreviewModel()
                {
                    CompanyId = item.CompanyId,
                    DateFrom = item.DateFrom,
                    DateTo = item.DateTo,
                    DriverA = item.driver != null ? item.driver.DisplayName : "",
                    DriverB = item.driver1 != null ? item.driver1.DisplayName : "",
                    Id = item.Id,
                    IsActive = item.IsActive,
                    IsFinished = item.IsFinished,
                    PayRateBobtailDriverA = item.PayRateBobtailDriverA,
                    PayRateBobtailDriverB = item.PayRateBobtailDriverB,
                    PayRateEmptyDriverA = item.PayRateEmptyDriverA,
                    PayRateEmptyDriverB = item.PayRateEmptyDriverB,
                    PayRateFullDriverA = item.PayRateFullDriverA,
                    PayRateFullDriverB = item.PayRateFullDriverB,
                    PayTypeDriverA = item.PayTypeDriverA == 0 ? "Per Mile" : (item.PayTypeDriverA == 1 ? "Percentage" : (item.PayTypeDriverA == 2 ? "Per Hour" : "")),
                    PayTypeDriverB = item.PayTypeDriverB == 0 ? "Per Mile" : (item.PayTypeDriverB == 1 ? "Percentage" : (item.PayTypeDriverB == 2 ? "Per Hour" : "")),
                    Trailer = item.vehicle != null ? item.vehicle.VehicleID : "",
                    TripNumber = item.TripNumber,
                    Truck = item.vehicle1 != null ? item.vehicle1.VehicleID : ""
                });

            List<TripPreviewModel> list = items.OrderBy(item => item.Id).Skip(offset).Take(pageSize).ToList();
            foreach (TripPreviewModel preview in list)
            {
                load_point startPoint = context.load_point.Where(pp => pp.TourId == preview.Id && pp.OrderNumber > 0 && pp.ActionType == (int)LoadPointType.Loading).OrderBy(pp => pp.DateLoad).FirstOrDefault();
                preview.DateFrom = startPoint != null ? startPoint.DateLoad : default(DateTime?);

                load_point endPoint = context.load_point.Where(pp => pp.TourId == preview.Id && pp.OrderNumber > 0 && pp.ActionType == (int)LoadPointType.Unloading).OrderByDescending(pp => pp.DateLoad).FirstOrDefault();
                preview.DateTo = endPoint != null ? endPoint.DateLoad : default(DateTime?);
            }

            return new ResponseModel<List<TripPreviewModel>>()
            {
                IsSuccess = true,
                TotalItems = items.Count(),
                Result = list
            };
        }
        public ResponseModel<List<LoadPointModel>> GetForPlanner(int driverId, DateTime date)
        {
            DateTime dateFrom = date.Date.AddDays(-date.Day + 1);
            DateTime dateTo = date.Date.AddMonths(1).AddDays(-date.Day + 1);
            var temp = (from dpPoint in context.load_point
                       join dpLoad in context.loads on dpPoint.LoadId equals dpLoad.Id
                       join dpTrip in context.tours on dpLoad.TourId equals dpTrip.Id
                       where dpPoint.DateLoad >= dateFrom &&
                             dpPoint.DateLoad < dateTo &&
                             dpTrip != null &&
                             dpTrip.IsActive == true &&
                             dpTrip.IsFinished == false &&
                             (dpTrip.DriverAId == driverId || dpTrip.DriverBId == driverId)
                       select new LoadPointModel()
                       {
                           Id = dpPoint.Id,
                           TourId = dpPoint.TourId,
                           LoadId = dpPoint.LoadId,
                           OrderNumber = dpPoint.OrderNumber,
                           ActionType = dpPoint.ActionType,
                           DateLoad = dpPoint.DateLoad,
                           Location = dpPoint.Location,
                           Description = dpPoint.Description,
                           Qty = dpPoint.Qty,
                           Weight = dpPoint.Weight,
                           DeliveryNotes = dpPoint.DeliveryNotes,
                           NumbersPO = dpPoint.NumbersPO,
                           Lat = dpPoint.Lat,
                           Lng = dpPoint.Lng,
                           EmptyPosition = dpPoint.EmptyPosition,
                           Contact = dpPoint.Contact,
                           Phone = dpPoint.Phone,
                           DriverId = dpPoint.DriverId,
                           CompanyId = dpPoint.CompanyId
                       });
            /*IQueryable < TripPreviewModel > items = context.load_point.AsNoTracking().Where(item => item.DateLoad >= dateFrom &&
                                                       item.DateLoad < dateTo &&
                                                       item.load.tour != null &&
                                                       item.load.tour.IsFinished == false &&
                                                       item.load.tour.IsActive == true &&
                                                       (item.load.tour.DriverAId == driverId || item.load.tour.DriverBId == driverId)).Select(item => new TripPreviewModel()
                                                       {
                                                           CompanyId = item.load.tour.CompanyId,
                                                           DriverA = item.load.tour.driver != null ? item.load.tour.driver.DisplayName : "",
                                                           DriverB = item.load.tour.driver1 != null ? item.load.tour.driver1.DisplayName : "",
                                                           Id = item.Id,
                                                           IsActive = item.load.tour.IsActive,
                                                           IsFinished = item.load.tour.IsFinished,
                                                           PayRateBobtailDriverA = item.load.tour.PayRateBobtailDriverA,
                                                           PayRateBobtailDriverB = item.load.tour.PayRateBobtailDriverB,
                                                           PayRateEmptyDriverA = item.load.tour.PayRateEmptyDriverA,
                                                           PayRateEmptyDriverB = item.load.tour.PayRateEmptyDriverB,
                                                           PayRateFullDriverA = item.load.tour.PayRateFullDriverA,
                                                           PayRateFullDriverB = item.load.tour.PayRateFullDriverB,
                                                           PayTypeDriverA = item.load.tour.PayTypeDriverA,
                                                           PayTypeDriverB = item.load.tour.PayTypeDriverB,
                                                           Trailer = item.load.tour.vehicle != null ? item.load.tour.vehicle.DisplayName : "",
                                                           TripNumber = item.load.tour.TripNumber,
                                                           Truck = item.load.tour.vehicle1 != null ? item.load.tour.vehicle1.DisplayName : ""
                                                       });*/

            /*foreach(TripPreviewModel item in temp)
            {
                IQueryable<load_point> points = context.load_point.AsNoTracking().Where(tt => tt.load != null && tt.load.TourId == item.Id).OrderBy(tt => tt.DateLoad);
                if (points.Count() > 0)
                {
                    item.DateFrom = points.FirstOrDefault().DateLoad;
                    item.DateTo = points.OrderByDescending(tt => tt.DateLoad).FirstOrDefault().DateLoad;
                }
            }*/

            return new ResponseModel<List<LoadPointModel>>()
            {
                IsSuccess = true,
                TotalItems = temp.Count(),
                Result = temp.ToList()
            };
        }
        public ResponseModel<TripModel> Get(int id)
        {
            tour dr = context.tours.Where(item => item.Id == id).FirstOrDefault();
            if (dr != null)
            {
                return new ResponseModel<TripModel>()
                {
                    IsSuccess = true,
                    Result = dr.ToModel()
                };
            }
            else
            {
                return new ResponseModel<TripModel>()
                {
                    IsSuccess = false,
                    Message = "Trip not found!"
                };
            }
        }
        public ResponseModel<string> GetNextTripNumber()
        {
            IQueryable<tour> query = context.tours;
            if (query.Count() > 0)
            {
                int tripNumber = query.Max(item => item.Id) + 1;
                return new ResponseModel<string>()
                {
                    IsSuccess = true,
                    Result = tripNumber > 0 ? tripNumber.ToString() : "1"
                };
            }
            else
            {
                return new ResponseModel<string>()
                {
                    IsSuccess = true,
                    Result = "1"
                };
            }
        }
        public ResponseModel<TripModel> Save(TripModel model)
        {
            try
            {
                bool isNew = false;
                tour dr = context.tours.Where(item => item.Id == model.Id).FirstOrDefault();
                if (dr == null)
                {
                    isNew = true;
                    dr = new tour();
                    dr.TripNumber = GetNextTripNumber().Result;
                    
                    context.tours.Add(dr);
                }

                dr.CompanyId = model.CompanyId;
                dr.DateFrom = model.DateFrom.HasValue ? model.DateFrom.Value.ToUniversalTime() : default(DateTime?);
                dr.DateTo = model.DateTo.HasValue ? model.DateTo.Value.ToUniversalTime() : default(DateTime?);
                dr.DriverAId = model.DriverAId;
                dr.DriverBId = model.DriverBId;
                dr.IsActive = model.IsActive;
                dr.IsFinished = model.IsFinished;
                dr.ModifiedBy = 1;
                dr.ModifiedDate = DateTime.UtcNow;
                dr.TrailerId = model.TrailerId;
                dr.TruckId = model.TruckId;
                dr.PayRateBobtailDriverA = model.PayRateBobtailDriverA;
                dr.PayRateBobtailDriverB = model.PayRateBobtailDriverB;
                dr.PayRateEmptyDriverA = model.PayRateEmptyDriverA;
                dr.PayRateEmptyDriverB = model.PayRateEmptyDriverB;
                dr.PayRateFullDriverA = model.PayRateFullDriverA;
                dr.PayRateFullDriverB = model.PayRateFullDriverB;
                dr.PayTypeDriverA = model.PayTypeDriverA;
                dr.PayTypeDriverB = model.PayTypeDriverB;

                context.SaveChanges();

                if (isNew)//when creating trip we generate starting load point
                {
                    load_point pt = null;
                    load_point previousUnloadPoint = context.load_point.AsNoTracking().Where(item => item.ActionType == (int)LoadPointType.Unloading && item.load.tour != null && item.load.tour.TruckId == model.TruckId).OrderByDescending(item => item.DateLoad).FirstOrDefault();

                    if (previousUnloadPoint != null)
                    {
                        pt = new load_point();
                        pt.Lat = previousUnloadPoint.Lat;
                        pt.Lng = previousUnloadPoint.Lng;
                        pt.Location = previousUnloadPoint.Location;
                        pt.DateLoad = previousUnloadPoint.DateLoad;
                        pt.CompanyId = dr.CompanyId;
                        pt.OrderNumber = 0; // OrderNubmer 0 means it's starting load point
                        pt.ActionType = (int)LoadPointType.Loading;
                        pt.Weight = 0;
                        pt.TourId = dr.Id;

                        context.load_point.Add(pt);
                        context.SaveChanges();
                    }
                }

                return new ResponseModel<TripModel>()
                {
                    IsSuccess = true,
                    Result = dr.ToModel()
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public ResponseModel<bool> Delete(int id)
        {
            tour dr = context.tours.Where(item => item.Id == id).FirstOrDefault();
            if (dr != null)
            {
                context.tours.Remove(dr);
                context.SaveChanges();

                return new ResponseModel<bool>()
                {
                    IsSuccess = true
                };
            }
            else
            {
                return new ResponseModel<bool>()
                {
                    IsSuccess = false,
                    Message = "Trip not found!"
                };
            }
        }
        public ResponseModel<bool> FinalizeTrip(int tripId)
        {
            tour tr = context.tours.Where(item => item.Id == tripId).FirstOrDefault();
            if (tr != null)
            {
                if (context.load_point.Where(item => item.TourId == tripId && item.ActionType == (int)LoadPointType.Loading).FirstOrDefault() == null ||
                    context.load_point.Where(item => item.TourId == tripId && item.ActionType == (int)LoadPointType.Unloading).FirstOrDefault() == null)
                {
                    return new ResponseModel<bool>()
                    {
                        IsSuccess = false,
                        Message = "Trip can't be finalized because it must contain at least one Loading and one Unloading point!"
                    };
                }
                else
                {
                    tr.IsFinished = true;
                    context.SaveChanges();

                    return new ResponseModel<bool>()
                    {
                        IsSuccess = true
                    };
                }
            }
            else
            {
                return new ResponseModel<bool>()
                {
                    IsSuccess = false,
                    Message = "Trip not found!"
                };
            }
        }
        public ResponseModel<bool> ReopenTrip(int tripId)
        {
            tour tr = context.tours.Where(item => item.Id == tripId).FirstOrDefault();
            if (tr != null)
            {
                tr.IsFinished = false;
                context.SaveChanges();

                return new ResponseModel<bool>()
                {
                    IsSuccess = true
                };
            }
            else
            {
                return new ResponseModel<bool>()
                {
                    IsSuccess = false,
                    Message = "Trip not found!"
                };
            }
        }
        public ResponseModel<bool> ImportLoads(List<int> loadIds, int tripId)
        {
            foreach (int loadId in loadIds)
            {
                load ld = context.loads.Where(item => item.Id == loadId).FirstOrDefault();
                if (ld != null)
                {
                    ld.TourId = tripId;
                    foreach (load_point pt in ld.load_point)
                    {
                        pt.TourId = tripId;
                    }

                    foreach (load_point pt in ld.load_point)
                    {
                        ResponseModel<bool> validationResult = LoadPointsManager.ValidateLoadPointDate(pt.ToModel());
                        if (!validationResult.IsSuccess)
                        {
                            return new ResponseModel<bool>()
                            {
                                IsSuccess = false,
                                Message = validationResult.Message
                            };
                        }
                    }
                }
            }
            context.SaveChanges();
            if (LoadPointsManager.ReorderAllLoadPoints(tripId, 0))
                return new ResponseModel<bool>()
                {
                    IsSuccess = true
                };
            else
                return new ResponseModel<bool>()
                {
                    IsSuccess = false,
                    Message = "There was an error when trying to reorder load points! Please try again."
                };
        }

        

    }
}
