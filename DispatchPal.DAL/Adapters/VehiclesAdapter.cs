﻿using DispatchPal.DAL.Adapters.Interface;
using DispatchPal.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DispatchPal.DAL.Adapters
{
    public class VehiclesAdapter : BaseAdapter, IVehiclesAdapter
    {
        public VehiclesAdapter(dbEntities1 context) : base(context)
        {
        }

        public ResponseModel<List<VehiclePreviewModel>> GetPreview(int vehicleType, int offset, int pageSize, string searchParam, bool active, int companyId)
        {
            if (searchParam == null)
                searchParam = "";

            IQueryable<VehiclePreviewModel> items = context.vehicles.AsNoTracking().Where(item =>
                item.VehicleType == vehicleType &&
                (companyId == 0 || item.CompanyId == companyId) &&
                (!active || item.IsActive == true) &&
                (item.VehicleID.Contains(searchParam) ||
                item.DisplayName.Contains(searchParam) ||
                item.Make.Contains(searchParam) ||
                item.Model.Contains(searchParam) ||
                item.PlateNumber.Contains(searchParam))).Select(item => new VehiclePreviewModel()
                {
                    Color = item.Color,
                    DispatchedBy = item.user != null ? (item.user.FirstName + " " + item.user.LastName) : "",
                    DisplayName = item.DisplayName,
                    DOTExpiration = item.DOTExpiration,
                    Id = item.Id,
                    InsuranceDate = item.InsuranceDate,
                    InsurancePremium = item.InsurancePremium,
                    InsuranceWith = item.InsuranceWith,
                    Insured = item.Insured,
                    IsActive = item.IsActive,
                    LeasedEnd = item.LeasedEnd,
                    LeasedFrom = item.LeasedFrom,
                    LeasedStart = item.LeasedStart,
                    Make = item.Make,
                    Model = item.Model,
                    Note = item.Note,
                    Number = item.Number,
                    Ownership = item.Ownership,
                    PlateExpiration = item.PlateExpiration,
                    PlateNumber = item.PlateNumber,
                    PurchaseDate = item.PurchaseDate,
                    PurchasedFrom = item.PurchasedFrom,
                    SellDate = item.SellDate,
                    Size = item.Size,
                    SoldTo = item.SoldTo,
                    State = item.w_state != null ? item.w_state.ISO : "",
                    Trailer = item.vehicle2 != null ? item.vehicle2.DisplayName : "",
                    TrailerNumber = item.TrailerNumber,
                    Type = item.Type,
                    TireType = item.TireType,
                    VehicleID = item.VehicleID,
                    VehicleType = item.VehicleType,
                    VIN = item.VIN,
                    WarrantyDate = item.WarrantyDate,
                    Year = item.Year
                });

            return new ResponseModel<List<VehiclePreviewModel>>()
            {
                IsSuccess = true,
                TotalItems = items.Count(),
                Result = items.OrderBy(item => item.Id).Skip(offset).Take(pageSize).ToList()
            };
        }

        public ResponseModel<List<VehicleModel>> Get(int vehicleType, int offset, int pageSize, string searchParam, bool active, int companyId)
        {
            if (searchParam == null)
                searchParam = "";

            IQueryable<vehicle> items = context.vehicles.AsNoTracking().Where(item =>
                item.VehicleType == vehicleType &&
                (companyId == 0 || item.CompanyId == companyId) &&
                (!active || item.IsActive == true) &&
                (item.DisplayName.Contains(searchParam) ||
                item.VehicleID.Contains(searchParam) ||
                item.Make.Contains(searchParam) ||
                item.Model.Contains(searchParam) ||
                item.VIN.Contains(searchParam) ||
                item.PlateNumber.Contains(searchParam)));

            return new ResponseModel<List<VehicleModel>>()
            {
                IsSuccess = true,
                TotalItems = items.Count(),
                Result = items.OrderBy(item => item.Id).Skip(offset).Take(pageSize).AsEnumerable().Select(item => item.ToModel()).ToList()
            };
        }

        public ResponseModel<VehicleModel> Get(int id)
        {
            vehicle vh = context.vehicles.Where(item => item.Id == id).FirstOrDefault();
            if (vh != null)
            {
                return new ResponseModel<VehicleModel>()
                {
                    IsSuccess = true,
                    Result = vh.ToModel()
                };
            }
            else
            {
                return new ResponseModel<VehicleModel>()
                {
                    IsSuccess = false,
                    Message = "Vehicle not found"
                };
            }
        }

        public ResponseModel<VehicleModel> Save(VehicleModel model)
        {
            try
            {
                vehicle dr = context.vehicles.Where(item => item.Id == model.Id).FirstOrDefault();
                if (dr == null)
                {
                    dr = new vehicle();
                    context.vehicles.Add(dr);
                }
                dr.VehicleType = model.VehicleType;
                dr.VehicleID = model.VehicleID;
                //dr.DisplayName = model.DisplayName;
                dr.Number = model.Number;
                dr.Make = model.Make;
                dr.Model = model.Model;
                dr.Year = model.Year;
                dr.Size = model.Size;
                dr.Color = model.Color;
                dr.CompanyId = model.CompanyId;
                dr.DispatchedBy = model.DispatchedBy;
                dr.DOTExpiration = model.DOTExpiration.HasValue ? model.DOTExpiration.Value.ToUniversalTime() : default(DateTime?);
                dr.InsuranceDate = model.InsuranceDate.HasValue ? model.InsuranceDate.Value.ToUniversalTime() : default(DateTime?);
                dr.InsurancePremium = model.InsurancePremium;
                dr.InsuranceWith = model.InsuranceWith;
                dr.Insured = model.Insured;
                dr.IsActive = model.IsActive;
                dr.LeasedEnd = model.LeasedEnd.HasValue ? model.LeasedEnd.Value.ToUniversalTime() : default(DateTime?);
                dr.LeasedFrom = model.LeasedFrom.HasValue ? model.LeasedFrom.Value.ToUniversalTime() : default(DateTime?);
                dr.LeasedStart = model.LeasedStart.HasValue ? model.LeasedStart.Value.ToUniversalTime() : default(DateTime?);
                dr.ModifiedBy = 1;
                dr.ModifiedDate = DateTime.UtcNow;
                dr.Note = model.Note;
                dr.Ownership = model.Ownership;
                dr.PlateExpiration = model.PlateExpiration.HasValue ? model.PlateExpiration.Value.ToUniversalTime() : default(DateTime?);
                dr.PlateNumber = model.PlateNumber;
                dr.PurchaseDate = model.PurchaseDate.HasValue ? model.PurchaseDate.Value.ToUniversalTime() : default(DateTime?);
                dr.PurchasedFrom = model.PurchasedFrom;
                dr.SellDate = model.SellDate.HasValue ? model.SellDate.Value.ToUniversalTime() : default(DateTime?);
                dr.SoldTo = model.SoldTo;
                dr.StateId = model.StateId;
                dr.TrailerId = model.TrailerId;
                dr.TrailerNumber = model.TrailerNumber;
                dr.Type = model.Type;
                dr.TireType = model.TireType;
                dr.VIN = model.VIN;
                dr.WarrantyDate = model.WarrantyDate.HasValue ? model.WarrantyDate.Value.ToUniversalTime() : default(DateTime?);

                context.SaveChanges();

                return new ResponseModel<VehicleModel>()
                {
                    IsSuccess = true,
                    Result = dr.ToModel()
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ResponseModel<bool> Delete(int id)
        {
            vehicle vh = context.vehicles.Where(item => item.Id == id).FirstOrDefault();
            if (vh != null)
            {
                context.vehicles.Remove(vh);
                context.SaveChanges();

                return new ResponseModel<bool>()
                {
                    IsSuccess = true
                };
            }
            else
            {
                return new ResponseModel<bool>()
                {
                    IsSuccess = true,
                    Message = "Vehicle not found!"
                };
            }
        }
    }
}
