﻿using DispatchPal.DAL.Adapters.Interface;
using DispatchPal.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DispatchPal.DAL.Adapters
{
    public class StatesAdapter : BaseAdapter, IStatesAdapter
    {
        public StatesAdapter(dbEntities1 context) : base(context)
        {
        }

        public ResponseModel<List<StateModel>> Get(string searchParam)
        {
            if (searchParam == null)
                searchParam = "";
            IQueryable<w_state> result = context.w_state.AsNoTracking().Where(item => item.Name.Contains(searchParam));

            return new ResponseModel<List<StateModel>>()
            {
                IsSuccess = true,
                TotalItems = result.Count(),
                Result = result.AsEnumerable().Select(item => item.ToModel()).ToList()
            };
        }

        public ResponseModel<StateModel> Get(int id)
        {
            w_state dr = context.w_state.AsNoTracking().Where(item => item.Id == id).FirstOrDefault();
            if (dr != null)
            {
                return new ResponseModel<StateModel>()
                {
                    IsSuccess = true,
                    Result = dr.ToModel()
                };
            }
            else
            {
                return new ResponseModel<StateModel>()
                {
                    IsSuccess = false,
                    Message = "State not found!"
                };
            }
        }
    }
}
