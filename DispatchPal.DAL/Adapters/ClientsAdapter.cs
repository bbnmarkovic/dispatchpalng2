﻿using DispatchPal.DAL.Adapters.Interface;
using DispatchPal.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DispatchPal.DAL.Adapters
{
    public class ClientsAdapter : BaseAdapter, IClientsAdapter
    {
        public ClientsAdapter(dbEntities1 context) : base(context)
        {
        }

        public ResponseModel<List<ClientModel>> Get(int clientType, int offset, int pageSize, string searchParam, bool active)
        {
            if (searchParam == null)
                searchParam = "";

            IQueryable<client> items = context.clients.AsNoTracking().Where(item =>
                (clientType == 0 || item.ClientTypeId == clientType) &&
                (!active || item.IsActive == true) &&
                (item.DisplayName.Contains(searchParam) ||
                item.Email.Contains(searchParam)));

            return new ResponseModel<List<ClientModel>>()
            {
                IsSuccess = true,
                TotalItems = items.Count(),
                Result = items.OrderBy(item => item.Id).Skip(offset).Take(pageSize).AsEnumerable().Select(item => item.ToModel()).ToList()
            };
        }
        public ResponseModel<ClientModel> Get(int id)
        {
            client dr = context.clients.Where(item => item.Id == id).FirstOrDefault();
            if (dr != null)
            {
                return new ResponseModel<ClientModel>()
                {
                    IsSuccess = true,
                    Result = dr.ToModel()
                };
            }
            else
            {
                return new ResponseModel<ClientModel>()
                {
                    IsSuccess = false,
                    Message = "Client not found!"
                };
            }
        }
        public ResponseModel<ClientModel> Save(ClientModel model)
        {
            try
            {
                client dr = context.clients.Where(item => item.Id == model.Id).FirstOrDefault();
                if (dr == null)
                {
                    dr = new client();
                    context.clients.Add(dr);
                }
                dr.ClientTypeId = model.ClientTypeId;
                dr.AccountNumber = model.AccountNumber;
                dr.Address = model.Address;
                dr.Address2 = model.Address2;
                dr.AlternatePhoneNumber = model.AlternatePhoneNumber;
                dr.City = model.City;
                dr.ContactName = model.ContactName;
                dr.DisplayName = model.DisplayName;
                dr.EIN = model.EIN;
                dr.Email = model.Email;
                dr.Fax = model.Fax;
                dr.Fee = model.Fee;
                dr.IsActive = model.IsActive;
                dr.LastPaid = model.LastPaid.ToUniversalTime();
                dr.ModifiedBy = 1;
                dr.ModifiedDate = DateTime.UtcNow;
                dr.MotorCarrierNumber = model.MotorCarrierNumber;
                dr.Name = model.Name;
                dr.PhoneNumber = model.PhoneNumber;
                dr.StateId = model.StateId;
                dr.ZipCode = model.ZipCode;

                context.SaveChanges();

                return new ResponseModel<ClientModel>()
                {
                    IsSuccess = true,
                    Result = dr.ToModel()
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public ResponseModel<bool> Delete(int id)
        {
            client dr = context.clients.Where(item => item.Id == id).FirstOrDefault();
            if (dr != null)
            {
                context.clients.Remove(dr);
                context.SaveChanges();

                return new ResponseModel<bool>()
                {
                    IsSuccess = true
                };
            }
            else
            {
                return new ResponseModel<bool>()
                {
                    IsSuccess = false,
                    Message = "Client not found!"
                };
            }
        }
        public ResponseModel<List<ClientTypeModel>> GetClientTypes(string searchParam)
        {
            if (searchParam == null)
                searchParam = "";

            IQueryable<ClientTypeModel> result = context.client_type.AsNoTracking().Where(item => item.Name.Contains(searchParam)).Select(item => new ClientTypeModel()
            {
                Id = item.Id,
                Name = item.Name
            });

            return new ResponseModel<List<ClientTypeModel>>()
            {
                IsSuccess = true,
                TotalItems = result.Count(),
                Result = result.ToList()
            };
        }
        public ResponseModel<ClientTypeModel> GetClientTypeById(int id)
        {
            client_type dr = context.client_type.Where(item => item.Id == id).FirstOrDefault();
            if (dr != null)
            {
                ClientTypeModel result = new ClientTypeModel()
                {
                    Id = dr.Id,
                    Name = dr.Name
                };

                return new ResponseModel<ClientTypeModel>()
                {
                    IsSuccess = true,
                    Result = result
                };
            }
            else
            {
                return new ResponseModel<ClientTypeModel>()
                {
                    IsSuccess = false,
                    Message = "Client not found!"
                };
            }
        }

    }
}
