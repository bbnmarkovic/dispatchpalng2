﻿using DispatchPal.DAL.Adapters.Interface;
using DispatchPal.DAL.Models;
using DispatchPal.DAL.Models.Reports;
using DispatchPal.DAL.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DispatchPal.DAL.Adapters
{
    public class InvoiceAdapter : BaseAdapter, IInvoiceAdapter
    {
        public InvoiceAdapter(dbEntities1 context) : base(context)
        {
        }

        public ResponseModel<InvoiceModel> Get(int loadId)
        {
            invoice inv = context.invoices.AsNoTracking().Where(item => item.LoadId == loadId).FirstOrDefault();
            if (inv != null)
            {
                return new ResponseModel<InvoiceModel>()
                {
                    IsSuccess = true,
                    Result = inv.ToModel()
                };
            }
            else
            {
                return new ResponseModel<InvoiceModel>()
                {
                    IsSuccess = true,
                    Result = InvoiceManager.GenerateInitialInvoice(loadId, context)
                };
            }
        }

        public ResponseModel<List<InvoiceModel>> GetList(int offset, int pageSize, string searchParam, bool active, int companyId)
        {
            if (searchParam == null)
                searchParam = "";

            IQueryable<invoice> items = context.invoices.AsNoTracking().Where(item =>
                !active || item.IsActive == true);

            return new ResponseModel<List<InvoiceModel>>()
            {
                IsSuccess = true,
                TotalItems = items.Count(),
                Result = items.OrderBy(item => item.LoadId).Skip(offset).Take(pageSize).AsEnumerable().Select(item => item.ToModel()).ToList()
            };
        }

        public ResponseModel<List<InvoicePreviewModel>> GetPendingInvoices(int offset, int pageSize, string searchParam, int aging, int companyId)
        {
            if (searchParam == null)
                searchParam = "";

            DateTime dueDateFrom = DateTime.Now.Date.AddDays(-aging);
            DateTime dueDateTo = dueDateFrom.AddDays(30);

            DateTime dueDate90 = DateTime.Now.AddDays(-90);

            IQueryable<invoice> items = context.invoices.AsNoTracking().Where(item =>
                (aging == 0 || (aging == 30 && item.InvoiceDueDate >= dueDateFrom) || ((aging == 60 || aging == 90) && item.InvoiceDueDate >= dueDateFrom && item.InvoiceDueDate <= dueDateTo) || (aging == 100000 && item.InvoiceDueDate <= dueDate90)) &&
                item.load.PaidInFull == false &&
                item.CompanyId == companyId);

            return new ResponseModel<List<InvoicePreviewModel>>()
            {
                IsSuccess = true,
                TotalItems = items.Count(),
                Result = items.OrderBy(item => item.LoadId).Skip(offset).Take(pageSize).AsEnumerable().Select(item => new InvoicePreviewModel()
                {
                    Id = item.LoadId,
                    LoadNumber = item.load != null ? item.load.LoadNumber : "",
                    InvoiceNumber = item.InvoiceNumber,
                    InvoiceDueDate = item.InvoiceDueDate,
                    InvoiceDate = item.InvoiceDate,
                    Total = item.Total,
                    AmountPaid = item.AmountPaid,
                    BalanceDue = item.BalanceRemaining,
                    Brokerage = (item.load != null && item.load.client != null) ? item.load.client.DisplayName : ""
                }).ToList()
            };
        }

        public ResponseModel<List<InvoicePreviewModel>> GetPaidInvoices(int offset, int pageSize, string searchParam, int companyId)
        {
            if (searchParam == null)
                searchParam = "";

            IQueryable<invoice> items = context.invoices.AsNoTracking().Where(item =>
                item.load.PaidInFull == true &&
                item.CompanyId == companyId);

            return new ResponseModel<List<InvoicePreviewModel>>()
            {
                IsSuccess = true,
                TotalItems = items.Count(),
                Result = items.OrderBy(item => item.LoadId).Skip(offset).Take(pageSize).AsEnumerable().Select(item => new InvoicePreviewModel()
                {
                    Id = item.LoadId,
                    LoadNumber = item.load != null ? item.load.LoadNumber : "",
                    InvoiceNumber = item.InvoiceNumber,
                    InvoiceDueDate = item.InvoiceDueDate,
                    InvoiceDate = item.InvoiceDate,
                    Total = item.Total,
                    PaidDate = item.PaymentDate,
                    Brokerage = (item.load != null && item.load.client != null) ? item.load.client.DisplayName : ""
                }).ToList()
            };
        }

        public ResponseModel<InvoiceModel> Save(InvoiceModel invModel)
        {
            load ld = context.loads.Where(item => item.Id == invModel.LoadId).FirstOrDefault();
            if (ld != null)
            {
                invoice inv = context.invoices.Where(item => item.LoadId == invModel.LoadId).FirstOrDefault();
                if (inv == null)
                {
                    inv = new invoice();
                    inv.CompanyId = ld.CompanyId;
                    string temp = ld.LoadNumber.Substring(5, ld.LoadNumber.Length - 5);
                    inv.InvoiceNumber = "INV_" + Convert.ToInt32(temp);
                    inv.LoadId = ld.Id;

                    context.invoices.Add(inv);
                }
                inv.IsActive = invModel.IsActive;
                //inv.AmountPaid = invModel.AmountPaid;
                //inv.BalanceRemaining = invModel.BalanceRemaining;
                //inv.Discount = invModel.Discount;
                //inv.DiscountAmount = invModel.DiscountAmount;
                inv.InvoiceDate = invModel.InvoiceDate.ToUniversalTime();
                inv.InvoiceDueDate = invModel.InvoiceDueDate.ToUniversalTime();
                inv.ModifiedDate = DateTime.UtcNow;
                //inv.PaymentDate = invModel.PaymentDate;
                inv.SendReceipt = invModel.SendReceipt;
                inv.ExternalNote = invModel.ExternalNote;
                //inv.SubTotal = invModel.SubTotal;
                //inv.Tax = invModel.Tax;
                //inv.TaxAmmount = invModel.TaxAmmount;
                //inv.TaxInclusive = invModel.TaxInclusive;
                //inv.Total = invModel.Total;

                context.SaveChanges();

                return new ResponseModel<InvoiceModel>()
                {
                    IsSuccess = true,
                    Result = inv.ToModel()
                };
            }
            else
            {
                return null;
            }
        }

        public ResponseModel<InvoiceModel> SaveInvoiceItem(InvoiceItemModel model)
        {
            invoice inv = context.invoices.Where(item => item.LoadId == model.InvoiceId).FirstOrDefault();
            invoice_item invItem = context.invoice_item.Where(item => item.Id == model.Id).FirstOrDefault();
            if (invItem == null)
            {
                if (inv != null)
                {
                    invItem = new invoice_item();
                    inv.invoice_item.Add(invItem);
                }
            }

            invItem.Discount = model.Discount;
            invItem.DiscountAmmount = model.DiscountAmmount;
            invItem.Note = model.Note;
            invItem.Price = model.Price;
            invItem.Quantity = model.Quantity;
            invItem.ServiceId = model.ServiceId;
            invItem.ServiceName = model.ServiceName;
            invItem.Tax = model.Tax;
            invItem.TaxAmmount = model.TaxAmmount;
            invItem.Total = model.Quantity * model.Price;

            InvoiceManager.RecalculateInvoice(inv);

            context.SaveChanges();

            return new ResponseModel<InvoiceModel>()
            {
                IsSuccess = true,
                Result  = inv.ToModel()
            };
        }

        public ResponseModel<InvoiceModel> SaveInvoicePayment(InvoicePaymentModel model)
        {
            invoice inv = context.invoices.Where(item => item.LoadId == model.InvoiceId).FirstOrDefault();
            invoice_payment invItem = context.invoice_payment.Where(item => item.Id == model.Id).FirstOrDefault();
            if (invItem == null)
            {
                if (inv != null)
                {
                    invItem = new invoice_payment();
                    inv.invoice_payment.Add(invItem);
                }
            }

            invItem.Amount = model.Amount;
            invItem.PaymentDate = model.PaymentDate.HasValue ? model.PaymentDate.Value.ToUniversalTime() : default(DateTime?);
            invItem.PayedBy = model.PayedBy;

            InvoiceManager.RecalculateInvoice(inv);

            context.SaveChanges();

            return new ResponseModel<InvoiceModel>()
            {
                IsSuccess = true,
                Result = inv.ToModel()
            };
        }

        public ResponseModel<InvoiceModel> RegenerateInvoiceItems(int loadId)
        {
            invoice inv = context.invoices.Where(item => item.LoadId == loadId).FirstOrDefault();
            if (inv != null)
            {
                List<invoice_item> items = InvoiceManager.GenerateInitialInvoiceItems(inv.load);
                context.invoice_item.RemoveRange(inv.invoice_item);
                inv.invoice_item.Clear();
                foreach (invoice_item item in items)
                {
                    inv.invoice_item.Add(item);
                }

                InvoiceManager.RecalculateInvoice(inv);

                context.SaveChanges();

                return new ResponseModel<InvoiceModel>()
                {
                    IsSuccess = true,
                    Result = inv.ToModel()
                };
            }
            else
            {
                return new ResponseModel<InvoiceModel>()
                {
                    IsSuccess = false,
                    Message = "Invoice not found!"
                };
            }
        }

        public ResponseModel<bool> DeleteInvoiceItem(int id)
        {
            try
            {
                invoice_item invItem = context.invoice_item.Where(item => item.Id == id).FirstOrDefault();
                if (invItem != null)
                {
                    invoice inv = context.invoices.Where(item => item.LoadId == invItem.InvoiceId).FirstOrDefault();

                    inv.invoice_item.Remove(invItem);
                    InvoiceManager.RecalculateInvoice(inv);

                    context.invoice_item.Remove(invItem);

                    context.SaveChanges();

                    return new ResponseModel<bool>()
                    {
                        IsSuccess = true
                    };
                }
                else
                {
                    return new ResponseModel<bool>()
                    {
                        IsSuccess = false,
                        Message = "Invoice not found!"
                    };
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ResponseModel<bool> DeleteInvoicePayment(int id)
        {
            try
            {
                invoice_payment invItem = context.invoice_payment.Where(item => item.Id == id).FirstOrDefault();
                if (invItem != null)
                {
                    invoice inv = context.invoices.Where(item => item.LoadId == invItem.InvoiceId).FirstOrDefault();

                    inv.invoice_payment.Remove(invItem);
                    InvoiceManager.RecalculateInvoice(inv);

                    context.invoice_payment.Remove(invItem);

                    context.SaveChanges();

                    return new ResponseModel<bool>()
                    {
                        IsSuccess = true
                    };
                }
                else
                {
                    return new ResponseModel<bool>()
                    {
                        IsSuccess = false,
                        Message = "Invoice not found!"
                    };
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ResponseModel<InvoiceReportModel> GenerateReportModel(int loadId)
        {
            invoice invItem = context.invoices.Where(item => item.LoadId == loadId).FirstOrDefault();
            if (invItem != null)
            {
                InvoiceReportModel reportModel = new InvoiceReportModel();

                company cmp = context.companies.Where(item => item.Id == invItem.CompanyId).FirstOrDefault();
                if (cmp != null)
                {
                    reportModel.CompanyAddress = cmp.Address;
                    reportModel.CompanyCity = cmp.City;
                    reportModel.CompanyEmail = cmp.Email;
                    reportModel.CompanyFax = cmp.Fax;
                    reportModel.CompanyName = cmp.Name;
                    reportModel.CompanyPhoneNumber = cmp.PhoneNumber;
                    reportModel.CompanyState = cmp.w_state.Name;
                    reportModel.CompanyZipCode = cmp.ZipCode;
                }

                client cl = context.clients.Where(item => item.Id == invItem.load.PartnerShipperId).FirstOrDefault();
                if (cl != null)
                {
                    reportModel.PartnerAddress = cl.Address;
                    reportModel.PartnerCity = cl.City;
                    reportModel.PartnerName = cl.DisplayName;
                    reportModel.PartnerState = cl.w_state.Name;
                    reportModel.PartnerZipCode = cl.ZipCode;
                }

                if (invItem.load != null && invItem.load.tour != null)
                {
                    reportModel.DriverA = invItem.load.tour.driver != null ? invItem.load.tour.driver.DisplayName : "";
                    reportModel.DriverB = invItem.load.tour.driver1 != null ? invItem.load.tour.driver1.DisplayName : "";

                    reportModel.Trailer = invItem.load.tour.vehicle != null ? invItem.load.tour.vehicle.DisplayName : "";
                    reportModel.Truck = invItem.load.tour.vehicle1 != null ? invItem.load.tour.vehicle1.DisplayName : "";

                    reportModel.LoadStartDate = invItem.load.tour.DateFrom != null ? (DateTime)invItem.load.tour.DateFrom : DateTime.Now;
                }


                if (invItem.load != null)
                {
                    reportModel.Amount = invItem.load.AgreedPrice;
                    reportModel.BillableMiles = 100; // invItem.load.Bil
                    reportModel.BOL = invItem.load.BOL;
                    reportModel.LoadNumber = invItem.load.LoadNumber;
                    reportModel.PO = invItem.load.PO;
                    reportModel.Rate = 10;// invItem.
                }

                reportModel.DueDate = invItem.InvoiceDueDate;
                reportModel.InvoiceDate = invItem.InvoiceDate;
                reportModel.InvoiceItems = invItem.invoice_item.Skip(1).Select(item => item.ToModel()).ToList();

                load_point startPoint = invItem.load.load_point.OrderBy(item => item.DateLoad).FirstOrDefault();
                reportModel.LocationStart = startPoint != null ? startPoint.Location : "";
                load_point endPoint = invItem.load.load_point.OrderBy(item => item.DateLoad).FirstOrDefault();
                reportModel.LocationEnd = endPoint!= null ? endPoint.Location : "";
                
                return new ResponseModel<InvoiceReportModel>()
                {
                    IsSuccess = true,
                    Result = reportModel
                };
            }
            else
            {
                return new ResponseModel<InvoiceReportModel>()
                {
                    IsSuccess = false,
                    Message = "Invoice not found!"
                };
            }
        }
    }
}
