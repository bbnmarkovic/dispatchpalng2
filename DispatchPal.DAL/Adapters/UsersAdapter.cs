﻿using DispatchPal.DAL.Adapters.Interface;
using DispatchPal.DAL.Models;
using DispatchPal.DAL.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DispatchPal.DAL.Adapters
{
    public class UsersAdapter : BaseAdapter, IUsersAdapter
    {
        public UsersAdapter(dbEntities1 context) : base(context)
        {
             
        }

        public ResponseModel<List<UserModel>> Get(int offset, int pageSize, string searchParam, bool active)
        {
            if (searchParam == null)
                searchParam = "";

            IQueryable<user> items = context.users.AsNoTracking().Where(item =>
                (!active || item.IsActive == true) &&
                ((item.FirstName == null && searchParam == "") || 
                item.FirstName.Contains(searchParam) ||
                (item.LastName == null && searchParam == "") ||
                item.LastName.Contains(searchParam)));

            return new ResponseModel<List<UserModel>>()
            {
                IsSuccess  = true,
                TotalItems = items.Count(),
                Result = items.OrderBy(item => item.Id).Skip(offset).Take(pageSize).AsEnumerable().Select(item => item.ToModel()).ToList()
            };
        }
        public ResponseModel<UserModel> Get(int id)
        {
            user dr = context.users.Where(item => item.Id == id).FirstOrDefault();
            if (dr != null)
            {
                return new ResponseModel<UserModel>()
                {
                    IsSuccess = true,
                    Result = dr.ToModel()
                };
            }
            else
            {
                return new ResponseModel<UserModel>()
                {
                    IsSuccess = false,
                    Message = "User not found!"
                };
            }
        }

        public ResponseModel<UserModel> Save(UserModel model)
        {
            try
            {
                user dr = context.users.Where(item => item.Id == model.Id).FirstOrDefault();
                if (dr == null)
                {
                    dr = new user();
                    dr.DateCreatedUtc = DateTime.UtcNow;

                    /*string newPassword = "pass"; //System.Configuration.ConfigurationManager.AppSettings["DefaultPassword"].ToString();
                    Guid g = new Guid();
                    dr.Password_uuid = g.ToString();
                    dr.Password = MD5Tools.GetEncriptedPassword(newPassword, g);*/

                    context.users.Add(dr);
                }

                string newPassword = "pass"; //System.Configuration.ConfigurationManager.AppSettings["DefaultPassword"].ToString();
                Guid g = new Guid();
                dr.Password_uuid = g.ToString();
                dr.Password = MD5Tools.GetEncriptedPassword(newPassword, g);

                dr.companies.Clear();
                foreach (int cmpId in model.Companies)
                {
                    company cmp = context.companies.Where(p => p.Id == cmpId).FirstOrDefault();
                    if (cmp != null)
                    {
                        dr.companies.Add(cmp);
                    }
                }

                dr.AccessFailedCount = model.AccessFailedCount;
                dr.Address = model.Address;
                dr.CompanyId = model.CompanyId;
                dr.DateActivatedUtc = model.DateActivatedUtc;
                dr.DateLastLoginUTC = model.DateLastLoginUTC;
                dr.Description = model.Description;
                dr.Email = model.Email;
                dr.EmailConfirmed = model.EmailConfirmed;
                dr.FirstName = model.FirstName;
                dr.InviteCode = model.InviteCode;
                dr.IsActive = model.IsActive;
                dr.LastName = model.LastName;
                dr.ModifiedBy = 1;
                dr.LockoutEnabled = model.LockoutEnabled;
                dr.LockoutEndDateUtc = model.LockoutEndDateUtc;
                dr.MobileNumber = model.MobileNumber;
                dr.PhoneNumber = model.PhoneNumber;
                dr.PhoneNumberConfirmed = model.PhoneNumberConfirmed;
                dr.ResetPasswordExpiryUtc = model.ResetPasswordExpiryUtc;
                dr.ResetPasswordKey = model.ResetPasswordKey;
                dr.Status = model.Status;
                dr.TwoFactorEnabled = model.TwoFactorEnabled;
                dr.UserId = model.UserId;
                dr.UserName = model.UserName;

                context.SaveChanges();

                return new ResponseModel<UserModel>()
                {
                    IsSuccess = true,
                    Result = dr.ToModel()
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ResponseModel<bool> Delete(int id)
        {
            user dr = context.users.Where(item => item.Id == id).FirstOrDefault();
            if (dr != null)
            {
                context.users.Remove(dr);
                context.SaveChanges();

                return new ResponseModel<bool>()
                {
                    IsSuccess = true
                };
            }
            else
            {
                return new ResponseModel<bool>()
                {
                    IsSuccess = false,
                    Message = "User not found!"
                };
            }
        }

        public ResponseModel<UserModel> GetUserByUsernameAndPassword(string userName, string password)
        {
            UserModel res = null;
            var item = context.users
                .FirstOrDefault(x => x.UserName.Equals(userName));
            if (item != null)
            {
                byte[] t = MD5Tools.GetEncriptedPassword(password, new Guid(item.Password_uuid));
                if (MD5Tools.ComparePassword(
                    MD5Tools.GetEncriptedPassword(password, new Guid(item.Password_uuid)),
                    item.Password))
                {
                    res = item.ToModel();
                }
            }
            return new ResponseModel<UserModel>()
            {
                IsSuccess = true,
                Result = res
            };
        }
    }
}
