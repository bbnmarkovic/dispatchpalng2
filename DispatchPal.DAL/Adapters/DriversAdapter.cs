﻿using DispatchPal.DAL.Adapters.Interface;
using DispatchPal.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DispatchPal.DAL.Adapters
{
    public class DriversAdapter : BaseAdapter, IDriversAdapter
    {
        public DriversAdapter(dbEntities1 context) : base(context)
        {
        }

        public ResponseModel<List<DriverPreviewModel>> GetPreview(int offset, int pageSize, string searchParam, bool active, int companyId)
        {
            if (searchParam == null)
                searchParam = "";

            IQueryable<DriverPreviewModel> items = context.drivers.AsNoTracking().Where(item =>
                (companyId == 0 || item.CompanyId == companyId) &&
                (!active || item.IsActive == true) &&
                (item.DriverID.Contains(searchParam) ||
                item.FirstName.Contains(searchParam) ||
                item.LastName.Contains(searchParam))).Select(item => new DriverPreviewModel()
                {
                    Address = item.Address1 + ((item.Address2 != null && item.Address2 != "") ? (", " + item.Address2) : "") + ((item.City != null && item.City != "") ? (", " + item.City) : "") + (item.w_state1 != null ? (", " + item.w_state1.ISO) : "") + ((item.ZipCode != null && item.ZipCode != "") ? (", " + item.ZipCode) : ""),
                    CDL = item.CDL,
                    CDLExpirationDate = item.CDLExpirationDate,
                    CDLState = item.w_state != null ? item.w_state.ISO : "",
                    CellPhone = item.CellPhone,
                    DispatchedBy = item.user != null ? (item.user.FirstName + " " + item.LastName) : "",
                    DisplayName = item.DisplayName,
                    DOB = item.DOB,
                    DriverID = item.DriverID,
                    Email = item.Email,
                    FirstName = item.FirstName,
                    FuelCard = item.FuelCard,
                    Id = item.Id,
                    LastName = item.LastName,
                    MedicalExpirationDate = item.MedicalExpirationDate,
                    MiddleName = item.MiddleName,
                    PayMathod = item.PayMathod,
                    RateType = 0,
                    SendEMail = item.SendEMail,
                    SendSMS = item.SendSMS,
                    SSN = item.SSN,
                    Truck = item.vehicles.FirstOrDefault() != null ? item.vehicles.FirstOrDefault().VehicleID : (item.vehicles1.FirstOrDefault() != null ? item.vehicles1.FirstOrDefault().VehicleID : ""),
                    Trailer = (item.vehicles.FirstOrDefault() != null && item.vehicles.FirstOrDefault().vehicle2 != null) ? item.vehicles.FirstOrDefault().vehicle2.VehicleID : ""
                });
            
            return new ResponseModel<List<DriverPreviewModel>>()
            {
                IsSuccess = true,
                TotalItems = items.Count(),
                Result = items.OrderBy(item => item.Id).Skip(offset).Take(pageSize).ToList()
            };
        }
        public ResponseModel<List<DriverModel>> Get(int offset, int pageSize, string searchParam, bool active, int companyId)
        {
            if (searchParam == null)
                searchParam = "";

            IQueryable<driver> items = context.drivers.AsNoTracking().Where(item=>
                (companyId == 0 || item.CompanyId == companyId) &&
                (!active || item.IsActive == true) &&
                (item.DriverID.Contains(searchParam) ||
                item.FirstName.Contains(searchParam) ||
                item.LastName.Contains(searchParam)));

            return new ResponseModel<List<DriverModel>>()
            {
                IsSuccess  = true,
                TotalItems = items.Count(),
                Result = items.OrderBy(item => item.Id).Skip(offset).Take(pageSize).AsEnumerable().Select(item => item.ToModel()).ToList()
            };
        }
        public ResponseModel<List<DriverModel>> GetDriversForAutoComplete(string searchParam, int companyId)
        {
            if (searchParam == null)
                searchParam = "";
            DateTime date = DateTime.Now;
            IQueryable<driver> items = context.drivers.AsNoTracking().Where(item =>
                (companyId == 0 || item.CompanyId == companyId) &&
                item.IsActive == true &&
                (item.DriverID.Contains(searchParam) ||
                item.FirstName.Contains(searchParam) ||
                item.LastName.Contains(searchParam)));
            
            return new ResponseModel<List<DriverModel>>()
            {
                IsSuccess = true,
                TotalItems = items.Count(),
                Result = items.OrderBy(item => item.Id).Skip(0).Take(20).AsEnumerable().Select(item => item.ToModel()).ToList()
            };
        }
        public ResponseModel<DriverModel> Get(int id)
        {
            driver dr = context.drivers.Where(item => item.Id == id).FirstOrDefault();
            if (dr != null)
            {
                return new ResponseModel<DriverModel>()
                {
                    IsSuccess = true,
                    Result = dr.ToModel()
                };
            }
            else
            {
                return new ResponseModel<DriverModel>()
                {
                    IsSuccess = false,
                    Message = "Driver not found!"
                };
            }
        }
        public ResponseModel<DriverModel> Save(DriverModel model)
        {
            try
            {
                driver dr = context.drivers.Where(item => item.Id == model.Id).FirstOrDefault();
                if (dr == null)
                {
                    dr = new driver();
                    context.drivers.Add(dr);
                }
                dr.Address1 = model.Address1;
                dr.Address2 = model.Address2;
                dr.CDL = model.CDL;
                dr.CDLClass = model.CDLClass;
                dr.CDLEndorsement = model.CDLEndorsement;
                dr.CDLExpirationDate = model.CDLExpirationDate.HasValue ? model.CDLExpirationDate.Value.ToUniversalTime() : default(DateTime?);
                dr.CDLStateId = model.CDLStateId;
                dr.CellPhone = model.CellPhone;
                dr.CellPhoneCarrier = model.CellPhoneCarrier;
                dr.City = model.City;
                dr.CompanyId = model.CompanyId;
                dr.DispatchedBy = model.DispatchedBy;
                dr.DisplayName = model.DisplayName;
                dr.DOB = model.DOB.HasValue ? model.DOB.Value.ToUniversalTime() : default(DateTime?);
                dr.DriverID = model.DriverID;
                dr.Email = model.Email;
                dr.EmergencyContactName = model.EmergencyContactName;
                dr.EmergencyContactPhone = model.EmergencyContactPhone;
                dr.Relationship = model.Relationship;
                dr.Fax = model.Fax;
                dr.FEIN = model.FEIN;
                dr.FirstName = model.FirstName;
                dr.FuelCard = model.FuelCard;
                dr.HireDate = model.HireDate.HasValue ? model.HireDate.Value.ToUniversalTime() : default(DateTime?);
                dr.HomePhone = model.HomePhone;
                dr.IsActive = model.IsActive;
                dr.LastName = model.LastName;
                dr.MedicalExpirationDate = model.MedicalExpirationDate.HasValue ? model.MedicalExpirationDate.Value.ToUniversalTime() : default(DateTime?);
                dr.MiddleName = model.MiddleName;
                dr.ModifiedBy = model.ModifiedBy;
                dr.ModifiedDate = DateTime.UtcNow;
                dr.Note = model.Note;
                dr.PayMathod = model.PayMathod;
                dr.RateType = model.RateType;
                dr.PreEmploymentDrugMVR = model.PreEmploymentDrugMVR;
                dr.PreEmploymentDrugMVRDate = model.PreEmploymentDrugMVRDate.HasValue ? model.PreEmploymentDrugMVRDate.Value.ToUniversalTime() : default(DateTime?);
                dr.PreEmploymentDrugTest = model.PreEmploymentDrugTest;
                dr.PreEmploymentDrugTestDate = model.PreEmploymentDrugTestDate.HasValue ? model.PreEmploymentDrugTestDate.Value.ToUniversalTime() : default(DateTime?);
                dr.RateAmountBobtail = model.RateAmountBobtail;
                dr.RateAmountEmpty = model.RateAmountEmpty;
                dr.RateAmountLoaded = model.RateAmountLoaded;
                dr.TeamRateAmountBobtail = model.TeamRateAmountBobtail;
                dr.TeamRateAmountEmpty = model.TeamRateAmountEmpty;
                dr.TeamRateAmountLoaded = model.TeamRateAmountLoaded;
                dr.SendEMail = model.SendEMail;
                dr.SendSMS = model.SendSMS;
                dr.SSN = model.SSN;
                dr.StateId = model.StateId;
                dr.TerminationDate = model.TerminationDate;
                dr.YearsOfEprience = model.YearsOfEprience;
                dr.ZipCode = model.ZipCode;

                context.SaveChanges();

                return new ResponseModel<DriverModel>()
                {
                    IsSuccess = true,
                    Result = dr.ToModel()
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public ResponseModel<bool> Delete(int id)
        {
            driver dr = context.drivers.Where(item => item.Id == id).FirstOrDefault();
            if (dr != null)
            {
                context.drivers.Remove(dr);
                context.SaveChanges();

                return new ResponseModel<bool>()
                {
                    IsSuccess = true
                };
            }
            else
            {
                return new ResponseModel<bool>()
                {
                    IsSuccess = false,
                    Message = "Driver not found!"
                };
            }
        }
    }
}
