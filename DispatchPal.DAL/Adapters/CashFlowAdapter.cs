﻿using DispatchPal.DAL.Adapters.Interface;
using DispatchPal.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DispatchPal.DAL.Adapters
{
    public class CashFlowAdapter : BaseAdapter, ICashFlowAdapter
    {
        public CashFlowAdapter(dbEntities1 context) : base(context)
        {
        }

        public ResponseModel<List<CashFlowPreviewModel>> GetPreview(int offset, int pageSize, string searchParam, bool active)
        {
            if (searchParam == null)
                searchParam = "";

            IQueryable<CashFlowPreviewModel> items = context.cash_flow.AsNoTracking().Where(item =>
                (!active || item.IsActive == true) &&
                (item.driver.FirstName.Contains(searchParam) ||
                item.driver.LastName.Contains(searchParam) ||
                item.vehicle1.VehicleID.Contains(searchParam))).Select(item => new CashFlowPreviewModel()
                {
                    Id = item.Id,
                    Driver = item.driver != null ? item.driver.DisplayName : "",
                    Truck = item.vehicle1 != null ? item.vehicle1.VehicleID : "",
                    Trailer = item.vehicle != null ? item.vehicle.VehicleID : "",
                    Note = item.Note,
                    Type = item.cash_flow_type != null ? item.cash_flow_type.Name : "",
                    Date = item.Date,
                    Amount = item.Amount,
                    IsSettled = item.IsSettled,
                    IsActive = item.IsActive
                });

            return new ResponseModel<List<CashFlowPreviewModel>>()
            {
                IsSuccess = true,
                TotalItems = items.Count(),
                Result = items.OrderBy(item => item.Id).Skip(offset).Take(pageSize).ToList()
            };
        }

        public ResponseModel<CashFlowModel> Get(int id)
        {
            cash_flow dr = context.cash_flow.Where(item => item.Id == id).FirstOrDefault();
            if (dr != null)
            {
                return new ResponseModel<CashFlowModel>()
                {
                    IsSuccess = true,
                    Result = dr.ToModel()
                };
            }
            else
            {
                return new ResponseModel<CashFlowModel>()
                {
                    IsSuccess = false,
                    Message = "Transaction not found"
                };
            }
        }

        public ResponseModel<List<CashFlowTypeModel>> GetTypes()
        {
            IQueryable<CashFlowTypeModel> result = context.cash_flow_type.Select(item => new CashFlowTypeModel()
            {
                Id = item.Id,
                Name = item.Name
            });

            return new ResponseModel<List<CashFlowTypeModel>>()
            {
                IsSuccess = true,
                TotalItems = result.Count(),
                Result = result.ToList()
            };
        }

        public ResponseModel<CashFlowModel> Save(CashFlowModel model)
        {
            try
            {
                cash_flow dr = context.cash_flow.Where(item => item.Id == model.Id).FirstOrDefault();
                if (dr == null)
                {
                    dr = new cash_flow();
                    context.cash_flow.Add(dr);
                }
                dr.DriverId = model.DriverId;
                dr.TruckId = model.TruckId;
                dr.TrailerId = model.TrailerId;
                dr.TypeId = model.TypeId;
                dr.Amount = model.Amount;
                dr.Date = model.Date.ToUniversalTime();
                dr.Note = model.Note;
                dr.IsSettled = model.IsSettled;
                dr.IsActive = model.IsActive;

                context.SaveChanges();

                return new ResponseModel<CashFlowModel>()
                {
                    IsSuccess = true,
                    Result = dr.ToModel()
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ResponseModel<bool> Delete(int id)
        {
            cash_flow dr = context.cash_flow.Where(item => item.Id == id).FirstOrDefault();
            if (dr != null)
            {
                dr.IsActive = false;
                context.SaveChanges();

                return new ResponseModel<bool>()
                {
                    IsSuccess = true
                };
            }
            else
            {
                return new ResponseModel<bool>()
                {
                    IsSuccess = false,
                    Message = "Transaction not found!"
                };
            }
        }

    }
}
