﻿using DispatchPal.DAL.Adapters.Interface;
using DispatchPal.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DispatchPal.DAL.Adapters
{
    public class ServicesAdapter : BaseAdapter, IServicesAdapter
    {
        public ServicesAdapter(dbEntities1 context) : base(context)
        {
        }

        public ResponseModel<List<ServiceModel>> Get(int offset, int pageSize, string searchParam, int companyId)
        {
            if (searchParam == null)
                searchParam = "";

            IQueryable<service> items = context.services.AsNoTracking().Where(item =>
                (companyId == 0 || item.CompanyId == companyId) &&
                (item.Description.Contains(searchParam) ||
                item.Job.Contains(searchParam)));

            return new ResponseModel<List<ServiceModel>>()
            {
                IsSuccess = true,
                TotalItems = items.Count(),
                Result = items.OrderBy(item => item.Id).Skip(offset).Take(pageSize).AsEnumerable().Select(item => item.ToModel()).ToList()
            };
        }
        public ResponseModel<ServiceModel> Get(int id)
        {
            service dr = context.services.Where(item => item.Id == id).FirstOrDefault();
            if (dr != null)
            {
                return new ResponseModel<ServiceModel>()
                {
                    IsSuccess = true,
                    Result = dr.ToModel()
                };
            } else
            {
                return new ResponseModel<ServiceModel>()
                {
                    IsSuccess = false,
                    Message = "Service not found!"
                };
            }
        }

        public ResponseModel<ServiceModel> Save(ServiceModel model)
        {
            try
            {
                service dr = context.services.Where(item => item.Id == model.Id).FirstOrDefault();
                if (dr == null)
                {
                    dr = new service();
                    context.services.Add(dr);
                }
                dr.Cost = model.Cost;
                dr.Date = model.Date.HasValue ? model.Date.Value.ToUniversalTime() : default(DateTime?);
                dr.Description = model.Description;
                dr.Job = model.Job;
                dr.Location = model.Location;
                dr.Mileage = model.Mileage;
                dr.Part = model.Part;
                dr.ScheduledServiceDate = model.ScheduledServiceDate.HasValue ? model.ScheduledServiceDate.Value.ToUniversalTime() : default(DateTime?);
                dr.TruckId = model.TruckId;
                dr.WarrantyExpDate = model.WarrantyExpDate.HasValue ? model.WarrantyExpDate.Value.ToUniversalTime() : default(DateTime?);
                dr.CompanyId = model.CompanyId;

                context.SaveChanges();

                return new ResponseModel<ServiceModel>()
                {
                    IsSuccess = true,
                    Result = dr.ToModel()
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ResponseModel<bool> Delete(int id)
        {
            service dr = context.services.Where(item => item.Id == id).FirstOrDefault();
            if (dr != null)
            {
                context.services.Remove(dr);
                context.SaveChanges();

                return new ResponseModel<bool>()
                {
                    IsSuccess = true
                };
            }
            else
            {
                return new ResponseModel<bool>()
                {
                    IsSuccess = false,
                    Message = "Service not found!"
                };
            }
        }

    }
}
