﻿using DispatchPal.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DispatchPal.DAL.Utils
{
    public static class InvoiceManager
    {
        public static InvoiceModel GenerateInitialInvoice(int loadId, dbEntities1 context)
        {
            load ld = context.loads.Where(item => item.Id == loadId).FirstOrDefault();
            if (ld != null)
            {
                invoice inv = context.invoices.Where(item => item.LoadId == loadId).FirstOrDefault();
                if (inv == null)
                {
                    inv = new invoice();
                    inv.IsActive = true;
                    inv.CompanyId = ld.CompanyId;
                    inv.InvoiceDate = DateTime.Now;
                    inv.LoadId = ld.Id;
                    string temp = ld.LoadNumber.Substring(5, ld.LoadNumber.Length - 5);
                    inv.InvoiceNumber = "INV_" + Convert.ToInt32(temp);
                    context.invoices.Add(inv);
                }
                inv.AmountPaid = 0;
                inv.BalanceRemaining = ld.AgreedPrice * ld.FuelSurcharge;
                inv.Discount = 0;
                inv.DiscountAmount = 0;
                inv.InvoiceDueDate = DateTime.Now.AddDays(30);
                inv.ModifiedDate = DateTime.Now;
                inv.PaymentDate = DateTime.Now;
                inv.SendReceipt = true;
                inv.SubTotal = 0;
                inv.Tax = 0;
                inv.TaxAmmount = 0;
                inv.TaxInclusive = 0;
                inv.Total = ld.AgreedPrice * ld.FuelSurcharge;

                List<invoice_item> items = GenerateInitialInvoiceItems(ld);
                foreach (invoice_item itm in items)
                {
                    inv.invoice_item.Add(itm);
                }

                context.SaveChanges();

                return inv.ToModel();
            }
            else
            {
                return null;
            }
        }

        public static List<invoice_item> GenerateInitialInvoiceItems(load ld)
        {
            List<invoice_item> result = new List<invoice_item>();

            invoice_item invoiceItemAggreedPrice = new invoice_item();
            invoiceItemAggreedPrice.Discount = 0;
            invoiceItemAggreedPrice.DiscountAmmount = 0;
            invoiceItemAggreedPrice.Note = "";
            invoiceItemAggreedPrice.Price = ld.AgreedPrice;
            invoiceItemAggreedPrice.Quantity = 1;
            invoiceItemAggreedPrice.ServiceId = 1;
            invoiceItemAggreedPrice.ServiceName = "Aggreed Ammount";
            invoiceItemAggreedPrice.Tax = 0;
            invoiceItemAggreedPrice.TaxAmmount = 0;
            invoiceItemAggreedPrice.Total = invoiceItemAggreedPrice.Price * invoiceItemAggreedPrice.Quantity;
            result.Add(invoiceItemAggreedPrice);

            invoice_item invoiceItemFuelSurcharge = new invoice_item();
            invoiceItemFuelSurcharge.Discount = 0;
            invoiceItemFuelSurcharge.DiscountAmmount = 0;
            invoiceItemFuelSurcharge.Note = "";
            invoiceItemFuelSurcharge.Price = (ld.FuelSurcharge - Math.Floor(ld.FuelSurcharge)) * ld.AgreedPrice;
            invoiceItemFuelSurcharge.Quantity = 1;
            invoiceItemFuelSurcharge.ServiceId = 1;
            invoiceItemFuelSurcharge.ServiceName = "Fuel Surcharge";
            invoiceItemFuelSurcharge.Tax = 0;
            invoiceItemFuelSurcharge.TaxAmmount = 0;
            invoiceItemFuelSurcharge.Total = (ld.FuelSurcharge - Math.Floor(ld.FuelSurcharge)) * ld.AgreedPrice;
            result.Add(invoiceItemFuelSurcharge);

            return result;
        }

        public static void RecalculateInvoice(invoice inv)
        {
            decimal totalInvoice = (decimal)inv.invoice_item.Sum(item => item.Quantity * item.Price);
            decimal totalPayment = (decimal)inv.invoice_payment.Sum(item => item.Amount);
            inv.Total = totalInvoice;
            inv.AmountPaid = totalPayment;
            inv.BalanceRemaining = totalInvoice - totalPayment;

            //inv.load.PaidInFull = inv.BalanceRemaining <= 0;
        }
    }
}
