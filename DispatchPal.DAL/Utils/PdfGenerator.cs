﻿using DispatchPal.DAL.Models.Reports;
using HandlebarsDotNet;
using System;
using System.Drawing.Printing;
using System.IO;
using System.Web;

namespace DispatchPal.DAL.Utils
{
    public class PdfGenerator
    {
        public PdfGenerator()
        {
        }

        public byte[] RunTemplatingEngine(InvoiceReportModel invoiceItem)
        {
            Guid filePrefix = Guid.NewGuid();

            string source = string.Empty;
            using (FileStream fileStream = new FileStream(HttpContext.Current.Server.MapPath("~/pdf/invoice.html"), FileMode.Open))
            {
                using (StreamReader reader = new StreamReader(fileStream))
                {
                    source = reader.ReadToEnd();
                }
            }

            var template = Handlebars.Compile(source);
            var html = template(invoiceItem);

            var htmlToPdf = new NReco.PdfGenerator.HtmlToPdfConverter();
            var pdfBytes = htmlToPdf.GeneratePdf(html);

            return pdfBytes;
        }
    }
}
