﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DispatchPal.DAL.Utils
{
    public class LocalSettings
    {
        public static double TokenDurationIntervalInMinutes
        {
            get
            {
                try
                {
                    return
                        Convert.ToDouble(System.Configuration.ConfigurationManager.AppSettings["TokenDurationIntervalInMinutes"].ToString());
                }
                catch
                {
                    return 120;
                }
            }
        }
    }
}
