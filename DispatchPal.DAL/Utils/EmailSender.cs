﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace DispatchPal.DAL.Utils
{
    public class EmailSender
    {
        public static void Send(string email, Stream stream, string msg = "")
        {
            MailMessage mm = new MailMessage("dispatchpalsender@gmail.com", email);
            mm.Subject = "DispatchPal Invoice";
            mm.Body = "DispatchPal Invoice Attachment";
            if (stream != null)
                mm.Attachments.Add(new Attachment(stream, "invoice.pdf"));
            else
                mm.Subject = msg;
            mm.IsBodyHtml = true;
            SmtpClient smtp = new SmtpClient();
            smtp.Host = "smtp.gmail.com";
            smtp.EnableSsl = true;
            NetworkCredential NetworkCred = new NetworkCredential();
            NetworkCred.UserName = "dispatchpalsender@gmail.com";
            NetworkCred.Password = "123qwe!@#QWE";
            smtp.UseDefaultCredentials = true;
            smtp.Credentials = NetworkCred;
            smtp.Port = 587;
            smtp.Send(mm);
        }
    }
}
