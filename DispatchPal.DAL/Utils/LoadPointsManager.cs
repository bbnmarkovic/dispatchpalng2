﻿using DispatchPal.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static DispatchPal.DAL.Models.Enums.DplEnums;

namespace DispatchPal.DAL.Utils
{
    public class LoadPointsManager
    {
        public static bool ReorderAllLoadPoints(int tripId, int loadId)
        {
            try
            {
                using (var context = new dbEntities1())
                {
                    context.Configuration.LazyLoadingEnabled = false;
                    context.Configuration.ProxyCreationEnabled = false;

                    IQueryable<load_point> allPoints = null;
                    if (tripId > 0)
                        allPoints = context.load_point.Where(item => item.TourId == tripId && item.OrderNumber > 0).OrderBy(item => item.DateLoad);
                    else if (loadId > 0)
                        allPoints = context.load_point.Where(item => item.LoadId == loadId && item.OrderNumber > 0).OrderBy(item => item.DateLoad);

                    if (allPoints != null)
                    {
                        int index = 1;
                        foreach (load_point point in allPoints)
                        {
                            point.OrderNumber = index;
                            index++;
                        }
                        context.SaveChanges();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static void SetStartingLoadPoint(int tripId, int loadId)
        {
            using (var context = new dbEntities1())
            {
                context.Configuration.LazyLoadingEnabled = false;
                context.Configuration.ProxyCreationEnabled = false;

                tour tripItem = context.tours.Where(item => item.Id == tripId).FirstOrDefault();
                load loadItem = context.loads.AsNoTracking().Where(item => item.Id == loadId).FirstOrDefault();
                if (tripItem != null && loadItem != null)
                {
                    load_point currentStartingLoadPoint = context.load_point.AsNoTracking().Where(item => item.TourId == tripId && item.OrderNumber == 0).FirstOrDefault();
                    if (currentStartingLoadPoint == null)
                    {
                        load_point firstLoadPoint = context.load_point.AsNoTracking().Where(item => item.TourId == tripId && item.OrderNumber > 0 && item.ActionType == (int)LoadPointType.Loading).OrderBy(item => item.DateLoad).FirstOrDefault();
                        if (firstLoadPoint != null)
                        {
                            load_point lastUnloadPointForTruck = context.load_point.AsNoTracking().Where(item => item.ActionType == (int)LoadPointType.Unloading && item.load.tour != null && item.load.tour.TruckId == tripItem.TruckId && item.DateLoad < firstLoadPoint.DateLoad).OrderByDescending(item => item.DateLoad).FirstOrDefault();
                            if (lastUnloadPointForTruck != null)
                            {
                                load_point startLoadPoint = new load_point();
                                startLoadPoint.TourId = tripId;
                                startLoadPoint.Lat = lastUnloadPointForTruck.Lat;
                                startLoadPoint.Lng = lastUnloadPointForTruck.Lng;
                                startLoadPoint.Location = lastUnloadPointForTruck.Location;
                                startLoadPoint.DateLoad = lastUnloadPointForTruck.DateLoad.ToUniversalTime();
                                startLoadPoint.OrderNumber = 0;
                                startLoadPoint.ActionType = (int)LoadPointType.Loading;
                                startLoadPoint.Weight = 0;


                                loadItem.load_point.Add(startLoadPoint);
                            }
                        }
                    }
                    else
                    {
                        load_point firstLoadPoint = context.load_point.AsNoTracking().Where(item => item.TourId == tripId && item.OrderNumber > 0 && item.ActionType == (int)LoadPointType.Loading).OrderBy(item => item.DateLoad).FirstOrDefault();
                        if (firstLoadPoint != null && firstLoadPoint.DateLoad < currentStartingLoadPoint.DateLoad)
                        {
                            load_point lastUnloadPointForTruck = context.load_point.AsNoTracking().Where(item => item.ActionType == (int)LoadPointType.Unloading && item.load.tour != null && item.load.tour.TruckId == tripItem.TruckId && item.DateLoad < firstLoadPoint.DateLoad).OrderByDescending(item => item.DateLoad).FirstOrDefault();
                            if (lastUnloadPointForTruck != null)
                            {
                                load_point startLoadPoint = new load_point();
                                startLoadPoint.TourId = tripId;
                                startLoadPoint.Lat = lastUnloadPointForTruck.Lat;
                                startLoadPoint.Lng = lastUnloadPointForTruck.Lng;
                                startLoadPoint.Location = lastUnloadPointForTruck.Location;
                                startLoadPoint.DateLoad = lastUnloadPointForTruck.DateLoad.ToUniversalTime();
                                startLoadPoint.OrderNumber = 0;
                                startLoadPoint.ActionType = (int)LoadPointType.Loading;
                                startLoadPoint.Weight = 0;


                                loadItem.load_point.Add(startLoadPoint);
                            }
                        }
                    }
                    context.SaveChanges();
                }
            }
        }

        public static bool SetStartAndEndDates(int tripId, int loadId)
        {
            try
            {
                using (var context = new dbEntities1()) {

                    context.Configuration.LazyLoadingEnabled = false;
                    context.Configuration.ProxyCreationEnabled = false;

                    tour tr = context.tours.Where(item => item.Id == tripId).FirstOrDefault();
                    if (tr != null)
                    {
                        List<load_point> points = context.load_point.Where(item => item.TourId == tripId).ToList();

                        load_point firstPoint = points.OrderBy(item => item.DateLoad).FirstOrDefault();
                        load_point lastPoint = points.OrderByDescending(item => item.DateLoad).FirstOrDefault();

                        tr.DateFrom = firstPoint != null ? firstPoint.DateLoad : default(DateTime?);
                        tr.DateTo = lastPoint != null ? lastPoint.DateLoad : default(DateTime?);
                    }

                    load ld = context.loads.Where(item => item.Id == loadId).FirstOrDefault();
                    if (ld != null)
                    {
                        List<load_point> points = context.load_point.Where(item => item.LoadId == loadId).ToList();

                        load_point firstPoint = points.OrderBy(item => item.DateLoad).FirstOrDefault();
                        load_point lastPoint = points.OrderByDescending(item => item.DateLoad).FirstOrDefault();

                        ld.DateFrom = firstPoint != null ? firstPoint.DateLoad : default(DateTime?);
                        ld.DateTo = lastPoint != null ? lastPoint.DateLoad : default(DateTime?);
                    }

                    context.SaveChanges();

                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static ResponseModel<bool> ValidateLoadPointDate(LoadPointModel model)
        {
            using(var context = new dbEntities1())
            {
                context.Configuration.LazyLoadingEnabled = false;
                context.Configuration.ProxyCreationEnabled = false;

                tour tr = context.tours.Where(item => item.Id == model.TourId).FirstOrDefault();

                if (tr != null)
                {
                    tour nextTour = context.tours.AsNoTracking().Where(item => item.Id != tr.Id &&
                                                                        item.TruckId == tr.TruckId &&
                                                                        item.DateFrom > tr.DateFrom).OrderBy(item => item.DateFrom).FirstOrDefault();

                    if (nextTour != null && nextTour.DateFrom < model.DateLoad)
                    {
                        return new ResponseModel<bool>()
                        {
                            IsSuccess = false,
                            Message = "Load point can't be added after date (" + nextTour.DateFrom.Value.ToString("MM/dd/yyyy") + ")"
                        };
                    }
                    else
                    {
                        load_point startPoint = context.load_point.AsNoTracking().Where(item => item.TourId == model.TourId && item.OrderNumber == 0).FirstOrDefault();
                        if (startPoint != null)
                        {
                            if (startPoint.DateLoad > model.DateLoad)
                            {
                                return new ResponseModel<bool>()
                                {
                                    IsSuccess = false,
                                    Message = "Load point can't be added before starting load point (" + startPoint.DateLoad.ToString("MM/dd/yyyy") + ")"
                                };
                            }
                        }
                        else
                        {

                            if (tr != null)
                            {
                                load_point previousUnloadPoint = context.load_point.AsNoTracking().Where(item => item.ActionType == (int)LoadPointType.Unloading &&
                                                                                                                        item.TourId != model.TourId &&
                                                                                                                        item.DateLoad < tr.DateFrom &&
                                                                                                                        item.load.tour != null &&
                                                                                                                        item.load.tour.TruckId == tr.TruckId).OrderByDescending(item => item.DateLoad).FirstOrDefault();
                                if (previousUnloadPoint != null && previousUnloadPoint.DateLoad > model.DateLoad)
                                {
                                    return new ResponseModel<bool>()
                                    {
                                        IsSuccess = false,
                                        Message = "Load point can't be added before the last unload point for truck (" + previousUnloadPoint.DateLoad.ToString("MM/dd/yyyy") + ")"
                                    };
                                }
                            }
                        }
                    }
                }

                return new ResponseModel<bool>()
                {
                    IsSuccess = true
                };
            }
        }
    }
}
