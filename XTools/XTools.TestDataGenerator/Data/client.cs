//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace XTools.TestDataGenerator.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class client
    {
        public client()
        {
            this.brokers = new HashSet<broker>();
            this.loads = new HashSet<load>();
            this.loads1 = new HashSet<load>();
            this.loads2 = new HashSet<load>();
        }
    
        public int Id { get; set; }
        public int ClientTypeId { get; set; }
        public string AccountNumber { get; set; }
        public string DisplayName { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public Nullable<int> StateId { get; set; }
        public string ZipCode { get; set; }
        public string PhoneNumber { get; set; }
        public string AlternatePhoneNumber { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string Fee { get; set; }
        public string MotorCarrierNumber { get; set; }
        public string EIN { get; set; }
        public System.DateTime LastPaid { get; set; }
        public string ContactName { get; set; }
        public int ModifiedBy { get; set; }
        public System.DateTime ModifiedDate { get; set; }
        public bool IsActive { get; set; }
    
        public virtual ICollection<broker> brokers { get; set; }
        public virtual w_state w_state { get; set; }
        public virtual client_type client_type { get; set; }
        public virtual ICollection<load> loads { get; set; }
        public virtual ICollection<load> loads1 { get; set; }
        public virtual ICollection<load> loads2 { get; set; }
    }
}
