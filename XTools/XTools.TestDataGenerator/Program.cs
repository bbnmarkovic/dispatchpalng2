﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XTools.TestDataGenerator.Data;
using XTools.TestDataGenerator.ModelsSqlServer;

namespace XTools.TestDataGenerator
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Start:");

            string newPassword = "pass"; //System.Configuration.ConfigurationManager.AppSettings["DefaultPassword"].ToString();
                Guid g = new Guid();
          
            using (ptSqlServerModel model1 = new ptSqlServerModel())
            {

                var x = model1.users.Add(new ModelsSqlServer.user()
                {
                    CompanyId = 1,
                    FirstName = "Test",
                    LastName = "Testovic",
                    Email = "test@gmail.com",
                    UserName = "test",
                    Password_uuid = g.ToString(),
                    Password = MD5Tools.GetEncriptedPassword(newPassword, g),
            });
                model1.users.Add(x);
                model1.SaveChanges();
            }
            Console.ReadLine();
        }
    }



}

/*
 * 
 * 
 * 
 * 

    public class City
    {
        public City(double lat, double lng, string name)
        {
            Lat = (decimal)lat;
            Lng = (decimal)lng;
            Name = name;

        }
        public string Name { get; set; }
        public decimal Lat { get; set; }
        public decimal Lng { get; set; }
    }

    public static class Utility
    {

        public static City[] Cities =
        {
            new City(41.873485, -87.838615, "Chicago, IL, United States"),
            new City(39.770459, -86.153781, "Indianapolis, IN, United States"),
            new City(40.710490, -74.005073, "New York, NY, USA"),
            new City(38.946793, -77.025492, "Washington, DC, USA"),
            new City(25.754312, -80.195301, "Miami, FL, USA"),
            new City(39.131933, -94.628780, "Kansas, MO, USA"),
            new City(39.739443, -104.986267, "Denver, CO, USA"),
            new City(36.241953, -115.134402, "Las Vegas, NV, USA"),
            new City(34.046376, -118.242616, "Los Angeles, CA, USA"),
            new City(37.771433, -122.422839, "San Francisco, CA 94103, USA"),
            new City(19.434348, -99.136975, "Mexico City"),
        };

        public static company CompanyNew(int i)
        {
            return new company
            {
                Name = "company." + i,
                UserId = 1,
                ModifiedDate = DateTime.Now,
                ModifiedBy = 1,
                IsActive = true
            };
        }

        public static user UserNew(int i, int companyId)
        {
            Guid g = new Guid();
            user user = new user
            {
                CompanyId = companyId,
                AccessFailedCount = 0,
                LockoutEnabled = true,
                TwoFactorEnabled = false,
                PhoneNumber = "",
                PhoneNumberConfirmed = true,
                LockoutEndDateUtc = new DateTime(2020, 1, 1),
                UserName = "dp.test." + i,
                Email = "dp.test." + i + "@gmail.com",
                FirstName = "Test" + i,
                LastName = "Testing" + i,
                EmailConfirmed = true,
                Password = MD5Tools.GetEncriptedPassword("pass", g),
                Password_uuid = g.ToString(),
                DateCreatedUtc = DateTime.UtcNow,
                PasswordSalt = g.ToString(),
                ResetPasswordExpiryUtc = DateTime.UtcNow.AddDays(1),
                Status = 1,
                ProfileImg = "",
                ModifiedBy = 1,
                IsActive = true,
                ModifiedDate = DateTime.Now
            };

            return user;
        }

        public static driver DriverNew(int i, PersonNameGenerator personGenerator, int companyId)
        {

            var name = personGenerator.GenerateRandomFirstAndLastName().Split(' ');
            return new driver
            {
                CompanyId = companyId,
                DisplayName = "driver_" + name[0] + "_" + name[1],
                FirstName = name[0],
                LastName = name[1],
                ModifiedDate = DateTime.Now,
                ModifiedBy = 1,
                IsActive = true
            };
        }

        public static client ClientNew(int i, PersonNameGenerator personGenerator, int companyId)
        {

            var name = personGenerator.GenerateRandomFirstAndLastName().Split(' ');
            return new client
            {
                ClientTypeId = 3,
                LastPaid = new DateTime(),
                Name = name[0],
                DisplayName = "broker_" + name[1],
                ModifiedDate = DateTime.Now,
                ModifiedBy = 1,
                IsActive = true
            };
        }

        public static vehicle VehicleNew(int i, PersonNameGenerator personGenerator, int companyId)
        {

            return new vehicle
            {
                CompanyId =  companyId,
                VehicleType = 0,
                Model = "Scania",
                DisplayName = "Truck." + i,
                TrailerNumber = (1000 + i).ToString(),
                ModifiedDate = DateTime.Now,
                ModifiedBy = 1,
                IsActive = true
            };
        }
        public static tour TourNew(int i, PersonNameGenerator personGenerator, int companyId, int userId, client[] clients, driver[] drivers, vehicle[] vehicles, dispatch_palEntities context, int date)
        {
            Random random = new Random();
            var d1 = random.Next(0, 10);
            var v1 = random.Next(0, 10);
            var b1 = random.Next(0, 10);
            var t = new tour
            {
                DriverAId = drivers[d1].Id,
                TruckId = vehicles[v1].Id,
               
                CompanyId = companyId,
                ModifiedBy = 1,
                IsActive = true,
                ModifiedDate = DateTime.Now
            };
            context.tours.Add(t);
            context.SaveChanges();
            var load = new load
            {
                LoadNumber = "LN-"+i+"_"+date,
                TourId = t.Id,
                DispatcherId = userId,
                CompanyId = companyId,
                Broker1Id = clients[b1].Id,
                ModifiedBy = 1,
                ModifiedDate = DateTime.Now,
                AgreedPrice = 0,
                BillableMiles = 0,
                BobtailMiles = 0,
                EmptyMiles = 0,
                ExtraStops = 0,
                FuelSurcharge = 0,
                TotalDistanceMi = 0,
                TotalDistanceKm = 0,
                Status = 1,
                IsActive = true
            };


            var p1 = random.Next(0, 10);
            var p2 = random.Next(0, 10);
            var p3 = random.Next(0, 10);
            var lp1 = new load_point
            {
                TourId = t.Id,
                CompanyId = companyId,
                ActionType = 0,
                Lat = Cities[p1].Lat,
                Lng = Cities[p1].Lng,
                Location = Cities[p1].Name,
                Address ="Address xxxxx",
                DateLoad = DateTime.Now.AddDays(date),
                DistanceFromStart = 0,

            };
            var lp2 = new load_point
            {
                TourId = t.Id,
                CompanyId = companyId,
                ActionType = 0,
                Lat = Cities[p2].Lat,
                Lng = Cities[p2].Lng,
                Location = Cities[p2].Name,
                Address = "Address xxxxx",
                DateLoad = DateTime.Now.AddDays(date),
                DistanceFromStart = 0

            };
            var lp3 = new load_point
            {
                TourId = t.Id,
                CompanyId = companyId,
                ActionType = 1,
                Lat = Cities[p3].Lat,
                Lng = Cities[p3].Lng,
                Location = Cities[p3].Name,
                Address = "Address xxxxx",
                DateLoad = DateTime.Now.AddDays(date + 1),
                DistanceFromStart = 0
            };
            load.load_point.Add(lp1);
            load.load_point.Add(lp2);
            load.load_point.Add(lp3);
            context.loads.Add(load);
            context.SaveChanges();
            return t;
        }
    }



 
 * Random random = new Random();
var personGenerator = new PersonNameGenerator();

            using (var context = new dispatch_palEntities())
            {
                context.Configuration.AutoDetectChangesEnabled = false;
                context.Configuration.LazyLoadingEnabled = false;
                for (int i = 1; i< 20; i++)
                {
                    var company = Utility.CompanyNew(i);
context.companies.Add(company);
                    context.SaveChanges();
                    Console.WriteLine(DateTime.Now.ToString("hh:mm:ss.fff") + " , i: " + i + " | Company saved : ");
                    var u = Utility.UserNew(i, company.Id);
context.users.Add(u);
                    context.SaveChanges();
                    Console.WriteLine(DateTime.Now.ToString("hh:mm:ss.fff") + " , i: " + i + " | User saved : ");

                    for (int j = 0; j< 10; j++)
                    {
                        var broker = Utility.ClientNew(j, personGenerator, company.Id);
context.clients.Add(broker);
                        var driver = Utility.DriverNew(j, personGenerator, company.Id);
context.drivers.Add(driver);
                        var vehicle = Utility.VehicleNew(j, personGenerator, company.Id);
context.vehicles.Add(vehicle);
                    }

                    context.SaveChanges();
                    Console.WriteLine(DateTime.Now.ToString("hh:mm:ss.fff") + " , i: " + i + " | Broker, drivers saved : ");


                    client[] clients = context.clients.Take(100).ToArray();
driver[] drivers = context.drivers.Where(p => p.CompanyId == company.Id).ToArray();
vehicle[] vehicles = context.vehicles.Where(p => p.CompanyId == company.Id).ToArray();
                    for (int date = 1; date< 365; date++)
                    {
                        for (int j = 0; j< 20; j++)
                        {
                            Utility.TourNew(i, personGenerator, company.Id, u.Id, clients, drivers, vehicles, context, date);
                        }
                    }



                    Console.WriteLine(DateTime.Now.ToString("hh:mm:ss.fff") + " , i: " + i + " | Tour saved : ");
                }

                Console.WriteLine(DateTime.Now.ToString("hh:mm:ss.fff") + " THE END ");
                Console.ReadLine();

            }*/
