namespace XTools.TestDataGenerator.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("dispatch_pal.invoice")]
    public partial class invoice
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public invoice()
        {
            invoice_item = new HashSet<invoice_item>();
            invoice_payment = new HashSet<invoice_payment>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int LoadId { get; set; }

        [Required]
        [StringLength(45)]
        public string InvoiceNumber { get; set; }

        public DateTime InvoiceDate { get; set; }

        public DateTime InvoiceDueDate { get; set; }

        public int BusinessTaxID { get; set; }

        public decimal Total { get; set; }

        public decimal SubTotal { get; set; }

        public decimal AmountPaid { get; set; }

        public decimal Discount { get; set; }

        public decimal DiscountAmount { get; set; }

        public int TaxInclusive { get; set; }

        public decimal Tax { get; set; }

        public decimal TaxAmmount { get; set; }

        public decimal BalanceRemaining { get; set; }

        public DateTime PaymentDate { get; set; }

        public bool SendReceipt { get; set; }

        [Column(TypeName = "text")]
        [StringLength(65535)]
        public string ExternalNote { get; set; }

        public int ModifiedBy { get; set; }

        public DateTime ModifiedDate { get; set; }

        public bool IsActive { get; set; }

        public int? CompanyId { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<invoice_item> invoice_item { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<invoice_payment> invoice_payment { get; set; }

        public virtual load load { get; set; }
    }
}
