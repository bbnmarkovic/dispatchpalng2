namespace XTools.TestDataGenerator.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("dispatch_pal.tour")]
    public partial class tour
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tour()
        {
            loads = new HashSet<load>();
        }

        public int Id { get; set; }

        [StringLength(45)]
        public string TripNumber { get; set; }

        public DateTime? DateFrom { get; set; }

        public DateTime? DateTo { get; set; }

        public int? DriverAId { get; set; }

        public int? DriverBId { get; set; }

        public int? TruckId { get; set; }

        public int? TrailerId { get; set; }

        public int PayTypeDriverA { get; set; }

        public decimal PayRateFullDriverA { get; set; }

        public decimal PayRateEmptyDriverA { get; set; }

        public decimal PayRateBobtailDriverA { get; set; }

        public int PayTypeDriverB { get; set; }

        public decimal PayRateFullDriverB { get; set; }

        public decimal PayRateEmptyDriverB { get; set; }

        public decimal PayRateBobtailDriverB { get; set; }

        public int ModifiedBy { get; set; }

        public DateTime ModifiedDate { get; set; }

        public bool IsActive { get; set; }

        public bool IsFinished { get; set; }

        public int CompanyId { get; set; }

        public virtual driver driver { get; set; }

        public virtual driver driver1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<load> loads { get; set; }

        public virtual vehicle vehicle { get; set; }

        public virtual vehicle vehicle1 { get; set; }
    }
}
