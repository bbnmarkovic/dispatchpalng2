namespace XTools.TestDataGenerator.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("dispatch_pal.service")]
    public partial class service
    {
        public int Id { get; set; }

        public int? TruckId { get; set; }

        public decimal? Mileage { get; set; }

        public DateTime? Date { get; set; }

        [StringLength(450)]
        public string Location { get; set; }

        [StringLength(450)]
        public string Part { get; set; }

        [StringLength(450)]
        public string Job { get; set; }

        public decimal? Cost { get; set; }

        public DateTime? WarrantyExpDate { get; set; }

        public DateTime? ScheduledServiceDate { get; set; }

        [Column(TypeName = "text")]
        [StringLength(65535)]
        public string Description { get; set; }

        public int CompanyId { get; set; }

        public virtual vehicle vehicle { get; set; }
    }
}
