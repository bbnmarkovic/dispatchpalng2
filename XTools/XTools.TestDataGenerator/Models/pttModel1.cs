namespace XTools.TestDataGenerator.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class pttModel1 : DbContext
    {
        //name=ptSqlServerModel       pttModel1
        public pttModel1()
            : base("name=ptSqlServerModel")
        {
        }

        public virtual DbSet<broker> brokers { get; set; }
        public virtual DbSet<cash_flow> cash_flow { get; set; }
        public virtual DbSet<cash_flow_type> cash_flow_type { get; set; }
        public virtual DbSet<client> clients { get; set; }
        public virtual DbSet<client_app> client_app { get; set; }
        public virtual DbSet<client_type> client_type { get; set; }
        public virtual DbSet<company> companies { get; set; }
        public virtual DbSet<driver> drivers { get; set; }
        public virtual DbSet<invoice> invoices { get; set; }
        public virtual DbSet<invoice_item> invoice_item { get; set; }
        public virtual DbSet<invoice_payment> invoice_payment { get; set; }
        public virtual DbSet<load> loads { get; set; }
        public virtual DbSet<load_issue> load_issue { get; set; }
        public virtual DbSet<load_point> load_point { get; set; }
        public virtual DbSet<login_token> login_token { get; set; }
        public virtual DbSet<point> points { get; set; }
        public virtual DbSet<refresh_token> refresh_token { get; set; }
        public virtual DbSet<role> roles { get; set; }
        public virtual DbSet<service> services { get; set; }
        public virtual DbSet<tour> tours { get; set; }
        public virtual DbSet<uploaded_file> uploaded_file { get; set; }
        public virtual DbSet<user> users { get; set; }
        public virtual DbSet<vehicle> vehicles { get; set; }
        public virtual DbSet<w_state> w_state { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<broker>()
                .Property(e => e.DisplayName)
                .IsUnicode(false);

            modelBuilder.Entity<broker>()
                .Property(e => e.FirstName)
                .IsUnicode(false);

            modelBuilder.Entity<broker>()
                .Property(e => e.LastName)
                .IsUnicode(false);

            modelBuilder.Entity<broker>()
                .Property(e => e.PhoneNumber)
                .IsUnicode(false);

            modelBuilder.Entity<broker>()
                .Property(e => e.MobileNumber)
                .IsUnicode(false);

            modelBuilder.Entity<broker>()
                .Property(e => e.Email)
                .IsUnicode(false);

            modelBuilder.Entity<broker>()
                .Property(e => e.Fax)
                .IsUnicode(false);

            modelBuilder.Entity<broker>()
                .Property(e => e.Fax1)
                .IsUnicode(false);

            modelBuilder.Entity<cash_flow>()
                .Property(e => e.Note)
                .IsUnicode(false);

            modelBuilder.Entity<cash_flow_type>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<cash_flow_type>()
                .HasMany(e => e.cash_flow)
                .WithRequired(e => e.cash_flow_type)
                .HasForeignKey(e => e.TypeId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<client>()
                .Property(e => e.AccountNumber)
                .IsUnicode(false);

            modelBuilder.Entity<client>()
                .Property(e => e.DisplayName)
                .IsUnicode(false);

            modelBuilder.Entity<client>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<client>()
                .Property(e => e.Address)
                .IsUnicode(false);

            modelBuilder.Entity<client>()
                .Property(e => e.Address2)
                .IsUnicode(false);

            modelBuilder.Entity<client>()
                .Property(e => e.City)
                .IsUnicode(false);

            modelBuilder.Entity<client>()
                .Property(e => e.ZipCode)
                .IsUnicode(false);

            modelBuilder.Entity<client>()
                .Property(e => e.PhoneNumber)
                .IsUnicode(false);

            modelBuilder.Entity<client>()
                .Property(e => e.AlternatePhoneNumber)
                .IsUnicode(false);

            modelBuilder.Entity<client>()
                .Property(e => e.Fax)
                .IsUnicode(false);

            modelBuilder.Entity<client>()
                .Property(e => e.Email)
                .IsUnicode(false);

            modelBuilder.Entity<client>()
                .Property(e => e.Fee)
                .IsUnicode(false);

            modelBuilder.Entity<client>()
                .Property(e => e.MotorCarrierNumber)
                .IsUnicode(false);

            modelBuilder.Entity<client>()
                .Property(e => e.EIN)
                .IsUnicode(false);

            modelBuilder.Entity<client>()
                .Property(e => e.ContactName)
                .IsUnicode(false);

            modelBuilder.Entity<client>()
                .HasMany(e => e.brokers)
                .WithRequired(e => e.client)
                .HasForeignKey(e => e.PartnerId);

            modelBuilder.Entity<client>()
                .HasMany(e => e.loads)
                .WithOptional(e => e.client)
                .HasForeignKey(e => e.BrokerageId);

            modelBuilder.Entity<client>()
                .HasMany(e => e.loads1)
                .WithOptional(e => e.client1)
                .HasForeignKey(e => e.PartnerConsigneeId);

            modelBuilder.Entity<client>()
                .HasMany(e => e.loads2)
                .WithOptional(e => e.client2)
                .HasForeignKey(e => e.PartnerShipperId);

            modelBuilder.Entity<client_app>()
                .Property(e => e.Id)
                .IsUnicode(false);

            modelBuilder.Entity<client_app>()
                .Property(e => e.Secret)
                .IsUnicode(false);

            modelBuilder.Entity<client_app>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<client_app>()
                .Property(e => e.AllowedOrigin)
                .IsUnicode(false);

            modelBuilder.Entity<client_type>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<client_type>()
                .HasMany(e => e.clients)
                .WithRequired(e => e.client_type)
                .HasForeignKey(e => e.ClientTypeId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<company>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<company>()
                .Property(e => e.Address)
                .IsUnicode(false);

            modelBuilder.Entity<company>()
                .Property(e => e.Address1)
                .IsUnicode(false);

            modelBuilder.Entity<company>()
                .Property(e => e.City)
                .IsUnicode(false);

            modelBuilder.Entity<company>()
                .Property(e => e.ZipCode)
                .IsUnicode(false);

            modelBuilder.Entity<company>()
                .Property(e => e.Country)
                .IsUnicode(false);

            modelBuilder.Entity<company>()
                .Property(e => e.PhoneNumber)
                .IsUnicode(false);

            modelBuilder.Entity<company>()
                .Property(e => e.AlternatePhoneNumber)
                .IsUnicode(false);

            modelBuilder.Entity<company>()
                .Property(e => e.Fax)
                .IsUnicode(false);

            modelBuilder.Entity<company>()
                .Property(e => e.Email)
                .IsUnicode(false);

            modelBuilder.Entity<company>()
                .Property(e => e.Contact)
                .IsUnicode(false);

            modelBuilder.Entity<company>()
                .Property(e => e.Notes)
                .IsUnicode(false);

            modelBuilder.Entity<company>()
                .Property(e => e.MotorCarrierNumber)
                .IsUnicode(false);

            modelBuilder.Entity<company>()
                .Property(e => e.BusinessNumber)
                .IsUnicode(false);

            modelBuilder.Entity<company>()
                .Property(e => e.EIN)
                .IsUnicode(false);

            modelBuilder.Entity<company>()
                .Property(e => e.Domain)
                .IsUnicode(false);

            modelBuilder.Entity<company>()
                .Property(e => e.Logo)
                .IsUnicode(false);

            modelBuilder.Entity<company>()
                .HasMany(e => e.users)
                .WithOptional(e => e.company)
                .HasForeignKey(e => e.CompanyId);

            modelBuilder.Entity<company>()
                .HasMany(e => e.users1)
                .WithMany(e => e.companies)
                .Map(m => m.ToTable("rel_user_company", "dispatch_pal").MapLeftKey("CompanyId").MapRightKey("UserId"));

            modelBuilder.Entity<driver>()
                .Property(e => e.DriverID)
                .IsUnicode(false);

            modelBuilder.Entity<driver>()
                .Property(e => e.FirstName)
                .IsUnicode(false);

            modelBuilder.Entity<driver>()
                .Property(e => e.MiddleName)
                .IsUnicode(false);

            modelBuilder.Entity<driver>()
                .Property(e => e.LastName)
                .IsUnicode(false);

            modelBuilder.Entity<driver>()
                .Property(e => e.DisplayName)
                .IsUnicode(false);

            modelBuilder.Entity<driver>()
                .Property(e => e.SSN)
                .IsUnicode(false);

            modelBuilder.Entity<driver>()
                .Property(e => e.FEIN)
                .IsUnicode(false);

            modelBuilder.Entity<driver>()
                .Property(e => e.Address1)
                .IsUnicode(false);

            modelBuilder.Entity<driver>()
                .Property(e => e.Address2)
                .IsUnicode(false);

            modelBuilder.Entity<driver>()
                .Property(e => e.City)
                .IsUnicode(false);

            modelBuilder.Entity<driver>()
                .Property(e => e.ZipCode)
                .IsUnicode(false);

            modelBuilder.Entity<driver>()
                .Property(e => e.HomePhone)
                .IsUnicode(false);

            modelBuilder.Entity<driver>()
                .Property(e => e.CellPhone)
                .IsUnicode(false);

            modelBuilder.Entity<driver>()
                .Property(e => e.CellPhoneCarrier)
                .IsUnicode(false);

            modelBuilder.Entity<driver>()
                .Property(e => e.Email)
                .IsUnicode(false);

            modelBuilder.Entity<driver>()
                .Property(e => e.Fax)
                .IsUnicode(false);

            modelBuilder.Entity<driver>()
                .Property(e => e.EmergencyContactName)
                .IsUnicode(false);

            modelBuilder.Entity<driver>()
                .Property(e => e.EmergencyContactPhone)
                .IsUnicode(false);

            modelBuilder.Entity<driver>()
                .Property(e => e.Relationship)
                .IsUnicode(false);

            modelBuilder.Entity<driver>()
                .Property(e => e.FuelCard)
                .IsUnicode(false);

            modelBuilder.Entity<driver>()
                .Property(e => e.PayMathod)
                .IsUnicode(false);

            modelBuilder.Entity<driver>()
                .Property(e => e.CDL)
                .IsUnicode(false);

            modelBuilder.Entity<driver>()
                .Property(e => e.CDLClass)
                .IsUnicode(false);

            modelBuilder.Entity<driver>()
                .Property(e => e.CDLEndorsement)
                .IsUnicode(false);

            modelBuilder.Entity<driver>()
                .Property(e => e.Note)
                .IsUnicode(false);

            modelBuilder.Entity<driver>()
                .HasMany(e => e.cash_flow)
                .WithRequired(e => e.driver)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<driver>()
                .HasMany(e => e.tours)
                .WithOptional(e => e.driver)
                .HasForeignKey(e => e.DriverAId);

            modelBuilder.Entity<driver>()
                .HasMany(e => e.tours1)
                .WithOptional(e => e.driver1)
                .HasForeignKey(e => e.DriverBId);

            modelBuilder.Entity<driver>()
                .HasMany(e => e.vehicles)
                .WithOptional(e => e.driver)
                .HasForeignKey(e => e.DriverAId);

            modelBuilder.Entity<driver>()
                .HasMany(e => e.vehicles1)
                .WithOptional(e => e.driver1)
                .HasForeignKey(e => e.DriverBId);

            modelBuilder.Entity<driver>()
                .HasMany(e => e.uploaded_file)
                .WithMany(e => e.drivers)
                .Map(m => m.ToTable("driver_uploaded_file_r", "dispatch_pal").MapLeftKey("DriverId").MapRightKey("UploadedFileId"));

            modelBuilder.Entity<invoice>()
                .Property(e => e.InvoiceNumber)
                .IsUnicode(false);

            modelBuilder.Entity<invoice>()
                .Property(e => e.ExternalNote)
                .IsUnicode(false);

            modelBuilder.Entity<invoice>()
                .HasMany(e => e.invoice_item)
                .WithRequired(e => e.invoice)
                .HasForeignKey(e => e.InvoiceId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<invoice>()
                .HasMany(e => e.invoice_payment)
                .WithOptional(e => e.invoice)
                .HasForeignKey(e => e.InvoiceId);

            modelBuilder.Entity<invoice_item>()
                .Property(e => e.ServiceName)
                .IsUnicode(false);

            modelBuilder.Entity<invoice_item>()
                .Property(e => e.Note)
                .IsUnicode(false);

            modelBuilder.Entity<invoice_payment>()
                .Property(e => e.PayedBy)
                .IsUnicode(false);

            modelBuilder.Entity<load>()
                .Property(e => e.LoadNumber)
                .IsUnicode(false);

            modelBuilder.Entity<load>()
                .Property(e => e.PO)
                .IsUnicode(false);

            modelBuilder.Entity<load>()
                .Property(e => e.Confirmation)
                .IsUnicode(false);

            modelBuilder.Entity<load>()
                .Property(e => e.BOL)
                .IsUnicode(false);

            modelBuilder.Entity<load>()
                .Property(e => e.Commodity)
                .IsUnicode(false);

            modelBuilder.Entity<load>()
                .Property(e => e.Chassis)
                .IsUnicode(false);

            modelBuilder.Entity<load>()
                .Property(e => e.DriverNotes)
                .IsUnicode(false);

            modelBuilder.Entity<load>()
                .Property(e => e.InternalNotes)
                .IsUnicode(false);

            modelBuilder.Entity<load>()
                .Property(e => e.ExternalNotes)
                .IsUnicode(false);

            modelBuilder.Entity<load>()
                .Property(e => e.ShippingNumber)
                .IsUnicode(false);

            modelBuilder.Entity<load>()
                .Property(e => e.ShippingNote)
                .IsUnicode(false);

            modelBuilder.Entity<load>()
                .Property(e => e.DeliveryNumber)
                .IsUnicode(false);

            modelBuilder.Entity<load>()
                .Property(e => e.DeliveryNote)
                .IsUnicode(false);

            modelBuilder.Entity<load>()
                .HasOptional(e => e.invoice)
                .WithRequired(e => e.load)
                .WillCascadeOnDelete();

            modelBuilder.Entity<load>()
                .HasMany(e => e.load_issue)
                .WithRequired(e => e.load)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<load>()
                .HasMany(e => e.load_point)
                .WithOptional(e => e.load)
                .WillCascadeOnDelete();

            modelBuilder.Entity<load_issue>()
                .Property(e => e.Description)
                .IsUnicode(false);

            modelBuilder.Entity<load_point>()
                .Property(e => e.Location)
                .IsUnicode(false);

            modelBuilder.Entity<load_point>()
                .Property(e => e.Country)
                .IsUnicode(false);

            modelBuilder.Entity<load_point>()
                .Property(e => e.State)
                .IsUnicode(false);

            modelBuilder.Entity<load_point>()
                .Property(e => e.City)
                .IsUnicode(false);

            modelBuilder.Entity<load_point>()
                .Property(e => e.Address)
                .IsUnicode(false);

            modelBuilder.Entity<load_point>()
                .Property(e => e.ZipCode)
                .IsUnicode(false);

            modelBuilder.Entity<load_point>()
                .Property(e => e.Description)
                .IsUnicode(false);

            modelBuilder.Entity<load_point>()
                .Property(e => e.Qty)
                .IsUnicode(false);

            modelBuilder.Entity<load_point>()
                .Property(e => e.DeliveryNotes)
                .IsUnicode(false);

            modelBuilder.Entity<load_point>()
                .Property(e => e.NumbersPO)
                .IsUnicode(false);

            modelBuilder.Entity<load_point>()
                .Property(e => e.EstimatedTime)
                .IsUnicode(false);

            modelBuilder.Entity<load_point>()
                .Property(e => e.Contact)
                .IsUnicode(false);

            modelBuilder.Entity<load_point>()
                .Property(e => e.Phone)
                .IsUnicode(false);

            modelBuilder.Entity<point>()
                .Property(e => e.Loaction)
                .IsUnicode(false);

            modelBuilder.Entity<refresh_token>()
                .Property(e => e.Id)
                .IsUnicode(false);

            modelBuilder.Entity<refresh_token>()
                .Property(e => e.Subject)
                .IsUnicode(false);

            modelBuilder.Entity<refresh_token>()
                .Property(e => e.ClientAppId)
                .IsUnicode(false);

            modelBuilder.Entity<refresh_token>()
                .Property(e => e.ProtectedTicket)
                .IsUnicode(false);

            modelBuilder.Entity<role>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<role>()
                .HasMany(e => e.users)
                .WithMany(e => e.roles)
                .Map(m => m.ToTable("user_role", "dispatch_pal").MapLeftKey("RoleId").MapRightKey("UserId"));

            modelBuilder.Entity<service>()
                .Property(e => e.Location)
                .IsUnicode(false);

            modelBuilder.Entity<service>()
                .Property(e => e.Part)
                .IsUnicode(false);

            modelBuilder.Entity<service>()
                .Property(e => e.Job)
                .IsUnicode(false);

            modelBuilder.Entity<service>()
                .Property(e => e.Description)
                .IsUnicode(false);

            modelBuilder.Entity<tour>()
                .Property(e => e.TripNumber)
                .IsUnicode(false);

            modelBuilder.Entity<tour>()
                .HasMany(e => e.loads)
                .WithOptional(e => e.tour)
                .WillCascadeOnDelete();

            modelBuilder.Entity<uploaded_file>()
                .Property(e => e.UniqueFileName)
                .IsUnicode(false);

            modelBuilder.Entity<uploaded_file>()
                .Property(e => e.OriginalFileName)
                .IsUnicode(false);

            modelBuilder.Entity<uploaded_file>()
                .Property(e => e.FilePath)
                .IsUnicode(false);

            modelBuilder.Entity<uploaded_file>()
                .Property(e => e.FileSize)
                .IsUnicode(false);

            modelBuilder.Entity<uploaded_file>()
                .Property(e => e.Extension)
                .IsUnicode(false);

            modelBuilder.Entity<uploaded_file>()
                .HasMany(e => e.loads)
                .WithMany(e => e.uploaded_file)
                .Map(m => m.ToTable("load_uploaded_file_r", "dispatch_pal").MapLeftKey("UploadedFileId").MapRightKey("LoadId"));

            modelBuilder.Entity<user>()
                .Property(e => e.UserId)
                .IsUnicode(false);

            modelBuilder.Entity<user>()
                .Property(e => e.UserName)
                .IsUnicode(false);

            modelBuilder.Entity<user>()
                .Property(e => e.FirstName)
                .IsUnicode(false);

            modelBuilder.Entity<user>()
                .Property(e => e.LastName)
                .IsUnicode(false);

            modelBuilder.Entity<user>()
                .Property(e => e.Email)
                .IsUnicode(false);

            modelBuilder.Entity<user>()
                .Property(e => e.PhoneNumber)
                .IsUnicode(false);

            modelBuilder.Entity<user>()
                .Property(e => e.MobileNumber)
                .IsUnicode(false);

            modelBuilder.Entity<user>()
                .Property(e => e.Address)
                .IsUnicode(false);

            modelBuilder.Entity<user>()
                .Property(e => e.Description)
                .IsUnicode(false);

            modelBuilder.Entity<user>()
                .Property(e => e.Password_uuid)
                .IsUnicode(false);

            modelBuilder.Entity<user>()
                .Property(e => e.LoginGuid)
                .IsUnicode(false);

            modelBuilder.Entity<user>()
                .Property(e => e.PasswordHash)
                .IsUnicode(false);

            modelBuilder.Entity<user>()
                .Property(e => e.DPPasswordHash)
                .IsUnicode(false);

            modelBuilder.Entity<user>()
                .Property(e => e.SecurityStamp)
                .IsUnicode(false);

            modelBuilder.Entity<user>()
                .Property(e => e.PasswordSalt)
                .IsUnicode(false);

            modelBuilder.Entity<user>()
                .Property(e => e.DateLastLoginUTC)
                .IsUnicode(false);

            modelBuilder.Entity<user>()
                .Property(e => e.InviteCode)
                .IsUnicode(false);

            modelBuilder.Entity<user>()
                .Property(e => e.ResetPasswordKey)
                .IsUnicode(false);

            modelBuilder.Entity<user>()
                .Property(e => e.ProfileImg)
                .IsUnicode(false);

            modelBuilder.Entity<user>()
                .HasMany(e => e.drivers)
                .WithOptional(e => e.user)
                .HasForeignKey(e => e.DispatchedBy);

            modelBuilder.Entity<user>()
                .HasMany(e => e.loads)
                .WithOptional(e => e.user)
                .HasForeignKey(e => e.DispatcherId);

            modelBuilder.Entity<user>()
                .HasMany(e => e.vehicles)
                .WithOptional(e => e.user)
                .HasForeignKey(e => e.DispatchedBy);

            modelBuilder.Entity<vehicle>()
                .Property(e => e.VehicleID)
                .IsUnicode(false);

            modelBuilder.Entity<vehicle>()
                .Property(e => e.DisplayName)
                .IsUnicode(false);

            modelBuilder.Entity<vehicle>()
                .Property(e => e.Number)
                .IsUnicode(false);

            modelBuilder.Entity<vehicle>()
                .Property(e => e.Make)
                .IsUnicode(false);

            modelBuilder.Entity<vehicle>()
                .Property(e => e.Model)
                .IsUnicode(false);

            modelBuilder.Entity<vehicle>()
                .Property(e => e.Size)
                .IsUnicode(false);

            modelBuilder.Entity<vehicle>()
                .Property(e => e.Color)
                .IsUnicode(false);

            modelBuilder.Entity<vehicle>()
                .Property(e => e.InsuranceWith)
                .IsUnicode(false);

            modelBuilder.Entity<vehicle>()
                .Property(e => e.PlateNumber)
                .IsUnicode(false);

            modelBuilder.Entity<vehicle>()
                .Property(e => e.VIN)
                .IsUnicode(false);

            modelBuilder.Entity<vehicle>()
                .Property(e => e.TrailerNumber)
                .IsUnicode(false);

            modelBuilder.Entity<vehicle>()
                .Property(e => e.PurchasedFrom)
                .IsUnicode(false);

            modelBuilder.Entity<vehicle>()
                .Property(e => e.SoldTo)
                .IsUnicode(false);

            modelBuilder.Entity<vehicle>()
                .Property(e => e.Note)
                .IsUnicode(false);

            modelBuilder.Entity<vehicle>()
                .Property(e => e.Type)
                .IsUnicode(false);

            modelBuilder.Entity<vehicle>()
                .Property(e => e.TireType)
                .IsUnicode(false);

            modelBuilder.Entity<vehicle>()
                .HasMany(e => e.cash_flow)
                .WithOptional(e => e.vehicle)
                .HasForeignKey(e => e.TrailerId);

            modelBuilder.Entity<vehicle>()
                .HasMany(e => e.cash_flow1)
                .WithRequired(e => e.vehicle1)
                .HasForeignKey(e => e.TruckId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<vehicle>()
                .HasMany(e => e.services)
                .WithOptional(e => e.vehicle)
                .HasForeignKey(e => e.TruckId);

            modelBuilder.Entity<vehicle>()
                .HasMany(e => e.tours)
                .WithOptional(e => e.vehicle)
                .HasForeignKey(e => e.TrailerId);

            modelBuilder.Entity<vehicle>()
                .HasMany(e => e.tours1)
                .WithOptional(e => e.vehicle1)
                .HasForeignKey(e => e.TruckId);

            modelBuilder.Entity<vehicle>()
                .HasMany(e => e.vehicle1)
                .WithOptional(e => e.vehicle2)
                .HasForeignKey(e => e.TrailerId);

            modelBuilder.Entity<w_state>()
                .Property(e => e.ISO)
                .IsUnicode(false);

            modelBuilder.Entity<w_state>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<w_state>()
                .Property(e => e.NickName)
                .IsUnicode(false);

            modelBuilder.Entity<w_state>()
                .HasMany(e => e.clients)
                .WithOptional(e => e.w_state)
                .HasForeignKey(e => e.StateId);

            modelBuilder.Entity<w_state>()
                .HasMany(e => e.companies)
                .WithOptional(e => e.w_state)
                .HasForeignKey(e => e.StateId);

            modelBuilder.Entity<w_state>()
                .HasMany(e => e.drivers)
                .WithOptional(e => e.w_state)
                .HasForeignKey(e => e.CDLStateId);

            modelBuilder.Entity<w_state>()
                .HasMany(e => e.drivers1)
                .WithOptional(e => e.w_state1)
                .HasForeignKey(e => e.StateId);

            modelBuilder.Entity<w_state>()
                .HasMany(e => e.vehicles)
                .WithOptional(e => e.w_state)
                .HasForeignKey(e => e.StateId);
        }
    }
}
