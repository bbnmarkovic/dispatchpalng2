namespace XTools.TestDataGenerator.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("dispatch_pal.load_issue")]
    public partial class load_issue
    {
        public int Id { get; set; }

        public int LoadId { get; set; }

        [StringLength(450)]
        public string Description { get; set; }

        public bool IsResolved { get; set; }

        public virtual load load { get; set; }
    }
}
