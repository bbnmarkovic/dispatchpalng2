namespace XTools.TestDataGenerator.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("dispatch_pal.invoice_payment")]
    public partial class invoice_payment
    {
        public int Id { get; set; }

        public int? InvoiceId { get; set; }

        public decimal? Amount { get; set; }

        public DateTime? PaymentDate { get; set; }

        [StringLength(450)]
        public string PayedBy { get; set; }

        public virtual invoice invoice { get; set; }
    }
}
