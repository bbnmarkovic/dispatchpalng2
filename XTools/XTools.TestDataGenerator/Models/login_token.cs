namespace XTools.TestDataGenerator.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("dispatch_pal.login_token")]
    public partial class login_token
    {
        [Key]
        public Guid token { get; set; }

        public int user_id { get; set; }

        public DateTime created { get; set; }

        public DateTime accessed { get; set; }
    }
}
