namespace XTools.TestDataGenerator.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("dispatch_pal.refresh_token")]
    public partial class refresh_token
    {
        [StringLength(45)]
        public string Id { get; set; }

        [Required]
        [StringLength(45)]
        public string Subject { get; set; }

        [Required]
        [StringLength(45)]
        public string ClientAppId { get; set; }

        public DateTime IssuedUTC { get; set; }

        public DateTime ExpiresUTC { get; set; }

        [Required]
        [StringLength(1000)]
        public string ProtectedTicket { get; set; }
    }
}
