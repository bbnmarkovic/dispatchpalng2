namespace XTools.TestDataGenerator.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("dispatch_pal.broker")]
    public partial class broker
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public broker()
        {
            loads = new HashSet<load>();
        }

        public int Id { get; set; }

        public int PartnerId { get; set; }

        [Required]
        [StringLength(30)]
        public string DisplayName { get; set; }

        [StringLength(100)]
        public string FirstName { get; set; }

        [StringLength(100)]
        public string LastName { get; set; }

        [StringLength(15)]
        public string PhoneNumber { get; set; }

        [StringLength(15)]
        public string MobileNumber { get; set; }

        [StringLength(245)]
        public string Email { get; set; }

        [StringLength(245)]
        public string Fax { get; set; }

        [StringLength(250)]
        public string Fax1 { get; set; }

        public int Rate { get; set; }

        public decimal Rating { get; set; }

        public bool IsActive { get; set; }

        public int CompanyId { get; set; }

        public virtual client client { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<load> loads { get; set; }
    }
}
