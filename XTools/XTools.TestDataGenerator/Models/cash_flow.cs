namespace XTools.TestDataGenerator.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("dispatch_pal.cash_flow")]
    public partial class cash_flow
    {
        public int Id { get; set; }

        public int DriverId { get; set; }

        public int TruckId { get; set; }

        public int? TrailerId { get; set; }

        public int TypeId { get; set; }

        public DateTime Date { get; set; }

        public decimal Amount { get; set; }

        [Column(TypeName = "text")]
        [StringLength(65535)]
        public string Note { get; set; }

        public bool IsSettled { get; set; }

        public bool IsActive { get; set; }

        public int CompanyId { get; set; }

        public virtual driver driver { get; set; }

        public virtual vehicle vehicle { get; set; }

        public virtual vehicle vehicle1 { get; set; }

        public virtual cash_flow_type cash_flow_type { get; set; }
    }
}
