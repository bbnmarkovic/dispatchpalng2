namespace XTools.TestDataGenerator.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("dispatch_pal.driver")]
    public partial class driver
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public driver()
        {
            cash_flow = new HashSet<cash_flow>();
            tours = new HashSet<tour>();
            tours1 = new HashSet<tour>();
            vehicles = new HashSet<vehicle>();
            vehicles1 = new HashSet<vehicle>();
            uploaded_file = new HashSet<uploaded_file>();
        }

        public int Id { get; set; }

        [StringLength(45)]
        public string DriverID { get; set; }

        [StringLength(100)]
        public string FirstName { get; set; }

        [StringLength(100)]
        public string MiddleName { get; set; }

        [StringLength(100)]
        public string LastName { get; set; }

        [StringLength(255)]
        public string DisplayName { get; set; }

        [Column(TypeName = "date")]
        public DateTime? DOB { get; set; }

        [StringLength(45)]
        public string SSN { get; set; }

        [StringLength(45)]
        public string FEIN { get; set; }

        [StringLength(45)]
        public string Address1 { get; set; }

        [StringLength(255)]
        public string Address2 { get; set; }

        [StringLength(45)]
        public string City { get; set; }

        public int? StateId { get; set; }

        [StringLength(45)]
        public string ZipCode { get; set; }

        [StringLength(45)]
        public string HomePhone { get; set; }

        [StringLength(45)]
        public string CellPhone { get; set; }

        [StringLength(45)]
        public string CellPhoneCarrier { get; set; }

        [StringLength(45)]
        public string Email { get; set; }

        [StringLength(45)]
        public string Fax { get; set; }

        public bool SendSMS { get; set; }

        public bool SendEMail { get; set; }

        [StringLength(45)]
        public string EmergencyContactName { get; set; }

        [StringLength(45)]
        public string EmergencyContactPhone { get; set; }

        [StringLength(45)]
        public string Relationship { get; set; }

        [Column(TypeName = "date")]
        public DateTime? MedicalExpirationDate { get; set; }

        public bool PreEmploymentDrugTest { get; set; }

        [Column(TypeName = "date")]
        public DateTime? PreEmploymentDrugTestDate { get; set; }

        public bool PreEmploymentDrugMVR { get; set; }

        [Column(TypeName = "date")]
        public DateTime? PreEmploymentDrugMVRDate { get; set; }

        public int RateType { get; set; }

        public decimal? RateAmountLoaded { get; set; }

        public decimal? RateAmountEmpty { get; set; }

        public decimal? RateAmountBobtail { get; set; }

        public decimal? TeamRateAmountLoaded { get; set; }

        public decimal? TeamRateAmountEmpty { get; set; }

        public decimal? TeamRateAmountBobtail { get; set; }

        [Column(TypeName = "date")]
        public DateTime? HireDate { get; set; }

        [Column(TypeName = "date")]
        public DateTime? TerminationDate { get; set; }

        [StringLength(45)]
        public string FuelCard { get; set; }

        public int? DispatchedBy { get; set; }

        [StringLength(45)]
        public string PayMathod { get; set; }

        [StringLength(45)]
        public string CDL { get; set; }

        public int? CDLStateId { get; set; }

        [Column(TypeName = "date")]
        public DateTime? CDLExpirationDate { get; set; }

        [StringLength(45)]
        public string CDLClass { get; set; }

        [StringLength(45)]
        public string CDLEndorsement { get; set; }

        public sbyte? YearsOfEprience { get; set; }

        [StringLength(500)]
        public string Note { get; set; }

        public bool IsActive { get; set; }

        public int ModifiedBy { get; set; }

        public DateTime ModifiedDate { get; set; }

        public int CompanyId { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<cash_flow> cash_flow { get; set; }

        public virtual user user { get; set; }

        public virtual w_state w_state { get; set; }

        public virtual w_state w_state1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tour> tours { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tour> tours1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<vehicle> vehicles { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<vehicle> vehicles1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<uploaded_file> uploaded_file { get; set; }
    }
}
