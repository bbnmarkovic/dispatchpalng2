﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace XTools.TestDataGenerator
{
    public class MD5Tools
    {
        public static byte[] GetEncryptedBytes(string text)
        {
            var md5Hasher = new MD5CryptoServiceProvider();
            var encoder = new UTF8Encoding();
            byte[] hashedBytes = md5Hasher.ComputeHash(encoder.GetBytes(text));
            return hashedBytes;
        }

        public static byte[] GetEncryptedBytes(string text, string salt)
        {
            return GetEncryptedBytes(text + salt);
        }

        public static byte[] GetEncriptedPassword(string clearPassword, Guid Guid)
        {
            byte[] res = null;
            //uzmimo delove guida:
            string guid = Guid.ToString();
            int[] saltPositions = new int[] { 2, 4, 7, 10, 11, 14, 15, 20, 22, 25, 29, 31 };
            string salt = "";
            foreach (int pos in saltPositions)
                salt += guid[pos].ToString();

            res = GetEncryptedBytes(clearPassword, salt);
            return res;
        }

        public static bool ComparePassword(byte[] pass1, byte[] pass2)
        {
            bool res = pass1.Length == pass2.Length;
            if (res)
            {
                for (int pos = 0; pos < pass1.Length; pos++)
                    res &= pass1[pos] == pass2[pos];
            }
            return res;
        }
    }
}
