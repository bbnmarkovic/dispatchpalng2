namespace XTools.TestDataGenerator.ModelsSqlServer
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("invoice")]
    public partial class invoice
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public invoice()
        {
            invoice_item = new HashSet<invoice_item>();
            invoice_payment = new HashSet<invoice_payment>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int LoadId { get; set; }

        [Required]
        [StringLength(45)]
        public string InvoiceNumber { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime InvoiceDate { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime InvoiceDueDate { get; set; }

        public int BusinessTaxID { get; set; }

        public decimal Total { get; set; }

        public decimal SubTotal { get; set; }

        public decimal AmountPaid { get; set; }

        public decimal Discount { get; set; }

        public decimal DiscountAmount { get; set; }

        public int TaxInclusive { get; set; }

        public decimal Tax { get; set; }

        public decimal TaxAmmount { get; set; }

        public decimal BalanceRemaining { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime PaymentDate { get; set; }

        public short SendReceipt { get; set; }

        public string ExternalNote { get; set; }

        public int ModifiedBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime ModifiedDate { get; set; }

        public short IsActive { get; set; }

        public int? CompanyId { get; set; }

        public virtual load load { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<invoice_item> invoice_item { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<invoice_payment> invoice_payment { get; set; }
    }
}
