namespace XTools.TestDataGenerator.ModelsSqlServer
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class login_token
    {
        [Key]
        [StringLength(36)]
        public string token { get; set; }

        public int user_id { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime created { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime accessed { get; set; }
    }
}
