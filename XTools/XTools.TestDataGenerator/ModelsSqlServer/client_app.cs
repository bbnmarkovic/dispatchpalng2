namespace XTools.TestDataGenerator.ModelsSqlServer
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class client_app
    {
        [StringLength(255)]
        public string Id { get; set; }

        [Required]
        [StringLength(255)]
        public string Secret { get; set; }

        [Required]
        [StringLength(255)]
        public string Name { get; set; }

        public int ApplicationType { get; set; }

        public short Active { get; set; }

        public int RefreshTokenLifetime { get; set; }

        [Required]
        [StringLength(45)]
        public string AllowedOrigin { get; set; }
    }
}
