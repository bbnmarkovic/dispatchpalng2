namespace XTools.TestDataGenerator.ModelsSqlServer
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class ptSqlServerModel : DbContext
    {
        public ptSqlServerModel()
            : base("name=ptSqlServerModel")
        {
        }

        public virtual DbSet<broker> brokers { get; set; }
        public virtual DbSet<cash_flow> cash_flow { get; set; }
        public virtual DbSet<cash_flow_type> cash_flow_type { get; set; }
        public virtual DbSet<client> clients { get; set; }
        public virtual DbSet<client_app> client_app { get; set; }
        public virtual DbSet<client_type> client_type { get; set; }
        public virtual DbSet<company> companies { get; set; }
        public virtual DbSet<driver> drivers { get; set; }
        public virtual DbSet<invoice> invoices { get; set; }
        public virtual DbSet<invoice_item> invoice_item { get; set; }
        public virtual DbSet<invoice_payment> invoice_payment { get; set; }
        public virtual DbSet<load> loads { get; set; }
        public virtual DbSet<load_point> load_point { get; set; }
        public virtual DbSet<login_token> login_token { get; set; }
        public virtual DbSet<point> points { get; set; }
        public virtual DbSet<refresh_token> refresh_token { get; set; }
        public virtual DbSet<role> roles { get; set; }
        public virtual DbSet<service> services { get; set; }
        public virtual DbSet<sysdiagram> sysdiagrams { get; set; }
        public virtual DbSet<tour> tours { get; set; }
        public virtual DbSet<uploaded_file> uploaded_file { get; set; }
        public virtual DbSet<user> users { get; set; }
        public virtual DbSet<vehicle> vehicles { get; set; }
        public virtual DbSet<w_state> w_state { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<broker>()
                .Property(e => e.Rating)
                .HasPrecision(10, 2);

            modelBuilder.Entity<cash_flow>()
                .Property(e => e.Date)
                .HasPrecision(0);

            modelBuilder.Entity<cash_flow>()
                .Property(e => e.Amount)
                .HasPrecision(10, 4);

            modelBuilder.Entity<cash_flow_type>()
                .HasMany(e => e.cash_flow)
                .WithRequired(e => e.cash_flow_type)
                .HasForeignKey(e => e.TypeId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<client>()
                .Property(e => e.LastPaid)
                .HasPrecision(0);

            modelBuilder.Entity<client>()
                .Property(e => e.ModifiedDate)
                .HasPrecision(0);

            modelBuilder.Entity<client>()
                .HasMany(e => e.brokers)
                .WithRequired(e => e.client)
                .HasForeignKey(e => e.PartnerId);

            modelBuilder.Entity<client_type>()
                .HasMany(e => e.clients)
                .WithRequired(e => e.client_type)
                .HasForeignKey(e => e.ClientTypeId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<company>()
                .Property(e => e.ModifiedDate)
                .HasPrecision(0);

            modelBuilder.Entity<company>()
                .HasMany(e => e.users)
                .WithOptional(e => e.company)
                .HasForeignKey(e => e.CompanyId);

            modelBuilder.Entity<company>()
                .HasMany(e => e.users1)
                .WithMany(e => e.companies)
                .Map(m => m.ToTable("rel_user_company").MapLeftKey("CompanyId").MapRightKey("UserId"));

            modelBuilder.Entity<driver>()
                .Property(e => e.RateAmountLoaded)
                .HasPrecision(19, 4);

            modelBuilder.Entity<driver>()
                .Property(e => e.RateAmountEmpty)
                .HasPrecision(19, 4);

            modelBuilder.Entity<driver>()
                .Property(e => e.RateAmountBobtail)
                .HasPrecision(19, 4);

            modelBuilder.Entity<driver>()
                .Property(e => e.TeamRateAmountLoaded)
                .HasPrecision(19, 4);

            modelBuilder.Entity<driver>()
                .Property(e => e.TeamRateAmountEmpty)
                .HasPrecision(19, 4);

            modelBuilder.Entity<driver>()
                .Property(e => e.TeamRateAmountBobtail)
                .HasPrecision(19, 4);

            modelBuilder.Entity<driver>()
                .Property(e => e.ModifiedDate)
                .HasPrecision(0);

            modelBuilder.Entity<driver>()
                .HasMany(e => e.cash_flow)
                .WithRequired(e => e.driver)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<driver>()
                .HasMany(e => e.vehicles)
                .WithOptional(e => e.driver)
                .HasForeignKey(e => e.DriverAId);

            modelBuilder.Entity<driver>()
                .HasMany(e => e.vehicles1)
                .WithOptional(e => e.driver1)
                .HasForeignKey(e => e.DriverBId);

            modelBuilder.Entity<driver>()
                .HasMany(e => e.uploaded_file)
                .WithMany(e => e.drivers)
                .Map(m => m.ToTable("driver_uploaded_file_r").MapLeftKey("DriverId").MapRightKey("UploadedFileId"));

            modelBuilder.Entity<invoice>()
                .Property(e => e.InvoiceDate)
                .HasPrecision(0);

            modelBuilder.Entity<invoice>()
                .Property(e => e.InvoiceDueDate)
                .HasPrecision(0);

            modelBuilder.Entity<invoice>()
                .Property(e => e.Total)
                .HasPrecision(19, 4);

            modelBuilder.Entity<invoice>()
                .Property(e => e.SubTotal)
                .HasPrecision(19, 4);

            modelBuilder.Entity<invoice>()
                .Property(e => e.AmountPaid)
                .HasPrecision(19, 4);

            modelBuilder.Entity<invoice>()
                .Property(e => e.Discount)
                .HasPrecision(10, 2);

            modelBuilder.Entity<invoice>()
                .Property(e => e.DiscountAmount)
                .HasPrecision(19, 4);

            modelBuilder.Entity<invoice>()
                .Property(e => e.Tax)
                .HasPrecision(10, 2);

            modelBuilder.Entity<invoice>()
                .Property(e => e.TaxAmmount)
                .HasPrecision(19, 4);

            modelBuilder.Entity<invoice>()
                .Property(e => e.BalanceRemaining)
                .HasPrecision(10, 4);

            modelBuilder.Entity<invoice>()
                .Property(e => e.PaymentDate)
                .HasPrecision(0);

            modelBuilder.Entity<invoice>()
                .Property(e => e.ModifiedDate)
                .HasPrecision(0);

            modelBuilder.Entity<invoice>()
                .HasMany(e => e.invoice_item)
                .WithRequired(e => e.invoice)
                .HasForeignKey(e => e.InvoiceId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<invoice>()
                .HasMany(e => e.invoice_payment)
                .WithOptional(e => e.invoice)
                .HasForeignKey(e => e.InvoiceId);

            modelBuilder.Entity<invoice_item>()
                .Property(e => e.Price)
                .HasPrecision(19, 4);

            modelBuilder.Entity<invoice_item>()
                .Property(e => e.Quantity)
                .HasPrecision(19, 4);

            modelBuilder.Entity<invoice_item>()
                .Property(e => e.Total)
                .HasPrecision(19, 4);

            modelBuilder.Entity<invoice_item>()
                .Property(e => e.Tax)
                .HasPrecision(10, 2);

            modelBuilder.Entity<invoice_item>()
                .Property(e => e.TaxAmmount)
                .HasPrecision(19, 4);

            modelBuilder.Entity<invoice_item>()
                .Property(e => e.Discount)
                .HasPrecision(10, 2);

            modelBuilder.Entity<invoice_item>()
                .Property(e => e.DiscountAmmount)
                .HasPrecision(19, 4);

            modelBuilder.Entity<invoice_payment>()
                .Property(e => e.Amount)
                .HasPrecision(19, 4);

            modelBuilder.Entity<invoice_payment>()
                .Property(e => e.PaymentDate)
                .HasPrecision(0);

            modelBuilder.Entity<load>()
                .Property(e => e.AgreedPrice)
                .HasPrecision(19, 4);

            modelBuilder.Entity<load>()
                .Property(e => e.FuelSurcharge)
                .HasPrecision(19, 4);

            modelBuilder.Entity<load>()
                .Property(e => e.BillableMiles)
                .HasPrecision(10, 2);

            modelBuilder.Entity<load>()
                .Property(e => e.EmptyMiles)
                .HasPrecision(10, 2);

            modelBuilder.Entity<load>()
                .Property(e => e.BobtailMiles)
                .HasPrecision(10, 2);

            modelBuilder.Entity<load>()
                .Property(e => e.Weight)
                .HasPrecision(10, 3);

            modelBuilder.Entity<load>()
                .Property(e => e.Quantity)
                .HasPrecision(10, 3);

            modelBuilder.Entity<load>()
                .Property(e => e.Size)
                .HasPrecision(10, 3);

            modelBuilder.Entity<load>()
                .Property(e => e.Temperature)
                .HasPrecision(10, 3);

            modelBuilder.Entity<load>()
                .Property(e => e.FlatRate)
                .HasPrecision(10, 3);

            modelBuilder.Entity<load>()
                .Property(e => e.TotalDistanceMi)
                .HasPrecision(10, 3);

            modelBuilder.Entity<load>()
                .Property(e => e.TotalDistanceKm)
                .HasPrecision(10, 3);

            modelBuilder.Entity<load>()
                .Property(e => e.DateFrom)
                .HasPrecision(0);

            modelBuilder.Entity<load>()
                .Property(e => e.DateTo)
                .HasPrecision(0);

            modelBuilder.Entity<load>()
                .Property(e => e.BookingDate)
                .HasPrecision(0);

            modelBuilder.Entity<load>()
                .Property(e => e.ModifiedDate)
                .HasPrecision(0);

            modelBuilder.Entity<load>()
                .HasOptional(e => e.invoice)
                .WithRequired(e => e.load)
                .WillCascadeOnDelete();

            modelBuilder.Entity<load>()
                .HasMany(e => e.load_point)
                .WithOptional(e => e.load)
                .WillCascadeOnDelete();

            modelBuilder.Entity<load_point>()
                .Property(e => e.DateLoad)
                .HasPrecision(0);

            modelBuilder.Entity<load_point>()
                .Property(e => e.Weight)
                .HasPrecision(19, 4);

            modelBuilder.Entity<load_point>()
                .Property(e => e.Lat)
                .HasPrecision(15, 10);

            modelBuilder.Entity<load_point>()
                .Property(e => e.Lng)
                .HasPrecision(15, 10);

            modelBuilder.Entity<load_point>()
                .Property(e => e.Distance)
                .HasPrecision(19, 4);

            modelBuilder.Entity<load_point>()
                .Property(e => e.DistanceFromStart)
                .HasPrecision(19, 4);

            modelBuilder.Entity<login_token>()
                .Property(e => e.token)
                .IsFixedLength();

            modelBuilder.Entity<login_token>()
                .Property(e => e.created)
                .HasPrecision(0);

            modelBuilder.Entity<login_token>()
                .Property(e => e.accessed)
                .HasPrecision(0);

            modelBuilder.Entity<point>()
                .Property(e => e.Lat)
                .HasPrecision(10, 6);

            modelBuilder.Entity<point>()
                .Property(e => e.Lng)
                .HasPrecision(10, 6);

            modelBuilder.Entity<refresh_token>()
                .Property(e => e.IssuedUTC)
                .HasPrecision(0);

            modelBuilder.Entity<refresh_token>()
                .Property(e => e.ExpiresUTC)
                .HasPrecision(0);

            modelBuilder.Entity<role>()
                .HasMany(e => e.users)
                .WithMany(e => e.roles)
                .Map(m => m.ToTable("user_role").MapLeftKey("RoleId").MapRightKey("UserId"));

            modelBuilder.Entity<service>()
                .Property(e => e.Mileage)
                .HasPrecision(10, 3);

            modelBuilder.Entity<service>()
                .Property(e => e.Date)
                .HasPrecision(0);

            modelBuilder.Entity<service>()
                .Property(e => e.Cost)
                .HasPrecision(10, 4);

            modelBuilder.Entity<service>()
                .Property(e => e.WarrantyExpDate)
                .HasPrecision(0);

            modelBuilder.Entity<service>()
                .Property(e => e.ScheduledServiceDate)
                .HasPrecision(0);

            modelBuilder.Entity<tour>()
                .Property(e => e.DateFrom)
                .HasPrecision(0);

            modelBuilder.Entity<tour>()
                .Property(e => e.DateTo)
                .HasPrecision(0);

            modelBuilder.Entity<tour>()
                .Property(e => e.PayRateFullDriverA)
                .HasPrecision(10, 4);

            modelBuilder.Entity<tour>()
                .Property(e => e.PayRateEmptyDriverA)
                .HasPrecision(10, 4);

            modelBuilder.Entity<tour>()
                .Property(e => e.PayRateBobtailDriverA)
                .HasPrecision(10, 4);

            modelBuilder.Entity<tour>()
                .Property(e => e.PayRateFullDriverB)
                .HasPrecision(10, 4);

            modelBuilder.Entity<tour>()
                .Property(e => e.PayRateEmptyDriverB)
                .HasPrecision(10, 4);

            modelBuilder.Entity<tour>()
                .Property(e => e.PayRateBobtailDriverB)
                .HasPrecision(10, 4);

            modelBuilder.Entity<tour>()
                .Property(e => e.ModifiedDate)
                .HasPrecision(0);

            modelBuilder.Entity<uploaded_file>()
                .Property(e => e.UploadDate)
                .HasPrecision(0);

            modelBuilder.Entity<uploaded_file>()
                .HasMany(e => e.loads)
                .WithMany(e => e.uploaded_file)
                .Map(m => m.ToTable("load_uploaded_file_r").MapLeftKey("UploadedFileId").MapRightKey("LoadId"));

            modelBuilder.Entity<user>()
                .Property(e => e.LockoutEndDateUtc)
                .HasPrecision(0);

            modelBuilder.Entity<user>()
                .Property(e => e.DateCreatedUtc)
                .HasPrecision(0);

            modelBuilder.Entity<user>()
                .Property(e => e.DateActivatedUtc)
                .HasPrecision(0);

            modelBuilder.Entity<user>()
                .Property(e => e.ResetPasswordExpiryUtc)
                .HasPrecision(0);

            modelBuilder.Entity<user>()
                .Property(e => e.ModifiedDate)
                .HasPrecision(0);

            modelBuilder.Entity<user>()
                .HasMany(e => e.drivers)
                .WithOptional(e => e.user)
                .HasForeignKey(e => e.DispatchedBy);

            modelBuilder.Entity<user>()
                .HasMany(e => e.vehicles)
                .WithOptional(e => e.user)
                .HasForeignKey(e => e.DispatchedBy);

            modelBuilder.Entity<vehicle>()
                .Property(e => e.InsuranceDate)
                .HasPrecision(0);

            modelBuilder.Entity<vehicle>()
                .Property(e => e.InsurancePremium)
                .HasPrecision(19, 4);

            modelBuilder.Entity<vehicle>()
                .Property(e => e.ModifiedDate)
                .HasPrecision(0);

            modelBuilder.Entity<vehicle>()
                .HasMany(e => e.cash_flow)
                .WithOptional(e => e.vehicle)
                .HasForeignKey(e => e.TrailerId);

            modelBuilder.Entity<vehicle>()
                .HasMany(e => e.cash_flow1)
                .WithRequired(e => e.vehicle1)
                .HasForeignKey(e => e.TruckId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<vehicle>()
                .HasMany(e => e.services)
                .WithOptional(e => e.vehicle)
                .HasForeignKey(e => e.TruckId);

            modelBuilder.Entity<vehicle>()
                .HasMany(e => e.vehicle1)
                .WithOptional(e => e.vehicle2)
                .HasForeignKey(e => e.TrailerId);

            modelBuilder.Entity<w_state>()
                .HasMany(e => e.clients)
                .WithOptional(e => e.w_state)
                .HasForeignKey(e => e.StateId);

            modelBuilder.Entity<w_state>()
                .HasMany(e => e.companies)
                .WithOptional(e => e.w_state)
                .HasForeignKey(e => e.StateId);

            modelBuilder.Entity<w_state>()
                .HasMany(e => e.drivers)
                .WithOptional(e => e.w_state)
                .HasForeignKey(e => e.CDLStateId);

            modelBuilder.Entity<w_state>()
                .HasMany(e => e.drivers1)
                .WithOptional(e => e.w_state1)
                .HasForeignKey(e => e.StateId);

            modelBuilder.Entity<w_state>()
                .HasMany(e => e.vehicles)
                .WithOptional(e => e.w_state)
                .HasForeignKey(e => e.StateId);
        }
    }
}
