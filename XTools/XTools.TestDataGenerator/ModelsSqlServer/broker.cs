namespace XTools.TestDataGenerator.ModelsSqlServer
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("broker")]
    public partial class broker
    {
        public int Id { get; set; }

        public int PartnerId { get; set; }

        [Required]
        [StringLength(30)]
        public string DisplayName { get; set; }

        [StringLength(100)]
        public string FirstName { get; set; }

        [StringLength(100)]
        public string LastName { get; set; }

        [StringLength(15)]
        public string PhoneNumber { get; set; }

        [StringLength(15)]
        public string MobileNumber { get; set; }

        [StringLength(245)]
        public string Email { get; set; }

        [StringLength(245)]
        public string Fax { get; set; }

        [StringLength(250)]
        public string Fax1 { get; set; }

        public int Rate { get; set; }

        public decimal Rating { get; set; }

        public short IsActive { get; set; }

        public int CompanyId { get; set; }

        public virtual client client { get; set; }
    }
}
