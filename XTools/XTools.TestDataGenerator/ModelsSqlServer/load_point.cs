namespace XTools.TestDataGenerator.ModelsSqlServer
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class load_point
    {
        public int Id { get; set; }

        public int? TourId { get; set; }

        public int? LoadId { get; set; }

        public int OrderNumber { get; set; }

        public short ActionType { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime DateLoad { get; set; }

        [StringLength(255)]
        public string Location { get; set; }

        [StringLength(450)]
        public string Country { get; set; }

        [StringLength(450)]
        public string State { get; set; }

        [StringLength(450)]
        public string City { get; set; }

        [StringLength(450)]
        public string Address { get; set; }

        [StringLength(45)]
        public string ZipCode { get; set; }

        [StringLength(255)]
        public string Description { get; set; }

        [StringLength(45)]
        public string Qty { get; set; }

        public decimal? Weight { get; set; }

        [StringLength(255)]
        public string DeliveryNotes { get; set; }

        [StringLength(100)]
        public string NumbersPO { get; set; }

        public decimal? Lat { get; set; }

        public decimal? Lng { get; set; }

        public decimal Distance { get; set; }

        public decimal DistanceFromStart { get; set; }

        [StringLength(120)]
        public string EstimatedTime { get; set; }

        public short EmptyPosition { get; set; }

        public int? DriverId { get; set; }

        public int? CompanyId { get; set; }

        [StringLength(450)]
        public string Contact { get; set; }

        [StringLength(450)]
        public string Phone { get; set; }

        public virtual load load { get; set; }
    }
}
