namespace XTools.TestDataGenerator.ModelsSqlServer
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class cash_flow
    {
        public int Id { get; set; }

        public int DriverId { get; set; }

        public int TruckId { get; set; }

        public int? TrailerId { get; set; }

        public int TypeId { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime Date { get; set; }

        public decimal Amount { get; set; }

        public string Note { get; set; }

        public short IsSettled { get; set; }

        public short IsActive { get; set; }

        public int CompanyId { get; set; }

        public virtual driver driver { get; set; }

        public virtual vehicle vehicle { get; set; }

        public virtual vehicle vehicle1 { get; set; }

        public virtual cash_flow_type cash_flow_type { get; set; }
    }
}
