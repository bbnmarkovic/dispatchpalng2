namespace XTools.TestDataGenerator.ModelsSqlServer
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("vehicle")]
    public partial class vehicle
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public vehicle()
        {
            cash_flow = new HashSet<cash_flow>();
            cash_flow1 = new HashSet<cash_flow>();
            services = new HashSet<service>();
            vehicle1 = new HashSet<vehicle>();
        }

        public int Id { get; set; }

        public int VehicleType { get; set; }

        [StringLength(45)]
        public string VehicleID { get; set; }

        [StringLength(255)]
        public string DisplayName { get; set; }

        [StringLength(45)]
        public string Number { get; set; }

        [StringLength(45)]
        public string Make { get; set; }

        [StringLength(45)]
        public string Model { get; set; }

        public int? Year { get; set; }

        [StringLength(45)]
        public string Size { get; set; }

        [StringLength(45)]
        public string Color { get; set; }

        [StringLength(45)]
        public string InsuranceWith { get; set; }

        [StringLength(10)]
        public string PlateNumber { get; set; }

        public int? StateId { get; set; }

        [Column(TypeName = "date")]
        public DateTime? PlateExpiration { get; set; }

        [StringLength(45)]
        public string VIN { get; set; }

        [Column(TypeName = "date")]
        public DateTime? DOTExpiration { get; set; }

        public int? DispatchedBy { get; set; }

        public short? Ownership { get; set; }

        [Column(TypeName = "date")]
        public DateTime? LeasedFrom { get; set; }

        [Column(TypeName = "date")]
        public DateTime? LeasedStart { get; set; }

        [Column(TypeName = "date")]
        public DateTime? LeasedEnd { get; set; }

        [StringLength(45)]
        public string TrailerNumber { get; set; }

        [Column(TypeName = "date")]
        public DateTime? PurchaseDate { get; set; }

        [StringLength(45)]
        public string PurchasedFrom { get; set; }

        [Column(TypeName = "date")]
        public DateTime? SellDate { get; set; }

        [StringLength(45)]
        public string SoldTo { get; set; }

        public short? Insured { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? InsuranceDate { get; set; }

        public decimal? InsurancePremium { get; set; }

        public string Note { get; set; }

        [Column(TypeName = "date")]
        public DateTime? WarrantyDate { get; set; }

        [StringLength(45)]
        public string Type { get; set; }

        [StringLength(255)]
        public string TireType { get; set; }

        public int? DriverAId { get; set; }

        public int? DriverBId { get; set; }

        public int? TrailerId { get; set; }

        public int ModifiedBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime ModifiedDate { get; set; }

        public short IsActive { get; set; }

        public int CompanyId { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<cash_flow> cash_flow { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<cash_flow> cash_flow1 { get; set; }

        public virtual driver driver { get; set; }

        public virtual driver driver1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<service> services { get; set; }

        public virtual user user { get; set; }

        public virtual w_state w_state { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<vehicle> vehicle1 { get; set; }

        public virtual vehicle vehicle2 { get; set; }
    }
}
