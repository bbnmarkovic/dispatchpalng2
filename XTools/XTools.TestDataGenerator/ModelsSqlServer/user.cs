namespace XTools.TestDataGenerator.ModelsSqlServer
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("user")]
    public partial class user
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public user()
        {
            drivers = new HashSet<driver>();
            vehicles = new HashSet<vehicle>();
            companies = new HashSet<company>();
            roles = new HashSet<role>();
        }

        public int Id { get; set; }

        [StringLength(36)]
        public string UserId { get; set; }

        [Required]
        [StringLength(30)]
        public string UserName { get; set; }

        [StringLength(100)]
        public string FirstName { get; set; }

        [StringLength(100)]
        public string LastName { get; set; }

        [Required]
        [StringLength(100)]
        public string Email { get; set; }

        public short EmailConfirmed { get; set; }

        [StringLength(15)]
        public string PhoneNumber { get; set; }

        [StringLength(15)]
        public string MobileNumber { get; set; }

        public short PhoneNumberConfirmed { get; set; }

        [StringLength(245)]
        public string Address { get; set; }

        [StringLength(250)]
        public string Description { get; set; }

        [Required]
        public byte[] Password { get; set; }

        [Required]
        [StringLength(36)]
        public string Password_uuid { get; set; }

        [StringLength(45)]
        public string LoginGuid { get; set; }

        [StringLength(1000)]
        public string PasswordHash { get; set; }

        [StringLength(1000)]
        public string DPPasswordHash { get; set; }

        [StringLength(1000)]
        public string SecurityStamp { get; set; }

        [StringLength(100)]
        public string PasswordSalt { get; set; }

        public int? AccessFailedCount { get; set; }

        public short LockoutEnabled { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? LockoutEndDateUtc { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime DateCreatedUtc { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? DateActivatedUtc { get; set; }

        [StringLength(45)]
        public string DateLastLoginUTC { get; set; }

        public short TwoFactorEnabled { get; set; }

        [StringLength(45)]
        public string InviteCode { get; set; }

        [StringLength(45)]
        public string ResetPasswordKey { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? ResetPasswordExpiryUtc { get; set; }

        [StringLength(255)]
        public string ProfileImg { get; set; }

        public int Status { get; set; }

        public int ModifiedBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime ModifiedDate { get; set; }

        public short IsActive { get; set; }

        public int? CompanyId { get; set; }

        public virtual company company { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<driver> drivers { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<vehicle> vehicles { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<company> companies { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<role> roles { get; set; }
    }
}
