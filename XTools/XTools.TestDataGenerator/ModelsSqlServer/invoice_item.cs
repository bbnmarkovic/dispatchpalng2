namespace XTools.TestDataGenerator.ModelsSqlServer
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class invoice_item
    {
        public int Id { get; set; }

        public int InvoiceId { get; set; }

        public int? ServiceId { get; set; }

        [StringLength(450)]
        public string ServiceName { get; set; }

        public decimal? Price { get; set; }

        public decimal? Quantity { get; set; }

        public decimal? Total { get; set; }

        public decimal? Tax { get; set; }

        public decimal? TaxAmmount { get; set; }

        public decimal? Discount { get; set; }

        public decimal? DiscountAmmount { get; set; }

        public string Note { get; set; }

        public virtual invoice invoice { get; set; }
    }
}
