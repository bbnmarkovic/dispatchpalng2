namespace XTools.TestDataGenerator.ModelsSqlServer
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("service")]
    public partial class service
    {
        public int Id { get; set; }

        public int? TruckId { get; set; }

        public decimal? Mileage { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? Date { get; set; }

        [StringLength(450)]
        public string Location { get; set; }

        [StringLength(450)]
        public string Part { get; set; }

        [StringLength(450)]
        public string Job { get; set; }

        public decimal? Cost { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? WarrantyExpDate { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? ScheduledServiceDate { get; set; }

        public string Description { get; set; }

        public int CompanyId { get; set; }

        public virtual vehicle vehicle { get; set; }
    }
}
