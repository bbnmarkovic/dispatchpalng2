namespace XTools.TestDataGenerator.ModelsSqlServer
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("tour")]
    public partial class tour
    {
        public int Id { get; set; }

        [StringLength(45)]
        public string TripNumber { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? DateFrom { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? DateTo { get; set; }

        public int? DriverAId { get; set; }

        public int? DriverBId { get; set; }

        public int? TruckId { get; set; }

        public int? TrailerId { get; set; }

        public int PayTypeDriverA { get; set; }

        public decimal PayRateFullDriverA { get; set; }

        public decimal PayRateEmptyDriverA { get; set; }

        public decimal PayRateBobtailDriverA { get; set; }

        public int PayTypeDriverB { get; set; }

        public decimal PayRateFullDriverB { get; set; }

        public decimal PayRateEmptyDriverB { get; set; }

        public decimal PayRateBobtailDriverB { get; set; }

        public int ModifiedBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime ModifiedDate { get; set; }

        public short IsActive { get; set; }

        public short IsFinished { get; set; }

        public int CompanyId { get; set; }
    }
}
