namespace XTools.TestDataGenerator.ModelsSqlServer
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("company")]
    public partial class company
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public company()
        {
            users = new HashSet<user>();
            users1 = new HashSet<user>();
        }

        public int Id { get; set; }

        public int UserId { get; set; }

        [StringLength(255)]
        public string Name { get; set; }

        [StringLength(255)]
        public string Address { get; set; }

        [StringLength(255)]
        public string Address1 { get; set; }

        [StringLength(255)]
        public string City { get; set; }

        public int? StateId { get; set; }

        [StringLength(10)]
        public string ZipCode { get; set; }

        [StringLength(45)]
        public string Country { get; set; }

        [StringLength(45)]
        public string PhoneNumber { get; set; }

        [StringLength(45)]
        public string AlternatePhoneNumber { get; set; }

        [StringLength(45)]
        public string Fax { get; set; }

        [StringLength(255)]
        public string Email { get; set; }

        [StringLength(255)]
        public string Contact { get; set; }

        public string Notes { get; set; }

        [StringLength(45)]
        public string MotorCarrierNumber { get; set; }

        public byte[] Picture { get; set; }

        public int NextInvoiceNumber { get; set; }

        [StringLength(45)]
        public string BusinessNumber { get; set; }

        [StringLength(255)]
        public string EIN { get; set; }

        [StringLength(45)]
        public string Domain { get; set; }

        [StringLength(255)]
        public string Logo { get; set; }

        public int ModifiedBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime ModifiedDate { get; set; }

        public short IsActive { get; set; }

        public virtual w_state w_state { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<user> users { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<user> users1 { get; set; }
    }
}
