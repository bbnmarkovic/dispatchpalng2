namespace XTools.TestDataGenerator.ModelsSqlServer
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class uploaded_file
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public uploaded_file()
        {
            drivers = new HashSet<driver>();
            loads = new HashSet<load>();
        }

        public int Id { get; set; }

        public int DocumentType { get; set; }

        [StringLength(500)]
        public string UniqueFileName { get; set; }

        [StringLength(500)]
        public string OriginalFileName { get; set; }

        [StringLength(500)]
        public string FilePath { get; set; }

        [StringLength(45)]
        public string FileSize { get; set; }

        [StringLength(10)]
        public string Extension { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime UploadDate { get; set; }

        public int ClientId { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<driver> drivers { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<load> loads { get; set; }
    }
}
