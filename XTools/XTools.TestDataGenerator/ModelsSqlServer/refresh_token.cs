namespace XTools.TestDataGenerator.ModelsSqlServer
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class refresh_token
    {
        [StringLength(45)]
        public string Id { get; set; }

        [Required]
        [StringLength(45)]
        public string Subject { get; set; }

        [Required]
        [StringLength(45)]
        public string ClientAppId { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime IssuedUTC { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime ExpiresUTC { get; set; }

        [Required]
        [StringLength(1000)]
        public string ProtectedTicket { get; set; }
    }
}
