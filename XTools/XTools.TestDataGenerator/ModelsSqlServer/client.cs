namespace XTools.TestDataGenerator.ModelsSqlServer
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("client")]
    public partial class client
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public client()
        {
            brokers = new HashSet<broker>();
        }

        public int Id { get; set; }

        public int ClientTypeId { get; set; }

        [StringLength(255)]
        public string AccountNumber { get; set; }

        [StringLength(255)]
        public string DisplayName { get; set; }

        [StringLength(45)]
        public string Name { get; set; }

        [StringLength(255)]
        public string Address { get; set; }

        [StringLength(255)]
        public string Address2 { get; set; }

        [StringLength(255)]
        public string City { get; set; }

        public int? StateId { get; set; }

        [StringLength(45)]
        public string ZipCode { get; set; }

        [StringLength(45)]
        public string PhoneNumber { get; set; }

        [StringLength(45)]
        public string AlternatePhoneNumber { get; set; }

        [StringLength(45)]
        public string Fax { get; set; }

        [StringLength(255)]
        public string Email { get; set; }

        [StringLength(255)]
        public string Fee { get; set; }

        [StringLength(45)]
        public string MotorCarrierNumber { get; set; }

        [StringLength(255)]
        public string EIN { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime LastPaid { get; set; }

        [StringLength(45)]
        public string ContactName { get; set; }

        public int ModifiedBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime ModifiedDate { get; set; }

        public short IsActive { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<broker> brokers { get; set; }

        public virtual w_state w_state { get; set; }

        public virtual client_type client_type { get; set; }
    }
}
