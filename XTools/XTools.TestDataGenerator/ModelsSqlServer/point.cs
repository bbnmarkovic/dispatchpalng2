namespace XTools.TestDataGenerator.ModelsSqlServer
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("point")]
    public partial class point
    {
        public int Id { get; set; }

        public decimal? Lat { get; set; }

        public decimal? Lng { get; set; }

        public DbGeometry LatLng { get; set; }

        [StringLength(255)]
        public string Loaction { get; set; }
    }
}
