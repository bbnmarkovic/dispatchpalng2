namespace XTools.TestDataGenerator.ModelsSqlServer
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("load")]
    public partial class load
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public load()
        {
            load_point = new HashSet<load_point>();
            uploaded_file = new HashSet<uploaded_file>();
        }

        public int Id { get; set; }

        public int? TourId { get; set; }

        public int? DispatcherId { get; set; }

        [StringLength(45)]
        public string LoadNumber { get; set; }

        public decimal AgreedPrice { get; set; }

        public decimal FuelSurcharge { get; set; }

        public decimal BillableMiles { get; set; }

        public decimal EmptyMiles { get; set; }

        public decimal BobtailMiles { get; set; }

        public int ExtraStops { get; set; }

        [StringLength(45)]
        public string PO { get; set; }

        [StringLength(45)]
        public string Confirmation { get; set; }

        [StringLength(45)]
        public string BOL { get; set; }

        [StringLength(45)]
        public string Commodity { get; set; }

        public decimal? Weight { get; set; }

        public decimal? Quantity { get; set; }

        public decimal? Size { get; set; }

        public decimal? Temperature { get; set; }

        public int? LoadType { get; set; }

        public int? PartnerShipperId { get; set; }

        public int? PartnerConsigneeId { get; set; }

        public int? BrokerageId { get; set; }

        public int? BrokerId { get; set; }

        public int? BrokerRating { get; set; }

        public int? Broker1Id { get; set; }

        public decimal? FlatRate { get; set; }

        public decimal TotalDistanceMi { get; set; }

        public decimal TotalDistanceKm { get; set; }

        [StringLength(45)]
        public string Chassis { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? DateFrom { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? DateTo { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? BookingDate { get; set; }

        public short DispatchPaid { get; set; }

        public string DriverNotes { get; set; }

        public string InternalNotes { get; set; }

        public string ExternalNotes { get; set; }

        [StringLength(45)]
        public string ShippingNumber { get; set; }

        public string ShippingNote { get; set; }

        [StringLength(45)]
        public string DeliveryNumber { get; set; }

        public string DeliveryNote { get; set; }

        public int Status { get; set; }

        public short PaidInFull { get; set; }

        public short PendingIssue { get; set; }

        public short IsActive { get; set; }

        public int ModifiedBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime ModifiedDate { get; set; }

        public int CompanyId { get; set; }

        public virtual invoice invoice { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<load_point> load_point { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<uploaded_file> uploaded_file { get; set; }
    }
}
