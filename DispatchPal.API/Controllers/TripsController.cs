﻿using DispatchPal.API.ActionFilters;
using DispatchPal.DAL.Adapters.Interface;
using DispatchPal.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DispatchPal.API.Controllers
{
    public class TripsController : ApiController
    {
        private readonly ITripsAdapter tripsAdapter;
        private readonly ILoadsAdapter loadsAdapter;
        private readonly ILoadPointsAdapter loadPointsAdapter;

        public TripsController(ITripsAdapter _tripsAdapter, ILoadsAdapter _loadsAdapter, ILoadPointsAdapter _loadPointsAdapter)
        {
            tripsAdapter = _tripsAdapter;
            loadPointsAdapter = _loadPointsAdapter;
            loadsAdapter = _loadsAdapter;
        }

        [HttpGet]
        [AuthorizationRequired]
        [Route("api/trips")]
        public IHttpActionResult Get([FromUri]int offset = 0, [FromUri]int pageSize = 10, [FromUri]string searchParam = "", [FromUri]int active = -1, [FromUri]bool includeFinished = true, [FromUri]int companyId = 0)
        {
            try
            {
                var result = tripsAdapter.Get(offset, pageSize, searchParam, active, includeFinished, companyId);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpGet]
        [AuthorizationRequired]
        [Route("api/trips/planner")]
        public IHttpActionResult GetForPlanner([FromUri]int driverId, [FromUri]DateTime date)
        {
            try
            {
                var result = tripsAdapter.GetForPlanner(driverId, date);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpGet]
        [AuthorizationRequired]
        [Route("api/trips/{id}")]
        public IHttpActionResult Get(int id)
        {
            try
            {
                var result = tripsAdapter.Get(id);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpGet]
        [AuthorizationRequired]
        [Route("api/trips/next_trip_number")]
        public IHttpActionResult GetNextTripNumber()
        {
            try
            {
                var result = tripsAdapter.GetNextTripNumber();
                return Ok(result);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpPost]
        [AuthorizationRequired]
        [Route("api/trips/import-loads")]
        public IHttpActionResult ImportLoads([FromBody]ImportLoadsModel model)
        {
            try
            {
                var result = tripsAdapter.ImportLoads(model.LoadIds, model.TripId);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpGet]
        [AuthorizationRequired]
        [Route("api/trips/{id}/loads")]
        public IHttpActionResult GetLoads(int id)
        {
            try
            {
                var result = loadsAdapter.GetLoadsByTripId(id);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpGet]
        [AuthorizationRequired]
        [Route("api/trips/{id}/load_points")]
        public IHttpActionResult GetLoadPoints(int id)
        {
            try
            {
                var result = loadPointsAdapter.GetLoadPointsByTripId(id);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpGet]
        [AuthorizationRequired]
        [Route("api/trips/{id}/starting-point")]
        public IHttpActionResult GetStartingLoadPointDate(int id)
        {
            try
            {
                var result = loadPointsAdapter.GetStartingLoadPointDate(id);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpPost]
        [AuthorizationRequired]
        [Route("api/trips")]
        public IHttpActionResult Post([FromBody]TripModel model)
        {
            try
            {
                var result = tripsAdapter.Save(model);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpGet]
        [AuthorizationRequired]
        [Route("api/trips/{id}/finalize")]
        public IHttpActionResult FinalizeTrip([FromUri]int id)
        {
            try
            {
                var result = tripsAdapter.FinalizeTrip(id);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpGet]
        [AuthorizationRequired]
        [Route("api/trips/{id}/reopen")]
        public IHttpActionResult ReopenTrip([FromUri]int id)
        {
            try
            {
                var result = tripsAdapter.ReopenTrip(id);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        // PUT: api/Drivers/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Drivers/5
        public void Delete(int id)
        {
        }
    }
}
