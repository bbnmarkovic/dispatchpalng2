﻿using DispatchPal.API.ActionFilters;
using DispatchPal.API.Utils;
using DispatchPal.DAL.Adapters.Interface;
using DispatchPal.DAL.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace DispatchPal.API.Controllers
{
    public class LoadsController : ApiController
    {
        private readonly ILoadsAdapter loadsAdapter;
        private readonly ILoadPointsAdapter loadPointsAdapter;
        private readonly IFilesAdapter filesAdapter;

        public LoadsController(ILoadsAdapter _loadsAdapter,
            ILoadPointsAdapter _loadPointsAdapter,
            IFilesAdapter _filesAdapter)
        {
            loadsAdapter = _loadsAdapter;
            loadPointsAdapter = _loadPointsAdapter;
            filesAdapter = _filesAdapter;
        }

        [HttpGet]
        [AuthorizationRequired]
        [Route("api/loads")]
        public IHttpActionResult Get([FromUri]int offset = 0, [FromUri]int pageSize = 10, [FromUri]string searchParam = "", bool active = false, [FromUri]int companyId = 0)
        {
            try
            {
                var result = loadsAdapter.GetPreview(offset, pageSize, searchParam, active, companyId);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpGet]
        [AuthorizationRequired]
        [Route("api/loads/autocomplete")]
        public IHttpActionResult GetForAutocomplete([FromUri]int offset = 0, [FromUri]int pageSize = 10, [FromUri]string searchParam = "")
        {
            try
            {
                var result = loadsAdapter.GetForAutocomplete(offset, pageSize, searchParam);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpGet]
        [AuthorizationRequired]
        [Route("api/loads/autocomplete/{id}")]
        public IHttpActionResult GetForAutocompleteById(int id)
        {
            try
            {
                var result = loadsAdapter.GetForAutocomplete(id);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpGet]
        [AuthorizationRequired]
        [Route("api/loads/unassigned")]
        public IHttpActionResult GetUnassignedLoads([FromUri]DateTime? startDate = null, [FromUri]int offset = 0, [FromUri]int pageSize = 10, [FromUri]string searchParam = "", [FromUri]int companyId = 0)
        {
            try
            {
                var result = loadsAdapter.GetUnassignedLoads(startDate, offset, pageSize, searchParam, companyId);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpGet]
        [AuthorizationRequired]
        [Route("api/loads/driver-payroll/{driverId}")]
        public IHttpActionResult GetDriverPayroll([FromUri]int offset = 0, [FromUri]int pageSize = 10, [FromUri]int companyId = 0, int driverId = 0)
        {
            try
            {
                var result = loadsAdapter.GetDriverPayroll(offset, pageSize, companyId, driverId);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpGet]
        [AuthorizationRequired]
        [Route("api/loads/{id}")]
        public IHttpActionResult Get(int id)
        {
            try
            {
                var result = loadsAdapter.Get(id);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpGet]
        [AuthorizationRequired]
        [Route("api/loads/next_load_number")]
        public IHttpActionResult GetNextLoadNumber()
        {
            try
            {
                var result = loadsAdapter.GetNextLoadNumber();
                return Ok(result);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpGet]
        [AuthorizationRequired]
        [Route("api/loads/assign")]
        public IHttpActionResult AssignLoadToTrip([FromUri] int loadId, [FromUri] int tripId)
        {
            try
            {
                var result = loadsAdapter.AssignLoadToTrip(loadId, tripId);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpGet]
        [AuthorizationRequired]
        [Route("api/loads/{id}/load_points")]
        public IHttpActionResult GetLoadPoints(int id)
        {
            try
            {
                var result = loadPointsAdapter.GetLoadPointsByLoadId(id);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }


        [HttpPost]
        [AuthorizationRequired]
        [Route("api/loads")]
        public IHttpActionResult Post([FromBody]LoadModel model)
        {
            try
            {
                var result = loadsAdapter.Save(model);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpGet]
        [AuthorizationRequired]
        [Route("api/loads/issues/{id}/toggle")]
        public IHttpActionResult ToggleLoadIssueResolved(int id)
        {
            try
            {
                var result = loadsAdapter.ToggleLoadIssueResolved(id);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpPost]
        [AuthorizationRequired]
        [Route("api/loads/issues")]
        public IHttpActionResult CreateLoadIssue(LoadIssueModel loadIssue)
        {
            try
            {
                var result = loadsAdapter.SaveLoadIssue(loadIssue);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpGet]
        [Route("api/loads/file/{itemId}")]
        public IHttpActionResult GetFile(int itemId)
        {
            try
            {
                var result = filesAdapter.Get(itemId);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpPost]
        [Route("api/loads/upload/{documentType}/{itemId}")]
        public Task<HttpResponseMessage> UploadFile([FromUri] int documentType, [FromUri] int itemId)
        {
            if (!Request.Content.IsMimeMultipartContent())
            {
                Task<HttpResponseMessage> mytask = new Task<HttpResponseMessage>(delegate ()
                {
                    return new HttpResponseMessage()
                    {
                        StatusCode = HttpStatusCode.BadRequest
                    };
                });
            }

            string root = HttpContext.Current.Server.MapPath("~/Documents");
            if (!Directory.Exists(root))
            {
                Directory.CreateDirectory(root);
            }
            string userFolder = HttpContext.Current.Server.MapPath("~/Documents/1");
            if (!Directory.Exists(userFolder))
            {
                Directory.CreateDirectory(userFolder);
            }
            var provider = new MultipartFormDataStreamProvider(userFolder);

            List<FileInfo> filesToSave = new List<FileInfo>();

            var task = Request.Content.ReadAsMultipartAsync(provider).ContinueWith<HttpResponseMessage>(o =>
                {
                    try
                    {
                        if (o.IsFaulted || o.IsCanceled)
                        {
                            Request.CreateErrorResponse(HttpStatusCode.InternalServerError, o.Exception);
                        }
                        //throw new HttpResponseException(HttpStatusCode.InternalServerError);



                        foreach (MultipartFileData file in provider.FileData)
                        {
                            FileInfo finfo = new FileInfo(file.LocalFileName);
                            string guid = Guid.NewGuid().ToString();

                            File.Move(finfo.FullName, Path.Combine(userFolder, guid + "_dpl_" + file.Headers.ContentDisposition.FileName.Replace("\"", "")));
                            string sFileName = file.Headers.ContentDisposition.FileName.Replace("\"", "");
                            FileInfo FInfos = new FileInfo(Path.Combine(userFolder, guid + "_dpl_" + sFileName));

                            filesToSave.Add(FInfos);
                        }

                        filesAdapter.SaveFilesForLoad(itemId, documentType, filesToSave);

                        return Request.CreateResponse(HttpStatusCode.OK);
                    }
                    catch (Exception ex)
                    {
                        return new HttpResponseMessage()
                        {
                            StatusCode = HttpStatusCode.InternalServerError,
                            Content = new StringContent(ex.ToString())
                        };
                    }
                }
            );

            return task;
        }

        [HttpGet]
        [AuthorizationRequired]
        [Route("api/loads/{id}/remove")]
        public IHttpActionResult Delete(int id)
        {
            try
            {
                var result = loadsAdapter.Delete(id);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpGet]
        [AuthorizationRequired]
        [Route("api/loads/files/{id}/remove")]
        public IHttpActionResult DeleteFile(int id)
        {
            try
            {
                var result = filesAdapter.Delete(id);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpGet]
        [Route("api/loads/files/preview/{id}")]
        public HttpResponseMessage PreviewFile(int id)
        {
            try
            {
                var response = new HttpResponseMessage(HttpStatusCode.OK);
                var result = filesAdapter.Get(id);
                var stream = new FileStream(result.FilePath, FileMode.Open);
                response.Content = new StreamContent(stream);
                response.Content.Headers.ContentType = new MediaTypeHeaderValue(Helper.ReturnExtension(result.Extension));
                response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("inline")
                {
                    FileName = result.OriginalFileName,
                    Name = result.OriginalFileName,
                    FileNameStar = result.OriginalFileName
                };
                return response;
            }
            catch (Exception ex)
            {
                HttpResponseMessage response = this.Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
                throw new HttpResponseException(response);
                //return new HttpResponseMessage(HttpStatusCode.InternalServerError);
            }



            //return File(result, "application/pdf", "report.pdf");
        }

        [HttpGet]
        //[AuthorizationRequired]
        [Route("api/loads/files/{id}/download")]
        public HttpResponseMessage DownloadFile(int id)
        {
            var response = new HttpResponseMessage(HttpStatusCode.OK);
            var result = filesAdapter.Get(id);
            var stream = new FileStream(result.FilePath, FileMode.Open);
            response.Content = new StreamContent(stream);
            response.Content.Headers.ContentType = new MediaTypeHeaderValue(Helper.ReturnExtension(result.Extension));
            response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
            {
                FileName = result.OriginalFileName
            };
            return response;
        }

        // PUT: api/Drivers/5
        public void Put(int id, [FromBody]string value)
        {
        }

        

    }
}
