﻿using DispatchPal.API.ActionFilters;
using DispatchPal.DAL.Adapters.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DispatchPal.API.Controllers
{
    public class StatesController : ApiController
    {
        private readonly IStatesAdapter statesAdapter;

        public StatesController(IStatesAdapter _statesAdapter)
        {
            statesAdapter = _statesAdapter;
        }

        [HttpGet]
        [AuthorizationRequired]
        [Route("api/states")]
        public IHttpActionResult Get([FromUri] string searchParam = "")
        {
            try
            {
                var result = statesAdapter.Get(searchParam);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpGet]
        [AuthorizationRequired]
        [Route("api/states/{id}")]
        public IHttpActionResult Get(int id)
        {
            try
            {
                var result = statesAdapter.Get(id);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        // POST: api/States
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/States/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/States/5
        public void Delete(int id)
        {
        }
    }
}
