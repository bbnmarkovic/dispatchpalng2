﻿using DispatchPal.API.ActionFilters;
using DispatchPal.DAL.Adapters.Interface;
using DispatchPal.DAL.Models;
using System;
using System.Web.Http;

namespace DispatchPal.API.Controllers
{
    public class BrokersController : ApiController
    {
        private readonly IBrokersAdapter brokersAdapter;

        public BrokersController(IBrokersAdapter _brokersAdapter)
        {
            brokersAdapter = _brokersAdapter;
        }

        [HttpGet]
        [AuthorizationRequired]
        [Route("api/brokers")]
        public IHttpActionResult Get([FromUri]int offset = 0, [FromUri]int pageSize = 10, [FromUri]string searchParam = "", bool active = false, int brokerageId = 0)
        {
            try
            {
                var result = brokersAdapter.Get(offset, pageSize, searchParam, active, brokerageId);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpGet]
       // [AuthorizationRequired]
        [Route("api/brokers/{id}")]
        public IHttpActionResult Get(int id)
        {
            try
            {
                var result = brokersAdapter.Get(id);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpGet]
        [AuthorizationRequired]
        [Route("api/brokers/{id}/rating")]
        public IHttpActionResult GetRating(int id)
        {
            try
            {
                var result = brokersAdapter.GetBrokerRating(id);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpPost]
        [AuthorizationRequired]
        [Route("api/brokers")]
        public IHttpActionResult Post([FromBody]BrokerModel model)
        {
            try
            {
                var result = brokersAdapter.Save(model);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        // PUT: api/Drivers/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Drivers/5
        public void Delete(int id)
        {
        }
    }
}
