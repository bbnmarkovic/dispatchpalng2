﻿using DispatchPal.API.ActionFilters;
using DispatchPal.DAL.Adapters.Interface;
using DispatchPal.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DispatchPal.API.Controllers
{
    public class ServicesController : ApiController
    {
        private readonly IServicesAdapter servicesAdapter;

        public ServicesController(IServicesAdapter _servicesAdapter)
        {
            servicesAdapter = _servicesAdapter;
        }

        [HttpGet]
        [AuthorizationRequired]
        [Route("api/services")]
        public IHttpActionResult Get([FromUri]int offset = 0, [FromUri]int pageSize = 10, [FromUri]string searchParam = "", [FromUri]int companyId = 0)
        {
            try
            {
                var result = servicesAdapter.Get(offset, pageSize, searchParam, companyId);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpGet]
        [AuthorizationRequired]
        [Route("api/services/{id}")]
        public IHttpActionResult Get(int id)
        {
            try
            {
                var result = servicesAdapter.Get(id);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpPost]
        [AuthorizationRequired]
        [Route("api/services")]
        public IHttpActionResult Post([FromBody]ServiceModel model)
        {
            try
            {
                var result = servicesAdapter.Save(model);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        // PUT: api/Drivers/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Drivers/5
        public void Delete(int id)
        {
        }
    }
}
