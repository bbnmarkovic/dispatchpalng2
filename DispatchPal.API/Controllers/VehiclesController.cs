﻿using DispatchPal.API.ActionFilters;
using DispatchPal.DAL.Adapters.Interface;
using DispatchPal.DAL.Models;
using System;
using System.Web.Http;

namespace DispatchPal.API.Controllers
{
    public class VehiclesController : ApiController
    {
        private readonly IVehiclesAdapter vehiclesAdapter;

        public VehiclesController(IVehiclesAdapter _vehiclesAdapter)
        {
            vehiclesAdapter = _vehiclesAdapter;
        }

        [HttpGet]
        [AuthorizationRequired]
        [Route("api/vehicles")]
        public IHttpActionResult Get([FromUri]int vehicleType, [FromUri]int offset = 0, [FromUri]int pageSize = 10, [FromUri]string searchParam = "", [FromUri]bool active = false, [FromUri]int companyId = 0)
        {
            try
            {
                var result = vehiclesAdapter.Get(vehicleType, offset, pageSize, searchParam, active, companyId);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpGet]
        [AuthorizationRequired]
        [Route("api/vehicles/preview")]
        public IHttpActionResult GetPreview([FromUri]int vehicleType, [FromUri]int offset = 0, [FromUri]int pageSize = 10, [FromUri]string searchParam = "", bool active = false, [FromUri]int companyId = 0)
        {
            try
            {
                var result = vehiclesAdapter.GetPreview(vehicleType, offset, pageSize, searchParam, active, companyId);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpGet]
        [AuthorizationRequired]
        [Route("api/vehicles/{id}")]
        public IHttpActionResult Get(int id)
        {
            try
            {
                var result = vehiclesAdapter.Get(id);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpPost]
        [AuthorizationRequired]
        [Route("api/vehicles")]
        public IHttpActionResult Post([FromBody]VehicleModel model)
        {
            try
            {
                var result = vehiclesAdapter.Save(model);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        // PUT: api/Drivers/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Drivers/5
        public void Delete(int id)
        {
        }
    }
}
