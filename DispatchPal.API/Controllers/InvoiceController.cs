﻿using DispatchPal.API.ActionFilters;
using DispatchPal.DAL.Adapters.Interface;
using DispatchPal.DAL.Models;
using DispatchPal.DAL.Models.Reports;
using DispatchPal.DAL.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Mail;
using System.Web.Http;

namespace DispatchPal.API.Controllers
{
    public class InvoiceController : ApiController
    {
        private readonly IInvoiceAdapter invoiceAdapter;
        private readonly PdfGenerator pdfGenerator;
        public InvoiceController(IInvoiceAdapter _invoiceAdapter, PdfGenerator _pdfGenerator)
        {
            invoiceAdapter = _invoiceAdapter;
            pdfGenerator = _pdfGenerator;
        }

        [HttpGet]
        [AuthorizationRequired]
        [Route("api/invoices/{loadId}")]
        public IHttpActionResult Get(int loadId)
        {
            try
            {
                var result = invoiceAdapter.Get(loadId);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpGet]
        [AuthorizationRequired]
        [Route("api/invoices")]
        public IHttpActionResult Get([FromUri]int offset = 0, [FromUri]int pageSize = 10, [FromUri]string searchParam = "", [FromUri]bool active = false, [FromUri]int companyId = 0)
        {
            try
            {
                var result = invoiceAdapter.GetList(offset, pageSize, searchParam, active, companyId);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpGet]
        [AuthorizationRequired]
        [Route("api/invoices/paid")]
        public IHttpActionResult GetPaidInvoices([FromUri]int offset = 0, [FromUri]int pageSize = 10, [FromUri]string searchParam = "", [FromUri]int companyId = 0)
        {
            try
            {
                var result = invoiceAdapter.GetPaidInvoices(offset, pageSize, searchParam, companyId);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpGet]
        [AuthorizationRequired]
        [Route("api/invoices/pending")]
        public IHttpActionResult GetPendingInvoices([FromUri]int offset = 0, [FromUri]int pageSize = 10, [FromUri]string searchParam = "", [FromUri] int aging = 0, [FromUri]int companyId = 0)
        {
            try
            {
                var result = invoiceAdapter.GetPendingInvoices(offset, pageSize, searchParam, aging, companyId);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpPost]
        [AuthorizationRequired]
        [Route("api/invoices")]
        public IHttpActionResult Post([FromBody]InvoiceModel model)
        {
            try
            {
                var result = invoiceAdapter.Save(model);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpPost]
        [AuthorizationRequired]
        [Route("api/invoices/invoice_item")]
        public IHttpActionResult PostInvoiceItem([FromBody]InvoiceItemModel model)
        {
            try
            {

                var result = invoiceAdapter.SaveInvoiceItem(model);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpPost]
        [AuthorizationRequired]
        [Route("api/invoices/invoice_payment")]
        public IHttpActionResult PostInvoicePayment([FromBody]InvoicePaymentModel model)
        {
            try
            {
                var result = invoiceAdapter.SaveInvoicePayment(model);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpGet]
        [AuthorizationRequired]
        [Route("api/invoices/invoice_item/remove/{id}")]
        public IHttpActionResult RemoveInvoiceItem([FromUri] int id)
        {
            try
            {
                var result = invoiceAdapter.DeleteInvoiceItem(id);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpGet]
        [AuthorizationRequired]
        [Route("api/invoices/invoice_payment/remove/{id}")]
        public IHttpActionResult RemoveInvoicePayment([FromUri] int id)
        {
            try
            {
                var result = invoiceAdapter.DeleteInvoicePayment(id);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpGet]
        [AuthorizationRequired]
        [Route("api/invoices/{loadId}/regenerate")]
        public IHttpActionResult RegenerateInvoiceItems(int loadId)
        {
            try
            {
                var result = invoiceAdapter.RegenerateInvoiceItems(loadId);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpPost]
        [Route("api/invoices/pdf/preview")]
        public HttpResponseMessage GetPdf([FromBody]InvoiceModel inv)
        {
            try
            {
                ResponseModel<InvoiceReportModel> model = invoiceAdapter.GenerateReportModel(inv.LoadId);
                if (model.IsSuccess)
                {
                    byte[] byteArray = pdfGenerator.RunTemplatingEngine(model.Result);

                    if (byteArray != null)
                    {
                        Stream stream = new MemoryStream(byteArray);

                        HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
                        result.Content = new StreamContent(stream);
                        result.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment");
                        result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");
                        result.Content.Headers.ContentDisposition.FileName = "Invoice.pdf";
                        result.Content.Headers.Add("x-filename", "Invoice.pdf");
                        return result;
                    }
                    else
                    {
                        return new HttpResponseMessage(HttpStatusCode.BadRequest);
                    }
                } else
                {
                    return new HttpResponseMessage(HttpStatusCode.NotFound);
                }
            }
            catch (Exception ex)
            {
                HttpResponseMessage response = this.Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
                throw new HttpResponseException(response);
                //return new HttpResponseMessage(HttpStatusCode.InternalServerError);
            }

            

            //return File(result, "application/pdf", "report.pdf");
        }

        [HttpPost]
        [Route("api/invoices/pdf/email")]
        public IHttpActionResult GetPdfAndSendEmail([FromBody]InvoiceModel inv)
        {
            try
            {
                ResponseModel<InvoiceReportModel> model = invoiceAdapter.GenerateReportModel(inv.LoadId);
                if (model.IsSuccess)
                {
                    byte[] byteArray = pdfGenerator.RunTemplatingEngine(model.Result);
                    if (byteArray != null)
                    {
                        Stream stream = new MemoryStream(byteArray);

                        EmailSender.Send(model.Result.CompanyEmail, stream);

                        return Ok();
                    }
                    {
                        return BadRequest();
                    }
                }
                else
                {
                    return NotFound();
                }
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        public void Put(int id, [FromBody]string value)
        {
        }

        public void Delete(int id)
        {
        }
    }
}
