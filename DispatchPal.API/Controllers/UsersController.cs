﻿using DispatchPal.API.ActionFilters;
using DispatchPal.DAL.Adapters.Interface;
using DispatchPal.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DispatchPal.API.Controllers
{
    public class UsersController : ApiController
    {
        // GET: api/Users
        private readonly IUsersAdapter usersAdapter;

        public UsersController(IUsersAdapter _usersAdapter)
        {
            usersAdapter = _usersAdapter;
        }

        [HttpGet]
        [AuthorizationRequired]
        [Route("api/users")]
        public IHttpActionResult Get([FromUri]int offset = 0, [FromUri]int pageSize = 10, [FromUri]string searchParam = "", bool active = false)
        {
            try
            {
                var result = usersAdapter.Get(offset, pageSize, searchParam, active);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpGet]
        [AuthorizationRequired]
        [Route("api/users/{id}")]
        public IHttpActionResult Get(int id)
        {
            try
            {
                var result = usersAdapter.Get(id);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpPost]
        [Route("api/users")]
        public IHttpActionResult Post([FromBody]UserModel model)
        {
            try
            {
                var result = usersAdapter.Save(model);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        // PUT: api/Drivers/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Drivers/5
        public void Delete(int id)
        {
        }
    }
}
