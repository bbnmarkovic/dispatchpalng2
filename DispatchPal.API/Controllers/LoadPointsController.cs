﻿using DispatchPal.API.ActionFilters;
using DispatchPal.DAL.Adapters.Interface;
using DispatchPal.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DispatchPal.API.Controllers
{
    public class LoadPointsController : ApiController
    {
        private readonly ILoadPointsAdapter loadPointsAdapter;

        public LoadPointsController(ILoadPointsAdapter _loadPointsAdapter)
        {
            loadPointsAdapter = _loadPointsAdapter;
        }

        [HttpGet]
        [AuthorizationRequired]
        [Route("api/load_points")]
        public IHttpActionResult GetLoadPoints([FromUri]int loadId)
        {
            try
            {
                var result = loadPointsAdapter.GetLoadPointsByLoadId(loadId);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpGet]
        [AuthorizationRequired]
        [Route("api/load_points/reorder")]
        public IHttpActionResult ReorderLoadPoints([FromUri]int loadPointId, [FromUri]int order)
        {
            try
            {
                var result = loadPointsAdapter.SetOrderNumber(loadPointId, order);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpGet]
        [AuthorizationRequired]
        [Route("api/load_points/planner")]
        public IHttpActionResult ReorderLoadPoints([FromUri]int driverId, [FromUri]DateTime fromDate, [FromUri]DateTime toDate)
        {
            try
            {
                var result = loadPointsAdapter.GetLoadPointsForPlanner(driverId, fromDate, toDate);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpGet]
        [AuthorizationRequired]
        [Route("api/load_points/{id}")]
        public IHttpActionResult Get(int id)
        {
            try
            {
                var result = loadPointsAdapter.Get(id);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }
        
        [HttpPost]
        [AuthorizationRequired]
        [Route("api/load_points")]
        public IHttpActionResult Post([FromBody]LoadPointModel model)
        {
            try
            {
                var result = loadPointsAdapter.Save(model);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpPost]
        [AuthorizationRequired]
        [Route("api/load_points/starting")]
        public IHttpActionResult PostStarting([FromBody]LoadPointModel model)
        {
            try
            {
                var result = loadPointsAdapter.SaveStartingLoadPoint(model);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpPost]
        [AuthorizationRequired]
        [Route("api/load_points/update_distances")]
        public IHttpActionResult UpdateDistances([FromBody]List<LoadPointModel> loadPoints)
        {
            try
            {
                loadPointsAdapter.UpdateDistances(loadPoints);
                return Ok(true);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpGet]
        [AuthorizationRequired]
        [Route("api/load_points/{id}/remove")]
        public IHttpActionResult Delete(int id)
        {
            try
            {
                var result = loadPointsAdapter.Delete(id);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        // PUT: api/Drivers/5
        public void Put(int id, [FromBody]string value)
        {
        }
    }
}
