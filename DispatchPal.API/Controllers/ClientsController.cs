﻿using DispatchPal.API.ActionFilters;
using DispatchPal.DAL.Adapters.Interface;
using DispatchPal.DAL.Models;
using System;
using System.Web.Http;

namespace DispatchPal.API.Controllers
{
    public class ClientsController : ApiController
    {
        private readonly IClientsAdapter clientsAdapter;

        public ClientsController(IClientsAdapter _clientsAdapter)
        {
            clientsAdapter = _clientsAdapter;
        }

        [HttpGet]
        [AuthorizationRequired]
        [Route("api/clients")]
        public IHttpActionResult Get([FromUri]int clientType = 0, [FromUri]int offset = 0, [FromUri]int pageSize = 10, [FromUri]string searchParam = "", bool active = false)
        {
            try
            {
                var result = clientsAdapter.Get(clientType, offset, pageSize, searchParam, active);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpGet]
        [AuthorizationRequired]
        [Route("api/clients/{id}")]
        public IHttpActionResult Get(int id)
        {
            try
            {
                var result = clientsAdapter.Get(id);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpGet]
        [AuthorizationRequired]
        [Route("api/clients/types")]
        public IHttpActionResult GetTypes([FromUri]string searchParam = "")
        {
            try
            {
                var result = clientsAdapter.GetClientTypes(searchParam);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpGet]
        [AuthorizationRequired]
        [Route("api/clients/types/{id}")]
        public IHttpActionResult GetType(int id)
        {
            try
            {
                var result = clientsAdapter.GetClientTypeById(id);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpPost]
        [AuthorizationRequired]
        [Route("api/clients")]
        public IHttpActionResult Post([FromBody]ClientModel model)
        {
            try
            {
                var result = clientsAdapter.Save(model);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        // PUT: api/Drivers/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Drivers/5
        public void Delete(int id)
        {
        }
    }
}
