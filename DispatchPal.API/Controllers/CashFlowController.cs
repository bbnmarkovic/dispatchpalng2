﻿using DispatchPal.API.ActionFilters;
using DispatchPal.DAL.Adapters.Interface;
using DispatchPal.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DispatchPal.API.Controllers
{
    public class CashFlowController : ApiController
    {
        private readonly ICashFlowAdapter cashFlowAdapter;

        public CashFlowController(ICashFlowAdapter _cashFlowAdapter)
        {
            cashFlowAdapter = _cashFlowAdapter;
        }

        [HttpGet]
        [AuthorizationRequired]
        [Route("api/cash-flow")]
        public IHttpActionResult Get([FromUri]int offset = 0, [FromUri]int pageSize = 10, [FromUri]string searchParam = "", bool active = false)
        {
            try
            {
                var result = cashFlowAdapter.GetPreview(offset, pageSize, searchParam, active);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpGet]
        [AuthorizationRequired]
        [Route("api/cash-flow/{id}")]
        public IHttpActionResult Get(int id)
        {
            try
            {
                var result = cashFlowAdapter.Get(id);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpGet]
        [AuthorizationRequired]
        [Route("api/cash-flow/types")]
        public IHttpActionResult GetTypes()
        {
            try
            {
                var result = cashFlowAdapter.GetTypes();
                return Ok(result);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpPost]
        [AuthorizationRequired]
        [Route("api/cash-flow")]
        public IHttpActionResult Post([FromBody]CashFlowModel model)
        {
            try
            {
                var result = cashFlowAdapter.Save(model);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        // PUT: api/Drivers/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Drivers/5
        public void Delete(int id)
        {
        }
    }
}
