﻿using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using System.Web.Script.Serialization;
using Omu.ValueInjecter;
using DispatchPal.API.Filters;
using DispatchPal.DAL.Adapters.Interface;
using DispatchPal.DAL.Models;
using DispatchPal.DAL.Utils;

namespace DispatchPal.API.Controllers
{
    [ApiAuthenticationFilter]
    public class AuthenticateController : ApiController
    {
        #region Private member variable declarations

        private readonly IUsersAdapter usersAdapter;
        private readonly ILoginTokenAdapter loginTokenAdapter;

        #endregion

        #region Constructor method definitions

        public AuthenticateController(IUsersAdapter usersAdapter, ILoginTokenAdapter userTokenAdapter)
        {
            this.loginTokenAdapter = userTokenAdapter;
            this.usersAdapter = usersAdapter;
        }
        #endregion

        [HttpPost]
        [Route("api/login")]
        public HttpResponseMessage Authenticate()
        {
            if (System.Threading.Thread.CurrentPrincipal != null && System.Threading.Thread.CurrentPrincipal.Identity.IsAuthenticated)
            {
                var basicAuthenticationIdentity = System.Threading.Thread.CurrentPrincipal.Identity as BasicAuthenticationIdentity;
                if (basicAuthenticationIdentity != null)
                {
                    var userToken = basicAuthenticationIdentity.Token;
                    var response = GetAuthToken(userToken);
                    if (response != null)
                    {
                        var content =
                            (new JavaScriptSerializer() { MaxJsonLength = 50000000 }).Serialize(
                                (new BasicAuthenticationIdentityDto()).InjectFrom(basicAuthenticationIdentity));
                        response.Content = new StringContent(content, Encoding.UTF8, "application/json");
                        return response;
                    }
                }
            }
            return new HttpResponseMessage(HttpStatusCode.Unauthorized);
            //return null;
        }

        /// <summary>
        /// Returns auth token for the validated user.
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        private HttpResponseMessage GetAuthToken(string loginToken)
        {
            try
            {
                var loginTokenObj = loginTokenAdapter.Get(new Guid(loginToken));

                var token = new TokenModel
                {
                    AuthToken = loginTokenObj.token.ToString(),
                    IssuedOn = loginTokenObj.created,
                    ExpiresOn = loginTokenObj.created.AddMinutes(5),
                    UserId = loginToken
                };

                var response = Request.CreateResponse(HttpStatusCode.OK, "Authorized");
                response.Headers.Add("Token", token.AuthToken);
                response.Headers.Add("TokenExpiry", LocalSettings.TokenDurationIntervalInMinutes.ToString());
                response.Headers.Add("Access-Control-Expose-Headers", "Token,TokenExpiry");
                
                return response;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
