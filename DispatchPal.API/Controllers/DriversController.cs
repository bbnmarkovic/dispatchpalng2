﻿using DispatchPal.API.ActionFilters;
using DispatchPal.API.Utils;
using DispatchPal.DAL.Adapters.Interface;
using DispatchPal.DAL.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace DispatchPal.API.Controllers
{
    public class DriversController : ApiController
    {
        private readonly IDriversAdapter driversAdapter;
        private readonly IFilesAdapter filesAdapter;

        public DriversController(IDriversAdapter _driversAdapter, IFilesAdapter _filesAdapter)
        {
            driversAdapter = _driversAdapter;
            filesAdapter = _filesAdapter;
        }

        [HttpGet]
        [AuthorizationRequired]
        [Route("api/drivers")]
        public IHttpActionResult Get([FromUri]int offset = 0, [FromUri]int pageSize = 10, [FromUri]string searchParam = "", [FromUri]bool active = false, [FromUri]int companyId = 0)
        {
            try
            {
                var result = driversAdapter.Get(offset, pageSize, searchParam, active, companyId);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpGet]
        [AuthorizationRequired]
        [Route("api/drivers/preview")]
        public IHttpActionResult GetPreview([FromUri]int offset = 0, [FromUri]int pageSize = 10, [FromUri]string searchParam = "", [FromUri]int companyId = 0)
        {
            try
            {
                var result = driversAdapter.GetPreview(offset, pageSize, searchParam, false, companyId);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpGet]
        [AuthorizationRequired]
        [Route("api/drivers/auto-complete")]
        public IHttpActionResult GetDriversForAutoComplete([FromUri]string searchParam = "", [FromUri]int companyId = 0)
        {
            try
            {
                var result = driversAdapter.GetDriversForAutoComplete(searchParam, companyId);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpGet]
        [AuthorizationRequired]
        [Route("api/drivers/{id}")]
        public IHttpActionResult Get(int id)
        {
            try
            {
                var result = driversAdapter.Get(id);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpPost]
        [AuthorizationRequired]
        [Route("api/drivers")]
        public IHttpActionResult Post([FromBody]DriverModel model)
        {
            try
            {
                var result = driversAdapter.Save(model);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpPost]
        [Route("api/drivers/upload/{documentType}/{itemId}")]
        public Task<HttpResponseMessage> UploadFile([FromUri] int documentType, [FromUri] int itemId)
        {
            if (!Request.Content.IsMimeMultipartContent())
            {
                Task<HttpResponseMessage> mytask = new Task<HttpResponseMessage>(delegate ()
                {
                    return new HttpResponseMessage()
                    {
                        StatusCode = HttpStatusCode.BadRequest
                    };
                });
            }

            string root = HttpContext.Current.Server.MapPath("~/Documents");
            if (!Directory.Exists(root))
            {
                Directory.CreateDirectory(root);
            }
            string userFolder = HttpContext.Current.Server.MapPath("~/Documents/1");
            if (!Directory.Exists(userFolder))
            {
                Directory.CreateDirectory(userFolder);
            }
            var provider = new MultipartFormDataStreamProvider(userFolder);

            List<FileInfo> filesToSave = new List<FileInfo>();

            var task = Request.Content.ReadAsMultipartAsync(provider).ContinueWith<HttpResponseMessage>(o =>
            {
                try
                {
                    if (o.IsFaulted || o.IsCanceled)
                    {
                        Request.CreateErrorResponse(HttpStatusCode.InternalServerError, o.Exception);
                    }

                    foreach (MultipartFileData file in provider.FileData)
                    {
                        FileInfo finfo = new FileInfo(file.LocalFileName);
                        string guid = Guid.NewGuid().ToString();

                        File.Move(finfo.FullName, Path.Combine(userFolder, guid + "_dpl_" + file.Headers.ContentDisposition.FileName.Replace("\"", "")));
                        string sFileName = file.Headers.ContentDisposition.FileName.Replace("\"", "");
                        FileInfo FInfos = new FileInfo(Path.Combine(userFolder, guid + "_dpl_" + sFileName));

                        filesToSave.Add(FInfos);
                    }

                    filesAdapter.SaveFilesForDriver(itemId, documentType, filesToSave);

                    return Request.CreateResponse(HttpStatusCode.OK);
                }
                catch (Exception ex)
                {
                    return new HttpResponseMessage()
                    {
                        StatusCode = HttpStatusCode.InternalServerError,
                        Content = new StringContent(ex.ToString())
                    };
                }
            }
            );

            return task;
        }

        [HttpGet]
        [AuthorizationRequired]
        [Route("api/drivers/files/{id}/remove")]
        public IHttpActionResult DeleteFile(int id)
        {
            try
            {
                var result = filesAdapter.Delete(id);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpGet]
        //[AuthorizationRequired]
        [Route("api/drivers/files/{id}/download")]
        public HttpResponseMessage DownloadFile(int id)
        {
            var response = new HttpResponseMessage(HttpStatusCode.OK);
            var result = filesAdapter.Get(id);
            var stream = new FileStream(result.FilePath, FileMode.Open);
            response.Content = new StreamContent(stream);
            response.Content.Headers.ContentType = new MediaTypeHeaderValue(Helper.ReturnExtension(result.Extension));
            response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
            {
                FileName = result.OriginalFileName
            };
            return response;
        }

        // PUT: api/Drivers/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Drivers/5
        public void Delete(int id)
        {
        }
    }
}
