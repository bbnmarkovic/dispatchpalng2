﻿using DispatchPal.API.ActionFilters;
using DispatchPal.DAL.Adapters.Interface;
using DispatchPal.DAL.Models;
using System;
using System.Web.Http;

namespace DispatchPal.API.Controllers
{
    public class CompaniesController : ApiController
    {
        private readonly ICompaniesAdapter companiesAdapter;

        public CompaniesController(ICompaniesAdapter _companiesAdapter)
        {
            companiesAdapter = _companiesAdapter;
        }

        [HttpGet]
        [AuthorizationRequired]
        [Route("api/companies")]
        public IHttpActionResult Get([FromUri]int offset = 0, [FromUri]int pageSize = 10, [FromUri]string searchParam = "", bool active = false)
        {
            try
            {
                var result = companiesAdapter.Get(offset, pageSize, searchParam, active);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpGet]
        [AuthorizationRequired]
        [Route("api/companies/{id}")]
        public IHttpActionResult Get(int id)
        {
            try
            {
                var result = companiesAdapter.Get(id);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpPost]
        [AuthorizationRequired]
        [Route("api/companies")]
        public IHttpActionResult Post([FromBody]CompanyModel model)
        {
            try
            {
                var result = companiesAdapter.Save(model);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        // PUT: api/Drivers/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Drivers/5
        public void Delete(int id)
        {
        }
    }
}
