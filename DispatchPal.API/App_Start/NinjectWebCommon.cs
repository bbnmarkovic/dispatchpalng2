﻿[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(DispatchPal.API.App_Start.NinjectWebCommon), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethodAttribute(typeof(DispatchPal.API.App_Start.NinjectWebCommon), "Stop")]

namespace DispatchPal.API.App_Start
{
    using System;
    using System.Web;

    using Microsoft.Web.Infrastructure.DynamicModuleHelper;

    using Ninject;
    using Ninject.Web.Common;
    using DAL;
    using DAL.Adapters.Interface;
    using DAL.Adapters;

    public static class NinjectWebCommon 
    {
        private static readonly Bootstrapper bootstrapper = new Bootstrapper();

        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start() 
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            bootstrapper.Initialize(CreateKernel);
        }
        
        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            bootstrapper.ShutDown();
        }
        
        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            try
            {
                kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
                kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();

                RegisterServices(kernel);
                return kernel;
            }
            catch
            {
                kernel.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private static void RegisterServices(IKernel kernel)
        {
            kernel.Bind<dbEntities1>().ToSelf().InRequestScope();

            kernel.Bind<IDriversAdapter>().To<DriversAdapter>().InRequestScope();
            kernel.Bind<IVehiclesAdapter>().To<VehiclesAdapter>().InRequestScope();
            kernel.Bind<IClientsAdapter>().To<ClientsAdapter>().InRequestScope();
            kernel.Bind<ICompaniesAdapter>().To<CompaniesAdapter>().InRequestScope();
            kernel.Bind<IServicesAdapter>().To<ServicesAdapter>().InRequestScope();
            kernel.Bind<IStatesAdapter>().To<StatesAdapter>().InRequestScope();
            kernel.Bind<IUsersAdapter>().To<UsersAdapter>().InRequestScope();
            kernel.Bind<ILoadsAdapter>().To<LoadsAdapter>().InRequestScope();
            kernel.Bind<ILoadPointsAdapter>().To<LoadPointsAdapter>().InRequestScope();
            kernel.Bind<ITripsAdapter>().To<TripsAdapter>().InRequestScope();
            kernel.Bind<IBrokersAdapter>().To<BrokersAdapter>().InRequestScope();
            kernel.Bind<ILoginTokenAdapter>().To<LoginTokenAdapter>().InRequestScope();
            kernel.Bind<IFilesAdapter>().To<FilesAdapter>().InRequestScope();
            kernel.Bind<ICashFlowAdapter>().To<CashFlowAdapter>().InRequestScope();
            kernel.Bind<IInvoiceAdapter>().To<InvoiceAdapter>().InRequestScope();
        }        
    }
}
