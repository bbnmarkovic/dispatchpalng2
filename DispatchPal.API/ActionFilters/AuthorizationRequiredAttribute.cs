﻿using DispatchPal.API.Filters;
using DispatchPal.DAL.Adapters.Interface;
using DispatchPal.DAL.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Principal;
using System.Threading;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace DispatchPal.API.ActionFilters
{
    public class AuthorizationRequiredAttribute : ActionFilterAttribute
    {
        private const string Token = "Token";

        public CustomRoles[] Roles { get; set; }

        public override void OnActionExecuting(HttpActionContext filterContext)
        {
            //var loginTokenAdapter = new LoginTokenAdapter()
            var loginTokenAdapter = filterContext.ControllerContext.Configuration
                .DependencyResolver.GetService(typeof(ILoginTokenAdapter)) as ILoginTokenAdapter;

            var usersAdapter = filterContext.ControllerContext.Configuration
                .DependencyResolver.GetService(typeof(IUsersAdapter)) as IUsersAdapter;

            if (filterContext.Request.Headers.Contains(Token))
            {
                var tokenValue = filterContext.Request.Headers.GetValues(Token).First();

                //System.Diagnostics.Debug.WriteLine(filterContext.ActionArguments.ToString());

                Guid tokenValueGuid;
                try
                {
                    tokenValueGuid = new Guid(tokenValue);
                }
                catch
                {
                    tokenValueGuid = Guid.Empty;
                    var responseMessage = new HttpResponseMessage(HttpStatusCode.Unauthorized) { ReasonPhrase = "Invalid Request" };
                    filterContext.Response = responseMessage;
                }
                if (tokenValueGuid != Guid.Empty)
                {
                    var ut = loginTokenAdapter.Get(tokenValueGuid);
                    if (ut != null)
                    {
                        if ((DateTime.Now - ut.created).TotalMinutes > LocalSettings.TokenDurationIntervalInMinutes)
                            filterContext.Response = new HttpResponseMessage(HttpStatusCode.Unauthorized);
                        else
                        {
                            //load user by token::
                            var user = usersAdapter.Get(ut.user_id);
                            if (user != null && user.Result.IsActive)
                            {
                                var identity = new BasicAuthenticationIdentity(user.Result.UserName, "Basic")
                                {
                                    Token = ut.token.ToString(),
                                    Id = ut.user_id,
                                    FirstName = user.Result.FirstName,
                                    LastName = user.Result.LastName
                                };

                                //proveri da li je rola odgovarajuća:
                                var hasRole =
                                    this.Roles == null ||
                                    (this.Roles != null
                                    && (this.Roles.Length == 0 ||
                                              this.Roles.Any(
                                                  x => identity.Roles.Any(y => y.Name.Equals(x.ToString(), StringComparison.OrdinalIgnoreCase)))));
                                if (hasRole)
                                {
                                    //ToDo: dodaj IZIS propertije i role
                                    var genericPrincipal = new GenericPrincipal(identity, null);
                                    Thread.CurrentPrincipal = genericPrincipal;
                                }
                                else
                                {
                                    //GenericAuthenticationFilter.ChallengeAuthRequest(filterContext);
                                    filterContext.Response = new HttpResponseMessage(HttpStatusCode.Forbidden) { ReasonPhrase = "Role is not permitted" };
                                }
                            }
                            else
                            {
                                filterContext.Response = new HttpResponseMessage(HttpStatusCode.Unauthorized) { ReasonPhrase = "User does not exist" };
                            }
                        }
                    }
                    else
                    {
                        filterContext.Response = new HttpResponseMessage(HttpStatusCode.Unauthorized);
                    }
                }
                else
                // Validate Token
                //if (provider != null && !provider.ValidateToken(tokenValue))
                //var ut = userTokenAdapter.Get(new Guid(token))
                {
                    var responseMessage = new HttpResponseMessage(HttpStatusCode.Unauthorized) { ReasonPhrase = "Invalid Request" };
                    filterContext.Response = responseMessage;
                }
            }
            else
            {
                filterContext.Response = new HttpResponseMessage(HttpStatusCode.Unauthorized) { ReasonPhrase = "Please login" };
            }

            base.OnActionExecuting(filterContext);

        }
    }

    public enum CustomRoles
    {
        Administrator,
        User,
        Guest
    }
}