﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace DispatchPal.API.Filters
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false)]
    public class GenericAuthenticationFilter : AuthorizationFilterAttribute
    {

        /// <summary>
        /// Public default Constructor
        /// </summary>
        public GenericAuthenticationFilter()
        {
        }

        private readonly bool _isActive = true;

        /// <summary>
        /// parameter isActive explicitly enables/disables this filetr.
        /// </summary>
        /// <param name="isActive"></param>
        public GenericAuthenticationFilter(bool isActive)
        {
            _isActive = isActive;
        }

        /// <summary>
        /// Checks basic authentication request
        /// </summary>
        /// <param name="filterContext"></param>
        public override void OnAuthorization(HttpActionContext filterContext)
        {
            if (!_isActive) return;
            var identity = FetchAuthHeader(filterContext);
            if (identity == null)
            {
                ChallengeAuthRequest(filterContext);
                return;
            }
            var genericPrincipal = new GenericPrincipal(identity, null);
            Thread.CurrentPrincipal = genericPrincipal;
            //if (!OnAuthorizeUser(identity.Name, identity.Password, filterContext))
            if (!OnAuthorizeUser(identity.UserName, identity.Password, filterContext))
            {
                //ChallengeAuthRequest(filterContext);
                return;
            }
            base.OnAuthorization(filterContext);
        }

        /// <summary>
        /// Virtual method.Can be overriden with the custom Authorization.
        /// </summary>
        /// <param name="user"></param>
        /// <param name="pass"></param>
        /// <param name="filterContext"></param>
        /// <returns></returns>
        protected virtual bool OnAuthorizeUser(string username, string password, HttpActionContext actionContext)
        {
            if (string.IsNullOrWhiteSpace(username) || string.IsNullOrWhiteSpace(password))
                return false;
            return true;
        }

        /// <summary>
        /// Checks for autrhorization header in the request and parses it, creates user credentials and returns as BasicAuthenticationIdentity
        /// </summary>
        /// <param name="filterContext"></param>
        protected virtual BasicAuthenticationIdentity FetchAuthHeader(HttpActionContext filterContext)
        {
            string authHeaderValue = null;
            var authRequest = filterContext.Request.Headers.Authorization;
            if (authRequest != null && !String.IsNullOrEmpty(authRequest.Scheme) && (authRequest.Scheme == "Basic" /*|| authRequest.Scheme == "Bearer"*/))
                authHeaderValue = authRequest.Parameter;
            if (string.IsNullOrEmpty(authHeaderValue))
                return null;
            authHeaderValue = Encoding.Default.GetString(Convert.FromBase64String(authHeaderValue));
            var credentials = authHeaderValue.Split(':');
            return credentials.Length < 2
                ? null
                : new BasicAuthenticationIdentity(credentials[0], "Basic") { Password = credentials[1] };

            //return authRequest.Scheme == "Basic" 
            //	? (credentials.Length < 2 ? null : new BasicAuthenticationIdentity(credentials[0], "Basic"){ Password = credentials[1]})
            //	: (credentials.Length < 1 ? null : new BasicAuthenticationIdentity("", "Bearer") { Token = credentials[0] });
            //return credentials.Length < 1 ? null : new BasicAuthenticationIdentity(credentials[0]);
        }


        /// <summary>
        /// Send the Authentication Challenge request
        /// </summary>
        /// <param name="filterContext"></param>
        public static void ChallengeAuthRequest(HttpActionContext filterContext)
        {
            var dnsHost = filterContext.Request.RequestUri.DnsSafeHost;
            filterContext.Response = filterContext.Request.CreateResponse(HttpStatusCode.Unauthorized);

            var basic = "Basic";
            var xRequestedWithKey = "X-Requested-With";

            var xRequestedWith = filterContext.Request.Headers.Contains(xRequestedWithKey)
                ? filterContext.Request.Headers.GetValues(xRequestedWithKey).FirstOrDefault()
                : "";
            if (!string.IsNullOrWhiteSpace(xRequestedWith) &&
                xRequestedWith.Equals("XMLHttpRequest", StringComparison.OrdinalIgnoreCase))
                basic = "xBasic";

            filterContext.Response.Headers.Add("WWW-Authenticate", string.Format("{1} realm=\"{0}\"", dnsHost, basic));
        }
    }
}