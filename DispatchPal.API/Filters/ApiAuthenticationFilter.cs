﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Http.Controllers;
using NLog;
using DispatchPal.DAL.Adapters.Interface;

namespace DispatchPal.API.Filters
{
    public class ApiAuthenticationFilter : GenericAuthenticationFilter
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        
        /// <summary>
        /// Protected overriden method for authorizing user
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <param name="actionContext"></param>
        /// <returns></returns>
        protected override bool OnAuthorizeUser(string username, string password, HttpActionContext actionContext)
        {
            try
            {
                var loginTokenAdapter = actionContext.ControllerContext.Configuration
                    .DependencyResolver.GetService(typeof(ILoginTokenAdapter)) as ILoginTokenAdapter;
                
                loginTokenAdapter.RemoveOldTokens();

                var userToken = loginTokenAdapter.CreateNew(username, password);
                if (userToken != null)
                {
                    var basicAuthenticationIdentity = Thread.CurrentPrincipal.Identity as BasicAuthenticationIdentity;
                    if (basicAuthenticationIdentity != null)
                    {
                        basicAuthenticationIdentity.Id = userToken._user.Id;
                        basicAuthenticationIdentity.FirstName = userToken._user.FirstName;
                        basicAuthenticationIdentity.LastName = userToken._user.LastName;
                        basicAuthenticationIdentity.UserName = userToken._user.UserName;
                        basicAuthenticationIdentity.Roles.Clear();
                        basicAuthenticationIdentity.Token = userToken.token.token.ToString();
                    }

                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                logger.Error(ex, "ERROR IN OnAuthorizeUser");
                return false;
            }
        }
    }
}