﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;

namespace DispatchPal.API.Filters
{
    public class BasicAuthenticationIdentity : GenericIdentity
    {
        /// <summary>
        /// Get/Set for token
        /// </summary>
        public string Token { get; set; }
        /// <summary>
        /// Get/Set for password
        /// </summary>
        public string Password { get; set; }
        /// <summary>
        /// Get/Set for UserName
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        /// Get/Set for FirstName
        /// </summary>
        public string FirstName { get; set; }
        /// <summary>
        /// Get/Set for LastName
        /// </summary>
        public string LastName { get; set; }
        /// <summary>
        /// Get/Set for UserId
        /// </summary>
        public int Id { get; set; }

        public List<AppRole> Roles { get; set; }
        /// <summary>
        /// Basic Authentication Identity Constructor
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        public BasicAuthenticationIdentity(string userName, string type)
            : base(userName, type)
        {
            Roles = new List<AppRole>();

            //Password = password;
            UserName = userName;
        }
    }

    public class AppRole
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class BasicAuthenticationIdentityDto
    {
        public string Token { get; set; }
        public string Password { get; set; }
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Id { get; set; }
        public List<AppRole> Roles { get; set; }
    }

}